#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#
#  CMake config.
#
#############################################################################
cmake_minimum_required(VERSION 2.8)
if( NOT CMAKE_VERSION VERSION_LESS 3 )
	cmake_policy(SET CMP0042 OLD)
endif()

#============================================================================
# Macros that need to be set prior to project definition.
# Notice (the CMAKE_PREFIX_PATH - if you use it - cannot be a CACHE var).
#============================================================================
if (CMAKE_HOST_APPLE )
    set( CMAKE_PREFIX_PATH 
        "/opt/local/bin;/opt/local/libexec/qt5-mac;/opt/local/lib;/opt/local/include;${CMAKE_PREFIX_PATH}" )
endif()
#
set( CMAKE_BUILD_TYPE "Release"
	CACHE STRING "Build type: None Debug Release RelWithDebInfo MinSizeRel." )
#
set( CMAKE_INSTALL_PREFIX  "${CMAKE_BINARY_DIR}/installed" 
	CACHE PATH "Prefix prepended to install directories" )
#
set( CPACK_OUTPUT_FILE_PREFIX  "${CMAKE_BINARY_DIR}/installed" 
	CACHE PATH "Prefix prepended to package file" )
#
set( CMAKE_PROGRAM_PATH  
    "/opt/local/bin;/usr/texbin;/usr/local/bin;/usr/bin;C:/Program\ Files/TortoiseHg;${CMAKE_PROGRAM_PATH}" 
	CACHE PATH "Where to look with find_program." )

if (APPLE)
    # Fixes rpath misery for dynamic libs
    SET(CMAKE_MACOSX_RPATH 1)
    # use, i.e. don't skip the full RPATH for the build tree
    SET(CMAKE_SKIP_BUILD_RPATH  FALSE)

    # when building, don't use the install RPATH already
    # (but later on when installing)
    SET(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)

    SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
    SET(CMAKE_INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib")
    # add the automatically determined parts of the RPATH
    # which point to directories outside the build tree to the install RPATH
    SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

    # the RPATH to be used when installing, but only if it's not a system directory
    LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
    IF("${isSystemDir}" STREQUAL "-1")
        SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
    ENDIF("${isSystemDir}" STREQUAL "-1")

endif (APPLE)
#
#============================================================================
# Project name, configuration for CMake, macros for build & install.
#============================================================================
project( SIMPT )
include( CMakeCMakeConfig.txt )
include( CMakeBuildConfig.txt )

#============================================================================
# Configuration for CPP.
#============================================================================
include( CMakeCPPConfig.txt    )

#============================================================================
# Configuration for JAVA.
#============================================================================
if ( SIMPT_MAKE_JAVA_WRAPPER )
	find_package( Java REQUIRED  )
	find_package( JNI REQUIRED   )
	include_directories( ${JAVA_INCLUDE_PATH} )
	if( WIN32 )
		set( JAVA_PATH_SEPARATOR ";" )
	else()
		set( JAVA_PATH_SEPARATOR ":" )
	endif()
endif()

#============================================================================
# Configuration for PYTHON.
#============================================================================
if ( SIMPT_MAKE_PYTHON_WRAPPER )
	find_package( PythonInterp    REQUIRED )
	find_package( PythonLibsNew   REQUIRED )
	include_directories( ${PYTHON_INCLUDE_DIRS} )
	if( WIN32 )
		set( PYTHON_PATH_SEPARATOR ";" )
	else()
		set( PYTHON_PATH_SEPARATOR ":" )
	endif()
endif()

#============================================================================
# Configuration for SWIG.
#============================================================================
if( SIMPT_MAKE_JAVA_WRAPPER OR SIMPT_MAKE_PYTHON_WRAPPER )
  	find_package( SWIG REQUIRED )
	include( ${SWIG_USE_FILE} )
endif()
  	
#============================================================================
# Configuration for Git.
#============================================================================
find_package( Git QUIET)
if( GIT_FOUND )
	GIT_WC_INFO(${CMAKE_SOURCE_DIR}/.. SIMPT)
else()
    # This is done to eliminate blank output of undefined CMake variables.
    set( GIT_FOUND FALSE ) 
	set( SIMPT_WC_REVISION_HASH    "0000-0000-000" )	
	set( SIMPT_WC_LAST_CHANGED_DATE   "0000 0000 0000" )
endif()

#============================================================================
# Configuration for DOC: DOXYGEN AND LATEX
#============================================================================
if( SIMPT_MAKE_DOC ) 	
	find_package( Doxygen )	
	find_package( LATEX )
	if( NOT "${PDFLATEX_COMPILER}" STREQUAL "PDFLATEX_COMPILER-NOTFOUND" )
		set( LATEX_FOUND YES )
	endif()
endif()

#============================================================================
# Add subdirs
#============================================================================
add_subdirectory( main )
#
if( NOT SIMPT_MAKE_PACKAGE )
    add_subdirectory( test )
endif()
#
if( SIMPT_MAKE_DOC )
	add_subdirectory( doc )
endif()	
#
if( SIMPT_MAKE_PACKAGE )
	# This ensures that the "finalize" code of the packaging runs at the end
	# of the installation process.
	add_subdirectory( main/resources/cmake/packaging )
endif()
#============================================================================
# Overview report:
#============================================================================
include( CMakeConfigReport.txt )

#############################################################################
