/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ExplorationWizard
 */

#include "ExplorationWizard.h"

#include <memory>
#include <iostream>
#include <sstream>
#include <fstream>
#include <functional>


#include <algorithm>
#include <cctype>
#include <locale>

#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QComboBox>
#include <QDoubleValidator>
#include <QFileDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QListWidget>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QRegExp>
#include <QRegExpValidator>
#include <QSettings>
#include <QString>
#include <QVBoxLayout>

#include "parex_protocol/FileExploration.h"
#include "parex_protocol/ListSweep.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/RangeSweep.h"

#include "FilesPage.h"
#include "ParamPage.h"
#include "PathPage.h"
#include "SendPage.h"
#include "StartPage.h"
#include "TemplateFilePage.h"
#include <QDateTime>
#include <Qt>

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

namespace SimPT_Parex {

/*
 * ExplorationSetUp - Wizard
 */
ExplorationWizard::ExplorationWizard(const ptree &preferences, const std::shared_ptr<const Exploration> &lastExploration, QWidget *parent)
	: QWizard(parent), m_exploration(), m_preferences(preferences)
{
	setPage(Page_Start, new StartPage(m_exploration, lastExploration));
	setPage(Page_Path, new PathPage(m_exploration, m_preferences));
	setPage(Page_Template_Path, new TemplateFilePage(m_exploration, m_preferences));

	setPage(Page_Param, new ParamPage(m_exploration));
	setPage(Page_Files, new FilesPage(m_exploration, m_preferences));
	setPage(Page_Send, new SendPage(m_exploration));
}

std::shared_ptr<const Exploration> ExplorationWizard::GetExploration()
{
	return std::shared_ptr<Exploration>(m_exploration->Clone());
}

std::shared_ptr<const Exploration> ExplorationWizard::GetExplorationWithTimestamp()
{
	std::shared_ptr<Exploration> expl_clone(m_exploration->Clone());
	expl_clone->SetName(m_exploration->GetName() + QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate).toStdString());
	return expl_clone;
}


} // namespace
