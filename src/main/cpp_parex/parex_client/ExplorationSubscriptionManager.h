#ifndef EXPLORATION_SUBSCRIPTION_MANAGER_H
#define EXPLORATION_SUBSCRIPTION_MANAGER_H 

#include <vector>
#include <map>
#include <string>
#include <memory>
#include <algorithm>
#include <stdio.h>
#include <QString>
#include <QObject>

#include "parex_protocol/ServerInfo.h"
#include "parex_protocol/Exploration.h"
#include "parex_protocol/ExplorationTask.h"
#include "parex_protocol/ExplorationProgress.h"
#include "parex_protocol/ClientProtocol.h"


namespace SimPT_Parex {

using ProgressMap = std::map<std::string, std::vector<std::shared_ptr<ExplorationProgress>>>;

class ExplorationSubscriptionManager : public QObject
{
	Q_OBJECT
public:
	ExplorationSubscriptionManager(){}

	ExplorationSubscriptionManager(std::vector<ServerInfo> servers, 
			std::vector<std::string> exploration_names)
		: m_servers(servers), m_exploration_names(exploration_names) {}
	
	~ExplorationSubscriptionManager(){ disconnectAll(); }

	void addServer(std::shared_ptr<ServerInfo> server);
	void removeServer(std::shared_ptr<ServerInfo> server);
	void addExploration(std::string name);
	void removeExploration(std::string name);

	bool hasTask(std::string exploration_name, int task_id);
	ExplorationTask getTask(std::string exploration_name, int task_id);
	std::map<int, ExplorationTask> getTasks(std::string exploration_name);

	std::shared_ptr<ExplorationProgress> getExplorationProgressForTask(std::string exploration_name, int task_id);

	// Do a hard refresh, this resets all subscriptions to trigger a forced
	// update on the server side. Can be useful to make sure the UI is showing
	// the most up to date content
	void refresh();

signals:
	void UpdateReceived(std::shared_ptr<ProgressMap> updated_progress, std::shared_ptr<std::vector<std::string>> exploration_names);

private:
	void connectAll();
	void disconnectAll();
	std::vector<std::shared_ptr<ExplorationProgress>> getProgressList(std::string exploration_name);
	std::map<int, std::shared_ptr<ExplorationProgress>> getWeightedProgressList(std::string exploration_name);

	void initSubscription(ServerInfo server, std::string exploration_name);
	void dropSubscription(ServerInfo server, std::string exploration_name);

	std::vector<ServerInfo> m_servers;		
	std::vector<std::string> m_exploration_names;
	// First string is the exploration name
	std::map<std::string, std::vector<std::shared_ptr<ExplorationProgress>>> m_progress;
};

}

#endif