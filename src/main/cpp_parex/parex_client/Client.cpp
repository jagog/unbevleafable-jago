/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Client
 */

#include "Client.h"

#include "gui/WorkspaceWizard.h"
#include "parex_protocol/ClientProtocol.h"
#include "parex_protocol/ExplorationProgress.h"
#include "parex_protocol/Protocol.h"
#include "parex_protocol/ServerInfo.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/FileExploration.h"

#include "ExplorationSelection.h"
#include "ExplorationWizard.h"
#include "ServerDialog.h"
#include "Status.h"
#include "TaskOverview.h"

#include "workspace/CliWorkspace.h"
#include <util/misc/StringUtils.h>
#include "util/misc/XmlWriterSettings.h"

#include <QCloseEvent>
#include <QCoreApplication>
#include <QDir>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QInputDialog>
#include <QMenuBar>
#include <QMessageBox>
#include <QStatusBar>
#include <QToolBar>

#include <string>
#include <memory>

#include <cpp_logging/logger.h>

namespace SimPT_Shell { namespace Ws { class WorkspaceFactory; }}

namespace SimPT_Parex {

const QString Client::SETTING_ORGANIZATION = "SimPT Consortium";
const QString Client::SETTING_APPLICATION = "Parex Client";

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Shell;
using SimPT_Sim::Util::XmlWriterSettings;

Client::Client()
	: m_explorations(new std::vector<std::shared_ptr<const Exploration> >()),
	  m_exploration_owners(new std::map<std::string, std::vector<ServerInfo> >),
	  m_subscription_manager(new ExplorationSubscriptionManager()),
	  m_settings(SETTING_ORGANIZATION, SETTING_APPLICATION)
{
	// Set variables for QSettings
	QCoreApplication::setOrganizationName(SETTING_ORGANIZATION);
	QCoreApplication::setApplicationName(SETTING_APPLICATION);

	setWindowTitle("Parameter Exploration - Client");
    setMinimumSize(730, 320);

	QHBoxLayout* layout = new QHBoxLayout;

	m_status = new Status();
	layout->addWidget(m_status);

	// Actions
	action_server_list = new QAction("&Server List", this);
	//action_subscribe = new QAction("&Subscribe", this);
	action_start_exploration = new QAction("&Start Exploration...", this);
	action_delete_exploration = new QAction("&Delete exploration...", this);
	action_download_exploration = new QAction("&Download exploration...", this);
	action_workspace_wizard = new QAction("&Workspace...", this);
	action_task_overview = new QAction("&Task overview/Exploration Status", this);

	//action_subscribe->setDisabled(true);
	action_start_exploration->setDisabled(true);
	action_delete_exploration->setDisabled(true);
	action_download_exploration->setDisabled(true);
	action_task_overview->setDisabled(true);

	action_download_exploration->setVisible(false);

	//action_subscribe->setShortcut(QKeySequence("Ctrl+U"));
	action_start_exploration->setShortcut(QKeySequence("Ctrl+E"));
	action_delete_exploration->setShortcut(QKeySequence("Ctrl+Shift+E"));
	action_workspace_wizard->setShortcut(QKeySequence("Ctrl+W"));
	action_task_overview->setShortcut(QKeySequence("Ctrl+T"));

	// Toolbar
	QToolBar* toolbar = new QToolBar(this);
	toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
	toolbar->setMovable(false);
	toolbar->addAction(action_server_list);
	//toolbar->addAction(action_subscribe);
	toolbar->addSeparator();
	toolbar->addAction(action_start_exploration);
	toolbar->addAction(action_delete_exploration);
	toolbar->addAction(action_download_exploration);
	toolbar->addSeparator();
	toolbar->addAction(action_task_overview);
	addToolBar(toolbar);

	connect(action_server_list, SIGNAL(triggered()), this, SLOT(ShowServerOverview()));
	//connect(action_subscribe, SIGNAL(triggered()), this, SLOT(Subscribe()));
	connect(action_start_exploration, SIGNAL(triggered()), this, SLOT(StartExploration()));
	connect(action_delete_exploration, SIGNAL(triggered()), this, SLOT(DeleteExploration()));
//	connect(action_download_exploration, SIGNAL(triggered()), this, SLOT(DownloadExploration()));
	connect(action_workspace_wizard, SIGNAL(triggered()), this, SLOT(SetWorkspace()));
	connect(action_task_overview, SIGNAL(triggered()), this, SLOT(ShowExplorationStatusOverview()));

	// Status bar
	m_statusbar = new QStatusBar(this);
	m_statusbar->clearMessage();

	m_connection_button = new QPushButton("Not Connected");
	m_connection_button->setFlat(true);
	connect(m_connection_button, SIGNAL(clicked()), action_server_list, SLOT(trigger()));
	m_statusbar->addPermanentWidget(m_connection_button);
	setStatusBar(m_statusbar);

	QWidget* central = new QWidget;
	central->setLayout(layout);
	setCentralWidget(central);

	m_task_view = new TaskOverview();
//	connect(m_task_view, SIGNAL(StopTask(int)), this, SLOT(StopTask(int)));
//	connect(m_task_view, SIGNAL(StartTask(int)), this, SLOT(RestartTask(int)));

  	m_exploration_status_view = std::shared_ptr<ExplorationStatusOverview>(new ExplorationStatusOverview(m_subscription_manager, m_exploration_owners));
	m_exploration_status_view->setModal(true);

	connect(m_exploration_status_view.get(), SIGNAL(ResendSingleTask(ServerInfo*,const Exploration*,int)), this, SLOT(ResendSingleTask(ServerInfo*,const Exploration*,int)));
	connect(m_subscription_manager.get(), &ExplorationSubscriptionManager::UpdateReceived, m_status, &Status::ReceiveProgress);
	//connect(m_subscription_manager.get(), &ExplorationSubscriptionManager::UpdateReceived, m_exploration_status_view.get(), &ExplorationStatusOverview::ContactServers);
	// Set Workspace
	if (m_settings.contains("workspace"))
		m_workspace_path = m_settings.value("workspace").toString().toStdString();

	InitWorkspace();
    m_server_view = std::shared_ptr<ServerOverview>(new ServerOverview(m_workspace_path));
    connect(m_server_view.get(), SIGNAL(ListUpdated()), this, SLOT(UpdateConnectedButton()));
	connect(m_server_view.get(), SIGNAL(BackupRequested()), this, SLOT(Backup()));
	m_server_view->setModal(true);
	Read_Backup();
}

Client::~Client()
{
}


void Client::Backup()
{
	ptree writer;

	writer.put("SentExplorationsNr", m_exploration_owners->size());
	int i = 0;
	int j;

	for (auto map_entry : *m_exploration_owners) {
		writer.put("RunningExploration_" + std::to_string(i), map_entry.first);
		writer.put("RunningExploration_" + std::to_string(i) + ".ServersTaskedNr", map_entry.second.size());

		j = 0;
		for (auto server : map_entry.second) {
			writer.put_child("RunningExploration_" + std::to_string(i)+ ".ServerTasked_" + std::to_string(j), server.ToPtree());
			++j;
		}

		auto exploration = std::find_if(m_explorations->begin(), m_explorations->end(), [map_entry] (std::shared_ptr<const Exploration> e) {
			return e->GetName() == map_entry.first;
		});

		if (exploration != m_explorations->end()) {
			if (dynamic_cast<const ParameterExploration*>(exploration->get()) != nullptr) {
				writer.put_child("RunningExploration_" + std::to_string(i) + ".parameter_exploration", (**exploration).ToPtree());
			} else if (dynamic_cast<const FileExploration*>(exploration->get()) != nullptr) {
				writer.put_child("RunningExploration_" + std::to_string(i) + ".file_exploration", (**exploration).ToPtree());
			} else {
				assert("Exploration type unknown.");
			}
			//writer.put_child("RunningExploration_" + std::to_string(i) + ".rawExploration", (**exploration).ToPtree());
		} else {
			SimPT_Logging::ParexLogger::get()->error("{} UNKNOWN EXPLORATION", LOG_TAG_8);
			// error, something went wrong - we dont know where the exploration went
		}
		++i;
	}
	writer.put_child("ServerOverview", m_server_view->ToPtree());

	write_xml(m_workspace_path + "/client_backup.xml", writer, std::locale());
	//For readability
	//write_xml(m_workspace_path + "/client_backup.xml", writer, std::locale(), XmlWriterSettings::GetTab());
}

void Client::Read_Backup()
{
	QDir workspace(m_workspace_path.c_str());
  if (workspace.exists("client_backup.xml")) {
		try {
			ptree reader;
			read_xml(m_workspace_path + "/client_backup.xml", reader);

			for (int i = 0; i < reader.get<int>("SentExplorationsNr"); i++) {
				std::string expl_name = reader.get<std::string>("RunningExploration_" + std::to_string(i));
				SimPT_Logging::ParexLogger::get()->debug("{} [READ_BACKUP] Reading exploration with name: {} <---", LOG_TAG_8, expl_name);
				std::vector<ServerInfo> servers;
				for (int j = 0; j < reader.get<int>("RunningExploration_" + std::to_string(i)+ ".ServersTaskedNr"); j++) {
					ServerInfo s = ServerInfo(
						QString::fromStdString(reader.get<std::string>("RunningExploration_" + std::to_string(i)+ ".ServerTasked_" + std::to_string(j) + ".name")),
						QString::fromStdString(reader.get<std::string>("RunningExploration_" + std::to_string(i)+ ".ServerTasked_" + std::to_string(j) + ".address")),
						reader.get<int>("RunningExploration_" + std::to_string(i)+ ".ServerTasked_" + std::to_string(j) + ".port") );
					servers.push_back(s);
				}
				m_exploration_owners->insert(std::make_pair(expl_name, servers));

				ptree rawExplorationPtree = reader.get_child("RunningExploration_" + std::to_string(i));

				std::shared_ptr<const Exploration> exploration = nullptr;
				if (rawExplorationPtree.find("parameter_exploration") != rawExplorationPtree.not_found()) {
					exploration = std::make_shared<ParameterExploration>(rawExplorationPtree.get_child("parameter_exploration"));
				}	else if (rawExplorationPtree.find("file_exploration") != rawExplorationPtree.not_found()) {
					exploration = std::make_shared<FileExploration>(rawExplorationPtree.get_child("file_exploration"));
				}
				else {
					SimPT_Logging::ParexLogger::get()->error("{} CANT READ BACKUP", LOG_TAG_8);
				}

				if (exploration) {
					m_explorations->push_back(exploration);
				}
				//TODO maybe reconnecto to subscribe stuff? also probably error control
				//connect(....);
			}
			ptree ServerOverviewSubtree = reader.get_child("ServerOverview");
			m_server_view->FromPtree(ServerOverviewSubtree);
		}
		catch (std::exception& e) {
			SimPT_Logging::ParexLogger::get()->error("{} {}", LOG_TAG_8, e.what());

		}
		catch (...) {
			return;
		}
	}
}



void Client::InitWorkspace()
{
	if (m_workspace_path == "" || !QDir(QString(m_workspace_path.c_str())).exists()) {
		auto f = std::make_shared<Ws::WorkspaceFactory>();
		SimShell::Gui::WorkspaceWizard wizard(f);
		if (wizard.exec() == QDialog::Accepted)
			m_workspace_path = wizard.GetWorkspaceDir();
		else
			return;
	}
}

void Client::ShowServerOverview()
{
    m_server_view->show();
	Backup();
}

void Client::StartExploration()
{
    if(!m_server_view->CanStartExploration()) {
        return;
    }

	std::shared_ptr<const Exploration> last = m_explorations->size() ? m_explorations->back() : nullptr;

    std::shared_ptr<ExplorationWizard> wizard =
		std::make_shared<ExplorationWizard>(SimPT_Shell::Ws::CliWorkspace(m_workspace_path).GetPreferences(), last, this);

	if (wizard->exec() == QDialog::Accepted) {
    	m_exploration_wizard = wizard;
	 	SendExplorationTasks();
    }

	Backup();
}

void Client::SendExplorationTasks()
{
    std::shared_ptr<const Exploration> exploration = m_exploration_wizard->GetExplorationWithTimestamp();

    if(exploration == nullptr or exploration->GetNumberOfTasks() == 0) {
        QMessageBox::critical(this, "Exploration Error", "No exploration selected or tasks to divide.", QMessageBox::Abort);
    } else {
        SimPT_Logging::ParexLogger::get()->debug("{} PUSHING NEW EXPLORATION: {}", LOG_TAG_8, exploration->GetName().c_str());
        m_explorations->push_back(exploration);

        if(!m_server_view->CanStartExploration()) {
            return;
        }

        auto servers = m_server_view->GetCheckedServers();
        unsigned long server_count = servers.size();
		std::string parking_ip = "";
        ServerInfo* parking = m_server_view->GetParkingServer();
        if(parking)
			parking_ip = parking->GetAddress().toStdString();

        unsigned int task_count = exploration->GetNumberOfTasks();
        std::vector<std::vector<int> > task_csvs(server_count);

        for(unsigned int i = 0; i < task_count; i++){
        	(*(m_exploration_owners.get()))[exploration->GetName()].push_back(*servers[i % server_count]);
            task_csvs[i % server_count].push_back(i);
        }

        const Exploration* raw_exploration = exploration.get();
        std::string save_dir = exploration->GetName();
        std::string resultsdir = m_exploration_wizard->GetExploration()->GetSaveDirectory();

        for (unsigned int i = 0; i < server_count; i++) {
            ClientProtocol::GetSession(servers[i], [i, raw_exploration, task_csvs, parking_ip, save_dir, resultsdir](ClientProtocol& client){
                SimPT_Logging::ParexLogger::get()->debug("{} Sending expl with #t : {} and parking : {} and results dir: {}", LOG_TAG_8, task_csvs[i].size(), parking_ip.c_str(), resultsdir.c_str());
                client.SendExploration(raw_exploration, parking_ip, task_csvs[i], resultsdir);
            });
        }

		// Trigger parking workspace update if necessary;
		if (parking) {
			ClientProtocol::GetSession(m_server_view->GetParkingServer(), [raw_exploration, parking_ip, resultsdir](ClientProtocol& client){
				SimPT_Logging::ParexLogger::get()->debug("{} Sending parex workspace update.", LOG_TAG_8);
				std::vector<int> empty_tasks;
				client.SendExploration(raw_exploration, parking_ip, empty_tasks, resultsdir);
			});
		}

        m_subscription_manager->addExploration(exploration->GetName());
        for(auto server_ptr : servers){
        	m_subscription_manager->addServer(std::shared_ptr<ServerInfo>(new ServerInfo(*server_ptr)));
        }
        m_exploration_status_view->UpdateMembers(*m_explorations, m_server_view->GetCheckedServers());
    }
}

void Client::ResendSingleTask(ServerInfo* server, const Exploration* exploration_in, int task_id){
	std::shared_ptr<Exploration> exploration(exploration_in->Clone());
	exploration->SetName("[task" + std::to_string(task_id) + "]" + exploration->GetName());
	m_explorations->push_back(exploration);

    if(!m_server_view->CanStartExploration()) {
        return;
    }

	std::string parking_ip = "";
    ServerInfo* parking = m_server_view->GetParkingServer();
    if(parking)
		parking_ip = parking->GetAddress().toStdString();

    Exploration* raw_exploration = exploration.get();
    std::string save_dir = exploration->GetName();
    std::string resultsdir = exploration->GetSaveDirectory();

    ClientProtocol::GetSession(server, [raw_exploration, task_id, parking_ip, save_dir, resultsdir](ClientProtocol& client){
        client.SendExploration(raw_exploration, parking_ip, {task_id}, resultsdir);
    });

    m_subscription_manager->addExploration(exploration->GetName());
	m_subscription_manager->addServer(std::shared_ptr<ServerInfo>(new ServerInfo(*server)));
    m_exploration_status_view->UpdateMembers(*m_explorations, m_server_view->GetCheckedServers());
}

void Client::DeleteExploration()
{
	SimPT_Logging::ParexLogger::get()->debug("{} DeleteExploration", LOG_TAG_8);
	ExplorationSelection select_exploration;

	std::vector<std::string> client_side_names = std::vector<std::string>();
	for (auto i : *m_exploration_owners) {
		client_side_names.push_back(i.first);
	}

	select_exploration.UpdateExplorations(client_side_names);
	if (select_exploration.exec() == QDialog::Accepted) {
		if (select_exploration.Selected()) {
			if (QMessageBox::question(this, "Delete exploration", "Are you sure you want to delete the exploration(s) from the server?",
				QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes) {
				for (auto exploration_name : select_exploration.GetSelection()) {
					if(m_exploration_owners->find(exploration_name) != m_exploration_owners->end()){
				    auto owner_list = (*(m_exploration_owners.get()))[exploration_name];
				    for (auto owner_info : owner_list) {
				      ServerInfo* owner = new ServerInfo(owner_info);
							//TODO refactor this
				      ClientProtocol::GetFullExplorationName(owner, exploration_name, [owner,  exploration_name, this](std::string full_name){
				        ClientProtocol::GetSession(owner, [full_name, this, exploration_name, owner](ClientProtocol& client){
				          client.DeleteExploration(full_name);
				          delete owner;
				        });
				      });
				    }
						this->m_exploration_owners->erase(exploration_name);
					}
					for(auto it = m_explorations->begin(); it != m_explorations->end(); it++){
						if((*it)->GetName() == exploration_name){
							m_explorations->erase(it);
							break;
						}
					}
					m_subscription_manager->removeExploration(exploration_name);
				}
			}
		}
	}
	Backup();
	m_subscription_manager->refresh();
}

void Client::ShowTaskOverview()
{
	m_task_view->show();
}

void Client::ShowExplorationStatusOverview()
{
	SimPT_Logging::ParexLogger::get()->debug("{} ShowExplorationStatusOverview", LOG_TAG_8);
    if(m_explorations->size() > 0){
        m_exploration_status_view->UpdateMembers(*m_explorations, m_server_view->GetCheckedServers());
        m_exploration_status_view->show();
    } else {
        QMessageBox::critical(this, "Exploration Error", "No explorations have been started yet.", QMessageBox::Abort);
    }
}

void Client::SetWorkspace()
{
	m_workspace_path = "";
	InitWorkspace();
}

void Client::UpdateConnectedButton()
{
    int server_count = m_server_view->GetConnectedServers().size();
    QString button_text = "Not Connected";

    bool button_state = server_count == 0;

	//action_subscribe->setDisabled(button_state);
	action_start_exploration->setDisabled(button_state);
	action_delete_exploration->setDisabled(button_state);
	action_download_exploration->setDisabled(button_state);
	action_task_overview->setDisabled(button_state);

    if (server_count == 0){
    } else {
    	button_text = QString::fromStdString("Connected to " + std::to_string(server_count) + " server" + ((server_count > 1) ? "s" : ""));
    }

    m_connection_button->setText(button_text);
}

void Client::UpdateStatus(const ExplorationProgress *status)
{
	std::shared_ptr<const ExplorationProgress> progress(status);
	m_subscribed_exploration = progress->GetExploration().GetName();
	m_task_view->UpdateExploration(progress);
	action_task_overview->setDisabled(false);
}

} // namespace SimPT_Shell
