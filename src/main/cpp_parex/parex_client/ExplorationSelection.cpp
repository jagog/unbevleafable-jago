/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ExplorationSelection
 */

#include "ExplorationSelection.h"

#include <QVBoxLayout>
#include <QPushButton>
#include <QString>
#include <QStringList>
#include <QStringListModel>

namespace SimPT_Parex {

ExplorationSelection::ExplorationSelection(QWidget* parent): QDialog(parent),
	m_single_selection(false)
{
	DrawGui();
}

ExplorationSelection::ExplorationSelection(bool single, QWidget* parent):
	QDialog(parent), m_single_selection(single)
{
	DrawGui();
}

void ExplorationSelection::DrawGui()
{
	setWindowTitle("Select Explorations");

	QVBoxLayout *layout = new QVBoxLayout;

	m_exploration_widget = new QListView;

	m_exploration_widget->setEditTriggers(QAbstractItemView::NoEditTriggers);
	if (m_single_selection)
		m_exploration_widget->setSelectionMode(QAbstractItemView::SingleSelection);
	else
		m_exploration_widget->setSelectionMode(QAbstractItemView::MultiSelection);
	m_exploration_widget->setSelectionBehavior(QAbstractItemView::SelectItems);
	m_exploration_widget->clearSelection();

	QStringListModel *model = new QStringListModel();
	QStringList list;

	m_exploration_widget->setSelectionMode(QAbstractItemView::NoSelection);
	model->setStringList(list);

	m_exploration_widget->setModel(model);

	layout->addWidget(m_exploration_widget);

	QHBoxLayout *buttonLayout = new QHBoxLayout;
	m_ok_button = new QPushButton("Ok");
	buttonLayout->addWidget(m_ok_button);

	QPushButton *cancel = new QPushButton("Cancel");
	buttonLayout->addWidget(cancel);

	layout->addLayout(buttonLayout);

	setLayout(layout);

	connect(m_ok_button, SIGNAL(clicked()), this, SLOT(Accept()));
	connect(cancel, SIGNAL(clicked()), this, SLOT(reject()));
}

ExplorationSelection::~ExplorationSelection()
{
	delete m_exploration_widget;
}

void ExplorationSelection::Accept()
{
	m_selected_exploration.clear();

	auto selection = m_exploration_widget->selectionModel();
	auto selected_indices = selection->selectedIndexes();
	auto model = m_exploration_widget->model();

	for (auto index : selected_indices) {
		m_selected_exploration.push_back(model->data(index, 0).toString().toStdString());
	}

	accept();
}

void ExplorationSelection::setAddingNames(const bool& adding_names) {
	m_adding_names = adding_names;
}

void ExplorationSelection::UpdateExplorations(std::vector<std::string> explorations)
{
	QStringListModel* model = (QStringListModel*) m_exploration_widget->model();

	QStringList list;
	if (explorations.size() == 0) {
		list << "No explorations found!";
		m_exploration_widget->setSelectionMode(QAbstractItemView::NoSelection);
	}
	else {
		m_exploration_widget->setSelectionMode(QAbstractItemView::MultiSelection);
		if (m_adding_names) {
			list = model->stringList();
			for (auto name : explorations){
				list << QString(name.c_str());
			}
		}
		else {
			for (auto name : explorations){
				list << QString(name.c_str());
			}
			m_adding_names = true;
		}
	}
	list.removeDuplicates();
	model->setStringList(list);
}

std::vector<std::string> ExplorationSelection::GetSelection()
{
	return m_selected_exploration;
}

bool ExplorationSelection::Selected()
{
	return (m_selected_exploration.size() != 0);
}

} // namespace
