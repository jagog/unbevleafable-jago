#ifndef SIMPT_PAREX_START_PAGE_H_INCLUDED
#define SIMPT_PAREX_START_PAGE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for StartPage
 */

#include <QWizard>
#include <QWizardPage>
#include <boost/property_tree/ptree.hpp>
#include <map>

class QComboBox;
class QLineEdit;
class QListWidget;
class QRadioButton;

namespace SimPT_Parex {

class Exploration;

/**
 * Select type of new exploration
 */
class StartPage : public QWizardPage
{
	Q_OBJECT
public:
	/// Constructor
	StartPage(std::shared_ptr<Exploration>& exploration, const std::shared_ptr<const Exploration>& lastExploration);

	/// Destructor
	virtual ~StartPage();

private:
	enum { Page_Start, Page_Path, Page_Param, Page_Files, Page_Send, Page_Template_Path };

private:
	///
	virtual bool validatePage();

	///
	virtual int nextId() const;

private:
	QRadioButton*    m_select_new_sweep;
	QRadioButton*    m_select_new_file;
	QRadioButton*    m_select_new_template;
	QRadioButton*    m_select_edit;
	QRadioButton*    m_select_send;

	std::shared_ptr<Exploration>&          m_exploration;
	std::shared_ptr<const Exploration>     m_last_exploration;
};

} // namespace

#endif // end_of_include_guard
