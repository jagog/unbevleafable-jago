/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of Server Dialog
 */

#include "ServerDialog.h"

#include "gui/qtmodel/PTreeModel.h"
#include "gui/PTreeView.h"
#include "parex_protocol/ServerInfo.h"
#include "util/misc/StringUtils.h"
#include "util/misc/XmlWriterSettings.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QMessageBox>
#include <QFormLayout>

namespace SimPT_Parex {

using namespace std;
using SimPT_Sim::Util::XmlWriterSettings;
using boost::property_tree::ptree;
using namespace boost::property_tree::xml_parser;

ServerDialog::ServerDialog(std::string path):
	m_path(path), m_current_server(nullptr), m_last_server(nullptr)
{
	setWindowTitle("Servers");

	QVBoxLayout* layout = new QVBoxLayout;

	layout->addWidget(new QLabel("Servers:"));
	m_server_list = new QComboBox;
	layout->addWidget(m_server_list);

	QFormLayout* form_layout = new QFormLayout;
	form_layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);

	m_name = new QLineEdit;
	m_address = new QLineEdit;
	m_port = new QLineEdit;

	form_layout->addRow(tr("&Name"), m_name);
	form_layout->addRow(tr("&Address"), m_address);
	form_layout->addRow(tr("&Port"), m_port);

	QVBoxLayout* button_layout = new QVBoxLayout;

	QHBoxLayout* button_top = new QHBoxLayout;
	QHBoxLayout* button_bottom = new QHBoxLayout;

	QPushButton* button_connect = new QPushButton("Add To Server List");
	QPushButton* button_save = new QPushButton("Save Server");
	QPushButton* button_delete = new QPushButton("Delete server");
	button_top->addWidget(button_connect);
	button_top->addWidget(button_save);
	button_top->addWidget(button_delete);

	QPushButton* button_cancel = new QPushButton("Cancel");
	button_bottom->addStretch();
	button_bottom->addWidget(button_cancel);
	button_bottom->addStretch();

	button_layout->addLayout(button_top);
	button_layout->addLayout(button_bottom);

	layout->addLayout(form_layout);
	layout->addLayout(button_layout);

	setLayout(layout);

	connect(m_server_list, SIGNAL(currentIndexChanged(int)), this, SLOT(SelectionChanged(int)));

	connect(button_connect, SIGNAL(clicked()), this, SLOT(Connect()));
	connect(button_save, SIGNAL(clicked()), this, SLOT(SaveServer()));
	connect(button_delete, SIGNAL(clicked()), this, SLOT(DeleteServer()));
	connect(button_cancel, SIGNAL(clicked()), this, SLOT(reject()));

	LoadServerList();
}

ServerDialog::~ServerDialog()
{
	delete m_name;
	delete m_address;
	delete m_port;
	m_servers.clear();
	delete m_current_server;
	delete m_last_server;
	delete m_server_list;

}

void ServerDialog::Connect()
{
	m_current_server = new ServerInfo(m_name->text(), m_address->text(), m_port->text().toInt());
	SaveServerList(true);
	accept();
}

ServerInfo* ServerDialog::GetServer()
{
	return m_current_server;
}

void ServerDialog::UpdateName(const QString name)
{
	m_name->setText(name);
}

void ServerDialog::UpdateAddress(const QString address)
{
	m_address->setText(address);
}

void ServerDialog::UpdatePort(const QString port)
{
	m_port->setText(port);
}

void ServerDialog::SelectionChanged(int index)
{
	auto server = m_servers[index];
	m_name->setText(server->GetName());
	m_address->setText(server->GetAddress());
	m_port->setText(QString::number(server->GetPort()));
	m_current_server = server;
}

void ServerDialog::LoadServerList()
{
	try {
		ptree input;
		read_xml(m_path + "/serverList.xml", input);

		const std::string NOTFOUND = ")_NOT_FOUND_(";
		int size = input.get<int>("number");
		if (size == 0 && input.get("last", NOTFOUND) == NOTFOUND)
			return;

		for (int i = 0; i < size; i++) {
			std::string temp = "server";
			temp.append(to_string(i));
			m_servers.push_back(new ServerInfo(input.get<std::string>(temp + ".name"),
						input.get<std::string>(temp + ".address"),
						input.get<int>(temp + ".port")));
		}

		for (auto server : m_servers)
			m_server_list->addItem(server->GetName());

		if (input.get("last", NOTFOUND) != NOTFOUND) {
			m_servers.push_back(new ServerInfo(input.get<std::string>("last.name"),
						input.get<std::string>("last.address"),
						input.get<int>("last.port")));
			m_last_server = m_servers[m_servers.size()-1];
			m_server_list->addItem("Most recently connected");
		}

		m_server_list->setCurrentIndex(m_servers.size()-1);
		SelectionChanged(m_servers.size()-1);
	}
	catch(xml_parser_error &e) {

	}
}

void ServerDialog::SaveServerList(bool last)
{
	ptree output;
	int i = 0;
	int count = 0;
	for (auto server : m_servers) {
		if (m_server_list->itemText(i).toStdString() != "Most recently connected"){
			std::string temp = "server";
			temp.append(to_string(count));
			output.add(temp + ".name", server->GetName().toStdString());
			output.add(temp + ".address", server->GetAddress().toStdString());
			output.add(temp + ".port", server->GetPort());
			count++;
		}
		i++;
	}
	output.add("number", count);

	ServerInfo* last_server = nullptr;

	if (last && m_current_server != nullptr) {
		last_server = m_current_server;
	}
	else if (m_last_server != nullptr){
		last_server = m_last_server;
	}

	if (last_server != nullptr) {
		output.add("last.name", last_server->GetName().toStdString());
		output.add("last.address", last_server->GetAddress().toStdString());
		output.add("last.port", last_server->GetPort());
	}

	write_xml(m_path + "/serverList.xml", output, std::locale(), XmlWriterSettings::GetTab());
}

void ServerDialog::SaveServer()
{
	if (m_current_server != nullptr && m_current_server->GetName() == m_name->text()) { // Update values
		m_current_server->Update(m_address->text(), m_port->text().toInt());
	}
	else { // Insert new server
		if (m_name->text().toStdString() == "Most recently connected") {
			QMessageBox::critical(this, "Server Error", "The given name may not be used for a server.",
				QMessageBox::Ok);
			return;
		}

		ServerInfo* server = new ServerInfo(m_name->text(), m_address->text(), m_port->text().toInt());
		m_servers.push_back(server);
		m_server_list->addItem(server->GetName());
		m_server_list->setCurrentIndex(m_servers.size()-1);
	}
	SaveServerList(false);
}

void ServerDialog::DeleteServer()
{
	int index = m_server_list->currentIndex();
	m_servers.erase(m_servers.begin() + index);
	m_server_list->removeItem(index);
	SelectionChanged(index % m_servers.size());
	SaveServerList(false);
}

} // namespace SimPT_Shell
