#ifndef SIMPT_PAREX_STATUS_H_INCLUDED
#define SIMPT_PAREX_STATUS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Status
 */

#include "parex_protocol/ExplorationProgress.h"

#include <memory>
#include <QWidget>
#include <vector>
#include <map>
#include <string>
#include <memory>
#include <QtWidgets/QTableWidget>

class QTextEdit;



namespace SimPT_Parex {

using ProgressMap = std::map<std::string, std::vector<std::shared_ptr<ExplorationProgress>>>;
/**
 *
 * Widget to display status of subscribed exploration
 */
class Status : public QWidget
{
public:
	/// Constructor
	Status();

	/// Destructor
	virtual ~Status() {}

public slots:
	void ReceiveProgress(std::shared_ptr<ProgressMap> updated_progress, std::shared_ptr<std::vector<std::string>> exploration_names);

private:
	/// Update the status view
	void UpdateView();

	/// check if this is an exploration running accross multiple servers -> combine results.
	int	isCombinedProgress(QString name);
private:
	std::shared_ptr<ProgressMap>  				m_progress;
	std::shared_ptr<std::vector<std::string>> 	m_exploration_names;
	QTableWidget*                      			m_view;
};

} // namespace

#endif // end_of_include_guard
