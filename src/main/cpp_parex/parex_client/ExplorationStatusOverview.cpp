#include "ExplorationStatusOverview.h"
#include <QHeaderView>
#include <QLabel>
#include <QPushButton>
#include <QRadioButton>
#include <QStandardItemModel>
#include <QTableView>
#include <QTreeView>
#include <QListView>
#include <QAbstractItemView>
#include <QSplitter>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QVBoxLayout>
#include <QDateTime>
#include <Qt>
#include <util/misc/StringUtils.h>

#include <cpp_logging/logger.h>

using namespace std;
using SimPT_Sim::Util::ToString;

namespace SimPT_Parex {


ExplorationStatusOverview::ExplorationStatusOverview(std::shared_ptr<ExplorationSubscriptionManager> subscription_manager,
    std::shared_ptr<std::map<std::string, std::vector<ServerInfo> > > exploration_owners)
    : m_server_tree(nullptr), m_exploration_owners(exploration_owners),
    m_subscription_manager(subscription_manager)
{
	setWindowTitle("Parameter Exploration - Exploration Status Overview");
    setMinimumSize(600, 320);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);

    m_server_tree = new QTreeWidget(this);

    QHBoxLayout* button_row = new QHBoxLayout;

    QPushButton* refresh_button = new QPushButton("Refresh");
    connect(refresh_button, SIGNAL(clicked()), this, SLOT(ContactServers()));

    QPushButton* close_button = new QPushButton("Close");
    connect(close_button, SIGNAL(clicked()), this, SLOT(hide()));

    button_row->addWidget(refresh_button);
    button_row->addWidget(close_button);

    layout->addWidget(m_server_tree);
    layout->addLayout(button_row);
    setLayout(layout);

    //ContactServers();
    //DisplayInfo();

    if(m_explorations.size() == 0){
        SimPT_Logging::ParexLogger::get()->debug("{} No explorations have been started", LOG_TAG_3);
    }
}

ExplorationStatusOverview::~ExplorationStatusOverview()
{
    delete m_server_tree;
}

void ExplorationStatusOverview::UpdateMembers(std::vector<std::shared_ptr<const Exploration> > explorations, std::vector<ServerInfo*> servers_in)
{
    SimPT_Logging::ParexLogger::get()->debug("{} Update memebers: {}", LOG_TAG_3, explorations.size());
    m_explorations = explorations;
    m_servers = servers_in;
    ContactServers();
}

void ExplorationStatusOverview::DisplayInfo()
{
    QStringList header;
    header << "Item" << "Status";

    auto toCsv = [](std::vector<int> in) -> QString {
        QString failed_csv;
        for(int i : in) failed_csv += QString::number(i) + ", ";
        return failed_csv;
    };


    auto RunningTimeToString = [](unsigned int time) -> QString
    {
        int seconds = time % 60;
        time /= 60;
        int minutes = time % 60;
        time /= 60;
        int hours = time % 24;
        int days = time / 24;

        return QString::fromStdString(
            ToString(days, 2, '0') + ":" +
            ToString(hours, 2, '0') + ":" +
            ToString(minutes, 2, '0') + ":" +
            ToString(seconds, 2, '0'));
    };

    auto StateToString = [](const ExplorationTask &task) -> QString
    {
        switch(task.GetState()) {
        case TaskState::Waiting:
            if (task.GetNumberOfTries() == 0)
                return "Waiting";
            else
                return "Retrying ("
                    + QString::fromStdString(to_string(task.GetNumberOfTries())) + ")";
        case TaskState::Running:
            return "Running";
        case TaskState::Finished:
            return "Finished";
        case TaskState::Cancelled:
            return "Cancelled";
        case TaskState::Failed:
            return "Failed";
        default:
            assert(false && "This task is in an unknown state.");
            return "Unknown";
        }
    };

    m_server_tree->clear();
    m_server_tree->setColumnCount(2);
    m_server_tree->setHeaderLabels(header);

    QList<QTreeWidgetItem *> items;
    QList<QTreeWidgetItem *> expands;
    QList<QTreeWidgetItem *> button_items;
    std::vector<QString>     button_texts;
    std::vector<std::function<void(void)>> button_actions;

    // Build a branch for every exploration
    for(auto singleprog : m_progress){
        std::string name = singleprog.first;
        unsigned int task_total = 0;
        unsigned int done_total = 0;
        bool         all_parked = true;
        std::vector<int> error_total;

        QTreeWidgetItem* item = new QTreeWidgetItem((QTreeWidget*)0,
            { QString(singleprog.first.c_str()), "" });
        QTreeWidgetItem* detail_item = new QTreeWidgetItem(item,
            { QString("Server Details"), QString::number(singleprog.second.size()) + " Server" + (singleprog.second.size() > 1 ? "s" : "") });
        expands.append(detail_item);

        QList<QTreeWidgetItem *> detail_items;

        // Create a branch for every server that is on the exploration
        for(auto expl_cache : singleprog.second){
            task_total =  expl_cache.total;
            done_total += expl_cache.done;

            if (!expl_cache.parked)
                all_parked = false;

            QString addr_and_port;
            if(expl_cache.server_info != nullptr)
                addr_and_port = expl_cache.server_info->GetAddress() + ":" + QString::number(expl_cache.server_info->GetPort());

            QTreeWidgetItem* detail_detail_item = new QTreeWidgetItem(detail_item,
                { addr_and_port, QString::number(expl_cache.done) });

            for(int err : expl_cache.errors) error_total.push_back(err);

            QList<QTreeWidgetItem *> temp_items;

            temp_items.append(new QTreeWidgetItem(detail_detail_item,
                { QString("Tasks Finished"), QString::number(expl_cache.done) }));

            temp_items.append(new QTreeWidgetItem(detail_detail_item,
                { QString("Task Failed List"), toCsv(expl_cache.errors) }));
        }

        QTreeWidgetItem* task_detail_item = new QTreeWidgetItem(item,
            { QString("Tasks Details"), QString::number(task_total) + " Task" + (task_total > 1 ? "s" : "") });
        expands.append(task_detail_item);

        for (unsigned int i = 0; i < task_total; i++)
        {
            if(m_subscription_manager->hasTask(name, i)){
                ExplorationTask task = m_subscription_manager->getTask(name, i);
                auto state = task.GetState();

                QTreeWidgetItem* single_task_detail_item = new QTreeWidgetItem(task_detail_item,
                    { "Task " + QString::number(i) });

                QList<QTreeWidgetItem *> temp_items;

                temp_items.append(new QTreeWidgetItem(single_task_detail_item,
                    { QString("State"), StateToString(task) }));

                temp_items.append(new QTreeWidgetItem(single_task_detail_item,
                    { QString("Running Time"), RunningTimeToString(task.GetRunningTime()) }));

                temp_items.append(new QTreeWidgetItem(single_task_detail_item,
                    { QString("Node"), QString::fromStdString(task.getNodeIp())+":"+QString::number(task.getNodePort()) }));

                QString parking_status( task.getParkingStatus() ? "On server" : "Not on server" );
                temp_items.append(new QTreeWidgetItem(single_task_detail_item, {QString("Parked"), parking_status }));

                std::function<void(void)> action_event;

                QString action;
                if (state == TaskState::Running || state == TaskState::Waiting) {
                    action = "Stop";
                    action_event = [this, name, i](){
                        int ret = QMessageBox::question(this, "Stop Confirmation", "Are you sure you want to stop this task?",
                            QMessageBox::Cancel, QMessageBox::Ok);
                        switch(ret){
                            case QMessageBox::Ok:
                                StopTask(name, i);
                                break;
                            default:
                                return;
                        }
                    };
                    // QPushButton* button = new QPushButton("Stop");
                    // connect(button, SIGNAL(clicked()), stop_signal, SLOT(map()));
                    // stop_signal->setMapping(button, i);
                    // m_table->setIndexWidget(m_model->index(i,2), button);
                } else if (state == TaskState::Cancelled) {
                    action = "Restart";
                    action_event = [this, name, i](){
                        int ret = QMessageBox::question(this, "Restart Confirmation", "Are you sure you want to restart this task?",
                            QMessageBox::Cancel, QMessageBox::Ok);
                        switch(ret){
                            case QMessageBox::Ok:
                                RestartTask(name, i);
                                break;
                            default:
                                return;
                        }
                    };
                    // QPushButton* button = new QPushButton("Restart");
                    // connect(button, SIGNAL(clicked()), restart_signal, SLOT(map()));
                    // restart_signal->setMapping(button, i);
                    // m_table->setIndexWidget(m_model->index(i,2), button);
                }

                if(action.size() > 0){
                    temp_items.append(new QTreeWidgetItem(single_task_detail_item,
                        { QString("Action"), action }));
                    button_items.append(temp_items.last());
                    button_texts.push_back(action);
                    button_actions.push_back(action_event);
                }


                // SimPT_Logging::ParexLogger::get()->debug("{} getting exploration for task {}", LOG_TAG_3, i);
                // std::shared_ptr<ExplorationProgress> exploration_prog = m_subscription_manager->getExplorationProgressForTask(name, i);
                // const Exploration& exploration = exploration_prog->GetExploration();
                // for (unsigned int j = 0; j < exploration.GetParameters().size(); j++) {
                //     const std::string& param = exploration.GetParameters()[j];
                //     const std::string& value = exploration.GetValues(i)[j];
                //
                //     temp_items.append(new QTreeWidgetItem(single_task_detail_item,
                //         { QString::fromStdString(param).split('.').last(), QString::fromStdString(value) }));
                // }
            }
        }

        QList<QTreeWidgetItem *> child_items;

        item->setText(1, QString::number(done_total) + "/" + QString::number(task_total));

        child_items.append(detail_item);

        child_items.append(new QTreeWidgetItem(item,
            { QString("Task Count"), QString::number(task_total) }));

        child_items.append(new QTreeWidgetItem(item,
            { QString("Tasks Finished"), QString::number(done_total) }));

        child_items.append(new QTreeWidgetItem(item,
            { QString("Task Failed List"), toCsv(error_total) }));

        QString parking_status( all_parked? "All results on parking server" : "(Some) results not on parking server" );
        child_items.append(new QTreeWidgetItem(item,{ QString("Parking status"), parking_status } ));

        items.append(item);

        // Only expand this item if it hasn't finished
        if(done_total < task_total) expands.append(item);
    }

    m_server_tree->insertTopLevelItems(0, items);
    m_server_tree->resizeColumnToContents(0);

    for(auto widget_ptr : expands) widget_ptr->setExpanded(true);

    for(int i = 0; i < button_items.size(); i++){
        QPushButton* button = new QPushButton(button_texts[i]);
        m_server_tree->setItemWidget(
            button_items[i], 1, button
        );
        connect(button, &QPushButton::clicked, button_actions[i]);
    }
}

void ExplorationStatusOverview::StopTask(std::string exploration_name, unsigned int task_id){
    SimPT_Logging::ParexLogger::get()->debug("{} StopTask", LOG_TAG_3);
    if(m_exploration_owners->find(exploration_name) != m_exploration_owners->end()){
        auto owner_list = (*(m_exploration_owners.get()))[exploration_name];
        if(owner_list.size() > task_id){
            ServerInfo* owner = new ServerInfo(owner_list[task_id]);
            SimPT_Logging::ParexLogger::get()->debug("{} Created owner", LOG_TAG_3);
            ClientProtocol::GetFullExplorationName(owner, exploration_name, [owner, task_id](std::string full_name){
                SimPT_Logging::ParexLogger::get()->debug("{} exploration: {}", LOG_TAG_3, full_name.c_str());
                ClientProtocol::GetSession(owner, [full_name, task_id, owner](ClientProtocol& client){
                    SimPT_Logging::ParexLogger::get()->debug("{} Calling remote method...", LOG_TAG_3);
                    client.StopTask(full_name, task_id);

                    delete owner;
                });
            });
        }
    }
}

void ExplorationStatusOverview::RestartTask(std::string exploration_name, unsigned int task_id){
    // SimPT_Logging::ParexLogger::get()->debug("{} RestartTask", LOG_TAG_3);
    // if(m_exploration_owners->find(exploration_name) != m_exploration_owners->end()){
    //     auto owner_list = (*(m_exploration_owners.get()))[exploration_name];
    //     if(owner_list.size() > task_id){
    //         ServerInfo* owner = new ServerInfo(owner_list[task_id]);
    //         ClientProtocol::GetFullExplorationName(owner, exploration_name, [owner, task_id](std::string full_name){
    //             SimPT_Logging::ParexLogger::get()->debug("{} exploration: {}", LOG_TAG_3, full_name.c_str());
    //             ClientProtocol::GetSession(owner, [full_name, task_id, owner](ClientProtocol& client){
    //                 SimPT_Logging::ParexLogger::get()->debug("{} Calling remote method...", LOG_TAG_3);
    //                 client.RestartTask(full_name, task_id);

    //                 delete owner;
    //             });
    //         });
    //     }
    // }
    SimPT_Logging::ParexLogger::get()->debug("{} RestartTask", LOG_TAG_3);
    if(m_exploration_owners->find(exploration_name) != m_exploration_owners->end()){
        auto owner_list = (*(m_exploration_owners.get()))[exploration_name];
        if(owner_list.size() > task_id){
            ServerInfo* owner = new ServerInfo(owner_list[task_id]);
            for(auto expl_ptr : m_explorations){
                if(expl_ptr->GetName() == exploration_name)
                {
                    emit ResendSingleTask(owner, expl_ptr.get(), task_id);
                    break;
                }
            }
        }
    }
}
void ExplorationStatusOverview::ContactServers()
{
    m_progress.clear();
    if(m_explorations.size() == 0)
        QMessageBox::critical(this, "Exploration Error", "No exploration(s) selected.", QMessageBox::Abort);
    else {
        std::vector<std::string> names;
        for(auto explo : m_explorations) names.push_back(explo->GetName());

        for(unsigned int i = 0; i < m_servers.size(); i++){
            ClientProtocol::GetFullExplorationNames(m_servers[i], names, [this, i](std::vector<std::string> full_names){
                GetSingleStatus(i, full_names);
            });
        }
    }
    m_subscription_manager->refresh();
}

void ExplorationStatusOverview::GetSingleStatus(int i, std::vector<std::string> names)
{
    SimPT_Logging::ParexLogger::get()->debug("{} Inside GetSingleStatus", LOG_TAG_3);
    GetSingleStatus(i, names, 0);
}

void ExplorationStatusOverview::GetSingleStatus(int i, std::vector<std::string> names, unsigned int pos)
{
    if(pos >= names.size()) return;
    std::string name = names[pos];
    pos++;

    ClientProtocol::GetSession(m_servers[i], [this, i, name, names, pos](ClientProtocol& client){
        std::shared_ptr<CachedExplorationProgress> prog(new CachedExplorationProgress());

        connect(&client, &ClientProtocol::ExplorationStatusReceived, [this, i, name, names, &client, prog, pos](int total_in, int done_in, std::vector<int> error_in, bool parked){
            SimPT_Logging::ParexLogger::get()->debug("{} disconnecting...", LOG_TAG_3);
            disconnect(&client, &ClientProtocol::ExplorationStatusReceived, 0, 0);

            prog->errors = error_in;
            prog->total = total_in;
            prog->done  = done_in;
            prog->parked = parked;
            prog->server_info = std::shared_ptr<ServerInfo>(new ServerInfo(*m_servers[i]));

            AddProgress(name, *(prog.get()));
            SimPT_Logging::ParexLogger::get()->debug("{} calling callback", LOG_TAG_3);
            GetSingleStatus(i, names, pos);
        });

        SimPT_Logging::ParexLogger::get()->debug("{} Calling GetExplorationProgress for {}", LOG_TAG_3, name.c_str());
        client.GetExplorationProgress(name);
    });
}

void ExplorationStatusOverview::AddProgress(std::string key, CachedExplorationProgress progress)
{
    QString qtkey = QString(key.c_str());
    std::string pretty_name = qtkey.left(qtkey.length() - SERVER_TIMESTAMP_LENGTH).toStdString();
    if(m_progress.find(pretty_name) == m_progress.end())
        m_progress[pretty_name] = {};
    m_progress[pretty_name].push_back(progress);
    SimPT_Logging::ParexLogger::get()->debug("{} progress map size: {}", LOG_TAG_3, m_progress.size());
    DisplayInfo();
}

} //namespace SimPT_Parex
