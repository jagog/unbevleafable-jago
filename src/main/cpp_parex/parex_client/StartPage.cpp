/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for StartPage
 */

#include "StartPage.h"

#include "parex_protocol/FileExploration.h"
#include "parex_protocol/ListSweep.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/RangeSweep.h"

#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QComboBox>
#include <QDoubleValidator>
#include <QFileDialog>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QListWidget>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QRegExp>
#include <QRegExpValidator>
#include <QSettings>
#include <QString>
#include <QVBoxLayout>

#include <algorithm>
#include <cctype>
#include <fstream>
#include <functional>
#include <iostream>
#include <locale>
#include <memory>
#include <sstream>

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

namespace SimPT_Parex {

StartPage::StartPage(std::shared_ptr<Exploration> &exploration, const std::shared_ptr<const Exploration> &lastExploration)
	: m_exploration(exploration), m_last_exploration(lastExploration)
{
	setTitle("Select load");
	setSubTitle("Please select the way to load an exploration.");

	QVBoxLayout* layout = new QVBoxLayout;

	m_select_new_sweep = new QRadioButton("New sweep based exploration");
	m_select_new_file = new QRadioButton("New file based exploration");
	m_select_new_template = new QRadioButton("New template based exploration");
	m_select_edit = new QRadioButton("Edit previous exploration");
	m_select_send = new QRadioButton("Re-send previous exploration");

	layout->addWidget(m_select_new_sweep);
	layout->addWidget(m_select_new_file);
	layout->addWidget(m_select_new_template);
	layout->addWidget(m_select_edit);
	layout->addWidget(m_select_send);

	m_select_new_sweep->setChecked(true);
	m_select_edit->setEnabled(m_last_exploration != nullptr && dynamic_cast<const ParameterExploration*>(m_last_exploration.get()));
	m_select_send->setEnabled(m_last_exploration != nullptr);

	setLayout(layout);
}

StartPage::~StartPage() {}

bool StartPage::validatePage()
{
	if (m_select_edit->isChecked() || m_select_send->isChecked()) {
		assert(!m_select_edit->isChecked() || dynamic_cast<const ParameterExploration*>(m_last_exploration.get())); // m_select_edit->isChecked() implies (=>) dynamic_pointer_cast<const ParameterExploration>(lastExploration)
		m_exploration = std::shared_ptr<Exploration>(m_last_exploration->Clone());
	}

	return true;
}

int StartPage::nextId() const
{
	if (m_select_new_sweep->isChecked())
		return Page_Path;
	else if (m_select_new_file->isChecked())
		return Page_Files;
	else if (m_select_new_template->isChecked())
		return Page_Template_Path;
	else if (m_select_edit->isChecked())
		return Page_Param;
	else if (m_select_send->isChecked())
		return Page_Send;
	return -1;
}

} // namespace
