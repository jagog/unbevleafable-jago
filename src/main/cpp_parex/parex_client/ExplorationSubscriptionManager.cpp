#include "ExplorationSubscriptionManager.h"

#include <cpp_logging/logger.h>

namespace SimPT_Parex {

void ExplorationSubscriptionManager::addServer(std::shared_ptr<ServerInfo> server){
	for(auto it = m_servers.begin(); it != m_servers.end(); it++){
		auto server_loop = *it;
		if(server_loop.GetAddress() == server->GetAddress() and server_loop.GetPort() == server->GetPort()){
			return;
		}
	}
	m_servers.push_back(ServerInfo(*(server.get())));
	SimPT_Logging::ParexLogger::get()->debug("{} Pushed server: {}", LOG_TAG_5, m_servers.back().GetAddress().toStdString().c_str());
	refresh();
}

void ExplorationSubscriptionManager::removeServer(std::shared_ptr<ServerInfo> server){
	for(auto it = m_servers.begin(); it != m_servers.end(); it++){
		auto server_loop = *it;
		if(server_loop.GetAddress() == server->GetAddress() and server_loop.GetPort() == server->GetPort()){
			m_servers.erase(it);
			
		    ClientProtocol::GetFullExplorationNames(server.get(), m_exploration_names, [this, server](std::vector<std::string> full_names)
		    {
		    	for(auto full_name : full_names)
		    		dropSubscription(*(server.get()), full_name);
	        });
			return;
		}
	}
}

void ExplorationSubscriptionManager::addExploration(std::string name){
	if(std::find(m_exploration_names.begin(), m_exploration_names.end(), name) == m_exploration_names.end()){
		m_exploration_names.push_back(name);
		SimPT_Logging::ParexLogger::get()->debug("{} Pushed exploration: {}", LOG_TAG_5, name.c_str());
		refresh();
	}
}

void ExplorationSubscriptionManager::removeExploration(std::string name){
	if(std::find(m_exploration_names.begin(), m_exploration_names.end(), name) != m_exploration_names.end()){
		m_exploration_names.erase(std::find(m_exploration_names.begin(), m_exploration_names.end(), name));

		for(unsigned int i = 0; i < m_servers.size(); i++){
	        ClientProtocol::GetFullExplorationName(&m_servers[i], name, [this, i](std::string full_name){
	            dropSubscription(m_servers[i], full_name);
	        });
	    }
	    
		for(auto it = m_progress.begin(); it != m_progress.end(); it++){
			if(QString::fromStdString(it->first).startsWith(name.c_str())){
				m_progress.erase(it);
				break;
			}
		}
	}
}


bool ExplorationSubscriptionManager::hasTask(std::string exploration_name, int task_id){
	auto tasks = getTasks(exploration_name);
	return tasks.find(task_id) != tasks.end();
}

ExplorationTask ExplorationSubscriptionManager::getTask(std::string exploration_name, int task_id){
	return getTasks(exploration_name)[task_id];
}


std::shared_ptr<ExplorationProgress> ExplorationSubscriptionManager::getExplorationProgressForTask(std::string exploration_name, int task_id){
	return getWeightedProgressList(exploration_name)[task_id];
}


std::map<int, ExplorationTask> ExplorationSubscriptionManager::getTasks(std::string exploration_name){
	std::map<int, ExplorationTask> tasks;
	auto progri_map = getWeightedProgressList(exploration_name);
	for(auto kvpair : progri_map)
		tasks[kvpair.first] = kvpair.second->GetTask(kvpair.first);
	return tasks;
}

std::map<int, std::shared_ptr<ExplorationProgress>> ExplorationSubscriptionManager::getWeightedProgressList(std::string exploration_name){
	auto compareTasks = [](const ExplorationTask& task1, const ExplorationTask& task2) -> bool
	{
		auto taskVal = [](const ExplorationTask& task) -> int {
			switch(task.GetState()) {
		        case TaskState::Waiting:
		        	return 1;
		        case TaskState::Running:
		            return 2;
		        case TaskState::Finished:
		            return 5;
		        case TaskState::Cancelled:
		            return 4;
		        case TaskState::Failed:
		            return 3;
		        default:
		        	return -1;
			}
		};
		return taskVal(task1) > taskVal(task2);
	};

	std::map<int, std::shared_ptr<ExplorationProgress>> progri;
	auto progressList = getProgressList(exploration_name);
	for(auto prog_ptr : progressList){
		unsigned long task_count = prog_ptr->GetTotalTaskCount();
		for(unsigned long i = 0; i < task_count; i++){
	        if(progri.find(i) == progri.end()){
	        	progri[i] = prog_ptr;
	        } else if(compareTasks(prog_ptr->GetTask(i), progri[i]->GetTask(i))) {
	        	progri[i] = prog_ptr;
	        }
        }
	}
	return progri;
}


std::vector<std::shared_ptr<ExplorationProgress>> ExplorationSubscriptionManager::getProgressList(std::string exploration_name){
	std::vector<std::shared_ptr<ExplorationProgress>> progressList;
	for(auto kvpair : m_progress){
		if(QString(kvpair.first.c_str()).startsWith(QString(exploration_name.c_str()))){
			for(auto i : kvpair.second) progressList.push_back(i);
		}
	}
	return progressList;
}

void ExplorationSubscriptionManager::connectAll(){
	SimPT_Logging::ParexLogger::get()->debug("{} connecting to {} servers", LOG_TAG_5, m_servers.size());
	for(unsigned int i = 0; i < m_servers.size(); i++){
        ClientProtocol::GetFullExplorationNames(&m_servers[i], m_exploration_names, [this, i](std::vector<std::string> full_names){
            for(auto expl_name : full_names){
	            SimPT_Logging::ParexLogger::get()->debug("{} Requesting status for {}", LOG_TAG_5, expl_name.c_str());
	            initSubscription(m_servers[i], expl_name);
            }
        });
    }
}


void ExplorationSubscriptionManager::initSubscription(ServerInfo server, std::string exploration_name){
	SimPT_Logging::ParexLogger::get()->debug("{} Before seg", LOG_TAG_5);
	ClientProtocol::GetSession(&server, [this, exploration_name](ClientProtocol& client){
		SimPT_Logging::ParexLogger::get()->debug("{} Disconnecting client...", LOG_TAG_5);
		disconnect(&client, &ClientProtocol::ExplorationStatus, 0, 0);
		
		SimPT_Logging::ParexLogger::get()->debug("{} Unsubscribing client...", LOG_TAG_5);
		client.UnsubscribeUpdates(exploration_name);

		SimPT_Logging::ParexLogger::get()->debug("{} Connecting client...", LOG_TAG_5);
		connect(&client, &ClientProtocol::ExplorationStatus, [this](const ExplorationProgress* progress_in){
			SimPT_Logging::ParexLogger::get()->debug("{} got status {}", LOG_TAG_5, progress_in->GetExploration().GetName().c_str());
			std::shared_ptr<ExplorationProgress> progress(new ExplorationProgress(progress_in->ToPtree()));
			m_progress[progress->GetExploration().GetName()].push_back(progress);
			emit UpdateReceived(std::make_shared<ProgressMap>(m_progress), std::make_shared<std::vector<std::string>>(m_exploration_names));
		});

		SimPT_Logging::ParexLogger::get()->debug("{} Subscribing client...", LOG_TAG_5);
		client.SubscribeUpdates(exploration_name);
	});

}

void ExplorationSubscriptionManager::dropSubscription(ServerInfo server, std::string exploration_name){
	ClientProtocol::GetSession(&server, [this, exploration_name](ClientProtocol& client){
		disconnect(&client, &ClientProtocol::ExplorationStatus, 0, 0);	
		client.UnsubscribeUpdates(exploration_name);
	});
}

void ExplorationSubscriptionManager::disconnectAll()
{
	SimPT_Logging::ParexLogger::get()->debug("{} Enter", LOG_TAG_5);
	std::vector<std::string> names_copy;
	for(auto name : m_exploration_names) names_copy.push_back(name);

	for(unsigned int i = 0; i < m_servers.size(); i++){
		ServerInfo* temp_server = new ServerInfo(m_servers[i]);
        ClientProtocol::GetSession(temp_server, [i, names_copy](ClientProtocol& client){
        	SimPT_Logging::ParexLogger::get()->debug("{} inside GetSession", LOG_TAG_5);
            connect(&client, &ClientProtocol::ExplorationNames, [names_copy, i, &client](std::vector<std::string> names){
                disconnect(&client, &ClientProtocol::ExplorationNames, 0, 0);
                SimPT_Logging::ParexLogger::get()->debug("{} inside callback", LOG_TAG_5);
                for(std::string expl_name_found : names_copy){
                    QString compare = QString(expl_name_found.c_str());
                    for(std::string expl_name : names){
                        QString qs = QString::fromStdString(expl_name);
                        if(qs.startsWith(compare)){
							disconnect(&client, &ClientProtocol::ExplorationStatus, 0, 0);
							client.UnsubscribeUpdates(expl_name);
                        }
                    }
                }
            });
            client.RequestExplorationNames();
        });
    }
}

void ExplorationSubscriptionManager::refresh(){
	//disconnectAll();
	connectAll();
	emit UpdateReceived(std::make_shared<ProgressMap>(m_progress), std::make_shared<std::vector<std::string>>(m_exploration_names));
}

}
