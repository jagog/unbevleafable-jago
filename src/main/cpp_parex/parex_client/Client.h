#ifndef SIMPT_PAREX_CLIENT_H_INCLUDED
#define SIMPT_PAREX_CLIENT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Client
 */

#include "Status.h"
#include "parex_protocol/ClientProtocol.h"

#include <QMainWindow>
#include <QString>
#include <QPushButton>
#include <QSettings>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <vector>
#include <functional>
#include "ServerDialog.h"
#include "parex_protocol/ServerInfo.h"
#include "ServerOverview.h"
#include "ExplorationWizard.h"
#include "ExplorationSubscriptionManager.h"

namespace SimPT_Parex {

class ClientProtocol;
class Exploration;
class ExplorationPtreeView;
class ExplorationSelection;
class TaskOverview;


/**
 * Client for Parameter Exploration
 */
class Client : public QMainWindow
{
	Q_OBJECT
public:
	const static QString SETTING_ORGANIZATION;
	const static QString SETTING_APPLICATION;

	/// Constructor
	Client();

	/// Destructor
	virtual ~Client();



	///
	void Read_Backup();

private slots:

	/// Show the current server list dialog
	void ShowServerOverview();

	/// Start new exploration wizard and send exploration
	void StartExploration();

    /// send the tasks of an exploration to servers
    void SendExplorationTasks();

	/// Delete a running exploration
	void DeleteExploration();

	/// Start workspace wizard to set up workspace
	void SetWorkspace();

	/// Display the task overview window
	void ShowTaskOverview();

    /// Show the ExplorationStatusOverview
    void ShowExplorationStatusOverview();

	/// Called when a status update arrives at the client
	void UpdateStatus(const ExplorationProgress *status);

    /// Update the text in the connected button
    void UpdateConnectedButton();

    void ResendSingleTask(ServerInfo* server, const Exploration* exploration_in, int task_id);

	/// Backup explorations and server list to file.
	void Backup();

private:
	std::string 				                                           m_workspace_path;
	std::shared_ptr<ExplorationWizard>	                                   m_exploration_wizard;
    std::shared_ptr<std::vector<std::shared_ptr<const Exploration> > >     m_explorations;
    std::shared_ptr<std::map<std::string, std::vector<ServerInfo> > >      m_exploration_owners;
	std::string				                                               m_subscribed_exploration;
	std::shared_ptr<std::map<std::string, std::vector<ServerInfo>>>  m_client_exploration_owners;

    TaskOverview*                                   m_task_view;
    std::shared_ptr<ExplorationStatusOverview> 		m_exploration_status_view;
    std::shared_ptr<ServerOverview>                 m_server_view;
    std::shared_ptr<ExplorationSubscriptionManager> m_subscription_manager;

	Status*                        m_status;
	QStatusBar*                    m_statusbar;
	QPushButton*                   m_connection_button;

	QAction*	action_server_list;
	QAction*	action_subscribe;
	QAction*	action_start_exploration;
	QAction*	action_delete_exploration;
	QAction*	action_download_exploration;
	QAction*	action_workspace_wizard;
	QAction*	action_edit_workspace;
	QAction*	action_task_overview;
	QSettings	m_settings;

	/// Init the workspace to use
	void InitWorkspace();
};

} // namespace

#endif // end-of-include-guard
