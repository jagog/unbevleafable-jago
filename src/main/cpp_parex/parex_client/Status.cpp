/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Status
 */

#include "Status.h"


#include <QVBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QTextEdit>

#include <cpp_logging/logger.h>

namespace SimPT_Parex {

Status::Status()
	: m_progress(nullptr), m_exploration_names(nullptr)
{
	QVBoxLayout* layout = new QVBoxLayout;

	m_view = new QTableWidget(this);
	UpdateView();

	layout->addWidget(m_view);
	setLayout(layout);
}

void Status::UpdateView()
{
	m_view->clear();
	m_view->setRowCount(0);
	m_view->verticalHeader()->hide();

	QStringList header;

	if (!m_exploration_names || !m_exploration_names->size()) {
		header << tr("No info to display.");
		m_view->setColumnCount(1);
		m_view->setHorizontalHeaderLabels(header);

		int row = m_view->rowCount();
		m_view->insertRow(row);

		m_view->setItem(row,0, new QTableWidgetItem("Any subscriptions you have will be shown here."));
		m_view->item(row,0)->setFlags( m_view->item(0,0)->flags() ^ Qt::ItemIsEditable);
	}
	else {
		header << tr("Name") << tr("Total Tasks") << tr("Finished") << tr("Failed");
		m_view->setColumnCount(4);
		m_view->setHorizontalHeaderLabels(header);

		for (auto it = m_progress->begin(); it != m_progress->end(); it++)
		{
			std::string search_name = it->first;
			if(std::find(m_exploration_names->begin(), m_exploration_names->end(), search_name.substr(0,search_name.size() - 24)) == m_exploration_names->end())
				break;

			int combinedProgress = isCombinedProgress(QString::fromStdString(it->first));

			if (combinedProgress == -1)
			{
				int done 	= 0;
				int err 	= 0;
				int row = m_view->rowCount();

				for(auto progress : it->second){
					err 	= progress->GetTaskCount(TaskState::Failed);
					done 	= progress->GetTaskCount(TaskState::Finished);
				}

				m_view->insertRow( row );
				m_view->setItem(row, 0, new QTableWidgetItem( it->first.c_str() ));
				m_view->setItem(row, 1, new QTableWidgetItem( QString::number(it->second.back()->GetTotalTaskCount())));
				m_view->setItem(row, 2, new QTableWidgetItem( QString::number(done) ));
				m_view->setItem(row, 3, new QTableWidgetItem( QString::number(err)  ));

				for(unsigned i = 0; i < 4; i++)
					m_view->item(row, i)->setFlags(m_view->item(row, i)->flags() ^ Qt::ItemIsEditable);

				if(err)
					m_view->item(row,3)->setBackgroundColor(QColor::fromRgb(229,181,107));
			}
			else
			{
				int done 	= m_view->item(combinedProgress,2)->text().toInt();
				int err  	= m_view->item(combinedProgress,3)->text().toInt();

				done 	+= it->second.back()->GetTaskCount(TaskState::Finished);
				err 	+= it->second.back()->GetTaskCount(TaskState::Failed);

				m_view->item(combinedProgress,2)->setText(QString::number(done));
				m_view->item(combinedProgress,3)->setText(QString::number(err));
			}


		}
	}

	m_view->resizeColumnsToContents();
	m_view->horizontalHeader()->setStretchLastSection(true);
}

void Status::ReceiveProgress(std::shared_ptr<ProgressMap> updated_progress, std::shared_ptr<std::vector<std::string>> exploration_names)
{
	SimPT_Logging::ParexLogger::get()->debug("{} Received progress", LOG_TAG_7);
	m_progress = updated_progress;
	m_exploration_names = exploration_names;
	UpdateView();
}

int Status::isCombinedProgress(QString name)
{
	for(int i = 0; i < m_view->rowCount(); i++){
		if(m_view->item(i,0)->text().startsWith(name.left(name.size()-13)))
			return i;
	}
	return -1;
}


} // namespace
