#ifndef SERVER_CHECKLIST_H
#define SERVER_CHECKLIST_H

#include <iostream>
#include <QWidget>
#include <QDialog>
#include <QCheckBox>
#include <QMessageBox>
#include <QSettings>
#include <map>
#include <memory>
#include <QTableWidget>
#include <vector>
#include "parex_protocol/ServerInfo.h"
#include "parex_protocol/ClientProtocol.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_client/ExplorationStatusOverview.h"
#include "ExplorationWizard.h"

#include "gui/WorkspaceWizard.h"
#include "../../cpp_simptshell/workspace/CliWorkspace.h"

namespace SimPT_Shell { namespace Ws { class WorkspaceFactory; } }

class QStandardItemModel;
class QTimer;
class QTableView;

namespace SimPT_Parex {

/**
 * List of Servers
 */
class ServerOverview: public QDialog
{
    Q_OBJECT
public:
    /**
     * Constructor
     *
     * @param servers_in SOMETHING SOMETHING
     */
    ServerOverview(std::string workspace);

    /**
     * Destructor
     */
    virtual ~ServerOverview();

    /// Returns the connected servers
    std::vector<ServerInfo*> GetConnectedServers();

    /// Returns the servers that have been checked by the user
    std::vector<ServerInfo*> GetCheckedServers();

    /// Get the selected parking if there is one
    ServerInfo* GetParkingServer();

    ///
    boost::property_tree::ptree ToPtree() const;

    /// take empty ServerOverview, populate it using ptree
    void FromPtree(boost::property_tree::ptree pt);


    /// Returns true if an exploration can be started
    bool CanStartExploration();

protected:
    void showEvent(QShowEvent* e);

signals:
    void ListUpdated();
    void BackupRequested();

private:
    /// Show the dialog to add a server
    void ShowAddServer();

    /// Show the list of servers
    void ShowServerOverview();

    /// Refresh the buttons at the top of the dialog after pinging the servers
    void RefreshButtons();

    /// Ping all servers to see if they are available
    void Ping();

    /// delete currently selected servers from the server list.
    void DeleteSelection();

    /// add servers to the m_servers vector without using the dialog, used when reading backup
    void AddServer(ServerInfo* server);

    QTableWidget*               m_server_table;
    std::vector<ServerInfo*>    m_servers;
    std::vector<int>            m_server_pings;
    std::string                 m_workspace_path;


};

} // namespace simPT_Shell

#endif //SERVER_CHECKLIST_H
