#ifndef EXPRATION_STATUS_DIALOG_H
#define EXPRATION_STATUS_DIALOG_H

#include <iostream>
#include <QWidget>
#include <QDialog>
#include <QCheckBox>
#include <QMessageBox>
#include <QString>
#include <string>
#include <thread>
#include <map>
#include <memory>
#include <QTreeWidget>
#include <vector>

#include "ExplorationSubscriptionManager.h"
#include "parex_protocol/ServerInfo.h"
#include "parex_protocol/ClientProtocol.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/ExplorationProgress.h"
#include "parex_server/ClientHandler.h"

class QStandardItemModel;
class QTimer;
class QTableView;
class ExplorationProgress;

namespace SimPT_Parex {

/**
 * A more advanced version of the CachedExplorationProgress including a ExplorationProgress ptr
 */
// class InDepthExplorationProgress : public CachedExplorationProgress {
// public:
//     std::shared_ptr<ExplorationProgress> progress;

//     // Inherit the constructor so we don't have to copy paste
//     using CachedExplorationProgress::CachedExplorationProgress;

// };

/**
* Dialog diplaying the status of the current exploration
*/
class ExplorationStatusOverview: public QDialog
{
    Q_OBJECT
public:
    /**
     * Constructor
     *
     * @param exploration   The current exploration
     */
    ExplorationStatusOverview(std::shared_ptr<ExplorationSubscriptionManager> subscription_manager,
            std::shared_ptr<std::map<std::string, std::vector<ServerInfo> > > exploration_owners);

    /**
     * Destructor
     */
    virtual ~ExplorationStatusOverview();

    void UpdateMembers(std::vector<std::shared_ptr<const Exploration> > explorations, std::vector<ServerInfo*> servers_in);

signals:
    void ResendSingleTask(ServerInfo* server, const Exploration* exploration_in, int task_id) const;

private slots:
    void ContactServers();

private:
    void DisplayInfo();
    void GetSingleStatus(int i, std::vector<std::string> names);
    void GetSingleStatus(int i, std::vector<std::string> names, unsigned int pos);
    void AddProgress(std::string key, CachedExplorationProgress progress);

    void StopTask(std::string exploration_name, unsigned int task_id);
    void RestartTask(std::string exploration_name, unsigned int task_id);


    QTreeWidget*                                                     m_server_tree;
    std::vector<ServerInfo*>                                         m_servers;
    std::vector<std::shared_ptr<const Exploration> >                 m_explorations;
    std::shared_ptr<std::map<std::string, std::vector<ServerInfo>>>  m_exploration_owners;
    std::map<std::string, std::vector<CachedExplorationProgress> >   m_progress;
    const int SERVER_TIMESTAMP_LENGTH =                              24;
    std::shared_ptr<ExplorationSubscriptionManager>                  m_subscription_manager;
};

} // namespace simPT_Shell

#endif //EXPRATION_STATUS_DIALOG_H
