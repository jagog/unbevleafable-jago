/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ParamPage.
 */

#include "ParamPage.h"

#include "parex_protocol/FileExploration.h"
#include "parex_protocol/ListSweep.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/RangeSweep.h"

#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QComboBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QListView>
#include <QListWidget>
#include <QMessageBox>
#include <QRadioButton>
#include <QSettings>
#include <QString>
#include <QVBoxLayout>

#include <cctype>
#include <iostream>

using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

namespace SimPT_Parex {

ParamPage::ParamPage(const std::shared_ptr<Exploration> &exploration)
	 : m_exploration(exploration), m_current_index(-1)
{
	// Set title + text
	setTitle("Parameter selection");
	setSubTitle("Please specify the required parameters");

	// Set main layout
	QVBoxLayout* layout = new QVBoxLayout;

	m_parameters = new QComboBox;
	layout->addWidget(m_parameters);

	/* -- Original -- */
	QHBoxLayout* originalLayout = new QHBoxLayout;
	m_original_select = new QRadioButton("Original");
	m_original_select->setChecked(true);
	originalLayout->addWidget(m_original_select);
	m_original = new QLineEdit();
	originalLayout->addWidget(m_original);
	m_original->setReadOnly(true);

	/* -- List -- */
	QVBoxLayout* listLayout = new QVBoxLayout;
	m_list_select = new QRadioButton("List");
	listLayout->addWidget(m_list_select);
	m_list = new QLineEdit;
	listLayout->addWidget(m_list);

	/* -- Loop -- */
	QDoubleValidator* doubleValidator = new QDoubleValidator(this);

	QVBoxLayout* loopLayout = new QVBoxLayout;
	m_loop_select = new QRadioButton("Loop");
	loopLayout->addWidget(m_loop_select);

	QFormLayout* loopFormLayout = new QFormLayout;

	m_from = new QLineEdit;
	m_from->setValidator(doubleValidator);
	loopFormLayout->addRow("From", m_from);

	m_to = new QLineEdit;
	m_to->setValidator(doubleValidator);
	loopFormLayout->addRow("To", m_to);

	m_step = new QLineEdit;
	m_step->setValidator(doubleValidator);
	loopFormLayout->addRow("Step", m_step);

	loopLayout->addLayout(loopFormLayout);

	layout->addLayout(originalLayout);
	layout->addLayout(listLayout);
	layout->addLayout(loopLayout);

	setLayout(layout);

	// Connect all objects
	connect(m_parameters, SIGNAL(currentIndexChanged(int)), this, SLOT(SelectParameter(int)));
	connect(m_original_select, SIGNAL(toggled(bool)), this, SLOT(UpdateOriginal(bool)));
	connect(m_list_select, SIGNAL(toggled(bool)), this, SLOT(UpdateList(bool)));
	connect(m_loop_select, SIGNAL(toggled(bool)), this, SLOT(UpdateLoop(bool)));

	UpdateOriginal(true);
	UpdateList(false);
	UpdateLoop(false);
}


void ParamPage::UpdateOriginal(bool checked)
{
	m_original->setEnabled(checked);
}


void ParamPage::UpdateList(bool checked)
{
	m_list->setEnabled(checked);
}


void ParamPage::UpdateLoop(bool checked)
{
	m_from->setEnabled(checked);
	m_to->setEnabled(checked);
	m_step->setEnabled(checked);
}


void ParamPage::SelectParameter(int index)
{
	ParameterExploration *parameterExploration = dynamic_cast<ParameterExploration*>(m_exploration.get());
	assert(parameterExploration && "Exploration wasn't a ParameterExploration");

	if (index == m_current_index) {
		return;
	} else if (!SaveParameter()) {
		m_parameters->setCurrentIndex(m_current_index);
		return;
	}

	m_current_index = index;

	/* Clear input boxes */
	m_original->setText("");
	m_list->setText("");
	m_from->setText("");
	m_to->setText("");
	m_step->setText("");

	std::string parameter = m_parameters->currentText().toStdString();
	const ISweep *sweep = parameterExploration->GetSweep(parameter);

	if (sweep == nullptr) {
		m_original_select->setChecked(true);
		m_original->setText(QString::fromStdString(parameterExploration->GetOriginalValue(parameter)));
	} else if (const ListSweep *listSweep = dynamic_cast<const ListSweep*>(sweep)) {
		m_list_select->setChecked(true);

		QString listString;
		for (unsigned int i = 0; i < listSweep->GetNumberOfValues(); ++i) {
			if (i > 0) {
				listString += ", ";
			}
			listString += QString::fromStdString(listSweep->GetValue(i));
		}
		m_list->setText(listString);
	} else if (const RangeSweep *rangeSweep = dynamic_cast<const RangeSweep*>(sweep)) {
		m_loop_select->setChecked(true);

		m_from->setText(QString::number(rangeSweep->GetFrom()));
		m_to->setText(QString::number(rangeSweep->GetTo()));
		m_step->setText(QString::number(rangeSweep->GetStep()));
	} else {
		assert(false && "I wouldn't know what kind of sweep this is");
	}
}


void ParamPage::initializePage()
{
	ParameterExploration *parameterExploration = dynamic_cast<ParameterExploration*>(m_exploration.get());
	assert(parameterExploration && "Exploration wasn't a ParameterExploration");

	std::vector<std::string> parameters = parameterExploration->GetPossibleParameters();

	if (parameters.size() > 0)
	{
		for (const std::string &parameter : parameters) {
			m_parameters->addItem(QString::fromStdString(parameter));
		}
	} else {
		m_parameters->setEnabled(false);
		UpdateOriginal(false);
		UpdateList(false);
		UpdateLoop(false);
	}
}


bool ParamPage::validatePage()
{
	return SaveParameter();
}


bool ParamPage::SaveParameter()
{
	ParameterExploration *parameterExploration = dynamic_cast<ParameterExploration*>(m_exploration.get());
	assert(parameterExploration && "Exploration wasn't a ParameterExploration");

	if (m_current_index == -1)
		return true;

	std::string parameter = m_parameters->itemText(m_current_index).toStdString();

	if (m_original_select->isChecked()) {
		parameterExploration->RemoveSweep(parameter);
	} else if (m_list_select->isChecked()) {
		QStringList qlist = m_list->text().split(QRegExp(" *, *"), QString::SkipEmptyParts);
		if (qlist.empty()) {
			QMessageBox::warning(this, "Cannot save parameters", "The list does not contain any strings");
			return false;
		}

		if (!m_list->isModified()) {
			return true;
		}

		std::vector<std::string> list;
		for (const auto &e : qlist) {
			list.push_back(e.toStdString());
		}
		parameterExploration->SetSweep(parameter, new ListSweep(list));

		m_list->setModified(false);
	} else if (m_loop_select->isChecked()) {
		bool ok;
		double from = m_from->text().toDouble(&ok);
		if (!m_from->hasAcceptableInput() || !ok) {
			QMessageBox::warning(this, "Cannot save parameters", "'From' does not contain a valid number");
			return false;
		}

		double to = m_to->text().toDouble(&ok);
		if (!m_to->hasAcceptableInput() || !ok) {
			QMessageBox::warning(this, "Cannot save parameters", "'To' does not contain a valid number");
			return false;
		}

		double step = m_step->text().toDouble(&ok);
		if (!m_step->hasAcceptableInput() || !ok) {
			QMessageBox::warning(this, "Cannot save parameters", "'Step' does not contain a valid number");
			return false;
		}

		if (from > to) {
			QMessageBox::warning(this, "Cannot save parameters", "This is not a valid range: 'from' is greater than 'to'");
			return false;
		} else if (step <= 0) {
			QMessageBox::warning(this, "Cannot save parameters", "This is not a valid range: 'step' is smaller or equal to zero");
			return false;
		}

		if (!m_from->isModified() && !m_to->isModified() && !m_step->isModified()) {
			return true;
		}

		parameterExploration->SetSweep(parameter, new RangeSweep(from, to, step));

		m_from->setModified(false);
		m_to->setModified(false);
		m_step->setModified(false);
	}

	return true;
}


} // namespace
