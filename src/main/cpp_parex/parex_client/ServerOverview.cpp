#include "ServerOverview.h"
#include <QHeaderView>
#include <QPushButton>
#include <QRadioButton>
#include <QStandardItemModel>
#include <QDateTime>
#include <QVBoxLayout>
#include <QToolBar>
#include <QFileInfo>
#include <QMenu>
#include <util/misc/StringUtils.h>
#include "../parex_protocol/SecureShell.h"
#include "ServerDialog.h"
#include "util/misc/XmlWriterSettings.h"
#include "Client.h"

#include <cpp_logging/logger.h>

using namespace std;
using namespace boost::property_tree::xml_parser;
using SimPT_Sim::Util::ToString;
using boost::property_tree::ptree;
using SimPT_Sim::Util::XmlWriterSettings;

namespace SimPT_Parex {


ServerOverview::ServerOverview(std::string workspace)
    : m_server_table(nullptr), m_workspace_path(workspace)
{
	setWindowTitle("Parameter Exploration - Server List");
    setMinimumSize(450, 320);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);

    // Actions
    QAction* action_add_server = new QAction("&Add/Load Server...", this);
    QAction* action_ping = new QAction("&Ping Server(s)", this);
    QAction* action_delete = new QAction("&Delete Server(s)", this);
    action_delete->setEnabled(false);

    action_add_server->setShortcut(QKeySequence("Ctrl+C"));
    action_ping->setShortcut(QKeySequence("Ctrl+P"));

    // Toolbar
    QToolBar* toolbar = new QToolBar(this);
    toolbar->setContentsMargins(5, 5, 5, 5);
    toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    toolbar->setMovable(false);
    toolbar->addAction(action_add_server);
    toolbar->addAction(action_ping);
    toolbar->addAction(action_delete);
    layout->addWidget(toolbar);

    connect(action_ping, &QAction::triggered, [this](){Ping();});
    connect(action_add_server, &QAction::triggered, [this](){ShowAddServer();});
    connect(action_delete, &QAction::triggered, [this](){DeleteSelection();});

    QVBoxLayout* content = new QVBoxLayout();
    content->setContentsMargins(10,0,10,10);

    m_server_table = new QTableWidget(this);
    connect(m_server_table, &QTableWidget::itemSelectionChanged,
            [this, action_delete]() {
                    action_delete->setEnabled( m_server_table->selectionModel()->hasSelection() );
            });

    QHBoxLayout* button_row = new QHBoxLayout;
    QPushButton* close_button = new QPushButton("Close");

    connect(close_button, &QPushButton::clicked, [this](){
        emit BackupRequested();
        hide();
    });

    button_row->addWidget(close_button);

    content->addWidget(m_server_table);
    content->addLayout(button_row);

    layout->addLayout(content);
    setLayout(layout);

    ShowServerOverview();
    RefreshButtons();
}

ServerOverview::~ServerOverview()
{
    delete m_server_table;
}

std::vector<ServerInfo*> ServerOverview::GetConnectedServers()
{
    std::vector<ServerInfo*> connected_servers;
    for (unsigned int i = 0; i < m_server_pings.size(); i++){
        if (m_server_pings[i] == 0)
            connected_servers.push_back(m_servers[i]);
    }
    return connected_servers;
}

std::vector<ServerInfo*> ServerOverview::GetCheckedServers()
{
    std::vector<ServerInfo*> task_server_pairs;
    for (unsigned int i = 0; i < m_servers.size(); i++) {
        auto item = m_server_table->item(i,0);
        if ((item != 0) and (item->checkState() == Qt::Checked)) {
            task_server_pairs.push_back(m_servers[i]);
        }
    }
    return task_server_pairs;
}

ServerInfo* ServerOverview::GetParkingServer()
{
    for (unsigned int i = 0; i < m_servers.size(); i++) {
        QWidget* cell = m_server_table->cellWidget(i, 3);
        if (QRadioButton* radio = qobject_cast<QRadioButton*>(cell)){
            if(radio->isChecked())
                return m_servers[i];
        }
    }
    return nullptr;
}

bool ServerOverview::CanStartExploration()
{
    if(GetCheckedServers().size() == 0){
        QMessageBox::critical(this, "Exploration Error", "No valid servers selected.", QMessageBox::Abort);
        return 0;
    } else if (GetParkingServer() == nullptr){
        int ans = QMessageBox::warning(this, "No Parking Location",
                             "No parking location selected, results will have to be retreived manually, continue?",
                             QMessageBox::Ok | QMessageBox::Cancel);

        if (ans == QMessageBox::Cancel)
            return 0;
    }

    return 1;
}

void ServerOverview::FromPtree(boost::property_tree::ptree input)
{
  try {
      int size = input.get<int>("total");

      for (int i = 0; i < size; i++) {
          string temp = "server" + to_string(i);
          m_servers.push_back(
                  new ServerInfo(
                  input.get<string>(temp + ".name"),
                  input.get<string>(temp + ".address"),
                  input.get<int>(temp + ".port")
          ));
      }
  }
  catch (boost::property_tree::ptree_error &e){}
}

void ServerOverview::ShowAddServer()
{
    ServerDialog* dialog = new ServerDialog(m_workspace_path);
    if (dialog->exec() == QDialog::Accepted) {
        ServerInfo* server = dialog->GetServer();
        // Check if the server was previously added
        for(auto info_ptr : m_servers){
            if(info_ptr->GetAddress() == server->GetAddress() and info_ptr->GetPort() == server->GetPort()){
                return;
            }
        }
        // If not, append to the server list and update the UI
        m_servers.push_back(server);
        // the ping will check if the server is available and update the server table
        Ping();
    }
}

void ServerOverview::ShowServerOverview()
{
    QStringList header;
    header << "Name" << "IP" << "Ping Status" << "Parking";

    m_server_table->setRowCount(0);
    m_server_table->setColumnCount(4);
    m_server_table->setHorizontalHeaderLabels(header);
    //m_server_table->horizontalHeader()->setResizeMode(0, QHeaderView::Stretch);
    m_server_table->verticalHeader()->hide();
    m_server_table->setShowGrid(true);

    for (unsigned int i = 0; i < m_servers.size(); i++) {
        int row = m_server_table->rowCount();
        m_server_table->insertRow(row);

        int j = 0;

        QTableWidgetItem* first  = new QTableWidgetItem(m_servers[i]->GetName());
        QTableWidgetItem* second = new QTableWidgetItem(QString(m_servers[i]->GetAddress() + ":%1").arg(m_servers[i]->GetPort()));

        first->setFlags(first->flags() ^ Qt::ItemIsEditable);
        second->setFlags(second->flags() ^ Qt::ItemIsEditable);

        bool isReachable = false;
        QString ping_status("UNCHECKED");
        if(i < m_server_pings.size()){
            if(m_server_pings[i] == 0){
                isReachable = true;
                ping_status = "AVAILABLE";
            } else
            if(m_server_pings[i] == 1){
                ping_status = "BUSY";
            } else
            if(m_server_pings[i] == -1){
                ping_status = "UNREACHABLE";
            }
        }

        QTableWidgetItem* ping_status_cell = new QTableWidgetItem(ping_status);
        ping_status_cell->setFlags(ping_status_cell->flags() ^ Qt::ItemIsEditable);

        QTableWidgetItem* radio_parking = new QTableWidgetItem("");
        radio_parking->setFlags(radio_parking->flags() ^ Qt::ItemIsEditable);

        m_server_table->setItem(i, j++, first  );
        m_server_table->setItem(i, j++, second );
        m_server_table->setItem(i, j++, ping_status_cell);
        m_server_table->setItem(i, j, radio_parking);

        if(isReachable){
            QRadioButton* radio_btn = new QRadioButton();
            first->setCheckState(Qt::Unchecked);
            m_server_table->setCellWidget(i, j, radio_btn);
        }
    }
    m_server_table->resizeColumnsToContents();
    m_server_table->horizontalHeader()->setStretchLastSection(true);
}

void ServerOverview::RefreshButtons()
{
    int available = 0;
    for(int status : m_server_pings){
        if(status == 0) available++;
    }
}

void ServerOverview::Ping()
{
    m_server_pings.clear();

    if(m_servers.size() > 0){
        for(unsigned int i = 0; i < m_servers.size(); i++){
            m_server_pings.push_back(-1);
            ServerInfo* server = m_servers[i];

            ClientProtocol::GetSession(server, [i, this](ClientProtocol& client)
            {
                connect(&client, &ClientProtocol::PingReceived, [i, this, &client](int result){
                    SimPT_Logging::ParexLogger::get()->debug("{} Received ping inside lambda {} for {}", LOG_TAG_21, result, i);
                    m_server_pings[i] = result;
                    ShowServerOverview();

                    if(result == 0)
                        RefreshButtons();

                    disconnect(&client, &ClientProtocol::PingReceived, 0, 0);
                    emit ListUpdated();
                });

                client.Ping();
            });
        }
    }
    emit ListUpdated();
    ShowServerOverview();
}

void ServerOverview::DeleteSelection()
{
    int selectedRow = 0;

    while (m_server_table->selectionModel()->hasSelection())
    {
        selectedRow = m_server_table->selectionModel()->selectedIndexes().last().row();
        m_server_table->removeRow(selectedRow);

        ClientProtocol::GetSession(m_servers[selectedRow], [this](ClientProtocol& connection){
            emit connection.Ended();
        });

        m_servers.erase(m_servers.begin() + selectedRow);

    }
    Ping();
    ShowServerOverview();
}

boost::property_tree::ptree ServerOverview::ToPtree() const {
  ptree output;
  int i = 0;
  for (auto server : m_servers) {
      string temp = "server";
      temp.append(to_string(i));
      output.add(temp + ".name", server->GetName().toStdString());
      output.add(temp + ".address", server->GetAddress().toStdString());
      output.add(temp + ".port", server->GetPort());
      i++;
  }
  output.add("total", i);
  return output;
}

void ServerOverview::showEvent(QShowEvent* e)
{
    Ping();
}

} // namespace SimPT_Parex
