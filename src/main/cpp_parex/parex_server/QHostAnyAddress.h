#ifndef SIMPT_PAREX_QHOST_ANY_ADDRESS_H_
#define SIMPT_PAREX_QHOST_ANY_ADDRESS_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Hack for QT 4 -> Qt 5 transition
 */

#include <QHostAddress>

namespace SimPT_Parex {

inline QHostAddress::SpecialAddress QHostAnyAddress()
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
	return QHostAddress::Any;
#else
	return QHostAddress::AnyIPv4;
#endif
}

} // namespace

#endif // end-of-include-guard
