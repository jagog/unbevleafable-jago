#ifndef SIMPT_PAREX_SERVER_H_
#define SIMPT_PAREX_SERVER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Server
 */

#include <QTcpServer>
#include <memory>

namespace SimPT_Parex {

class ExplorationManager;

/**
 * Server class accepting TCP connections and setting up the server structures
 */
class Server : public QTcpServer
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param	minimumNodes	The minimal number of nodes this server should have
	 * @param	port		The port the server should listen on
	 * @param	parent		The QObject parent
	 */
	Server(int minimumNodes, int port = 8888, QObject *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~Server();

	/**
	 *	Get the number of tasks assigned to this server for a given exploration
	 *	(used in testing)
	 */
	 int GetNumberOfAssignedTasks(std::string expl_name);

private slots:
	void HandleConnection();

private:
	std::shared_ptr<ExplorationManager> m_exploration_manager;
};

} // namespace

#endif // end-of-include-guard
