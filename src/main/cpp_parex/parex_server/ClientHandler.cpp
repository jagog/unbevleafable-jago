/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of ClientHandler
 */

#include "ClientHandler.h"

#include "ExplorationManager.h"
#include "parex_protocol/Exploration.h"
#include "parex_protocol/ExplorationProgress.h"
#include "parex_protocol/ServerClientProtocol.h"
#include <cpp_simptshell/workspace/WorkspaceFactory.h>
#include <cpp_simshell/workspace/IWorkspace.h>
#include <QString>

#include <QTcpSocket>
#include <iostream>
#include <cpp_logging/logger.h>

#include <boost/filesystem.hpp>

#include <cstdlib>

class QTcpSocket;

namespace SimPT_Parex {

class ExplorationManager;
using namespace std;

ClientHandler::ClientHandler(QTcpSocket* socket,
		const std::shared_ptr<ExplorationManager>& explorationManager, QObject* parent)
	: QObject(parent), m_protocol(nullptr), m_exploration_manager(explorationManager)
{
	assert(socket && "socket cannot be a nullptr");
	assert(explorationManager.get() && "explorationManager cannot be a nullptr");

	m_protocol = new ServerClientProtocol(socket, parent);

	connect(m_protocol, SIGNAL(Ended()), this, SLOT(deleteLater()));
	connect(m_protocol, SIGNAL(Error(const std::string&)),
			this, SLOT(DisplayError(const std::string&)));

	// Connects for registering received Explorations
	connect(m_protocol, SIGNAL(ExplorationReceived(const Exploration*, std::vector<int>, std::string)),
			this, SLOT(RegisterExploration(const Exploration*, std::vector<int>, std::string)));
	connect(m_exploration_manager.get(), SIGNAL(Updated()), this, SLOT(Refresh()));

	connect(m_protocol, SIGNAL(DeleteExploration(const std::string&)),
			this, SLOT(DeleteExploration(const std::string&)));
	connect(m_protocol, SIGNAL(ExplorationNamesRequested()),
			this, SLOT(SendExplorationNames()));
	connect(m_protocol, SIGNAL(Subscribe(const std::string&)),
			this, SLOT(Subscribe(const std::string&)));
	connect(m_protocol, SIGNAL(Unsubscribe(const std::string&)),
			this, SLOT(Unsubscribe(const std::string&)));
	connect(m_protocol, SIGNAL(StopTask(const std::string&, int)),
			m_exploration_manager.get(), SLOT(StopTask(const std::string&, int)));
	connect(m_protocol, SIGNAL(RestartTask(const std::string&, int)),
			m_exploration_manager.get(), SLOT(RestartTask(const std::string&, int)));
	connect(m_protocol, SIGNAL(Ping()), this, SLOT(Ping()));

	connect(m_protocol, SIGNAL(RequestExplorationStatus(const std::string&)),
			this, SLOT(RequestExplorationStatus(const std::string&)));

	connect(m_protocol, SIGNAL(UpdateParkingWorkspace(const Exploration*, const std::string)),
			this, SLOT(UpdateParkingWorkspace(const Exploration*, const std::string)));
}

void ClientHandler::DisplayError(const std::string& error) const
{
	SimPT_Logging::ParexLogger::get()->error("{} ClientHandler Error: {}", LOG_TAG_6, error);
}

void ClientHandler::DeleteExploration(const std::string& name)
{
	Unsubscribe(name);
	m_exploration_manager->DeleteExploration(name);
}

void ClientHandler::Refresh()
{
	if (m_subscribed_exploration != "") {
		const ExplorationProgress *progress
			= m_exploration_manager->GetExplorationProgress(m_subscribed_exploration);
		if (progress) {
			CacheStatus(m_subscribed_exploration, progress);
			m_protocol->SendStatus(*progress);
		}
	} else {
		m_protocol->SendStatusDeleted("");
	}
}

CachedExplorationProgress ClientHandler::CacheStatus(std::string name, const ExplorationProgress* status){
	if(status != nullptr){
		int done = status->GetTaskCount(TaskState::Finished);
		int total = status->GetTotalTaskCount();
		std::vector<int> error;
		bool all_parked = true;

		for (unsigned int i = 0; i < status->GetTotalTaskCount(); i++) {
			if (status->GetTask(i).GetState() == TaskState::Failed) {
				error.push_back(i);
			}
			if (!status->GetTask(i).getParkingStatus())
				all_parked = false;

		}
		m_status_cache[name] = CachedExplorationProgress(error, total, done, all_parked);
		return m_status_cache[name];
	}
	return CachedExplorationProgress();
}


void ClientHandler::RegisterExploration(const Exploration* exploration, std::vector<int> tasks, std::string parking)
{
	SimPT_Logging::ParexLogger::get()->debug("{} RegisterExploration", LOG_TAG_6);
	m_exploration_manager->RegisterExploration(exploration, tasks, parking);
	Subscribe(exploration->GetName());
	SimPT_Logging::ParexLogger::get()->debug("{} RegisterExploration [DONE]", LOG_TAG_6);
}

void ClientHandler::SendExplorationNames()
{
	SimPT_Logging::ParexLogger::get()->debug("{} Sending exploration names to client {}", LOG_TAG_6, m_exploration_manager->GetExplorationNames().size());
	m_protocol->SendExplorationNames(m_exploration_manager->GetExplorationNames());
}

void ClientHandler::Subscribe(const std::string& name)
{
	m_subscribed_exploration = name;
	Refresh();
}

void ClientHandler::Unsubscribe(const std::string& name)
{
	if (m_subscribed_exploration == name){
		m_subscribed_exploration = "";
	}
	Refresh();
}

void ClientHandler::Ping()
{
	/* Always send 0 for now TODO: meaningful status */
	m_protocol->SendPing(0, "~/parex-results/");
}

void ClientHandler::RequestExplorationStatus(const std::string& name)
{
	const ExplorationProgress* progress = m_exploration_manager->GetExplorationProgress(name);
	if(progress){
		auto status = CacheStatus(name, progress);
		m_protocol->SendExplorationStatus(status.errors, status.total, status.done, status.parked);
	}else if(m_status_cache.find(name) != m_status_cache.end()){
		auto status = m_status_cache[name];
		m_protocol->SendExplorationStatus(status.errors, status.total, status.done, status.parked);
	}else{
		SimPT_Logging::ParexLogger::get()->debug("{} exploration with name [{}] was not found", LOG_TAG_6, name.c_str());
	}
}

void ClientHandler::UpdateParkingWorkspace(const Exploration* exploration, const std::string parking)
{
	SimPT_Shell::Ws::WorkspaceFactory f;

	std::string parking_dir 	= exploration->GetSaveDirectory();
	std::string index_file		= parking_dir + ".simPT-workspace.xml";

	if (parking_dir.at(0) == '~') {
		std::string home_env = std::getenv("HOME");
		parking_dir.erase(0, 1);
		parking_dir = home_env + parking_dir;
	}

	if (!boost::filesystem::exists(parking_dir) || !boost::filesystem::is_directory(parking_dir)) {
		try {
			boost::filesystem::create_directories(parking_dir);
		} catch (...) {
			SimPT_Logging::ParexLogger::get()->debug("{} Could not create parking directory {} --> workspace will not be initialized.", LOG_TAG_6, parking_dir.c_str());
			return;
		}
	}

	if (!boost::filesystem::exists(index_file) || !boost::filesystem::is_regular_file(index_file)) {
		try {
			f.InitWorkspace(parking_dir);
		} catch(...) {
			SimPT_Logging::ParexLogger::get()->debug("{} Error Updating Parex Workspace", LOG_TAG_6);
			return;
		}
	}

	shared_ptr<SimShell::Ws::IWorkspace> ws = f.CreateCliWorkspace(parking_dir);
	std::string project_folder = exploration->GetName().substr(0, exploration->GetName().size() -24 );

	ws->New("project", "parex_result_" + project_folder);

	SimPT_Logging::ParexLogger::get()->debug("{} Updated Parex Workspace.", LOG_TAG_6);
}

} // namespace
