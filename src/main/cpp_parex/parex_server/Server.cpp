/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Server
 */

#include "Server.h"

#include "ClientHandler.h"
#include "ExplorationManager.h"
#include "QHostAnyAddress.h"
#include "WorkerPool.h"
#include <cpp_logging/logger.h>

#include <iostream>
#include <string>

namespace SimPT_Parex {

class Exploration;

Server::Server(int minimumNodes, int port, QObject *parent)
	: QTcpServer(parent), m_exploration_manager(std::make_shared<ExplorationManager>())
{
	SimPT_Logging::ParexLogger::get()->debug("{} Starting server...\n", LOG_TAG_23);

	WorkerPool::globalInstance()->SetMinNumWorkers(minimumNodes);

	connect(this, SIGNAL(newConnection()), this, SLOT(HandleConnection()));

	if (!listen(QHostAnyAddress(), port)) {
		SimPT_Logging::ParexLogger::get()->debug("{} Error in set-up listening\n", LOG_TAG_23);
	}

	SimPT_Logging::ParexLogger::get()->debug("{} Server is listening on address {} on port {}\n", LOG_TAG_23, serverAddress().toString().toStdString(), serverPort());
}

Server::~Server()
{
	close();
}

void Server::HandleConnection()
{
	SimPT_Logging::ParexLogger::get()->debug("{} Incoming client connection detected\n", LOG_TAG_23);

	QTcpSocket *socket = nextPendingConnection();
	if (socket) {
		new ClientHandler(socket, m_exploration_manager, this); // QObject parent will clean up this client handler
	}
}

int Server::GetNumberOfAssignedTasks(std::string expl_name)
{
	return m_exploration_manager->GetNumberOfAssignedTasks(expl_name);
}


} // namespace
