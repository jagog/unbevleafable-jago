/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for WorkerRepresentative
 */

#include "WorkerRepresentative.h"
#include <cpp_logging/logger.h>
#include <QTimer>


namespace SimPT_Parex {

class SimResult;
using namespace std;

WorkerRepresentative::WorkerRepresentative(const QHostAddress& addr, quint16 port)
	: m_addr(new QHostAddress(addr)), m_port(port), m_socket(nullptr),
	  m_protocol(nullptr), m_currentTaskId(-1), m_currentTaskName("")
{
}

WorkerRepresentative::WorkerRepresentative(const QHostAddress& addr,
			quint16 port, int taskId, std::string taskName)
	: m_addr(new QHostAddress(addr)), m_port(port), m_socket(nullptr),
	  m_protocol(nullptr), m_currentTaskId(taskId), m_currentTaskName(taskName)
{
}

WorkerRepresentative::~WorkerRepresentative()
{
}

void WorkerRepresentative::Connected()
{
	if (m_currentTaskId != -1) {
		m_protocol = new ServerNodeProtocol(m_socket, this);
		connect(m_protocol, SIGNAL(Error(const std::string&)),
				this, SLOT(DisplayError(const std::string&)));
		connect(m_protocol, SIGNAL(ResultReceived(const SimResult&)),
				this, SLOT(Finished(const SimResult&)));
	} else {
		emit ReadyToWork();
	}
}

void WorkerRepresentative::Delete(const std::string name)
{
	m_protocol->Delete(name);
	Init();
}

void WorkerRepresentative::DisplayError(const std::string& error) const
{
	SimPT_Logging::ParexLogger::get()->error("{} Error: \n", LOG_TAG_24, error);
}

void WorkerRepresentative::DisplayError(QAbstractSocket::SocketError error) const
{
	SimPT_Logging::ParexLogger::get()->error("{} Socket Error: \n", LOG_TAG_24, error);
}

void WorkerRepresentative::DoWork(const SimTask &task)
{
	Init();
	m_currentTaskId = task.GetId();
	m_currentTaskName = task.GetExploration();
	m_protocol->SendTask(task);
}

void WorkerRepresentative::Finished(const SimResult& res)
{
	emit FinishedWork(res);

	//wait untill all events are processed
	QTimer::singleShot(0, this, SIGNAL(ReadyToWork()));
}

std::string WorkerRepresentative::GetExplName() const
{
	return m_currentTaskName;
}

const QHostAddress* WorkerRepresentative::GetSenderAddress() const
{
	return m_addr;
}

int WorkerRepresentative::GetSenderPort() const
{
	return m_port;
}

int WorkerRepresentative::GetTaskId() const
{
	return m_currentTaskId;
}

void WorkerRepresentative::Init()
{
	if (!m_protocol) {
		m_protocol = new ServerNodeProtocol(m_socket, this);
		connect(m_protocol, SIGNAL(Error(const std::string&)),
				this, SLOT(DisplayError(const std::string&)));
		connect(m_protocol, SIGNAL(ResultReceived(const SimResult&)),
				this, SLOT(Finished(const SimResult&)));
	}
}

void WorkerRepresentative::Setup()
{
	m_socket = new QTcpSocket();
	m_socket->connectToHost(*m_addr, m_port);

	connect(m_socket, SIGNAL(connected()), this, SLOT(Connected()));
	connect(m_socket, SIGNAL(disconnected()), this, SIGNAL(Disconnected()));

	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)),
			this, SLOT(DisplayError(QAbstractSocket::SocketError)));

}

void WorkerRepresentative::StopTask()
{
	Init();

	m_protocol->StopTask();
}

} // namespace
