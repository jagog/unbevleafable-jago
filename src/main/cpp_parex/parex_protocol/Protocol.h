#ifndef SIMPT_PAREX_PROTOCOL_H_
#define SIMPT_PAREX_PROTOCOL_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Protocol.
 */

#include <QObject>
#include <boost/property_tree/ptree_fwd.hpp>

class QTcpSocket;

namespace SimPT_Parex {

class Connection;
class ServerInfo;

/**
 * Base class for the XML/ptree protocols between client, server and node.
 */
class Protocol : public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 * @param	socket		Use socket for the connection
	 * @param	parent		The QObject parent
	 */
	Protocol(QTcpSocket* socket, QObject* parent = nullptr);

	/**
	 * Constructor
	 * @param	server	Use ServerInfo to connect to server
	 * @param	parent		The QObject parent
	 */
	Protocol(ServerInfo* server, QObject *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~Protocol();

	/**
	 * Check if still connected
	 */
	bool IsConnected();

signals:
	/// Signal emitted when the protocol has ended and the connection is closed
	void Ended();

	/// Signal emitted when an error occured
	void Error(const std::string &error);

protected:
	/// Sends a ptree over the connection
	void SendPtree(const boost::property_tree::ptree& pt);

	/// Method called when a ptree was received over the connection
	virtual void ReceivePtree(const boost::property_tree::ptree& pt) = 0;

	///
	std::string GetClientIP() const;

	///
	int GetClientPort() const ;

private slots:
	/// Handles an incomming message
	void Receive(const QByteArray& message);

private:
	Connection*  m_connection;
};

} // namespace

#endif // end-of-include-guard
