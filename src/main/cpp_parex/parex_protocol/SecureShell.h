#ifndef SECURESHELL_H_
#define SECURESHELL_H_

/**
 * @file
 * Interface for SecureShell
 */
#include <cstdio>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include <QObject>
#include <QString>
#include <QDir>

#include <libssh/libssh.h>
#include <libssh/sftp.h>

namespace SimPT_Parex{

class ServerInfo;

/**
 * Class for establishing a secure shell for file transfers on a server or client.
 */
class SecureShell: public QObject
{
    Q_OBJECT
public:
    /**
     * Constructor.
     */
    SecureShell();

    /**
     * Destructor.
     */
    ~SecureShell();

    /**
     * Connect to an SSH server.
     * @param server    An object containing server info used for the connection.
     */
    void Connect(ServerInfo* server);

    /**
     * Disconnect from an ssh server.
     */
    void Disconnect();

    /**
     * Check if ssh section is succesfully connected.
     */
    bool IsConnected();

    /**
     * Send a command via ssh.
     * @param command   The command to be sent via ssh.
     * @return true if sending succeeded, false if sending failed.
     */
    bool SendCommand(const std::string& command);

    /**
     * Return the response of the last SendCommand call.
     */
    QString GetLastResponse() { return m_last_command_response; }

    /**
     * Setup an sftp session and cause file transmission.
     * it is utilises SftpSetup() and SftpCopy to achieve this goal.
     * @param   source          File path of file to be sent.
     * @param   destination     File path of file destination.
     * @return  an integer with the SFTP/SSH error code.
     */
    int SendFile(const std::string& source, const std::string& destination);

    /**
     * Setup an sftp session and cause a recursive directory transmission
     * it utilises the SftpSetup(), SftpCopy and SftpDirCopy functions.
     * @param   source          Directory path of source folder.
     * @param   destination     Directory path of destination folder.
     * @param   hiddendst       Directory path of folder above destionation folder
     * @param   hiddensrc       Directory path of folder above destionation folder
     * @return  an integer with the SFTP/SSH status code
     */
    int SendDirectory(const std::string& source, std::string& destination, const std::string& hiddenfiles, const std::string& hiddenfilesdst);


private:
    /**
     * Set up an sftp session using an existing ssh session.
     * @return  the resulting sftp status code.
     */
    int SftpSetup();

    /**
     * Copy a file via an existing sftp session.
     * @return  the resulting sftp status code.
     */
    int SftpCopy(const std::string& source, const std::string& destination);

    /**
     * Copy a directory recursively via an existsing sftp session
     * @return  the resulting sftp status code.
     */
    int SftpDirCopy(const std::string& source, std::string& destination);

    /**
     * Setup an sftp session and cause a transmission of various .xml files needed for top-level project in workspace
     * it utilises the SftpSetup(), SftpCopy and SftpDirCopy functions.
     * @param   source          Directory path of source folder.
     * @param   destination     Directory path of destination folder.
     * @return  an integer with the SFTP/SSH status code
     */
    int SendIndex(const std::string& source, const std::string& destination);


    ssh_session     m_ssh_session;
    sftp_session    m_sftp_session;
    int             m_connect_state;
    int             m_auth_state;
    QString         m_last_command_response;
};

} //namespace

#endif //end-of-include-guard
