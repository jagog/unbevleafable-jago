/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Connection.
 */

#include "Connection.h"

#include <QThread>
#include <QHostAddress>
#include <QDataStream>
#include <cassert>

namespace SimPT_Parex {

#define QDATASTREAM_VERSION QDataStream::Qt_4_7 // Ensure compatibility of data conversions

Connection::Connection(QTcpSocket *socket, QObject *parent)
	: QObject(parent), m_socket(socket), m_size(0)
{
	assert(m_socket);
	assert(m_socket->state() == QAbstractSocket::ConnectedState);

	m_socket->setParent(nullptr);

	connect(m_socket, SIGNAL(readyRead()), this, SLOT(ReadMessage()));
	connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(HandleError(QAbstractSocket::SocketError)));
}

Connection::~Connection()
{
	if (IsConnected())
		m_socket->deleteLater();
}

bool Connection::IsConnected()
{
	return (m_socket != nullptr &&
		m_socket->state() == QAbstractSocket::ConnectedState);
}

std::string Connection::GetPeerAddress() const {
	return m_socket->peerAddress().toString().toStdString();
}

int Connection::GetPeerPort() const {
	return m_socket->peerPort();
}

void Connection::SendMessage(const QByteArray &message)
{
	assert(m_socket->state() == QAbstractSocket::ConnectedState);

	QDataStream dataStream(m_socket);
	dataStream.setVersion(QDATASTREAM_VERSION);

	dataStream << static_cast<quint32>(message.size()); // Size of the message, to send individual messages over a TCP stream
	m_socket->write(message);
}

void Connection::ReadMessage()
{
	// Tries to read messages every time a new segment of the TCP stream arrives
	// First reads the size (if enough bytes available on the stream)
	// Then reads the number of bytes indicated by that stream, if available
	// The size is stored between calls in m_size until the whole message arrives

	while (m_socket->bytesAvailable() >= qint64(sizeof(quint32))) {
		while (m_size == 0) {
			if (m_socket->bytesAvailable() < qint64(sizeof(quint32)))
				return;
			QDataStream dataStream(m_socket);
			dataStream.setVersion(QDATASTREAM_VERSION);
			dataStream >> m_size;
		}

		if (m_socket->bytesAvailable() < m_size)
			return;

		QByteArray message = m_socket->read(m_size);
		m_size = 0;

		emit ReceivedMessage(message);
	}
}

void Connection::HandleError(QAbstractSocket::SocketError socketError)
{
	if (socketError == QAbstractSocket::RemoteHostClosedError) {
		emit ConnectionClosed();
	} else {
		emit Error(m_socket->errorString().toStdString());
		m_socket->abort();
	}
}

} // namespace
