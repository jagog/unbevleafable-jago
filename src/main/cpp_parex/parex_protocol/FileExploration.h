#ifndef SIMPT_PAREX_FILE_EXPLORATION_H_
#define SIMPT_PAREX_FILE_EXPLORATION_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for FileExploration.
 */

#include "Exploration.h"

#include <utility>

namespace SimPT_Parex {

/**
 * Class describing a file-based exploration and its simulation tasks.
 */
class FileExploration : public Exploration
{
public:
	/**
	 * Constructor.
	 *
	 * @param	name		The name of the exploration.
	 * @param	files		The file names grouped with their contents.
	 * @param	preferences	The (workspace) preferences of the exploration.
	 */
	FileExploration(const std::string& name, const std::vector<std::pair<std::string, boost::property_tree::ptree>>& files, const boost::property_tree::ptree& preferences);

	/**
	 * Constructor.
	 * Constructs a new exploration with the given pt.
	 * See ToPtree() for more information about the format of such a tree.
	 */
	FileExploration(const boost::property_tree::ptree& pt);

	/**
	 * Copy constructor.
	 */
	FileExploration(const FileExploration& other);

	/**
	 * Assignment operator.
	 */
	FileExploration &operator=(const FileExploration& other);

	/**
	 * Destructor.
	 */
	virtual ~FileExploration() {};

	/**
	 * Clone function.
	 */
	virtual FileExploration* Clone() const;

	/**
	 * Returns all the parameters in the exploration.
	 */
	virtual std::vector<std::string> GetParameters() const;

	/**
	 * Return the values of a certain task.
	 * The index of a value associated with a parameter in the vector is the same as the index of this parameter.
	 * @param	index		The task index.
	 */
	virtual std::vector<std::string> GetValues(unsigned int index) const;

	/**
	 * Returns the number of tasks the exploration currently contains.
	 */
	virtual unsigned int GetNumberOfTasks() const;

	/**
	 * Creates a task with the parameters on the given index.
	 * This index is based on the Cartesian product of the exploration parameters.
	 */
	virtual SimTask* CreateTask(unsigned int index) const;


	/**
	 * Convert the exploration to a ptree.
	 *
	 * The format of an exploration ptree is as follows:
	 *	<name>m_name</name>
	 *	<preferences>m_preferences</preferences> (== the format of the workspace preferences)
	 *	<files>
	 *		<file> (for every n in m_file_names (in order))
	 *			<name>n</name>
	 *			<contents>
	 *				m_file_contents[n]
	 *			</contents>
	 *		</file>
	 *		[...]
	 *	</files>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

private:
	/**
	 * Convert the given ptree to an exploration.
	 * Note that only the datamembers for this class will be read, not the Exploration-class.
	 * @throws	ptree_bad_path		When the parameter doesn't exist.
	 */
	virtual void ReadPtree(const boost::property_tree::ptree& pt);

private:
	std::vector<std::string>                    m_file_names;
	std::vector<boost::property_tree::ptree>    m_file_contents;
};

} // namespace

#endif // end-of-include-guard
