#ifndef SIMPT_PAREX_SERVER_NODE_PROTOCOL_H_
#define SIMPT_PAREX_SERVER_NODE_PROTOCOL_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ServerNodeProtocol.
 */

#include "Protocol.h"
#include "SimTask.h"
#include "SimResult.h"

#include <QObject>
#include <QTcpSocket>
#include <QByteArray>

namespace SimPT_Parex {

/**
 * Protocol at the server side to communicate with the node.
 */
class ServerNodeProtocol: public Protocol
{
	Q_OBJECT
public:
	///
	ServerNodeProtocol(QTcpSocket* socket, QObject* parent = 0);

	///
	virtual ~ServerNodeProtocol();

public slots:
	/// Sends a task to the server
	void SendTask(const SimTask& task);

	/// Sends an acknowledgment of a received result to the server
	void SendAck();

	/// Orders the node to stop its current task
	void StopTask();

	/// Orders the node to delete all files it has for a certain exploration
	void Delete(const std::string& name);

signals:
	/// Signals emitted when a sim result was received
	void ResultReceived(const SimResult& result);

protected:
	/// Receive a ptree message (implements of Protocol::ReceivePtree).
	virtual void ReceivePtree(const boost::property_tree::ptree& reader);
};

} // namespace

#endif // end-of-include-guard
