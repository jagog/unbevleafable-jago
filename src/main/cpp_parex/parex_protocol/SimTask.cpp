/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SimTask.
 */

#include <boost/property_tree/xml_parser.hpp>
#include "SimTask.h"

namespace SimPT_Parex {

using boost::property_tree::ptree;

SimTask::SimTask(): m_task_id(0)
{
}

SimTask::SimTask(int id, std::string tree, std::string name)
	: m_task_id(id), m_name(name)
{
	std::stringstream s;
	s << tree;
	read_xml(s, m_simulation);
}

SimTask::SimTask(int id, std::string tree, std::string workspace, std::string name)
	: m_task_id(id), m_name(name)
{
	std::stringstream s;
	s << tree;
	read_xml(s, m_simulation);

	std::stringstream w;
	w << workspace;
	read_xml(w, m_workspace);
}

SimTask::SimTask(int id, const ptree& simulation, std::string name)
	: m_task_id(id), m_name(name), m_simulation(simulation)
{
}

SimTask::SimTask(int id, const ptree& simulation, const ptree& workspace, std::string name)
	: m_task_id(id), m_name(name), m_simulation(simulation), m_workspace(workspace)
{
}

SimTask::SimTask(const SimTask& st)
	: QObject(), m_task_id(st.m_task_id), m_name(st.m_name), m_simulation(st.m_simulation),
	  m_workspace(st.m_workspace)
{
}

SimTask::~SimTask()
{
}

std::string SimTask::GetExploration() const
{
        return m_name;
}

int SimTask::GetId() const
{
        return m_task_id;
}

boost::property_tree::ptree SimTask::GetWorkspace() const
{
	return m_workspace;
}

ptree SimTask::ToPtree() const
{
	return m_simulation;
}

std::string SimTask::ToString() const
{
	std::stringstream s;
	write_xml(s, m_simulation);
	return s.str();
}

std::string SimTask::WorkspaceToString() const
{
	std::stringstream s;
	write_xml(s, m_workspace);
	return s.str();
}

} // namespace
