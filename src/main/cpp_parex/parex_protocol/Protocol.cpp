/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Protocol.
 */

#include "Protocol.h"

#include "Connection.h"
#include "ServerInfo.h"

#include <QTcpSocket>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

using boost::property_tree::ptree;

namespace SimPT_Parex {

Protocol::Protocol(QTcpSocket* socket, QObject *parent)
	: QObject(parent), m_connection(new Connection(socket))
{
	m_connection->setParent(this);
	connect(m_connection, SIGNAL(ReceivedMessage(const QByteArray&)), this, SLOT(Receive(const QByteArray&)));
	connect(m_connection, SIGNAL(ConnectionClosed()), this, SIGNAL(Ended()));
	connect(m_connection, SIGNAL(Error(const std::string&)), this, SIGNAL(Error(const std::string&)));
	connect(m_connection, SIGNAL(Error(const std::string&)), this, SIGNAL(Ended()));
}

Protocol::Protocol(ServerInfo* server, QObject *parent)
	: QObject(parent)
{
	QTcpSocket *socket = new QTcpSocket();
	socket->connectToHost(server->GetAddress(), server->GetPort());
	if (socket->waitForConnected(2000)) {
		m_connection = new Connection(socket, this);

		connect(m_connection, SIGNAL(ReceivedMessage(const QByteArray&)), this, SLOT(Receive(const QByteArray&)));
		connect(m_connection, SIGNAL(ConnectionClosed()), this, SIGNAL(Ended()));
		connect(m_connection, SIGNAL(Error(const std::string&)), this, SIGNAL(Error(const std::string&)));
		connect(m_connection, SIGNAL(Error(const std::string&)), this, SIGNAL(Ended()));
	}
	else {
		m_connection = nullptr;
	}
}

Protocol::~Protocol()
{
}

std::string Protocol::GetClientIP() const {
	return m_connection->GetPeerAddress();
}

int Protocol::GetClientPort() const {
	return m_connection->GetPeerPort();
}

bool Protocol::IsConnected()
{
	return (m_connection != nullptr && m_connection->IsConnected());
}

void Protocol::SendPtree(const ptree &pt)
{
	std::ostringstream stream;
	write_xml(stream, pt);
	QByteArray message(stream.str().data(), stream.str().size());
	m_connection->SendMessage(message);
}

void Protocol::Receive(const QByteArray &message)
{
	std::istringstream s(message.data());
	ptree pt;
	read_xml(s, pt); // TODO More error handling, for example of the possible exceptions of this call?
	emit ReceivePtree(pt);
}

} // namespace
