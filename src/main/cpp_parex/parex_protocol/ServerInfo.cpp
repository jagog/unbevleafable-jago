/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ServerInfo
 */

#include "ServerInfo.h"

namespace SimPT_Parex {

ServerInfo::ServerInfo(QString name, QString address, int port):
	m_name(name), m_address(address), m_port(port)
{
}

ServerInfo::ServerInfo(std::string name, std::string address, int port):
	m_name(QString(name.c_str())), m_address(QString(address.c_str())), m_port(port)
{
}

ServerInfo::~ServerInfo()
{
}

void ServerInfo::Update(QString address, int port)
{
	m_address = address;
	m_port = port;
}

QString ServerInfo::GetName()
{
	return m_name;
}

QString ServerInfo::GetAddress()
{
	return m_address;
}

int ServerInfo::GetPort()
{
	return m_port;
}

boost::property_tree::ptree ServerInfo::ToPtree() const
{
	boost::property_tree::ptree pt;
	pt.put("name", m_name.toUtf8().constData());
	pt.put("address", m_address.toUtf8().constData());
	pt.put("port", std::to_string(m_port));
	return pt;
}

} // namespace
