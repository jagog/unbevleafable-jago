#include "SecureShell.h"
#include "ServerInfo.h"
#include <assert.h>
#include <QtCore/qdiriterator.h>
#include <vector>
#include <iostream>

#include <cpp_logging/logger.h>

namespace SimPT_Parex {

SecureShell::SecureShell()
    : m_ssh_session(nullptr), m_sftp_session(nullptr), m_connect_state(-1), m_auth_state(-1)
{}

SecureShell::~SecureShell()
{
    if (m_connect_state == SSH_OK)
        ssh_disconnect(m_ssh_session);
    if (m_ssh_session != NULL)
        ssh_free(m_ssh_session);
}

void SecureShell::Connect(ServerInfo* server)
{
    if(IsConnected())
        return;

    m_ssh_session = ssh_new();
    if (m_ssh_session == NULL) {
        SimPT_Logging::ParexLogger::get()->error("{} Could not create ssh session", LOG_TAG_17);
        return;
    }


    const char*     host = server->GetAddress().toStdString().c_str();
    int             port = server->GetPort();
    unsigned int    log  = 0;

    ssh_options_set( m_ssh_session, SSH_OPTIONS_HOST,          host );
    ssh_options_set( m_ssh_session, SSH_OPTIONS_PORT,          &port);
    ssh_options_set( m_ssh_session, SSH_OPTIONS_LOG_VERBOSITY, &log);

    m_connect_state = ssh_connect( m_ssh_session );
    if (m_connect_state != SSH_OK) {
        ssh_free(m_ssh_session);
        m_ssh_session = NULL;
        SimPT_Logging::ParexLogger::get()->error("{} Connection to {}:{} failed", LOG_TAG_17, host, port);
        return;
    }

    m_auth_state = ssh_userauth_autopubkey(m_ssh_session, NULL);
    if ( m_auth_state != SSH_AUTH_SUCCESS ) {
        ssh_disconnect( m_ssh_session );
        ssh_free( m_ssh_session );
        m_ssh_session = NULL;
        SimPT_Logging::ParexLogger::get()->error("{} Authentication with {}:{} failed", LOG_TAG_17, host, port);
        return;
    }
}

void SecureShell::Disconnect()
{
    if(!IsConnected())
        return;

    ssh_disconnect(m_ssh_session);
    ssh_free(m_ssh_session);

    m_ssh_session   =   nullptr;
    m_connect_state =   -1;
    m_auth_state    =   -1;
}


bool SecureShell::IsConnected()
{
    return m_ssh_session    != NULL &&
           m_connect_state  == SSH_OK &&
           m_auth_state     == SSH_AUTH_SUCCESS;
}


bool SecureShell::SendCommand(const std::string& command)
{
    if (!IsConnected())
        return false;

    ssh_channel channel;
    int status;
    int nbytes;
    char buffer[256];
    memset(buffer, '\0', sizeof(buffer));

    channel = ssh_channel_new(m_ssh_session);
    if (channel == NULL){
        SimPT_Logging::ParexLogger::get()->error("{} Channel request failed", LOG_TAG_17);
        return false;
    }


    status = ssh_channel_open_session(channel);
    if (status != SSH_OK) {
        SimPT_Logging::ParexLogger::get()->error("{} Channel open failed", LOG_TAG_17);
        ssh_channel_free(channel);
        return false;
    }

    status = ssh_channel_request_exec(channel, command.c_str());
    if (status != SSH_OK) {
        SimPT_Logging::ParexLogger::get()->error("{} Execution of command '{}' failed", LOG_TAG_17, command.c_str());
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        return false;
    }

    nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
    while (nbytes > 0)
    {
        if (write(1, buffer, nbytes) != (unsigned int) nbytes)
        {
            ssh_channel_close(channel);
            ssh_channel_free(channel);
            return false;
        }
        nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
    }
    m_last_command_response = QString(buffer);

    if (nbytes < 0) {
        SimPT_Logging::ParexLogger::get()->error("{} Error during channel read", LOG_TAG_17);
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        return false;
    }

    ssh_channel_send_eof(channel);
    ssh_channel_close(channel);
    ssh_channel_free(channel);
    return true;
}

int SecureShell::SendFile(const std::string& source, const std::string& destination)
{
    if (!IsConnected() || m_sftp_session != NULL){
        SimPT_Logging::ParexLogger::get()->error("{} SendFile called but not connected.", LOG_TAG_17);
        return false;
    }

    int rc;

    rc = SftpSetup();
    if (rc != SSH_OK){
        SimPT_Logging::ParexLogger::get()->error("{} The SFTP setup failed with error code {}", LOG_TAG_17, rc);
        return false;
    }



    rc = SftpCopy(source, destination);
    if (rc != SSH_OK){
        SimPT_Logging::ParexLogger::get()->error("{} Copying the file {} failed with error code {}", LOG_TAG_17, source.c_str(), rc);
        return false;
    }

    if (m_sftp_session != NULL){
        sftp_free(m_sftp_session);
        m_sftp_session = NULL;
    }

    return rc;
}

int SecureShell::SendDirectory(const std::string& source, std::string& destination, const std::string& hiddenfiles, const std::string& hiddenfilesdst)
{
    if(!IsConnected() || m_sftp_session != NULL){
        SimPT_Logging::ParexLogger::get()->error("{} SendDirectory called but not connected.", LOG_TAG_17);
        return -1;
    }

    int rc = SftpSetup();
    if (rc != SSH_OK){
        SimPT_Logging::ParexLogger::get()->error("{} The SFTP setup failed with error code {}", LOG_TAG_17, rc);
        return rc;
    }

    rc = SftpDirCopy(source, destination);
    if (rc != SSH_OK){
        SimPT_Logging::ParexLogger::get()->error("{} Copy of directory {} failed with error code {}", LOG_TAG_17, source.c_str(), rc);
        return rc;
    }

    rc = SendIndex(hiddenfiles, hiddenfilesdst);

    if(m_sftp_session != NULL)
        sftp_free(m_sftp_session);

    return rc;
}

int SecureShell::SftpSetup()
{
    int rc;

    m_sftp_session = sftp_new(m_ssh_session);
    if (m_sftp_session == NULL)
        return SSH_ERROR;

    rc = sftp_init(m_sftp_session);
    if (rc != SSH_OK) {
        sftp_free(m_sftp_session);
        m_sftp_session = NULL;
        return rc;
    }

    return SSH_OK;
}

int SecureShell::SftpCopy(const std::string &source, const std::string &destination)
{
    assert(m_sftp_session != NULL);

    int rc;
    char buf[BUFSIZ];
    ssize_t size;

    int source_file = open(source.c_str(), O_RDONLY);

    if (source_file == -1) {
        sftp_free(m_sftp_session);
        m_sftp_session = NULL;
        SimPT_Logging::ParexLogger::get()->error("{} could not open file {}", LOG_TAG_17, source.c_str());
        return SSH_ERROR;
    }

    sftp_file destination_file = sftp_open(m_sftp_session, destination.c_str(), O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);

    if (destination_file == NULL) {
        SimPT_Logging::ParexLogger::get()->error("{} could not open destination file {}", LOG_TAG_17, destination.c_str());
        sftp_free(m_sftp_session);
        m_sftp_session = NULL;
        return SSH_ERROR;
    }

    while ((size = read(source_file, buf, BUFSIZ)) > 0) {
        sftp_write(destination_file, buf, size);
    }

    close(source_file);
    rc = sftp_close(destination_file);
    return rc;
}

int SecureShell::SftpDirCopy(const std::string& source, std::string& destination)
{
    if(destination.size() && destination.back() != '/')
        destination += "/";

    std::string command = "mkdir -p " + destination;
    SendCommand(command);

    QDirIterator dit(source.c_str(), QDir::Files | QDir::NoDotAndDotDot | QDir::Hidden | QDir::NoSymLinks, QDirIterator::Subdirectories);

    while (dit.hasNext())
    {
        std::string source_filename = dit.next().toStdString();
        std::string destination_filename = destination + dit.fileName().toStdString();

        int rc = SftpCopy(source_filename , destination_filename);
        if( rc != SSH_OK )
            SimPT_Logging::ParexLogger::get()->error("{} An error occurred while copying the file {}, error code: {}", LOG_TAG_17, source_filename.c_str(), rc);
    }

    return 0;
}

int SecureShell::SendIndex(const std::string& source, const std::string& destination)
{
    QDir dir(QString::fromStdString(source));

    if (!(dir.exists(".simPT-workspace.xml") && dir.exists(".simPT-gui-preferences.xml") && dir.exists(".simPT-cli-preferences.xml"))) {
        SimPT_Logging::ParexLogger::get()->error("{} An error occurred trying to copy the index, one or more of the preference files / workspace.xml file is missing.", LOG_TAG_17);
    }

    int rc   = 0;
    rc      += SftpCopy(dir.absoluteFilePath(".simPT-workspace.xml").toStdString()          , destination + ".simPT-workspace.xml" );
    rc      += SftpCopy(dir.absoluteFilePath(".simPT-gui-preferences.xml").toStdString()    , destination + ".simPT-gui-preferences.xml" );
    rc      += SftpCopy(dir.absoluteFilePath(".simPT-cli-preferences.xml").toStdString()    , destination + ".simPT-cli-preferences.xml" );

    if( rc != SSH_OK ) {
        SimPT_Logging::ParexLogger::get()->error("{} An error occurred while copying the cli-pref, gui-pref and workspace.xml files from directory {}, error code: {}", LOG_TAG_17, source.c_str(), rc);
    }

    return rc;

}
} //namespace
