/*
 *  This file is part of the simPT_Shell 2 (a.k.a. vleaf2) software.
 *  simPT_Shell 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  simPT_Shell 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with simPT_Shell 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2011, 2012 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Implementation of ClientProtocol
 *
 * @author	Coding with(out) Discipline
 */

#include "ClientProtocol.h"
#include <iostream>
#include "Exploration.h"
#include "ExplorationProgress.h"
#include "FileExploration.h"
#include "ParameterExploration.h"
#include "ServerInfo.h"
#include "util/misc/StringUtils.h"

#include <boost/property_tree/ptree.hpp>

using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Parex {

std::map<std::string, std::shared_ptr<ClientProtocol> > ClientProtocol::m_connection_map = std::map<std::string, std::shared_ptr<ClientProtocol> >();

ClientProtocol::ClientProtocol(ServerInfo* server, QObject *parent)
	: Protocol(server, parent)
{
}

bool ClientProtocol::SendExploration(const Exploration *exploration,
	const std::string& parking_ip, std::vector<int> tasks, const std::string& results_dir)
{
	if (!IsConnected())
		return false;

	std::string csv_task = "";
	for(unsigned int i = 0; i < tasks.size(); i++){
		int task = tasks[i];
		csv_task += to_string(task);
		if(i < tasks.size() - 1)
			csv_task += ",";
	}

	ptree writer;

	writer.put("Exploration.metadata.parking", parking_ip);
	writer.put("Exploration.metadata.tasks", csv_task);
	writer.put("Exploration.metadata.resultsdir", results_dir);

	if (dynamic_cast<const ParameterExploration*>(exploration) != nullptr) {
		writer.put_child("Exploration.parameter_exploration", exploration->ToPtree());
	} else if (dynamic_cast<const FileExploration*>(exploration) != nullptr) {
		writer.put_child("Exploration.file_exploration", exploration->ToPtree());
	} else {
		assert("Exploration type unknown.");
	}

	SendPtree(writer);
	return true;
}

bool ClientProtocol::DeleteExploration(const std::string &name)
{
	if (!IsConnected()) {
		return false;
	}

	ptree writer;
	writer.put("Control.Delete", name);

	SendPtree(writer);
	return true;
}

bool ClientProtocol::RequestExplorationNames()
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.RequestExplorations", "Explorations");

	SendPtree(writer);
	return true;
}

bool ClientProtocol::SubscribeUpdates(const std::string &explorationName)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Subscribe", explorationName);

	SendPtree(writer);
	return true;
}

bool ClientProtocol::UnsubscribeUpdates(const std::string &explorationName)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Unsubscribe", explorationName);

	SendPtree(writer);
	return true;
}

bool ClientProtocol::StopTask(const std::string &name, int task)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Stop.name", name);
	writer.put("Control.Stop.id", to_string(task));

	SendPtree(writer);
	return true;
}

bool ClientProtocol::RestartTask(const std::string &name, int task)
{
	if (!IsConnected())
		return false;

	ptree writer;
	writer.put("Control.Start.name", name);
	writer.put("Control.Start.id", to_string(task));

	SendPtree(writer);
	return true;
}

bool ClientProtocol::Ping(){
	if(!IsConnected()) return false;

	ptree writer;
	writer.put("Control.Ping", "");

	SendPtree(writer);
	return true;
}

bool ClientProtocol::GetExplorationProgress(const std::string& exploration_name){
	if(!IsConnected()) return false;

	ptree writer;
	writer.put("Control.GetExplorationProgress", exploration_name);

	SendPtree(writer);
	return true;
}

/*
void ClientProtocol::RegisterCallback(const std::shared_ptr<ResponseCallback> callback){
	m_callbacks.push_back(callback);
}
*/

void ClientProtocol::ReceivePtree(const ptree &reader)
{
	if (reader.find("Control") != reader.not_found()) {
		//for(auto callback : m_callbacks)
		//	callback->Parse(reader);

		ptree control_pt = reader.get_child("Control");

		if (control_pt.find("ExplorationNames") != control_pt.not_found()) {
			std::vector<std::string> names;
			int size = control_pt.get<int>("ExplorationNames.size");
			for (int i = 0; i < size; ++i) {
				names.push_back(control_pt.get<std::string>(
					"ExplorationNames.exploration" + to_string(i) + ".name"));
			}

			emit ExplorationNames(names);
		} else if (control_pt.find("Status") != control_pt.not_found()) {
			ptree status_pt = control_pt.get_child("Status");
			if (status_pt.find("deleted") != status_pt.not_found()) {
				emit ExplorationDeleted();
			} else {
				emit ExplorationStatus(
					new ExplorationProgress(status_pt.get_child("exploration_progress")));
			}
		} else if (control_pt.find("Ping") != control_pt.not_found()){
			int status_code = control_pt.get<int>("Ping.status");
			emit PingReceived(status_code);
		} else if (control_pt.find("SendExplorationProgress") != control_pt.not_found())
		{
			try {
				int done  = control_pt.get<int>("SendExplorationProgress.Tasks.Ready");
				int total = control_pt.get<int>("SendExplorationProgress.Tasks.Total");
				std::string error_csv  = control_pt.get<std::string>("SendExplorationProgress.Tasks.Error");
				bool parked = control_pt.get("SendExplorationProgress.Tasks.Parked", false);

				std::vector<std::string> strs;
				std::vector<int> error;
				if(error_csv.size() > 0){
					boost::split(strs, error_csv, boost::is_any_of(","));
					for(std::string s : strs) error.push_back(atoi(s.c_str()));
				}

				emit ExplorationStatusReceived(total, done, error, parked);
			} catch (...){}
		}
	}
}

void ClientProtocol::GetSession(ServerInfo* server, std::function<void(ClientProtocol&)> callback){

	std::string ServerID = server->GetAddress().toUtf8().constData() + std::to_string(server->GetPort());
	std::shared_ptr<ClientProtocol> client = nullptr;

	if ( m_connection_map.find(ServerID) == m_connection_map.end() ) {
		client = std::make_shared<ClientProtocol>(server);
		m_connection_map.insert( std::make_pair(ServerID, client) );

		if (!client->IsConnected()) {
			m_connection_map.erase(ServerID);
		} else {
			auto disconnected = [ServerID](){
				m_connection_map.erase(ServerID);
			};
			connect(client.get(), &ClientProtocol::Ended, disconnected);
			callback(*client);
		}
	}
	else {
		client = m_connection_map.find(ServerID)->second;

		if (!client->IsConnected()) {
			m_connection_map.erase(ServerID);
		} else {
			callback(*client);
		}
	}
}

void ClientProtocol::GetFullExplorationName(ServerInfo* server, std::string short_name, std::function<void(std::string)> callback){
    GetFullExplorationNames(server, {short_name}, [callback](std::vector<std::string> names){
    	if(names.size() > 0) callback(names[0]);
    });
}

void ClientProtocol::GetFullExplorationNames(ServerInfo* server, std::vector<std::string> short_names, std::function<void(std::vector<std::string>)> callback)
{
    ClientProtocol::GetSession(server, [&callback, short_names](ClientProtocol& client)
    {
        connect(&client, &ClientProtocol::ExplorationNames, [&client, callback, short_names](std::vector<std::string> names){
            disconnect(&client, &ClientProtocol::ExplorationNames, 0, 0);

            std::vector<std::string> full_names;
            for(auto short_name : short_names){
	            QString compare = QString(short_name.c_str());
	            for(std::string expl_name : names){
	                QString qs = QString(expl_name.c_str());
	                if(qs.startsWith(compare)){
	                    full_names.push_back(expl_name);
	                    break;
	                }
	            }
            }
            callback(full_names);
        });
        client.RequestExplorationNames();
    });
}

} // namespace simPT_Shell
