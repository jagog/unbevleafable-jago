#ifndef SIMPT_PAREX_RANGE_SWEEP_H_
#define SIMPT_PAREX_RANGE_SWEEP_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for RangeSweep.
 */

#include "ISweep.h"

namespace SimPT_Parex {

/**
 * A sweep over a range of numerical parameters.
 */
class RangeSweep : public ISweep
{
public:
	/**
	 * Constructor.
	 * @param	from		The value the sweep starts from.
	 * @param	to		The value the sweep ends.
	 * @param	step		The step size of the sweep.
	 */
	RangeSweep(double from, double to, double step);

	/**
	 * Constructor.
	 * Contains the information for a range sweep.
	 * See ToPtree() for more information about the format.
	 * @throws	ptree_bad_data		When the ptree doesn't have the correct data.
	 * @throws	ptree_bad_path		When the ptree doesn't have the correct format.
	 */
	RangeSweep(const boost::property_tree::ptree& pt);

	/**
	 *
	 */
	virtual ~RangeSweep() {}

	/**
	 * Polymorphic copy constructor
	 */
	virtual RangeSweep *Clone() const { return new RangeSweep(*this); }

	/**
	 * Gets the from value of the range
	 */
	double GetFrom() const { return m_from; }

	/**
	 * Gets the to value of the range
	 */
	double GetTo() const { return m_to; }

	/**
	 * Gets the step value of the range
	 */
	double GetStep() const { return m_step; }


	virtual unsigned int GetNumberOfValues() const;

	/**
	 * Returns the value on the given index.
	 * The index is the position of the value if the range is written down from 'm_from' to 'm_to'.
	 */
	virtual std::string GetValue(unsigned int index) const;

	/**
	 * Convert the range sweep to a ptree.
	 *
	 * The format of a range sweep ptree is as follows:
	 *	<range>
	 *		<from>m_from</from>
	 *		<to>m_to</to>
	 *		<step>m_step</step>
	 *	</range>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

private:
	/**
	 * Convert the given ptree to a range sweep.
	 * @throws	ptree_bad_data		When the ptree doesn't have the correct data.
	 * @throws	ptree_bad_path		When the ptree doesn't have the correct format.
	 */
	void ReadPtree(const boost::property_tree::ptree& pt);

private:
	double m_from;
	double m_to;
	double m_step;
};

} // namespace

#endif // end-of-include-guard
