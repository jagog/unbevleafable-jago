#ifndef SIMPT_PAREX_LIST_SWEEP_H_
#define SIMPT_PAREX_LIST_SWEEP_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ListSweep.
 */

#include "ISweep.h"

namespace SimPT_Parex {

/**
 * A sweep over a list of parameters.
 */
class ListSweep : public ISweep
{
public:
	/// Constructor.
	ListSweep(const std::vector<std::string>& values);

	/// Constructor.
	ListSweep(const boost::property_tree::ptree& pt);

	///
	virtual ~ListSweep() {}

	/// Polymorphic copy constructor
	virtual ListSweep *Clone() const { return new ListSweep(*this); }

	///
	virtual unsigned int GetNumberOfValues() const;

	///
	virtual std::string GetValue(unsigned int index) const;

	/**
	 * Convert the list sweep to a ptree.
	 *
	 * The format of a list sweep ptree is as follows:
	 *	<list>
	 *		<value>v</value> for every v in m_values
	 *	</list>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

private:
	/**
	 * Convert the given ptree to a list sweep.
	 * @throws	ptree_bad_path		When the ptree doesn't have the correct format.
	 */
	void ReadPtree(const boost::property_tree::ptree& pt);

private:
	std::vector<std::string> m_values;
};

} // namespace

#endif // end-of-include-guard
