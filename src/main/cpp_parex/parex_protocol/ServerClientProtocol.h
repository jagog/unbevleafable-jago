#ifndef SIMPT_PAREX_SERVER_CLIENT_PROTOCOL_H_
#define SIMPT_PAREX_SERVER_CLIENT_PROTOCOL_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ServerClientProtocol
 */

#include "Protocol.h"
#include <vector>

class QTcpSocket;

namespace SimPT_Parex {

class Exploration;
class ExplorationProgress;
class SimTask;

/**
 * ServerClient Protocol to deal with Client-Server communication (Server-side)
 */
class ServerClientProtocol: public Protocol
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 * @param socket	Connected socket to set-up the connection
	 */
	ServerClientProtocol(QTcpSocket* socket, QObject* parent = 0);

	/**
	 * Destructor
	 */
	virtual ~ServerClientProtocol();


	/**
	 * Acknowledgement for sent exploration
	 * @param	name	The name of the exploration
	 */
	void SendAck(const std::string& name);

	/**
	 * Send list of Exploration names
	 * @param	names		The names of the explorations
	 */
	void SendExplorationNames(const std::vector<std::string>& names);

	/**
	 * Send status for given exploration
	 * @param	status		The progress of the exploration to send to the client
	 */
	void SendStatus(const ExplorationProgress& status);

	/**
	 * Send a deleted status for the exploration with the specified name
	 * @param	name		The name of the deleted/nonexistent exploration
	 */
	void SendStatusDeleted(const std::string& name);

	/**
	 * Respond to a client ping with a status.
	 * @param	status	The availability of this server, 0 for available, 1 for unavailable.
	 */
	void SendPing(const int status, const std::string& resultsdir);

	/**
	 * Send status of exploration(s) to the client.
	 * @param
	 */
	void SendExplorationStatus(std::vector<int> error, int total, int done, bool parked);


signals:
	/**
	 * Emitted when an exploration is received
	 * @param	exploration	The exploration that is received
	 */
	void ExplorationReceived(const Exploration* exploration, std::vector<int> task_list, std::string parking_ip);

	/**
	 * Emitted when a request to stop/delete the given exploration is received
	 * @param	name		The name for the exploration to stop/delete
	 */
	void DeleteExploration(const std::string& name);

	/**
	 * Emitted when the names of the explorations are requested
	 */
	void ExplorationNamesRequested();

	/**
	 * Emitted when a request for subscription arrives
	 * @param	name		The exploration name to receive updates for
	 */
	void Subscribe(const std::string& name);

	/**
	 * Emitted when a request for unsubscription arrives
	 * @param	name		The exploration name to unsubscribe to
	 */
	void Unsubscribe(const std::string& name);

	/**
	 * Emitted when a request for stopping a task arrives
	 */
	void StopTask(const std::string& name, int id);

	/**
	 * Emitted when a request for stopping a task arrives
	 */
	void RestartTask(const std::string& name, int id);

	/**
	 * Emitted when a ping arrives
	 */
	void Ping();

	/**
	 * Emitted when the client requests exploration status with the given name.
	 * @param	name	The exploration name of which we want to receive status.
	 */
	void RequestExplorationStatus(const std::string& name);

	/**
	 * Emitted when this server is the parking server and we need to update /
	 * create the index file to create a workspace for the results
	 */
	void UpdateParkingWorkspace(const Exploration* exploration, const std::string parking_ip);

protected:
	/**
	 * Receive a ptree message
	 * Implementation of Protocol::ReceivePtree
	 * @param	reader	The received pt
	 */
	virtual void ReceivePtree(const boost::property_tree::ptree& reader);
};

} // namespace

#endif // end-of-include-guard
