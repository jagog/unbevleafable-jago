#ifndef SIMPT_PAREX_ISWEEP_H_
#define SIMPT_PAREX_ISWEEP_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface class ISweep.
 */

#include <boost/property_tree/ptree.hpp>
#include <string>
#include <vector>

namespace SimPT_Parex {

/**
 * An interface class for a parameter sweep.
 */
class ISweep
{
public:
	virtual ~ISweep() {}

	/// Polymorphic copy constructor
	virtual ISweep *Clone() const = 0;

	/// Returns the number of values in the sweep.
	virtual unsigned int GetNumberOfValues() const = 0;

	/// Returns the value on the given index.
	virtual std::string GetValue(unsigned int index) const = 0;

	/// Convert the given ptree to a sweep.
	virtual boost::property_tree::ptree ToPtree() const = 0;
};

} // namespace

#endif // end-of-include-guard
