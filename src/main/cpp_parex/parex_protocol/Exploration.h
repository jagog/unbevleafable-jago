#ifndef SIMPT_PAREX_EXPLORATION_H_
#define SIMPT_PAREX_EXPLORATION_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Exploration.
 */

#include "SimTask.h"

#include <boost/property_tree/ptree.hpp>
#include <string>
#include <vector>

namespace SimPT_Parex {

/**
 * Class describing a generic exploration.
 */
class Exploration
{
public:
	/**
	 * Constructor.
	 * @param	name		The name of the exploration.
	 * @param	preferences	The (workspace) preferences of the exploration.
	 */
	Exploration(const std::string& name, const boost::property_tree::ptree& preferences);

	/**
	 * Constructor.
	 * Constructs a new exploration with the given pt.
	 * See ToPtree() for more information about the format of such a tree.
	 */
	Exploration(const boost::property_tree::ptree& pt);

	/**
	 * Copy constructor.
	 */
	Exploration(const Exploration& other);

	/**
	 * Assignment operator.
	 */
	Exploration &operator=(const Exploration& other);

	/**
	 * Destructor.
	 */
	virtual ~Exploration() {};

	/**
	 * Clone function.
	 */
	virtual Exploration* Clone() const = 0;

	/**
	 * Changes the name of the exploration.
	 * @param	name		The new name of the exploration.
	 */
	void SetName(const std::string& name) { m_name = name; }

	/**
	 * Returns the name of the exploration.
	 */
	const std::string& GetName() const { return m_name; }

	/**
	 * Changes the save directory of the exploration on the remote server
	 * @param	name		The new name of the exploration.
	 */
	void SetSaveDirectory(const std::string& name) { m_save_dir = name; }

	/**
	 * Returns the save directory of the exploration on the remote server
	 */
	const std::string& GetSaveDirectory() const { return m_save_dir; }

	/**
	 * Returns the preferences of the exploration.
	 */
	const boost::property_tree::ptree& GetPreferences() const { return m_preferences; };

	/**
	 * Returns all the parameters in the exploration.
	 */
	virtual std::vector<std::string> GetParameters() const = 0;

	/**
	 * Return the values of a certain task.
	 * The index of a value associated with a parameter in the vector is the same as the index of this parameter.
	 * @param	index		The task index.
	 */
	virtual std::vector<std::string> GetValues(unsigned int index) const = 0;

	/**
	 * Returns the number of tasks the exploration currently contains.
	 */
	virtual unsigned int GetNumberOfTasks() const = 0;

	/**
	 * Creates a task with the parameters on the given index.
	 * This index is based on the Cartesian product of the exploration parameters.
	 */
	virtual SimTask* CreateTask(unsigned int index) const = 0;

	/**
	 * Convert the exploration to a ptree.
	 *
	 * The format of an exploration ptree is as follows:
	 * <name>m_name</name>
	 * <preferences>
	 * 	m_preferences (== the format of the workspace preferences)
	 * </preferences>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

private:
	/**
	 * Convert the given ptree to an exploration.
	 * @throws	ptree_bad_path		When the parameter doesn't exist.
	 */
	virtual void ReadPtree(const boost::property_tree::ptree& pt);

protected:
	std::string m_name;
	std::string m_save_dir;
	boost::property_tree::ptree m_preferences;
};

} // namespace

#endif // end-of-include-guard
