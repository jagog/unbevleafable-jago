/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ServerNodeProtocol
 */

#include "ServerNodeProtocol.h"

namespace SimPT_Parex {

using boost::property_tree::ptree;

ServerNodeProtocol::ServerNodeProtocol(QTcpSocket *socket, QObject *parent)
	: Protocol(socket, parent)
{
}

ServerNodeProtocol::~ServerNodeProtocol()
{
}

void ServerNodeProtocol::SendAck()
{
	ptree writer;
	writer.put("SimResult.Ack", "");
	SendPtree(writer);
}

void ServerNodeProtocol::SendTask(const SimTask &task)
{
	ptree writer;
	writer.put("SimTask.Id", task.GetId());
	writer.put("SimTask.Tree", task.ToString());
	writer.put("SimTask.Workspace", task.WorkspaceToString());
	writer.put("SimTask.Exploration", task.GetExploration());
	writer.put("SimTask.ParkingDir", task.GetParkingDirectory());
	writer.put("SimTask.ParkingIp", task.GetParkingIp());
	SendPtree(writer);
}

void ServerNodeProtocol::StopTask()
{
	ptree writer;
	writer.put("Control.Stop", "");
	SendPtree(writer);
}

void ServerNodeProtocol::Delete(const std::string &name)
{
	ptree writer;
	writer.put("Control.Delete", name);
	SendPtree(writer);
}

void ServerNodeProtocol::ReceivePtree(const boost::property_tree::ptree &reader)
{
	if (reader.find("SimResult") != reader.not_found()) {
		std::string exploration = reader.get<std::string>("SimResult.Exploration");
		int task_id = reader.get<int>("SimResult.Id");
		SimResult::ResultType success = static_cast<SimResult::ResultType>(reader.get<int>("SimResult.Success"));
		SimResult result(exploration, task_id, success, this->GetClientIP(), this->GetClientPort());

		result.setParkingStatus( reader.get("SimResult.Parked", false) );

		SendAck();

		emit ResultReceived(result);
	}
}

} // namespace
