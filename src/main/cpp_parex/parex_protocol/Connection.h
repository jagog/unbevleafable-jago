#ifndef SIMPT_PAREX_CONNECTION_H_
#define SIMPT_PAREX_CONNECTION_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Connection.
 */

#include <QObject>
#include <QTcpSocket>
#include <string>

namespace SimPT_Parex {

/**
 * Class managing a TCP stream of messages
 */
class Connection: public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param	socket		The connected TCP socket of this connection
	 * @param	parent		The parent of this QObject
	 */
	Connection(QTcpSocket *socket, QObject *parent = 0);

	/**
	 * Destructor, closes the connection
	 */
	virtual ~Connection();

	/**
	 * Check if still connected
	 */
	bool IsConnected();

	std::string GetPeerAddress() const;

	int GetPeerPort() const ;

public slots:
	/**
	 * Sends a message over the connection
	 *
	 * @param	message		The message to be sent
	 */
	void SendMessage(const QByteArray &message);

signals:
	/**
	 * Signal emitted when a message is received over the connection
	 *
	 * @param	message		The received message
	 */
	void ReceivedMessage(const QByteArray &message);

	/**
	 * Signal emitted when the remote host closed the connection
	 */
	void ConnectionClosed();

	/**
	 * Signal emitted when an error occurred in the connection
	 *
	 * @param	error		The error message
	 */
	void Error(const std::string &error);

private slots:
	/**
	 * Read incoming message
	 */
	void ReadMessage();

	/**
	 * Handle error
	 */
	void HandleError(QAbstractSocket::SocketError socketError);

private:
	/**
	 * Constructor
	 */
	Connection(const Connection&);
	Connection &operator=(const Connection&);

	QTcpSocket *m_socket;
	quint32 m_size;
};

} // namespace

#endif // end-of-include-guard
