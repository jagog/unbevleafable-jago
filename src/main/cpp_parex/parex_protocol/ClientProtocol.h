#ifndef SIMPT_PAREX_CLIENT_PROTOCOL_H_
#define SIMPT_PAREX_CLIENT_PROTOCOL_H_
/*
 *  This file is part of the simPT_Shell 2 (a.k.a. vleaf2) software.
 *  simPT_Shell 2 is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or any
 *  later version.
 *  simPT_Shell 2 is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with simPT_Shell 2. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Copyright 2013 Jan Broeckhove, UA/CoMP.
 */
/**
 * @file
 * Interface for ClientProtocol
 *
 * @author 	Coding with(out) Discipline
 */

#include <vector>
#include <QObject>
#include "Protocol.h"
#include <boost/algorithm/string.hpp>
//#include "ResponseCallbacks.h"
#include <functional>
#include <memory>

namespace SimPT_Parex {

class Exploration;
class ExplorationProgress;
class ServerInfo;

/**
 * Client side of the protocol
 */
class ClientProtocol: public Protocol
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param socket	The socket to connect through
	 */
	ClientProtocol(ServerInfo *server, QObject *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~ClientProtocol() {}

	/**
	 * Get a new client session for the specified server
	 */
	static void GetSession(ServerInfo* server, std::function<void(ClientProtocol&)> callback);

	/**
	 * Shorthand to get a full exploration name given a client side name
	 */
    static void GetFullExplorationName(ServerInfo* server, std::string short_name, std::function<void(std::string)> callback);

	/**
	 * Shorthand to get a full exploration names given a client side name list
	 */
	static void GetFullExplorationNames(ServerInfo* server, std::vector<std::string> short_names, std::function<void(std::vector<std::string>)> callback);

	/**
	 * Send exploration
	 *
	 * @param exploration		The Exploration to send
	 * @param parking_ip		The IP of the file storage server
	 * @param tasks				List of tasks the server should execute (= subset of sent exploration)
	 */
	bool SendExploration(const Exploration *exploration,
		const std::string& parking_ip, std::vector<int> tasks, const std::string& results_dir);

	/**
	 * Request a stop and delete of the given exploration
	 *
	 * @param name		The name of the exploration to stop and delete
	 */
	bool DeleteExploration(const std::string &name);

	/**
	 * Request a list of current Explorations
	 */
	bool RequestExplorationNames();

	/**
	 * Subscribe for regular updates
	 *
	 * @param exploration_name	The name of the exploration to subscribe to
	 */
	bool SubscribeUpdates(const std::string &explorationName);

	/**
	 * Unsubscribe for regular updates
	 *
	 * @param exploration_name 	The name of the exploration to unsubscribe
	 */
	bool UnsubscribeUpdates(const std::string &explorationName);

	/**
	 * Request to stop a task
	 */
	bool StopTask(const std::string &name, int task);

	/**
	 * Request to restart a task
	 */
	bool RestartTask(const std::string &name, int task);

	/**
	 * Check if the server is online and ready to work
	 */
	bool Ping();

	/**
	 * Check up on the status in the server. How many tasks are finished,
	 * how many have failed and how many it had to do in total.
	 */
	bool GetExplorationProgress(const std::string& exploration_name);

	//void RegisterCallback(const std::shared_ptr<ResponseCallback> callback);

signals:
	/**
	 * Signal emitted when exploration names are received
	 */
	void ExplorationNames(const std::vector<std::string> &names) const;

	/**
	 * Signal emitted when new status update is received
	 */
	void ExplorationStatus(const ExplorationProgress* exploration) const;

	/**
	 * Signal emitted when subscribed exploration has been deleted
	 */
	void ExplorationDeleted() const;

	/**
	 * Emitted when ping response is received
	 */
	void PingReceived(int code) const;

	/**
	 * Emitted when a exploration status update is received
	 */
	void ExplorationStatusReceived(int total, int done, std::vector<int> error, bool parked) const;



protected:
	/**
	 * Receive a ptree message
	 * Implementation of Protocol::ReceivePtree
	 *
	 * @param	reader	The received pt
	 */
	virtual void ReceivePtree(const boost::property_tree::ptree &reader);

private:
	static std::map<std::string, std::shared_ptr<ClientProtocol> > m_connection_map;
	//std::vector<std::shared_ptr<ResponseCallback>> m_callbacks;
};

} // namespace simPT_Shell

#endif // end-of-include-guard
