/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ParameterExploration.
 */

#include "ParameterExploration.h"

#include "Exploration.h"
#include "ListSweep.h"
#include "RangeSweep.h"
#include "SimTask.h"
#include "ptree/PTreeUtils.h"

#include <iostream>

using SimPT_Sim::Util::PTreeUtils;
using boost::property_tree::ptree;
using namespace std;

namespace SimPT_Parex {

ParameterExploration::ParameterExploration(const string& name,
	const ptree& original, const ptree& preferences)
	: Exploration(name, preferences), m_original(original)
{
}

ParameterExploration::ParameterExploration(const ptree& pt)
	: Exploration(pt)
{
	ReadPtree(pt);
}

ParameterExploration::ParameterExploration(const ParameterExploration &other)
	: Exploration(other.m_name, other.m_preferences), m_original(other.m_original)
{
	m_save_dir = other.m_save_dir;
	for (const auto& pair : other.m_sweeps) {
		m_sweeps.push_back(make_pair(pair.first, pair.second->Clone()));
	}
}

ParameterExploration &ParameterExploration::operator=(const ParameterExploration &other)
{
	if (this != &other) {
		this->Exploration::operator=(other);

		m_original = other.m_original;

		for (const auto &pair : m_sweeps) {
			delete pair.second;
		}
		m_sweeps.clear();

		for (const auto &pair : other.m_sweeps) {
			m_sweeps.push_back(make_pair(pair.first, pair.second->Clone()));
		}
	}

	return *this;
}

ParameterExploration::~ParameterExploration()
{
	for (const auto &pair : m_sweeps) {
		delete pair.second;
	}
}

ParameterExploration* ParameterExploration::Clone() const
{
	return new ParameterExploration(*this);
}

vector<string> ParameterExploration::GetParameters() const
{
	vector<string> parameters;
	for (auto pair : m_sweeps) {
		parameters.push_back(pair.first);
	}
	return parameters;
}

vector<string> ParameterExploration::GetValues(unsigned int index) const
{
	assert(index < GetNumberOfTasks() && "The given index doesn't exist.");

	vector<string> values(m_sweeps.size());

	unsigned int i = m_sweeps.size();
	for (auto it = m_sweeps.rbegin(); it != m_sweeps.rend(); it++) {
		ISweep* sweep = it->second;
		unsigned int nrOfValues = sweep->GetNumberOfValues();

		values[--i] = sweep->GetValue(index % nrOfValues);
		index /= nrOfValues;
	}

	return values;
}

unsigned int ParameterExploration::GetNumberOfTasks() const
{
	unsigned int tasks = 1;
	for (auto pair : m_sweeps) {
		tasks *= pair.second->GetNumberOfValues();
	}

	return tasks;
}

SimTask* ParameterExploration::CreateTask(unsigned int index) const
{
	assert(index < GetNumberOfTasks() && "The given index doesn't exist.");

	// Find the correct values per parameter.
	vector<string> parameterValues = GetValues(index);

	// Create the exploration tree.
	ptree pt(m_original);
	auto& parameter_pt = pt.get_child("vleaf2.parameters");
	auto value_it = parameterValues.begin();
	for (auto it = m_sweeps.begin(); it != m_sweeps.end(); it++) {
		PTreeUtils::PutIndexedChild(parameter_pt, it->first, ptree(*value_it));
		value_it++;
	}

	return new SimTask(index, pt, m_preferences, m_name);
}

vector<string> ParameterExploration::GetPossibleParameters() const
{
	vector<string> parameters;
	for (auto pair : PTreeUtils::Flatten(m_original.get_child("vleaf2.parameters"), true)) {
		parameters.push_back(pair.first);
	}
	return parameters;
}

string ParameterExploration::GetOriginalValue(const string& parameter) const
{
	return PTreeUtils::GetIndexedChild(m_original, "vleaf2.parameters." + parameter).data();
}

const ISweep* ParameterExploration::GetSweep(const string& parameter) const
{
	auto entry = find_if(m_sweeps.begin(), m_sweeps.end(),
		[parameter](const pair<string, ISweep*>& pair) {return pair.first == parameter;});
	if (entry != m_sweeps.end()) {
		return entry->second;
	} else {
		return nullptr;
	}
}

void ParameterExploration::SetSweep(const string& parameter, ISweep* sweep)
{
	assert(sweep != nullptr && "The sweep wasn't initialized.");
	assert(sweep->GetNumberOfValues() > 0 && "The sweep doesn't contain any value.");

	RemoveSweep(parameter);
	m_sweeps.push_back(make_pair(parameter, sweep));
}

void ParameterExploration::RemoveSweep(const string& parameter)
{
	auto entry = find_if(m_sweeps.begin(), m_sweeps.end(),
		[parameter](const pair<string, ISweep*>& pair) {return pair.first == parameter;});
	if (entry != m_sweeps.end()) {
		delete entry->second;
		m_sweeps.erase(entry);
	}
}

boost::property_tree::ptree ParameterExploration::ToPtree() const
{
	ptree pt = Exploration::ToPtree();
	pt.put_child("original", m_original);

	for (const auto& entry : m_sweeps) {
		ptree sweep_pt;
		sweep_pt.put("parameter", entry.first);
		for (auto pair : entry.second->ToPtree()) {
			sweep_pt.push_back(pair);
		}
		pt.add_child("sweeps.sweep", sweep_pt);
	}

	return pt;
}

void ParameterExploration::ReadPtree(const boost::property_tree::ptree& pt)
{
	m_original = pt.get_child("original");

	for (const auto& pair : m_sweeps) {
		delete pair.second;
	}
	m_sweeps.clear();

	const auto &sweeps_pt = pt.get_child_optional("sweeps");
	if (sweeps_pt) {
		for (const auto& pair : sweeps_pt.get()) {
			assert(pair.first == "sweep" && "Found ptree in sweeps with that isn't a sweep");
			if (pair.first != "sweep") {
				continue;
			}

			const auto &sweep_pt = pair.second;
			string parameter = sweep_pt.get<string>("parameter");

			if (sweep_pt.find("list") != sweep_pt.not_found()) {
				m_sweeps.push_back(make_pair(parameter, new ListSweep(sweep_pt)));
			}
			else if (sweep_pt.find("range") != sweep_pt.not_found()) {
				m_sweeps.push_back(make_pair(parameter, new RangeSweep(sweep_pt)));
			}
			else {
				assert(false && "The sweep must be a list or range sweep.");
			}
		}
	}
}

} // namespace
