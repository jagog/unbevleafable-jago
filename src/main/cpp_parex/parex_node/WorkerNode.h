#ifndef SIMPT_PAREX_WORKER_NODE_H_INCLUDED
#define SIMPT_PAREX_WORKER_NODE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for WorkerNode
 */

#include "NodeAdvertiser.h"
#include "parex_protocol/SimResult.h"

#include <QHostAddress>
#include <QTcpServer>
#include <queue>

class QThread;

namespace SimPT_Parex {

class NodeProtocol;
class SimTask;
class Simulator;

/**
 * Broadcasts location on the network, then listens
 * to incoming connections. The server then connects to this node and
 * sends it a job. After completing the job the worker sends the answer
 * back over the network.
 */
class WorkerNode: public QTcpServer
{
	Q_OBJECT
public:
	/**
	 * Constructor immediately starts trying to connect to a server
	 *
	 * @param    simulator   The simulator of the worker node (takes ownership)
	 * @param    verbose     Whether to print information to the standard output or not
	 * @param    parent      The QObject parent of this object
	 */
	WorkerNode(Simulator* simulator, bool verbose = true, QObject* parent = nullptr);

	/**
	 * Destroy the node, closing all open connections
	 */
	virtual ~WorkerNode();

signals:
	/// Emitted whenever a new task is ready to be simulated.
	void NewTask(const SimTask&);

private slots:
	void ConnectionReceived();
	void Delete(const std::string& name);
	void DisplayError(const std::string&) const;
	void FinishedWork(const SimResult&);
	void HandleError(QAbstractSocket::SocketError);
	void StartedWork(const SimTask* task);
	void StopTask();
	void ResultSent();

private:
	void StartAdvertiser();
	bool ParkResults();

private:
	Simulator*                  m_simulator;
	QThread*                    m_simulator_thread;
	NodeAdvertiser              m_advertiser;
	bool                        m_verbose;

	NodeProtocol*               m_protocol;
	const SimTask*              m_current_task;

	QHostAddress                m_last_address;
	std::queue<const SimTask*>  m_queue;
	std::queue<SimResult>       m_results;

private:
	// TODO: Make configurable with cmd line arguments
	static const int g_broadcast_port; ///< Port to send broadcasts to
};

} // namespace

#endif // end-of-include-guard
