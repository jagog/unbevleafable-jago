/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for NodeAdvertiser
 */

#include "NodeAdvertiser.h"

#include <QTimer>
#include <QUdpSocket>
#include <cpp_logging/logger.h>

#include <iostream>

using namespace std;

namespace SimPT_Parex {

const int NodeAdvertiser::g_initial_interval = 1000;
const int NodeAdvertiser::g_maximal_interval = 30 * 1000;

NodeAdvertiser::NodeAdvertiser(int port, bool verbose, QObject *parent)
	: QObject(parent), m_port(port), m_verbose(verbose),
	  m_resend_timer(new QTimer(this)), m_socket(new QUdpSocket(this))
{
	connect(m_resend_timer, SIGNAL(timeout()), this, SLOT(SendAdvertisement()));
}

NodeAdvertiser::~NodeAdvertiser()
{
	m_socket->close();
}

void NodeAdvertiser::BackoffTimer()
{
	if (m_resend_timer->interval() < g_maximal_interval) {
		int newInterval = m_resend_timer->interval() * 2;
		if (newInterval >= g_maximal_interval) {
			//when we are at the max interval, switch from multiple single-shot timers to one continuous timer
			newInterval = g_maximal_interval;
			m_resend_timer->setSingleShot(false);
		}
		m_resend_timer->start(newInterval);
	}
}

void NodeAdvertiser::SendAdvertisement()
{
	if (m_verbose) {
		SimPT_Logging::ParexLogger::get()->debug("{} Trying to find server, time passed since last tick: {} msec.", LOG_TAG_31, m_resend_timer->interval());
	}
	QByteArray datagram;
	if (m_exploration == "") {
		datagram = "Job Application" + QByteArray(" ") + QByteArray::number(m_server_port);
	} else {
		datagram = "Server Discovery" + QByteArray(" ")
		        + QByteArray::number(m_task_id) + QByteArray(" ")
		        + QByteArray::number(m_server_port) + QByteArray(" ")
		        + QByteArray(m_exploration.c_str());
	}
	m_socket->writeDatagram(datagram, QHostAddress::Broadcast, m_port);
	BackoffTimer();
}

void NodeAdvertiser::Start(int serverPort)
{
	m_resend_timer->stop();
	m_server_port = serverPort;
	m_exploration = "";
	m_resend_timer->setSingleShot(true);
	m_resend_timer->start(g_initial_interval);
}

void NodeAdvertiser::Start(int serverPort, const std::string &exploration, int taskId)
{
	m_resend_timer->stop();
	m_server_port = serverPort;
	m_exploration = exploration;
	m_task_id = taskId;
	m_resend_timer->setSingleShot(true);
	m_resend_timer->start(g_initial_interval);
}

void NodeAdvertiser::Stop()
{
	m_resend_timer->stop();
}

} // namespace
