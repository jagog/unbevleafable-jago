#ifndef SIMPT_PAREX_SIMULATOR_H_INCLUDED
#define SIMPT_PAREX_SIMULATOR_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for the Simulator.
 */

#include "parex_protocol/SimTask.h"
#include "parex_protocol/SimResult.h"

#include <QObject>
#include <memory>

namespace SimPT_Shell {
        class CliController;
}

namespace SimPT_Parex {

/**
 * Simulator handling requested simulation tasks.
 */
class Simulator: public QObject
{
	Q_OBJECT
public:
	/// Constructor
	Simulator();

	/// Destructor
	virtual ~Simulator();

  ///
  void SetTaskStopper (bool b);

public slots:
	/// Do the real task of solving a SimTask and saving the results as requested by that task.
	virtual void SolveTask(const SimTask & task);

	/// Stop the current task
	void StopTask();

	/// Delete a folder on the disk.
	/// @param name		Name of the exploration to delete
	void Delete(const std::string& name);

signals:
	/// Emitted when task is solved. If it was unsuccssful or interrupted a flag is set in SimResult.
	void TaskSolved(const SimResult &);

private:
	std::shared_ptr<SimPT_Shell::CliController>   m_controller;
	bool                                          m_stopped;
  bool                                          m_task_stopper;
};

} // namespace

#endif // end-of-include-guard
