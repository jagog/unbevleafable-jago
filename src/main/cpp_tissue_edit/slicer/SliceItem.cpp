/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SliceItem.
 */

#include "SliceItem.h"

#include "bio/Cell.h"
#include "bio/Edge.h"
#include "bio/NeighborNodes.h"
#include "bio/Node.h"
#include "util/container/circular_iterator.h"

#include <QLineF>
#include <utility>

class QColor;
class QGraphicsScene;
namespace SimPT_Shell { class EditControlLogic; }
namespace SimPT_Sim { class Wall; }

namespace SimPT_Editor {

using namespace SimPT_Sim;
using namespace SimPT_Sim::Container;

SliceItem::SliceItem(const QPolygonF& polygon, const QLineF& cut, std::shared_ptr<EditControlLogic> tissue, QGraphicsScene* scene)
		: QGraphicsPolygonItem(polygon), m_cut(cut), m_tissue(tissue), m_scene(scene)
{
	setZValue(0);
	setBrush(QColor(220,220,220,255));
	setAcceptHoverEvents(true);
	setFlag(GraphicsItemFlag::ItemIsSelectable);
}

SliceItem::~SliceItem() {}

QVariant SliceItem::itemChange(GraphicsItemChange change, const QVariant& value)
{
	if (change == QGraphicsItem::ItemSelectedHasChanged && isSelected()) {
		TruncateLeaf();
		emit Truncated();
	}
	return QGraphicsPolygonItem::itemChange(change, value);
}

void SliceItem::hoverEnterEvent(QGraphicsSceneHoverEvent* )
{
	setBrush(QColor(51, 102, 0, 255));
}

void SliceItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* )
{
	setBrush(QColor(220,220,220,255));
}

void SliceItem::TruncateLeaf()
{
	std::list<Cell*> cellsToBeDeleted;
	std::list<Cell*> cellsToBeSliced;
	auto mesh = m_tissue->GetMesh()->GetMesh();

	for (auto cell : m_tissue->GetCells()) {
		bool keep = false;
		bool fullyContained = true;
		for (Node* node : cell->GetNodes()) {
			QPointF point((*node)[0], (*node)[1]);
			if (polygon().containsPoint(point, Qt::OddEvenFill) || polygon().contains(point)) {
				keep = true;
			}
			else {
				fullyContained = false;
			}
		}
		if (!keep) {
			cellsToBeDeleted.push_back(cell);
		}
		else if (!fullyContained) {
			cellsToBeSliced.push_back(cell);
		}
	}

	std::vector<QPointF> corners = polygon().toStdVector();
	std::map<Cell*, std::list<Node*>> newNodes;
	std::map<Node*, std::list<NeighborNodes>> newOwners;
	for (auto cell : cellsToBeSliced) {
		newNodes.insert(std::make_pair(cell, std::list<Node*>()));
		auto node = make_const_circular(cell->GetNodes());
		do {
			Node* n1 = *node;
			Node* n2 = *next(node);
			QLineF line((*n1)[0], (*n1)[1], (*n2)[0], (*n2)[1]);
			QPointF intersection;

			if (m_cut.intersect(line, &intersection) == QLineF::BoundedIntersection) {
				if (find(newNodes[cell].begin(), newNodes[cell].end(), *node) == newNodes[cell].end()) {
					if (intersection == QPointF((*n1)[0], (*n1)[1])) {
						newNodes[cell].push_back(n1);
					}
					else if (intersection == QPointF((*n2)[0], (*n2)[1])) {
						newNodes[cell].push_back(n2);
					}
					else {
						auto owner = find_if(newOwners.begin(), newOwners.end(),
							[intersection](std::pair<Node*, std::list<NeighborNodes>> entry)
								{return intersection == QPointF((*entry.first)[0], (*entry.first)[1]);});
						if (owner == newOwners.end()) {
							Edge edge(n1, n2);
							assert(mesh->GetEdgeOwners(edge).size() > 0 && "Edge doesn't exist in the mesh.");

							auto newNode = mesh->BuildNode({{intersection.x(), intersection.y(), 0}}, mesh->IsAtBoundary(&edge));
							assert(newOwners.find(newNode) == newOwners.end() && "The created node already existed.");

							newOwners[newNode] = std::list<NeighborNodes>();
							for (NeighborNodes neighbor : mesh->GetEdgeOwners(edge)) {
								if (std::find_if(newOwners[newNode].begin(), newOwners[newNode].end(),
									[neighbor](NeighborNodes nb) {return nb.GetCell() == neighbor.GetCell();})
									== newOwners[newNode].end()) {

									newOwners[newNode].push_back(
										NeighborNodes(neighbor.GetCell(), n1, n2));
								}
							}
							newNodes[cell].push_back(newNode);
						}
						else {
							newNodes[cell].push_back(owner->first);
						}
					}
				}
			}
		} while (++node != cell->GetNodes().begin());
	}

	for (auto entry : newOwners) {
		for (auto nb : entry.second) {
			mesh->AddNodeToCell(nb.GetCell(), entry.first, nb.GetNb1(), nb.GetNb2());
		}
	}

	// Update node owning walls.
	std::list<Wall*> tmpWalls;
	for (auto cell : cellsToBeSliced) {
		for (Wall* w : cell->GetWalls()) {
			if (std::find(tmpWalls.begin(), tmpWalls.end(), w) == tmpWalls.end()) {
				tmpWalls.push_back(w);
				mesh->UpdateNodeOwningWalls(w);
			}
		}
	}


	// Lambda to compare two nodes by their distance to the first endpoint of the cut.
	auto cmp = [this](Node* p1, Node* p2) {
		return pow(m_cut.p1().x() - (*p1)[0], 2) + pow(m_cut.p1().y() - (*p1)[1], 2)
			< pow(m_cut.p1().x() - (*p2)[0], 2) + pow(m_cut.p1().y() - (*p2)[1], 2);
	};

	// Lambda to check whether a point is contained in the polygon that contained the point.
	auto contains_point = [this](const QPointF& p) {
		QPolygonF rect(QRectF(p.x() - g_accuracy / 4, p.y() - g_accuracy / 4, g_accuracy / 2, g_accuracy / 2));	//Lower-bound to get the diagonal less than g_accuracy.
		return (polygon().contains(p) || polygon().containsPoint(p, Qt::OddEvenFill) || polygon().intersected(rect).size() > 0);
	};

	//Split the cells and delete the parts outside the slice.
	auto it = newNodes.begin();
	while (it != newNodes.end()) {
		std::list<Cell*> currentCells({it->first});
		auto& nodes = it->second;
		if (nodes.size() > 1) {
			nodes.sort(cmp);
			auto current = nodes.begin();
			auto next = std::next(nodes.begin());
			while (next != nodes.end()) {
				auto currentCell = std::find_if(currentCells.begin(), currentCells.end(), [current, next](Cell* c){
						auto first = std::find(c->GetNodes().begin(), c->GetNodes().end(), *current);
						auto second = std::find(c->GetNodes().begin(), c->GetNodes().end(), *next);
						return (first != c->GetNodes().end() && second != c->GetNodes().end());
					});

				if (currentCell != currentCells.end()) {
					std::vector<Cell*> newCells = m_tissue->SplitCell(*currentCell, *current, *next);
					if (newCells.size() > 1) {
						assert(newCells.size() == 2 && "The cell should be split into two parts.");

						currentCells.erase(currentCell);
						currentCells.push_back(newCells.front());
						currentCells.push_back(newCells.back());

						current = nodes.erase(current);
						next++;
					}
					else {
						current++;
						next++;
					}
				}
				else { //When the points are in two different cells.
					current++;
					next++;
				}
			}
		}

		for (auto c : currentCells) {
			Node* nonCutNode = nullptr;
			for (auto n : c->GetNodes()) {
				if (std::find(nodes.begin(), nodes.end(), n) == nodes.end()) {
					nonCutNode = n;
					break;
				}
			}
			assert(nonCutNode != nullptr && "There has to be at least one node in this cell that isn't located on the cut.");

			if (!contains_point(QPointF((*nonCutNode)[0], (*nonCutNode)[1]))) {
				cellsToBeDeleted.push_back(c);
			}
		}

		it++;
	}

	m_tissue->DeleteCells(cellsToBeDeleted);
}

} // namespace
