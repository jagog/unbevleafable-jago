/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for DiamondTile.
 */

#include "DiamondTile.h"

#include <cmath>

namespace SimPT_Editor {

const double DiamondTile::g_diagonal_length = 10.0;
const QPolygonF DiamondTile::g_start_polygon(QPolygonF() << QPointF(0, 0) << QPointF(g_diagonal_length / 2, g_diagonal_length / 2) << QPointF(g_diagonal_length, 0) << QPointF(g_diagonal_length / 2, -g_diagonal_length / 2));

namespace {

QPainterPath PolygonToPath(const QPolygonF &polygon)
{
	QPainterPath path;
	path.moveTo(polygon.first());
	path.lineTo(polygon.at(1));
	path.lineTo(polygon.at(2));
	return path;
}

}

DiamondTile::DiamondTile()
	: DiamondTile(g_start_polygon, nullptr)
{
}

DiamondTile::~DiamondTile()
{
}

DiamondTile *DiamondTile::Left() const
{
	return new DiamondTile(m_polygon.translated(-g_diagonal_length, 0), parentItem());
}

DiamondTile *DiamondTile::Right() const
{
	return new DiamondTile(m_polygon.translated(g_diagonal_length, 0), parentItem());
}

DiamondTile *DiamondTile::Up() const
{
	return new DiamondTile(m_polygon.translated(-g_diagonal_length / 2, -g_diagonal_length / 2), parentItem());
}

DiamondTile *DiamondTile::Down() const
{
	return new DiamondTile(m_polygon.translated(g_diagonal_length / 2, g_diagonal_length / 2), parentItem());
}

DiamondTile::DiamondTile(const QPolygonF &polygon, QGraphicsItem *parentItem)
	: Tile(polygon, PolygonToPath(polygon))
{
	setParentItem(parentItem);
}

} // namespace
