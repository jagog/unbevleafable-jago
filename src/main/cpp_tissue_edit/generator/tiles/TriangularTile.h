#ifndef SIMPT_EDITOR_TRIANGULAR_TILE_H_INCLUDED
#define SIMPT_EDITOR_TRIANGULAR_TILE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for TriangularTile.
 */

#include "generator/tiles/Tile.h"

#include <QTransform>

namespace SimPT_Editor {

/**
 * A triangular tile
 */
class TriangularTile : public Tile
{
public:
	/**
	 * Constructor
	 */
	TriangularTile();

	/**
	 * Destructor
	 */
	virtual ~TriangularTile();

	/**
	 * Creates a new tile to the left of this tile (should normally be the opposite of Tile::Right())
	 * Parent is set to parent of this tile
	 * Implementation of Tile::Left()
	 *
	 * @return	Tile*	A new tile to the left of this one (ownership is returned with pointer)
	 */
	virtual TriangularTile* Left() const;

	/**
	 * Creates a new tile to the right of this tile (should normally be the opposite of Tile::Left())
	 * Parent is set to parent of this tile
	 * Implementation of Tile::Right()
	 *
	 * @return	Tile*	A new tile to the right of this one (ownership is returned with pointer)
	 */
	virtual TriangularTile* Right() const;

	/**
	 * Creates a new tile on the row under this tile (should normally be the opposite of Tile::Down())
	 * Parent is set to parent of this tile
	 * Implementation of Tile::Up()
	 *
	 * @return	Tile*	A new tile to below this one (ownership is returned with pointer)
	 */
	virtual TriangularTile* Up() const;

	/**
	 * Creates a new tile on the row under this tile (should normally be the opposite of Tile::Up())
	 * Parent is set to parent of this tile
	 * Implementation of Tile::Down()
	 *
	 * @return	Tile*	A new tile to below this one (ownership is returned with pointer)
	 */
	virtual TriangularTile* Down() const;

private:
	TriangularTile(const QPolygonF &polygon, bool odd, QGraphicsItem *parentItem);

private:
	bool m_odd;

private:
	static const double g_side_length;
	static const double g_height;
	static const QPolygonF g_start_polygon;
};

} // namespace

#endif // end-of-include-guard
