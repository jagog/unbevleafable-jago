#ifndef SIMPT_EDITOR_REGULAR_GENERATOR_DIALOG_H_INCLUDED
#define SIMPT_EDITOR_REGULAR_GENERATOR_DIALOG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for RegularGeneratorDialog.
 */

#include <QDialog>
#include <QPolygonF>

#include <list>
#include <memory>
#include <utility>
#include <vector>


class QGraphicsPolygonItem;
class QGraphicsScene;

namespace SimPT_Editor {

class RegularTiling;
class TransformationWidget;

/**
 * Dialog for generating regular patterns in a cell.
 */
class RegularGeneratorDialog : public QDialog
{
	Q_OBJECT
public:
	/**
	 * Constructor
	 *
	 * @param	boundaryPolygon		The polygon inside which to generate the cell complex
	 * @param	initialScale		The initial scale of the generator graphics view
	 * @param	parent			The parent of the dialog
	 */
	RegularGeneratorDialog(const QPolygonF &boundaryPolygon, double initialScale = 1.0, QWidget *parent = nullptr);

	/**
	 * Destructor
	 */
	virtual ~RegularGeneratorDialog();


	/**
	 * Retrieves the generated polygons after the dialog has successfully executed
	 *
	 * @return	list<QPolygonF>		The polygons of the generated cells
	 * 					The polygons are open, meaning that the first point and last point aren't equal
	 */
	std::list<QPolygonF> GetGeneratedPolygons() const;

private slots:
	void UpdateTiling(int index);
	void UpdateTransformation();

private:
	void SetupSceneItems(const QPolygonF &boundaryPolygon);
	void SetupGui(double scale);


	QGraphicsScene *m_scene;
	QGraphicsPolygonItem *m_boundary;
	RegularTiling *m_tiling;
	TransformationWidget *m_transformation;
	
	class ITileFactory;
	template <class TileType> class TileFactory;

	static const double g_scene_margin;
	static const std::vector<std::pair<std::shared_ptr<const ITileFactory>, std::string>> g_tile_factories;
};

} // namespace

#endif // end-of-include-guard
