/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for EditorActions.
 */

#include "EditorActions.h"
#include "PTreePanels.h"
#include "SelectByIDWidget.h"
#include "TissueEditor.h"
#include "TissueGraphicsView.h"
#include "UndoStack.h"

#include <cpp_sim/util/misc/InstallDirs.h>
#include "workspace/util/Compressor.h"
#include "gui/EnabledActions.h"
#include "gui/PTreeContainer.h"
#include "gui/PTreeEditorWindow.h"

#include <cassert>
#include <QAction>
#include <QDockWidget>
#include <QIcon>
#include <QMenuBar>
#include <QToolBar>
#include <string>

namespace SimPT_Editor {

EditorActions::EditorActions(TissueEditor* parent,
	TissueGraphicsView* view, PTreePanels* panels, UndoStack* undoStack)
	: QMenuBar(parent), m_parent(parent), m_view(view),
	  m_panels(panels), m_undo_stack(undoStack)
{
	assert(parent != nullptr && "The parent isn't initialized.");
	assert(view != nullptr && "The view isn't initialized.");
	assert(panels != nullptr && "The panels isn't initialized.");
	assert(undoStack != nullptr && "The undo stack isn't initialized.");

	Initialize();
}

EditorActions::~EditorActions() {}

void EditorActions::FixToggle()
{
	m_node_mode->setChecked(m_view->GetMode() == Mode::NODE);
	m_edge_mode->setChecked(m_view->GetMode() == Mode::EDGE);
	m_cell_mode->setChecked(m_view->GetMode() == Mode::CELL);
}

void EditorActions::Initialize()
{
	//Creation of menus
	m_menu_project = new QMenu("&Project", this);
	m_menu_edit = new QMenu("&Edit", this);
	m_menu_view = new QMenu("&View", this);

	//Creation of actions for project menu

	//Action for creating a new tissue
	m_new_tissue = new QAction("&New", m_menu_project);
	m_new_tissue->setShortcut(QKeySequence("Ctrl+N"));
	m_new_tissue->setStatusTip("Creates a new tissue");
	connect(m_new_tissue, SIGNAL(triggered()), m_parent, SLOT(NewLeaf()));

	//Action for opening an existing tissue
	m_open_tissue = new QAction("&Open", m_menu_project);
	m_open_tissue->setShortcut(QKeySequence("Ctrl+O"));
	m_open_tissue->setStatusTip("Opens an existing project");
	connect(m_open_tissue, SIGNAL(triggered()), m_parent, SLOT(OpenLeaf()));

	//Action for saving the current tissue
	m_save_tissue = new QAction("&Save", m_menu_project);
	m_save_tissue->setShortcut(QKeySequence("Ctrl+S"));
	m_save_tissue->setStatusTip("Saves the current project");
	m_save_tissue->setEnabled(false);
	connect(m_save_tissue, SIGNAL(triggered()), m_parent, SLOT(SaveLeaf()));

	//Action for closing the current tissue
	m_close_tissue = new QAction("&Close", m_menu_project);
	m_close_tissue->setStatusTip("Closes the current project");
	m_close_tissue->setEnabled(false);
	connect(m_close_tissue, SIGNAL(triggered()), m_parent, SLOT(CloseLeaf()));

	//Action for closing the program
	m_quit = new QAction("&Quit", m_menu_project);
	m_quit->setShortcut(QKeySequence("Ctrl+Q"));
	m_quit->setStatusTip("Quits the program");
	connect(m_quit, SIGNAL(triggered()), m_parent, SLOT(close()));

	//Creation of actions for the edit menu

	//Submenu for choosing the editing mode.
	QMenu* menuMode = new QMenu("&Mode", m_menu_edit);

	//Action for switching to Node mode
	m_node_mode = new QAction("&Node", menuMode);
	m_node_mode->setShortcut(QKeySequence("Ctrl+1"));
	m_node_mode->setStatusTip("Set editing mode to 'Node'");
	m_node_mode->setEnabled(false);
	m_node_mode->setCheckable(true);
	connect(m_node_mode, SIGNAL(triggered()), m_parent, SLOT(SetNodeMode()));
	menuMode->addAction(m_node_mode);

	//Action for switching to Edge mode
	m_edge_mode = new QAction("&Edge", menuMode);
	m_edge_mode->setShortcut(QKeySequence("Ctrl+2"));
	m_edge_mode->setStatusTip("Set editing mode to 'Edge'");
	m_edge_mode->setEnabled(false);
	m_edge_mode->setCheckable(true);
	connect(m_edge_mode, SIGNAL(triggered()), m_parent, SLOT(SetEdgeMode()));
	menuMode->addAction(m_edge_mode);

	//Action for switching to Cell mode
	m_cell_mode = new QAction("&Cell", menuMode);
	m_cell_mode->setShortcut(QKeySequence("Ctrl+3"));
	m_cell_mode->setStatusTip("Set editing mode to 'Cell'");
	m_cell_mode->setEnabled(false);
	m_cell_mode->setCheckable(true);
	connect(m_cell_mode, SIGNAL(triggered()), m_parent, SLOT(SetCellMode()));
	menuMode->addAction(m_cell_mode);

	//Submenu for generating actions.
	QMenu* menuGenerate = new QMenu("&Generate", m_menu_edit);

	//Generate regular pattern action
	m_generate_regular_pattern = new QAction("&Regular", menuGenerate);
	m_generate_regular_pattern->setShortcut(QKeySequence("Ctrl+Shift+R"));
	m_generate_regular_pattern->setStatusTip("Generate a regular cell pattern in the selected cell");
	connect(m_generate_regular_pattern, SIGNAL(triggered()), m_parent, SLOT(GenerateRegularPattern()));
	m_generate_regular_pattern->setEnabled(false);
	menuGenerate->addAction(m_generate_regular_pattern);

	//Generate Voronoi pattern action
	m_generate_voronoi_pattern = new QAction("&Voronoi Tesselation", menuGenerate);
	m_generate_voronoi_pattern->setShortcut(QKeySequence("Ctrl+Shift+V"));
	m_generate_voronoi_pattern->setStatusTip("Generate a Voronoi cell pattern in the selected cell");
	connect(m_generate_voronoi_pattern, SIGNAL(triggered()), m_parent, SLOT(GenerateVoronoiPattern()));
	m_generate_voronoi_pattern->setEnabled(false);
	menuGenerate->addAction(m_generate_voronoi_pattern);

	//Delete item action
	m_delete_item = new QAction("&Delete", m_menu_edit);
	m_delete_item->setShortcut(QKeySequence("Ctrl+D"));
	m_delete_item->setStatusTip("Delete the selected item");
	connect(m_delete_item, SIGNAL(triggered()), m_view, SLOT(DeleteItem()));
	m_delete_item->setEnabled(false);

	//Split edge action
	m_split_edge = new QAction("&Split edge", m_menu_edit);
	m_split_edge->setShortcut(QKeySequence("Ctrl+B"));
	m_split_edge->setStatusTip("Split the selected edge");
	connect(m_split_edge, SIGNAL(triggered()), m_view, SLOT(SplitEdge()));
	m_split_edge->setEnabled(false);

	//Split cell action
	m_split_cell = new QAction("&Split cell", m_menu_edit);
	m_split_cell->setShortcut(QKeySequence("Ctrl+B"));
	m_split_cell->setStatusTip("Split the selected cell");
	connect(m_split_cell, SIGNAL(triggered()), m_view, SLOT(SplitCell()));
	m_split_cell->setEnabled(false);

	//Create cell action
	m_create_cell = new QAction("&Create cell", m_menu_edit);
	m_create_cell->setShortcut(QKeySequence("Ctrl+A"));
	m_create_cell->setStatusTip("Create a cell with the selected endpoints");
	m_create_cell->setEnabled(false);
	connect(m_create_cell, SIGNAL(triggered()), m_view, SLOT(CreateCell()));

	//Copy attributes action
	m_copy_attributes = new QAction("&Copy attributes", m_menu_edit);
	m_copy_attributes->setShortcut(QKeySequence("Ctrl+C"));
	m_copy_attributes->setStatusTip("Copy attributes to the selected items from ...");
	m_copy_attributes->setEnabled(false);
	connect(m_copy_attributes, SIGNAL(triggered()), m_view, SLOT(CopyAttributes()));

	//Undo action
	m_undo = new QAction("&Undo", m_menu_edit);
	m_undo->setShortcut(QKeySequence("Ctrl+z"));
	m_undo->setStatusTip("Undo the last action");
	m_undo->setEnabled(false);
	connect(m_undo, SIGNAL(triggered()), m_parent, SLOT(Undo()));

	//Redo action
	m_redo = new QAction("&Redo", m_menu_edit);
	m_redo->setShortcut(QKeySequence("Ctrl+Shift+z"));
	m_redo->setStatusTip("Redo the last action");
	m_redo->setEnabled(false);
	connect(m_redo, SIGNAL(triggered()), m_parent, SLOT(Redo()));

	//Creation of actions for the view menu

	//Show attribute panel
	m_show_attribute_panel = m_panels->AttributePanel()->GetDock()->toggleViewAction();
	m_panels->AttributePanel()->GetDock()->toggleViewAction()->setText("Attribute panel");
	m_show_attribute_panel->setStatusTip("Show the attribute panel");
	m_show_attribute_panel->setEnabled(false);

	//Show geometric panel
	m_show_geometric_panel = m_panels->GeometricPanel()->GetDock()->toggleViewAction();
	m_panels->GeometricPanel()->GetDock()->toggleViewAction()->setText("Geometric panel");
	m_show_geometric_panel->setStatusTip("Show the geometric panel");
	m_show_geometric_panel->setEnabled(false);

	//Show parameters panel
	m_show_parameters_panel = m_panels->ParametersPanel()->GetDock()->toggleViewAction();
	m_panels->ParametersPanel()->GetDock()->toggleViewAction()->setText("Parameters panel");
	m_show_parameters_panel->setStatusTip("Show the parameters panel");
	m_show_parameters_panel->setEnabled(false);

	//Transparent cells
	m_transparent_cells = new QAction("&Transparent cells", m_menu_view);
	m_transparent_cells->setStatusTip("Set background of cells transparent");
	m_transparent_cells->setCheckable(true);
	m_transparent_cells->setEnabled(false);
	connect(m_transparent_cells, SIGNAL(toggled(bool)), m_view, SLOT(SetTransparentCells(bool)));

	//Set background
	m_set_background = new QAction("&Set background...", m_menu_view);
	m_set_background->setStatusTip("Set background image");
	m_set_background->setEnabled(false);
	connect(m_set_background, SIGNAL(triggered()), m_view, SLOT(SetBackground()));

	//Set display mode
	m_set_display_mode = new QAction("&Set color scheme ...", m_menu_view);
	m_set_display_mode->setStatusTip("Set the color scheme for cells in display mode");
	m_set_display_mode->setEnabled(false);
	connect(m_set_display_mode, SIGNAL(triggered()), m_view, SLOT(SetDisplayModePreferences()));


	//Cancel action
	m_cancel = new QAction("&Cancel", m_parent);
	m_cancel->setStatusTip("Cancel the current action");
	m_cancel->setEnabled(false);
	connect(m_cancel, SIGNAL(triggered()), m_view, SLOT(CancelAction()));

	// Setting up menus

	// Project menu
	m_menu_project->addAction(m_new_tissue);
	m_menu_project->addAction(m_open_tissue);
	m_menu_project->addAction(m_save_tissue);
	m_menu_project->addAction(m_close_tissue);
	m_menu_project->addAction(m_quit);
	addMenu(m_menu_project);

	// Edit menu
	m_menu_edit->addMenu(menuMode);
	m_menu_edit->addMenu(menuGenerate);
	m_menu_edit->addAction(m_delete_item);
	m_menu_edit->addAction(m_split_edge);
	m_menu_edit->addAction(m_split_cell);
	m_menu_edit->addAction(m_create_cell);
	m_menu_edit->addAction(m_copy_attributes);
	m_menu_edit->addAction(m_undo);
	m_menu_edit->addAction(m_redo);
	addMenu(m_menu_edit);

	// View menu
	m_menu_view->addAction(m_show_attribute_panel);
	m_menu_view->addAction(m_show_geometric_panel);
	m_menu_view->addAction(m_show_parameters_panel);
	m_menu_view->addAction(m_transparent_cells);
	m_menu_view->addAction(m_set_background);
	m_menu_view->addAction(m_set_display_mode);
	addMenu(m_menu_view);

	// Toolbar
	QToolBar* toolBar = new QToolBar("Toolbar");

	QIcon nodeIcon(QString::fromStdString(SimPT_Sim::Util::InstallDirs::GetDataDir() + "/icons/editor/node.png"));
	QIcon edgeIcon(QString::fromStdString(SimPT_Sim::Util::InstallDirs::GetDataDir() + "/icons/editor/edge.png"));
	QIcon cellIcon(QString::fromStdString(SimPT_Sim::Util::InstallDirs::GetDataDir() + "/icons/editor/cell.png"));
	QIcon cancelIcon(QString::fromStdString(SimPT_Sim::Util::InstallDirs::GetDataDir() + "/icons/Tango/22x22/actions/process-stop.png"));

	toolBar->addAction(m_node_mode);
	toolBar->addAction(m_edge_mode);
	toolBar->addAction(m_cell_mode);
	toolBar->addAction(m_cancel);

	m_node_mode->setIcon(nodeIcon);
	m_edge_mode->setIcon(edgeIcon);
	m_cell_mode->setIcon(cellIcon);
	m_cancel->setIcon(cancelIcon);

	QWidget* spacer = new QWidget();
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	toolBar->addWidget(spacer);
	toolBar->addWidget(m_view->GetSelectByIDWidget());

	m_parent->addToolBar(toolBar);
}

void EditorActions::ItemsSelected(unsigned int count)
{
	assert((m_view->GetMode() == Mode::NODE
	        || m_view->GetMode() == Mode::EDGE || m_view->GetMode() == Mode::CELL)
		&& "This mode isn't allowed.");

	m_delete_item->setEnabled(count == 1 && m_view->SelectedIsDeletable());
	m_split_edge->setEnabled(count == 1 && m_view->GetMode() == Mode::EDGE);
	m_split_cell->setEnabled(count == 2
		&& m_view->GetMode() == Mode::NODE && m_view->SelectedIsSplitable());
	m_create_cell->setEnabled(count == 2
		&& m_view->GetMode() == Mode::NODE && m_view->SelectedIsAtBoundary());
	m_copy_attributes->setEnabled(count > 0
		&& (m_view->GetMode() == Mode::CELL
		        || m_view->GetMode() == Mode::EDGE || m_view->GetMode() == Mode::NODE));
	m_generate_regular_pattern->setEnabled(count == 1 && m_view->GetMode() == Mode::CELL);
	m_generate_voronoi_pattern->setEnabled(count == 1 && m_view->GetMode() == Mode::CELL);
}

void EditorActions::LeafOpened()
{
	m_new_tissue->setEnabled(true);
	m_open_tissue->setEnabled(true);
	m_save_tissue->setEnabled(true);
	m_close_tissue->setEnabled(true);

	m_node_mode->setEnabled(true);
	m_edge_mode->setEnabled(true);
	m_cell_mode->setEnabled(true);
	m_undo->setEnabled(false);
	m_redo->setEnabled(false);

	m_show_attribute_panel->setEnabled(true);
	m_show_geometric_panel->setEnabled(true);
	m_show_parameters_panel->setEnabled(true);
	m_transparent_cells->setEnabled(true);
	m_set_background->setEnabled(true);
	m_set_display_mode->setEnabled(true);
}

void EditorActions::LeafClosed()
{
	m_save_tissue->setEnabled(false);
	m_close_tissue->setEnabled(false);

	m_node_mode->setEnabled(false);
	m_edge_mode->setEnabled(false);
	m_cell_mode->setEnabled(false);

	m_delete_item->setEnabled(false);
	m_split_edge->setEnabled(false);
	m_split_cell->setEnabled(false);
	m_create_cell->setEnabled(false);
	m_copy_attributes->setEnabled(false);
	m_generate_regular_pattern->setEnabled(false);
	m_generate_voronoi_pattern->setEnabled(false);
	m_undo->setEnabled(false);
	m_redo->setEnabled(false);

	m_cancel->setEnabled(false);

	m_show_attribute_panel->setEnabled(false);
	m_show_geometric_panel->setEnabled(false);
	m_show_parameters_panel->setEnabled(false);
	m_transparent_cells->setEnabled(false);
	m_set_background->setEnabled(false);
	m_set_display_mode->setEnabled(false);
}

void EditorActions::ModeChanged()
{
	FixToggle();

	bool modesEnabled = (m_view->GetMode() == Mode::DISPLAY
	        || m_view->GetMode() == Mode::NODE || m_view->GetMode() == Mode::EDGE
		|| m_view->GetMode() == Mode::CELL || m_view->GetMode() == Mode::NONE);

	m_node_mode->setEnabled(modesEnabled);
	m_edge_mode->setEnabled(modesEnabled);
	m_cell_mode->setEnabled(modesEnabled);

	m_delete_item->setEnabled(false);
	m_split_edge->setEnabled(false);
	m_split_cell->setEnabled(false);
	m_create_cell->setEnabled(false);
	m_copy_attributes->setEnabled(false);
	m_generate_regular_pattern->setEnabled(false);
	m_generate_voronoi_pattern->setEnabled(false);

	m_undo->setEnabled(m_view->GetMode() != Mode::CELL_CREATE
	        && m_view->GetMode() != Mode::CELL_SLICE && m_undo_stack->CanUndo());
	m_redo->setEnabled(m_view->GetMode() != Mode::CELL_CREATE
	        && m_view->GetMode() != Mode::CELL_SLICE && m_undo_stack->CanRedo());

	m_cancel->setEnabled(m_view->GetMode() == Mode::CELL_COPY
	        || m_view->GetMode() == Mode::CELL_CREATE || m_view->GetMode() == Mode::CELL_SLICE
	        || m_view->GetMode() == Mode::EDGE_COPY || m_view->GetMode() == Mode::NODE_COPY);
}

void EditorActions::Modified()
{
	m_undo->setEnabled(m_undo_stack->CanUndo());
	m_redo->setEnabled(m_undo_stack->CanRedo());
}

void EditorActions::Show(bool visible)
{
	m_menu_project->menuAction()->setVisible(visible);
	m_menu_edit->menuAction()->setVisible(visible);
	m_menu_view->menuAction()->setVisible(visible);
}

} // namespace
