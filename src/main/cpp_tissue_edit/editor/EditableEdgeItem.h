#ifndef SIMPT_EDITOR_EDITABLE_EDGE_ITEM_H_INCLUDED
#define SIMPT_EDITOR_EDITABLE_EDGE_ITEM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for EditableEdgeItem.
 */

#include "editor/EditableItem.h"
#include "bio/Edge.h"

#include <QGraphicsLineItem>
#include <QGraphicsScene>

namespace SimPT_Editor {

class EditableNodeItem;

/**
 * Editable graphical representation for Edge.
 */
class EditableEdgeItem : public EditableItem, public QGraphicsLineItem
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 * Remember that the order of the endpoints doesn't matter.
	 *
	 * @param	node1		The first endpoint of the edge.
	 * @param	node2		The second endpoint of the edge.
	 */
	EditableEdgeItem(EditableNodeItem* node1, EditableNodeItem* node2);

	/**
	 * Destructor.
	 */
	virtual ~EditableEdgeItem();

	/**
	 * Returns the first endpoint of the edge item.
	 */
	EditableNodeItem* First() const { return m_endpoint1; };

	/**
	 * Returns the second endpoint of the edge item.
	 */
	EditableNodeItem* Second() const { return m_endpoint2; };

	/**
	 * Checks whether the endpoints of the edge are at the boundary of the cell complex.
	 *
	 * @return	True if the endpoints are at the boundary.
	 */
	virtual bool IsAtBoundary() const;

	/**
	 * Checks whether the given endpoint is an endpoint of this edge.
	 *
	 * @param	endpoint		The given endpoint.
	 * @return	True if the edge contains the endpoint.
	 */
	bool ContainsEndpoint(EditableNodeItem* endpoint) const;

	/**
	 * Checks whether the given endpoints match the endpoints of this edge.
	 *
	 * @param	endpoint1		The first endpoint of the edge.
	 * @param	endpoint2		The second endpoint of the edge.
	 * @return	True if the endpoints do match.
	 */
	bool ContainsEndpoints(EditableNodeItem* endpoint1, EditableNodeItem* endpoint2) const;

	/**
	 * Returns the connecting node when this edge and the given edge connect.
	 *
	 * @param	edge			The given edge.
	 * @return	Nullptr if the edges don't connect.
	 */
	EditableNodeItem* ConnectingNode(EditableEdgeItem* edge) const;

	/**
	 * Returns the edge associated with this item.
	 *
	 * @return	The edge.
	 */
	SimPT_Sim::Edge Edge() const;

	/**
	 * Highlights the edge in the canvas.
	 *
	 * @param	highlighted	True if the edge should be highlighted.
	 */
	virtual void Highlight(bool highlighted);

	/**
	 * Set the color the item should get when it gets highlighted.
	 *
	 * @param	color		The given color.
	 */
	virtual void SetHighlightColor(const QColor& color = DEFAULT_HIGHLIGHT_COLOR);

	/**
	 * Merge the given edge with this edge into one.
	 * The edges should have exactly one connection point.
	 *
	 * @param	edge		The given edge.
	 * @return	The new edge.
	 */
	EditableEdgeItem* MergeEdge(EditableEdgeItem* edge);

	/**
	 * Split the edge in two edges on the given node.
	 *
	 * @param	node		The given node.
	 * @return	The two new edges.
	 */
	std::pair<EditableEdgeItem*, EditableEdgeItem*> SplitEdge(EditableNodeItem* node);


signals:
	/**
	 * Emitted when a given edge should be replaced with two other edges.
	 *
	 * @param	oldEdge		The edge which should be replaced.
	 * @param	edge1		The first replacement edge.
	 * @param	edge2		The second replacement edge.
	 */
	void EdgeSplitted(EditableEdgeItem* oldEdge, EditableEdgeItem* edge1, EditableEdgeItem* edge2);

	/**
	 * Emitted when the two given edges should be replaced by the new edge.
	 *
	 * @param	oldEdge1	The edge which should be replaced.
	 * @param	oldEdge2	The first replacement edge.
	 * @param	edge		The second replacement edge.
	 */
	void EdgeMerged(EditableEdgeItem* oldEdge1, EditableEdgeItem* oldEdge2, EditableEdgeItem* edge);

public slots:
	/**
	 * Update the edge.
	 */
	virtual void Update();

private:
	/**
	 * Reimplemented to paint based upon selection or changed mode.
	 */
	virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);


	/**
	 * Datamembers.
	 */
	EditableNodeItem* m_endpoint1;
	EditableNodeItem* m_endpoint2;

	QColor m_highlight_color;

private:
	static const QColor DEFAULT_HIGHLIGHT_COLOR;
};

} // namespace

#endif // end_of_include_guard
