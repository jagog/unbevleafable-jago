/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for UndoLeafStack
 */

#include "UndoStack.h"

namespace SimPT_Editor {

UndoStack::UndoStack() {}

UndoStack::~UndoStack() {}

void UndoStack::Initialize(const boost::property_tree::ptree& tissue)
{
	m_stack.clear();
	m_stack.push_back(tissue);
	m_current = m_stack.begin();
}

void UndoStack::Push(const boost::property_tree::ptree& tissue)
{
	assert(m_stack.size() > 0 && "The undo stack isn't initialized.");
	m_stack.erase(std::next(m_current), m_stack.end());
	m_stack.push_back(tissue);

	while (m_stack.size() > m_max_tissues) {
		m_stack.pop_front();
	}

	m_current = std::prev(m_stack.end());
}

bool UndoStack::CanUndo() const {
	assert(m_stack.size() > 0 && "The undo stack isn't initialized.");
	return (m_current != m_stack.begin());
}

bool UndoStack::CanRedo() const {
	assert(m_stack.size() > 0 && "The undo stack isn't initialized.");
	return (std::next(m_current) != m_stack.end());
}

const boost::property_tree::ptree& UndoStack::Undo()
{
	assert(m_stack.size() > 0 && "The undo stack isn't initialized.");
	if (CanUndo()) {
		m_current--;
	}
	return *m_current;
}

const boost::property_tree::ptree& UndoStack::Redo()
{
	assert(m_stack.size() > 0 && "The undo stack isn't initialized.");
	if (CanRedo()) {
		m_current++;
	}
	return *m_current;
}

} // namespace
