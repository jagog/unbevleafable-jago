#ifndef SIMPT_EDITOR_EDITABLE_MESH_H_INCLUDED
#define SIMPT_EDITOR_EDITABLE_MESH_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for EditableMesh.
 */

#include "bio/Mesh.h"

#include <boost/property_tree/ptree.hpp>
#include <QLineF>
#include <QPolygonF>

namespace SimPT_Editor {

using namespace SimPT_Sim;

/**
 * An editable mesh with actions to alter the construction of the mesh.
 */
class EditableMesh
{
public:
	/**
	 * Constructor.
	 * Initialize the EditableMesh with a given ptree.
	 *
	 * @param	pt			The given ptree.
	 */
	EditableMesh(const boost::property_tree::ptree& pt);

	/**
	 * Destructor.
	 */
	~EditableMesh();

	/**
	 * Checks whether the two given nodes can split a (given) cell (excluding the boundary polygon).
	 * The nodes shouldn't be adjacent to each other, but should be part of the same cell.
	 *
	 * @param	node1		The first node.
	 * @param	node2		The second node.
	 * @param	cell		The cell that should be tested. (If this isn't given,
	 *                              it will resolved to the correct cell if possible.)
	 * @return	True if the given cell or the resolved cell can be split.
	 */
	bool CanSplitCell(Node* node1, Node* node2, Cell* cell = nullptr) const;

	/**
	 * Creates a new cell with the two given nodes and a third new point.
	 * Both given nodes should be located at the boundary of the mesh.
	 *
	 * If the cell can't be created because of inconsistency in the mesh, it won't create a new cell.
	 * This inconsistency implies that the third point should be located outside the boundary polygon
	 * and that the new edges may not intersect with the old ones.
	 *
	 * @param	node1		The first endpoint.
	 * @param	node2		The second endpoint.
	 * @param	newPoint	The third endpoint (a new node will be created).
	 * @return	The new cell if it can be created, otherwise a nullptr.
	 */
	Cell* CreateCell(Node* node1, Node* node2, const QPointF& newPoint);

	/**
	 * Deletes the given cell from the mesh. The deletion of all those cells should keep the mesh consistent.
	 *
	 * @param	cells		The cells that will be deleted.
	 */
	void DeleteCells(const std::list<Cell*>& cells);

	/**
	 * Deletes the given node from the mesh (which should stay consistent). See
	 * the documentation of IsDeletableNode(Node*) for the conditions for a node
	 * to be deletable. After this node has been deleted, there will be an edge
	 * between the neighbors of this node.
	 *
	 * @param	node		The node that will be deleted.
	 * @return	True if the node can be deleted.
	 */
	bool DeleteTwoDegreeNode(Node* node);

	/**
	 * Place a node at the given coordinates.
	 *
	 * @param	node		The node which should be moved.
	 * @param	x		The x-coordinate.
	 * @param	y		The y-coordinate.
	 * @return	True if the node can be moved (without violation the mesh consistency).
	 */
	bool DisplaceNode(Node* node, double x, double y);

	/**
	 * Get the mesh.
	 */
	std::shared_ptr<SimPT_Sim::Mesh>& GetMesh();

	/**
	 * Checks whether the given cell is deletable (the mesh should stay consistent).
	 * The cell should be at the boundary, can't be the only cell in the mesh
	 * and should be present in the mesh.
	 *
	 * @return	True if the cell is deletable.
	 */
	bool IsDeletableCell(Cell* cell) const;

	/**
	 * Checks whether the given node is deletable.
	 * A node is deletable if it has a degree of two and is only part of cells with more than three nodes.
	 *
	 * @param	node		The given node.
	 * @return	True if the node is deletable.
	 */
	bool IsDeletableNode(Node* node) const;

	/**
	 * Replace a given cell with a set of new cells.
	 *
	 * @param	cell			The old cell that should be replaced.
	 * @param	newCells		The endpoints of the new cells.
	 *
	 * @precondition	Every endpoint of the new cells should already be present in the mesh.
	 * @precondition	Every endpoint in the old cell be an endpoint of at least one of the new cells.
	 * @precondition	The endpoints of the new cells should be in order of connection.
	 * @precondition	The sum of the areas of the new cells should be equal to the area of the old cell.
	 */
	void ReplaceCell(Cell* cell, std::list<QPolygonF> newCells);

	/**
	 * Split a given edge at the center.
	 *
	 * @param	edge		The given edge.
	 * @return	The new made node at the split.
	 */
	Node* SplitEdge(const Edge& edge);

	/**
	 * Does the same as SplitCell, but finds the correct cell that should be split.
	 * If this cell can't be found, an empty vector will be returned.
	 */
	std::vector<Cell*> SplitCell(Node* node1, Node* node2);

	/**
	 * Splits a cell in two by the line made by the two given nodes.
	 * If the cell can't be split, an empty vector will be returned.
	 *
	 * @param	cell		The cell that should get splitted.
	 * @param	node1		The first node of the cut.
	 * @param	node2		The second node of the cut.
	 * @return	The two new cells if the splitting succeeded, otherwise the old cell will be returned.
	 */
	std::vector<Cell*> SplitCell(Cell* cell, Node* node1, Node* node2);

private:
	/**
	 * Deletes the given node from the mesh. The node has to be connected to
	 * exactly two other nodes. After this node has been deleted, there will
	 * be an edge between the neighbors of this node. Any two-degree node can
	 * be deleted without any other consistency check on the mesh (for
	 * performance reasons). Therefore, use this function with caution.
	 * For a consistent deletion of a node, use DeleteTwoDegreeNode.
	 *
	 * @param	node		The node that will be deleted.
	 * @return	True if the node is a two-degree node.
	 */
	bool DeleteTwoDegreeNodeInconsistent(Node* node);

	/**
	 * Fix the IDs of all the cells. The IDs start at 0 (except boundary polygon which
	 * must be -1) and must be consecutive. The order of the cells by ID will be kept.
	 */
	void FixCellIDs();

	/**
	 * Fix the IDs of all the nodes. The IDs should start at 0 and be subsequent.
	 * The order of the nodes by ID will be kept.
	 */
	void FixNodeIDs();

	/**
	 * Fix the IDs of all the walls. The IDs should start at 0 and be subsequent.
	 * The order of the walls by ID will be kept.
	 */
	void FixWallIDs();

	/**
	 * Checks whether the given included edges intersect with any of the
	 * edges in the mesh, excluding those in the excluded edges.
	 *
	 * @param	excluded	The edges that are excluded from the consistency check.
	 * @param	included	The edges that are included in the consistency check.
	 * @return	True if the mesh is still consistent after including those edges.
	 */
	bool IsConsistent(const std::list<QLineF>& excluded, const std::list<QLineF>& included) const;

	/**
	 * Checks whether the edges connected to the given node intersect with any of the edges in the mesh.
	 * Note: This function is optimized for using dynamic node movement, as it only checks on surrounding edges.
	 *
	 * @param	node		The given node.
	 * @return	True if the mesh is consistent.
	 */
	bool IsConsistent(Node* node) const;

	/**
	 * Let the mesh check whether the node is at the boundary (using its neighbor
	 * cells) and set it boundary property accordingly.
	 *
	 * @param	node			The node that should be checked.
	 */
	void SetAtBoundary(Node* node);

private:
	std::shared_ptr<SimPT_Sim::Mesh>      m_mesh;
	std::vector<Cell*>&    m_cells;
	std::vector<Node*>&    m_nodes;
	std::vector<Wall*>&    m_walls;

	static constexpr double g_accuracy = 1.0e-12;	// Needed for floating-point errors in Qt intersections.
};

} // namespace

#endif // end_of_include_guard
