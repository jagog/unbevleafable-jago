/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for GraphicsView of tissue.
 */

#include "TissueGraphicsView.h"

#include "BackgroundDialog.h"
#include "CopyAttributesDialog.h"
#include "EditableCellItem.h"
#include "EditableEdgeItem.h"
#include "EditableNodeItem.h"
#include "EditorActions.h"
#include "PolygonUtils.h"
#include "SelectByIDWidget.h"

#include "EditControlLogic.h"
#include "generator/RegularGeneratorDialog.h"
#include "generator/VoronoiGeneratorDialog.h"
#include "slicer/TissueSlicer.h"
#include "bio/Cell.h"
#include "bio/Edge.h"
#include "bio/Node.h"
#include "bio/Wall.h"
#include "model/ComponentTraits.h"
#include "util/container/circular_iterator.h"

#include <boost/property_tree/ptree_fwd.hpp>
#include <QGraphicsPixmapItem>
#include <QInputDialog>
#include <QLayout>
#include <QMenuBar>
#include <QPixmap>
#include <QPoint>
#include <QToolTip>
#include <QWheelEvent>

#include <algorithm>
#include <cassert>
#include <iterator>
#include <list>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace SimPT_Editor {

using std::shared_ptr;
using namespace SimPT_Sim;

TissueGraphicsView::TissueGraphicsView(QWidget* parent)
	: PanAndZoomView(), m_tissue(nullptr), m_background_item(new QGraphicsPixmapItem()),
	  m_scene(this), m_mode(Mode::NONE), m_cells_transparent(false),// m_colorizer_map(nullptr),
	  m_colorizer_pref("size"), m_tissue_slicer(nullptr),
	  m_background_dialog(new BackgroundDialog(m_background_item, this)),
	  m_select_by_id(nullptr)
{
	setParent(parent);

	//Editing operations on the scene happens in constant time.
	//It gives better performance for dynamic scenes.
	m_scene.setItemIndexMethod(QGraphicsScene::ItemIndexMethod::NoIndex);
	setScene(&m_scene);
	setMouseTracking(true);
	setRenderHints(QPainter::Antialiasing);

	m_scene.addItem(m_background_item);
	m_background_item->setVisible(false);
	m_background_item->setZValue(-1);

	m_select_by_id = new SelectByIDWidget(this);
	m_select_by_id->setDisabled(true);
	connect(m_select_by_id, SIGNAL(IDsSelected(const std::list<unsigned int>&, bool)),
	        this, SLOT(SelectItems(const std::list<unsigned int>&, bool)));

	connect(&m_scene, SIGNAL(selectionChanged()), this, SLOT(UpdateSelection()));
}

TissueGraphicsView::~TissueGraphicsView()
{
	Cleanup();
	//delete m_colorizer_map;
	//m_colorizer_map = nullptr;
	m_select_by_id = nullptr;
}

void TissueGraphicsView::Initialize(shared_ptr<EditControlLogic> tissue, Mode mode)
{
	Cleanup();
	m_tissue = tissue;

	for (Node* node : tissue->GetNodes()) {
		AddNode(node);
	}
	for (Cell* cell : tissue->GetCells()) {
		auto nodes = cell->GetNodes();
		std::list<EditableNodeItem*> nodeItems;
		for (auto it = nodes.begin(); it != nodes.end(); it++) {
			EditableNodeItem* nodeItem = GetEditableNode((*it));
			nodeItems.push_back(nodeItem);
		}

		std::list<EditableEdgeItem*> edgeItems;
		auto cit = SimPT_Sim::Container::make_const_circular(nodeItems);
		do {
			double found = false;
			for (EditableEdgeItem* edge : m_edges) {
				if (edge->ContainsEndpoints(*cit, *next(cit))) {
					edgeItems.push_back(edge);
					found = true;
				}
			}
			if (!found) {
				edgeItems.push_back(AddEdge(*cit, *next(cit)));
			}
		} while (++cit != nodeItems.begin());

		AddCell(nodeItems, edgeItems, cell);
	}

	connect(m_tissue.get(), SIGNAL(Moved(Node*, double, double)),
	        this, SLOT(UpdateMovedNode()));
	connect(this, SIGNAL(StatusInfoChanged(const std::string&)),
	        m_tissue.get(), SIGNAL(StatusInfoChanged(const std::string&)));
	SetMode(mode);
	SetTransparentCells(m_cells_transparent);
}

bool TissueGraphicsView::SelectedIsAtBoundary() const
{
	bool editableItemPresent = false;

	for (QGraphicsItem* item : m_scene.selectedItems()) {
		EditableItem* editableItem = dynamic_cast<EditableItem*>(item);
		if (editableItem != nullptr) {
			editableItemPresent = true;
			if (!editableItem->IsAtBoundary()) {
				return false;
			}
		}
	}

	return editableItemPresent;
}

bool TissueGraphicsView::SelectedIsDeletable() const
{
	assert(m_tissue != nullptr && "The tissue isn't initialized.");

	bool editableItemPresent = false;

	if (m_mode == Mode::NODE) {
		for (QGraphicsItem* item : m_scene.selectedItems()) {
			EditableNodeItem* nodeItem = dynamic_cast<EditableNodeItem*>(item);
			if (nodeItem != nullptr) {
				editableItemPresent = true;
				if (!m_tissue->GetMesh()->IsDeletableNode(nodeItem->Node())) {
					return false;
				}
			}
		}
	}
	else if (m_mode == Mode::CELL) {
		for (QGraphicsItem* item : m_scene.selectedItems()) {
			EditableCellItem* cellItem = dynamic_cast<EditableCellItem*>(item);
			if (cellItem != nullptr) {
				editableItemPresent = true;
				if (!m_tissue->GetMesh()->IsDeletableCell(cellItem->Cell())) {
					return false;
				}
			}
		}
	}

	return editableItemPresent;
}

bool TissueGraphicsView::SelectedIsSplitable() const
{
	if (m_scene.selectedItems().size() != 2 || m_mode != Mode::NODE) {
		return false;
	}

	EditableNodeItem* item1 = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().front());
	EditableNodeItem* item2 = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().back());

	assert(item1 != nullptr && item2 != nullptr && "Other items than nodes were selected in 'NODE' mode.");

	return m_tissue->GetMesh()->CanSplitCell(item1->Node(), item2->Node());
}

EditableCellItem* TissueGraphicsView::AddCell(std::list<EditableNodeItem*> endpoints, std::list<EditableEdgeItem*> edges, Cell* cell)
{
	EditableCellItem* cellItem = new EditableCellItem(endpoints, edges, cell);
	cellItem->SetTransparent(m_cells_transparent);
	m_scene.addItem(cellItem);
	m_cells.push_back(cellItem);
	return cellItem;
}

EditableEdgeItem* TissueGraphicsView::AddEdge(EditableNodeItem* endpoint1, EditableNodeItem* endpoint2)
{
	EditableEdgeItem* edgeItem = new EditableEdgeItem(endpoint1, endpoint2);
	m_scene.addItem(edgeItem);
	m_edges.push_back(edgeItem);
	return edgeItem;
}

EditableNodeItem* TissueGraphicsView::AddNode(Node* node)
{
	EditableNodeItem* nodeItem = new EditableNodeItem(node, g_node_radius);
	m_scene.addItem(nodeItem);
	m_nodes.push_back(nodeItem);
	return nodeItem;
}

Mode TissueGraphicsView::GetMode() const
{
	return m_mode;
}

SelectByIDWidget* TissueGraphicsView::GetSelectByIDWidget() const {
	return m_select_by_id;
}

void TissueGraphicsView::SetMode(Mode mode)
{
	if (m_mode == Mode::DISPLAY && mode != Mode::DISPLAY) {
		QColor color(255, 255, 255, 255);
		for (EditableCellItem* item : m_cells) {
			item->SetColor(color);
		}
	} else if (mode == Mode::DISPLAY) {
		//assert(m_colorizer_map != nullptr && "The colorizer map must be initialized to use 'DISPLAY'-mode.");
		if (m_colorizer_pref == "<no color>") {
			for (EditableCellItem* item : m_cells) {
				QColor color(255, 255, 255, 255);
				item->SetColor(color);
			}
		} else {

			const auto cell_colorizer = m_factory->CreateCellColor(m_colorizer_pref.toStdString(), m_ptree);
			for (EditableCellItem* item : m_cells) {
			        const auto c = cell_colorizer(item->Cell());
				QColor color = QColor::fromRgbF(get<0>(c), get<1>(c), get<2>(c));
				item->SetColor(color);
			}
		}
	}

	if (m_mode != mode) {
		m_mode = mode;

		if (m_mode == Mode::DISPLAY || m_mode == Mode::NODE || m_mode == Mode::EDGE || m_mode == Mode::CELL) {
			m_scene.clearSelection();
			m_tissue->Deselect();
			SetEditableItemFlags();
		}

		setCursor(m_mode == Mode::CELL_CREATE ? Qt::CrossCursor : Qt::ArrowCursor);
		//setMouseTracking(m_mode == Mode::CELL_SLICE);
		emit ModeChanged();

		std::string info;
		if (m_mode == Mode::DISPLAY) {
			info = "Mode changed to 'Display'";
		}
		else if (m_mode == Mode::NODE) {
			info = "Mode changed to 'Node'";
		}
		else if (m_mode == Mode::EDGE) {
			info = "Mode changed to 'Edge'";
		}
		else if (m_mode == Mode::CELL) {
			info = "Mode changed to 'Cell'";
		}
		emit StatusInfoChanged(info);

		//Change maximum ID for select by ID.
		if (m_mode != Mode::NODE && m_mode != Mode::EDGE && m_mode != Mode::CELL) {
			m_select_by_id->SetMaxID(0); //Reset the text in the text box.
			m_select_by_id->setDisabled(true);
		}
		else {
			unsigned int maxID = 0;
			if (m_mode == Mode::NODE) {
				maxID = m_nodes.size() - 1;
			}
			else if (m_mode == Mode::EDGE) {
				maxID = m_tissue->GetMesh()->GetMesh()->GetWalls().size() - 1;
			}
			else if (m_mode == Mode::CELL) {
				maxID = m_cells.size() - 1;
			}
			m_select_by_id->SetMaxID(maxID);
			m_select_by_id->setDisabled(false);
		}
	}
}

void TissueGraphicsView::GenerateRegularPattern()
{
	assert(m_mode == Mode::CELL && "Tried to generate a pattern in a cell in a different mode than 'CELL'.");
	assert(m_scene.selectedItems().size() == 1 && "Can't generate a pattern in multiple cells.");

	EditableCellItem* cellItem = dynamic_cast<EditableCellItem*>(m_scene.selectedItems().front());
	assert(cellItem != nullptr && "An item different from a cell has been selected in mode 'CELL'.");

	RegularGeneratorDialog dialog(cellItem->polygon(), Scaling(), parentWidget());
	if (dialog.exec()) {
		m_tissue->ReplaceCell(cellItem->Cell(), dialog.GetGeneratedPolygons());
		Initialize(m_tissue, Mode::CELL);
	}
}

void TissueGraphicsView::GenerateVoronoiPattern()
{
	assert(m_mode == Mode::CELL && "Tried to generate a pattern in a cell in a different mode than 'CELL'.");
	assert(m_scene.selectedItems().size() == 1 && "Can't generate a pattern in multiple cells.");

	EditableCellItem* cellItem = dynamic_cast<EditableCellItem*>(m_scene.selectedItems().front());
	assert(cellItem != nullptr && "An item different from a cell has been selected in mode 'CELL'.");

	VoronoiGeneratorDialog dialog(cellItem->polygon(), Scaling(), this);
	if (dialog.exec()) {
		m_tissue->ReplaceCell(cellItem->Cell(), dialog.GetGeneratedPolygons());
		Initialize(m_tissue, Mode::CELL);
	}
}

void TissueGraphicsView::SetColorComponent(const boost::property_tree::ptree& parameters)
{
        m_ptree = parameters;
        const auto model_group  = m_ptree.get<string>("model.group", "");
        m_factory = ComponentFactoryProxy::Create(model_group);
}

void TissueGraphicsView::Cleanup()
{
	for (auto item : m_cells) {
		delete item;
	}
	for (auto item : m_edges) {
		delete item;
	}
	for (auto item : m_nodes) {
		delete item;
	}

	m_cells.clear();
	m_edges.clear();
	m_nodes.clear();

	SetMode(Mode::NONE);

	delete m_tissue_slicer;
	m_tissue_slicer = nullptr;
}

void TissueGraphicsView::CancelAction()
{

	if (m_mode == Mode::CELL_CREATE) {
		assert(m_scene.selectedItems().size() == 2 && "There should be two nodes selected when in this mode.");

		m_mode = Mode::NODE;
		SetEditableItemFlags();
		setCursor(Qt::ArrowCursor);
		emit ModeChanged();
		emit ItemsSelected(2);
	}
	else if (m_mode == Mode::CELL_SLICE) {
		delete m_tissue_slicer;
		m_tissue_slicer = nullptr;

		m_mode = Mode::CELL;
		SetEditableItemFlags();
		emit ModeChanged();
	}
	else if (m_mode == Mode::NODE_COPY) {
		m_mode = Mode::NODE;
		emit ModeChanged();
		emit ItemsSelected(m_scene.selectedItems().size());
	}
	else if (m_mode == Mode::EDGE_COPY) {
		m_mode = Mode::EDGE;
		emit ModeChanged();
		emit ItemsSelected(m_scene.selectedItems().size());

	}
	else if (m_mode == Mode::CELL_COPY) {
		m_mode = Mode::CELL;
		emit ModeChanged();
		emit ItemsSelected(m_scene.selectedItems().size());
	}
	else {
		assert(false && "This mode doesn't represent an action.");
	}
}

void TissueGraphicsView::CopyAttributes()
{
	assert(m_scene.selectedItems().size() > 0 && "At least one cell item should be selected.");

	if (m_mode == Mode::CELL) {
		SetMode(Mode::CELL_COPY);
	}
	else if (m_mode == Mode::EDGE) {
		SetMode(Mode::EDGE_COPY);
	}
	else if (m_mode == Mode::NODE) {
		SetMode(Mode::NODE_COPY);
	}
	else {
		assert(false && "The mode should be 'CELL', 'EDGE' or 'NODE'.");
	}
}

void TissueGraphicsView::CreateCell()
{
	assert(m_mode == Mode::NODE && m_scene.selectedItems().size() == 2 && "Two node items should be selected in 'NODE'-mode");

	SetMode(Mode::CELL_CREATE);
}

void TissueGraphicsView::DeleteItem()
{
	assert(m_scene.selectedItems().size() == 1 && "Can't delete multiple items.");

	if (m_mode == Mode::NODE) {
		EditableNodeItem* nodeItem = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().front());
		if (nodeItem != nullptr) {
			DeleteNode(nodeItem);
		}
	}
	else if (m_mode == Mode::CELL) {
		EditableCellItem* cellItem = dynamic_cast<EditableCellItem*>(m_scene.selectedItems().front());
		if (cellItem != nullptr) {
			DeleteCell(cellItem);
		}
	}
}

void TissueGraphicsView::SplitEdge()
{
	assert(m_mode == Mode::EDGE && "Tried to split an edge in a different mode than 'EDGE'.");
	assert(m_scene.selectedItems().size() == 1 && "Only one edge can be split.");

	EditableEdgeItem* edgeItem = dynamic_cast<EditableEdgeItem*>(m_scene.selectedItems().front());
	assert(edgeItem != nullptr && "An item different from an edge has been selected in mode 'EDGE'.");

	Node* node = m_tissue->SplitEdge(edgeItem->Edge());
	EditableNodeItem* nodeItem = AddNode(node);
	auto newEdges = edgeItem->SplitEdge(nodeItem);

	m_scene.addItem(newEdges.first);
	m_scene.addItem(newEdges.second);
	m_scene.removeItem(edgeItem);

	newEdges.first->setFlags(edgeItem->flags());
	newEdges.second->setFlags(edgeItem->flags());

	m_edges.push_back(newEdges.first);
	m_edges.push_back(newEdges.second);
	m_edges.remove(edgeItem);

	delete edgeItem;
}

void TissueGraphicsView::SplitCell()
{
	assert(m_mode == Mode::NODE && "Tried to split a cell in a different mode than 'NODE'.");
	assert(m_scene.selectedItems().size() == 2 && "Two nodes should be selected.");

	EditableNodeItem* item1 = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().front());
	EditableNodeItem* item2 = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().back());

	assert(item1 != nullptr && item2 != nullptr && "Other items than nodes were selected in 'NODE' mode.");

	m_tissue->SplitCell(item1->Node(), item2->Node());
	Initialize(m_tissue, Mode::NODE);
}

void TissueGraphicsView::UpdateMovedNode()
{
	assert(m_mode == Mode::NODE && "Can't update a moved node when not in mode 'NODE'.");
	assert(m_scene.selectedItems().size() == 1 && "Can only update one selected node.");

	EditableNodeItem* node = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().front());
	assert(node != nullptr && "Another item different from a node has been selected in mode 'NODE'.");

	node->Update();
}

void TissueGraphicsView::SetBackground()
{
	m_background_dialog->show();
	m_background_dialog->activateWindow();
}

void TissueGraphicsView::SetDisplayModePreferences()
{
	QStringList items;
	items.append("<no color>");

        for (const auto& e : m_factory->ListCellColor()) {
                items.append(QString::fromStdString(e));
        }
	bool ok;
	QString pref = QInputDialog::getItem(this, "Cell Color scheme preferences",
	        "Select the cell color scheme.", items, items.indexOf(m_colorizer_pref), false, &ok);

	if (ok) {
		m_colorizer_pref = pref;
		if (m_mode == Mode::DISPLAY) {
			SetMode(Mode::DISPLAY);	//Reinitialize the cell colorizer.
		}
	}
}

void TissueGraphicsView::SetTransparentCells(bool transparent)
{
	//Change transparency
	m_cells_transparent = transparent;

	//For every cell currently present on the scene, update transparency to the new transparency
	for (EditableCellItem* cell : m_cells) {
		cell->SetTransparent(m_cells_transparent);
	}

}

void TissueGraphicsView::UpdateSelection()
{
	if (m_scene.selectedItems().size() == 0) {
		m_tissue->Deselect();
	}
	else {
		if (m_mode == Mode::NODE) {
			std::list<Node*> selectedNodes;
			for (QGraphicsItem* item : m_scene.selectedItems()) {
				EditableNodeItem* node = dynamic_cast<EditableNodeItem*>(item);
				assert(node != nullptr && "A node has been selected in another mode than 'NODE'.");
				selectedNodes.push_back(node->Node());
			}
			m_tissue->SelectNodes(selectedNodes);
		}
		else if (m_mode == Mode::EDGE) {
			std::list<Edge> selectedEdges;
			for (QGraphicsItem* item : m_scene.selectedItems()) {
				EditableEdgeItem* edge = dynamic_cast<EditableEdgeItem*>(item);
				assert(edge != nullptr && "An edge has been selected in another mode than 'EDGE'.");
				selectedEdges.push_back(edge->Edge());
			}
			m_tissue->SelectEdges(selectedEdges);
		}
		else if (m_mode == Mode::CELL) {
			std::list<Cell*> selectedCells;
			for (QGraphicsItem* item : m_scene.selectedItems()) {
				EditableCellItem* cell = dynamic_cast<EditableCellItem*>(item);
				assert(cell != nullptr && "A cell has been selected in another mode than 'CELL'.");
				selectedCells.push_back(cell->Cell());
			}
			m_tissue->SelectCells(selectedCells);
		}
	}

	if (m_mode == Mode::NODE || m_mode == Mode::EDGE || m_mode == Mode::CELL) {
		emit ItemsSelected(m_scene.selectedItems().size());
	}
}

void TissueGraphicsView::FinishSliceAction()
{
	assert(m_mode == Mode::CELL_SLICE && "Can't be in another mode than CELL_SLICE.");
	assert(m_tissue_slicer != nullptr && "Can't be in CELL_SLICE mode if tissue slicer isn't initialized.");

	m_tissue_slicer->deleteLater();
	m_tissue_slicer = nullptr;

	Initialize(m_tissue, Mode::CELL);
}

void TissueGraphicsView::SelectItems(const std::list<unsigned int>& ids, bool keepItemsSelected)
{
	if (!keepItemsSelected) {
		m_scene.clearSelection();
	}

	std::vector<QGraphicsItem*> selectedItems;
	if (m_mode == Mode::NODE) {
		for (unsigned int id : ids) {
			auto item = std::find_if(m_nodes.begin(), m_nodes.end(), [id](EditableNodeItem* n){return n->Node()->GetIndex() == id;});
			if (item != m_nodes.end()) {
				(*item)->setSelected(true);
				selectedItems.push_back(*item);
			}
		}

	}
	else if (m_mode == Mode::EDGE) {
		for (unsigned int id : ids) {
			for (EditableEdgeItem* e : m_edges) {
				Wall* w = m_tissue->GetMesh()->GetMesh()->FindWall(e->Edge());
				if (w->GetIndex() == id) {
					e->setSelected(true);
					selectedItems.push_back(e);
				}
			}
		}
	}
	else if (m_mode == Mode::CELL) {
		for (unsigned int id : ids) {
			auto item = std::find_if(m_cells.begin(), m_cells.end(),
				[id](EditableCellItem* c){return static_cast<unsigned int>(c->Cell()->GetIndex()) == id;});
			if (item != m_cells.end()) {
				(*item)->setSelected(true);
				selectedItems.push_back(*item);
			}
		}
	}
	else {
		assert(false && "This mode is not supported.");
	}

	EnsureVisible(selectedItems);
}

void TissueGraphicsView::DeleteNode(EditableNodeItem* node)
{
	assert(node != nullptr && "Tried to delete a nullptr.");

	auto contains_node = [node](EditableEdgeItem* edge) {
		return edge->ContainsEndpoint(node);
	};
	
	//Delete the node in the mesh.
	if (!m_tissue->DeleteNode(node->Node())) {
		return;
	}

	//Get the two edges the deleted node was connected to.
	std::list<EditableEdgeItem*> connectedEdges;
	auto edge = std::find_if(m_edges.begin(), m_edges.end(), contains_node);
	while (edge != m_edges.end()) {
		connectedEdges.push_back(*edge);
		edge = std::find_if(++edge, m_edges.end(), contains_node);
	}
	assert(connectedEdges.size() == 2 && "More than two edges are connected to the deleted node.");
	EditableEdgeItem* oldEdge1 = connectedEdges.front();
	EditableEdgeItem* oldEdge2 = connectedEdges.back();

	//Merge the two edges together.
	EditableEdgeItem* newEdge = oldEdge1->MergeEdge(oldEdge2);
	m_edges.push_back(newEdge);
	m_edges.remove(oldEdge1);
	m_edges.remove(oldEdge2);
	m_scene.addItem(newEdge);

	//Destroy the items and remove them from the scene.
	m_nodes.remove(node);
	m_scene.removeItem(node);
	m_scene.removeItem(oldEdge1);
	m_scene.removeItem(oldEdge2);
	m_scene.clearSelection();

	delete oldEdge1;
	delete oldEdge2;
	delete node;
}

void TissueGraphicsView::DeleteCell(EditableCellItem* cell)
{
	assert(cell != nullptr && "Tried to delete a nullptr.");
	assert(cell->Cell()->HasBoundaryWall());

	m_tissue->DeleteCells({ cell->Cell() });

	Initialize(m_tissue, Mode::CELL);
}

void TissueGraphicsView::EnsureVisible(const std::vector<QGraphicsItem*>& items)
{
	if (items.size() == 0) {
		return;
	}
	else {
		QRectF current = items.front()->mapRectToScene(items.front()->boundingRect());
		for (auto it = std::next(items.begin()); it != items.end(); it++) {
			current = current.united((*it)->mapRectToScene((*it)->boundingRect()));
		}
		ensureVisible(current);
	}
}

EditableNodeItem* TissueGraphicsView::GetEditableNode(Node* node) const
{
	for (auto it = m_nodes.begin(); it != m_nodes.end(); it++) {
		if ((*it)->Contains(node)) {
			return (*it);
		}
	}
	return nullptr;
}

void TissueGraphicsView::SetEditableItemFlags(bool enable)
{
	bool allowed = (m_mode == Mode::NODE && enable);
	for (QGraphicsItem* item : m_nodes) {
		item->setFlag(QGraphicsItem::ItemIsSelectable, allowed);
		item->setFlag(QGraphicsItem::ItemIsMovable, allowed);
	}

	allowed = (m_mode == Mode::EDGE && enable);
	for (QGraphicsItem* item : m_edges) {
		item->setFlag(QGraphicsItem::ItemIsSelectable, allowed);
	}

	allowed = (m_mode == Mode::CELL && enable);
	for (QGraphicsItem* item : m_cells) {
		item->setFlag(QGraphicsItem::ItemIsSelectable, allowed);
	}
}

bool TissueGraphicsView::IsConsistent() const
{
	if (m_scene.selectedItems().size() == 1) {
		EditableNodeItem* selected = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().front());

		//Check whether the selected node is on top of another node.
		if (selected != nullptr) {
			for (EditableNodeItem* node : m_nodes) {
				if (selected != node && QLineF(selected->pos(), node->pos()).length() < 2*g_node_radius) {
					return false;
				}
			}
		}

		//Check whether the cell is still counter-clockwise (clockwise in the Qt-coordinate system).
		if (m_cells.size() == 1) {
			if (PolygonUtils::IsClockwise(m_cells.front()->polygon())) {
				return false;
			}
		}

		return true;
	}
	else {
		return false;
	}
}

void TissueGraphicsView::mouseMoveEvent(QMouseEvent* event)
{
	//Update and show tool tip if located on node in 'Node'-mode.
	if (m_mode == Mode::NODE) {
		EditableNodeItem* nodeItem = dynamic_cast<EditableNodeItem*>(itemAt(event->pos()));
		if (nodeItem != nullptr) {
			//Get all cells associated with the node.
			std::list<Cell*> cells;
			for (auto c : m_tissue->GetMesh()->GetMesh()->GetCells()) {
				if (std::find_if(c->GetNodes().begin(), c->GetNodes().end(),
					[nodeItem](Node* n){return n->GetIndex() == nodeItem->Node()->GetIndex();}) != c->GetNodes().end()) {
					cells.push_back(c);
				}
			}
			cells.sort([](Cell* c1, Cell* c2){return c1->GetIndex() < c2->GetIndex();});

			//Get all walls associated with the node.
			std::list<Wall*> walls = m_tissue->GetMesh()->GetMesh()->GetNodeOwningWalls(nodeItem->Node());
			walls.sort([](Wall* w1, Wall* w2){return w1->GetIndex() < w2->GetIndex();});

			//Update tool tip.
			nodeItem->SetToolTip(cells, walls);
			QToolTip::showText(event->globalPos(), nodeItem->toolTip());
		}
	}

	if (m_mode == Mode::NODE && m_scene.selectedItems().size() == 1) {
		PanAndZoomView::mouseMoveEvent(event);
		EditableNodeItem* nodeItem = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().front());

		assert(nodeItem != nullptr && "An item different from a node has been selected in mode 'NODE'.");
		assert(nodeItem->flags().testFlag(QGraphicsItem::ItemIsMovable) && "The node isn't movable.");

		Node* node = nodeItem->Node();
		if (nodeItem->pos().x() != (*node)[0] || nodeItem->pos().y() != (*node)[1]) {
			if (!m_tissue->MoveNode(node, nodeItem->pos().x(), nodeItem->pos().y())) {
				nodeItem->Revert();
			}
			else if (!IsConsistent()) {
				nodeItem->Revert();
				m_tissue->MoveNode(node, nodeItem->pos().x(), nodeItem->pos().y());
			}
		}
	}
	else if (m_mode != Mode::NODE || event->modifiers().testFlag(Qt::ControlModifier)) {
		//The case of 'NODE' mode should already be handled for the valid cases.
		//If the mode is still in mode and the view isn't in drag mode (ctrl-modifier), the event may not be passed.
		PanAndZoomView::mouseMoveEvent(event);

		if (m_mode == Mode::CELL_SLICE) {
			assert(m_tissue_slicer != nullptr && "The tissue slicer should exist when in 'CELL_SLICE' mode.");
			m_tissue_slicer->MoveCut(mapToScene(event->pos()));
		}
	}
}

void TissueGraphicsView::mousePressEvent(QMouseEvent* event)
{
	if (event->button() == Qt::LeftButton && (m_mode == Mode::NODE_COPY || m_mode == Mode::EDGE_COPY ||  m_mode == Mode::CELL_COPY)) {
		QGraphicsItem* item = dynamic_cast<QGraphicsItem*>(m_scene.itemAt(mapToScene(event->pos()), QTransform()));
		if (item != nullptr && item->flags().testFlag(QGraphicsItem::ItemIsSelectable)) {
			bool isSelected = item->isSelected();
			if (m_mode == Mode::NODE_COPY) {
				assert(m_tissue->SelectedNodes().size() > 0 && "There should be at least one node selected.");

				EditableNodeItem* nodeItem = dynamic_cast<EditableNodeItem*>(item);
				assert(nodeItem != nullptr && "Another item than a node has been selected in 'NODE_COPY'-mode");


				nodeItem->SetHighlightColor(QColor(0, 0, 102, 255));
				nodeItem->setSelected(true);

				CopyAttributesDialog dialog(nodeItem->Node()->NodeAttributes::ToPtree(), this);
				if (dialog.exec()) {
					m_tissue->CopyAttributes(dialog.SelectedAttributes());

					m_mode = Mode::NODE;
					emit ModeChanged();

					nodeItem->SetHighlightColor();
					nodeItem->setSelected(true);
				}
				else {
					nodeItem->SetHighlightColor();
					nodeItem->setSelected(isSelected);
				}
			}
			else if (m_mode == Mode::EDGE_COPY) {
				assert(m_tissue->SelectedWalls().size() > 0 && "There should be at least one edge selected.");

				EditableEdgeItem* edgeItem = dynamic_cast<EditableEdgeItem*>(item);
				assert(edgeItem != nullptr && "Another item than a node has been selected in 'EDGE_COPY'-mode");

				edgeItem->SetHighlightColor(QColor(0, 0, 102, 255));
				edgeItem->setSelected(true);

				Wall* wall = m_tissue->GetMesh()->GetMesh()->FindWall(edgeItem->Edge());
				CopyAttributesDialog dialog(wall->WallAttributes::ToPtree(), this);
				if (dialog.exec()) {
					m_tissue->CopyAttributes(dialog.SelectedAttributes());

					m_mode = Mode::EDGE;
					emit ModeChanged();

					edgeItem->SetHighlightColor();
					edgeItem->setSelected(true);
				}
				else {
					edgeItem->SetHighlightColor();
					edgeItem->setSelected(isSelected);
				}
			}
			else if (m_mode == Mode::CELL_COPY) {
				assert(m_tissue->SelectedCells().size() > 0 && "There should be at least one cell selected.");

				EditableCellItem* cellItem = dynamic_cast<EditableCellItem*>(item);
				assert(cellItem != nullptr && "Another item than a node has been selected in 'CELL_COPY'-mode");

				cellItem->SetHighlightColor(QColor(0, 0, 102, 255));
				cellItem->setSelected(true);

				CopyAttributesDialog dialog(cellItem->Cell()->CellAttributes::ToPtree(), this);
				if (dialog.exec()) {
					m_tissue->CopyAttributes(dialog.SelectedAttributes());

					m_mode = Mode::CELL;
					emit ModeChanged();

					cellItem->SetHighlightColor();
					cellItem->setSelected(true);
				}
				else {
					cellItem->SetHighlightColor();
					cellItem->setSelected(isSelected);
				}
			}
		}
	}
	else if (event->button() == Qt::LeftButton && event->modifiers().testFlag(Qt::ShiftModifier)) {
		QGraphicsItem* item = m_scene.itemAt(mapToScene(event->pos()), QTransform());
		if (item != nullptr) {
			item->setSelected(!item->isSelected());
		}
	}
	else if (event->button() == Qt::LeftButton && m_mode == Mode::CELL_CREATE) {
		EditableNodeItem* nodeItem1 = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().front());
		EditableNodeItem* nodeItem2 = dynamic_cast<EditableNodeItem*>(m_scene.selectedItems().back());

		assert(nodeItem1 != nullptr && nodeItem1->Node()->IsAtBoundary()
			&& "There aren't two nodes selected or the nodes aren't located at the boundary.");
		assert(nodeItem2 != nullptr && nodeItem2->Node()->IsAtBoundary()
			&& "There aren't two nodes selected or the nodes aren't located at the boundary.");

		QPointF thirdPoint(mapToScene(event->pos()));
		Cell* cell = m_tissue->CreateCell(nodeItem1->Node(), nodeItem2->Node(), thirdPoint);

		if (cell != nullptr) {
			Initialize(m_tissue, Mode::NODE);
		}
	}
	else if (event->button() == Qt::LeftButton && (m_mode != Mode::CELL_SLICE || m_tissue_slicer->CutEnded())) {
		if (event->modifiers().testFlag(Qt::ControlModifier)) {
			SetEditableItemFlags(false);
		}
		PanAndZoomView::mousePressEvent(event);
		if (event->modifiers().testFlag(Qt::ControlModifier)) {
			SetEditableItemFlags(true);
		}
	}
	else if (event->button() == Qt::RightButton) {
		if (m_mode == Mode::CELL) {
			m_scene.clearSelection();
			QGraphicsView::mousePressEvent(event);

			QPolygonF boundaryPolygon;
			for (auto node : m_tissue->GetMesh()->GetMesh()->GetBoundaryPolygon()->GetNodes()) {
				boundaryPolygon.append(QPointF((*node)[0], (*node)[1]));
			}

			QPointF point = mapToScene(event->pos());
			if (!boundaryPolygon.containsPoint(point, Qt::OddEvenFill)) {
				SetMode(Mode::CELL_SLICE);
				m_tissue_slicer = new TissueSlicer(m_tissue, &m_scene);
				connect(m_tissue_slicer, SIGNAL(Finished()), this, SLOT(FinishSliceAction()));
				m_tissue_slicer->StartCut(mapToScene(event->pos()));
			}
		}
		else if (m_mode == Mode::CELL_SLICE) {
			QPolygonF boundaryPolygon;
			for (auto node : m_tissue->GetMesh()->GetMesh()->GetBoundaryPolygon()->GetNodes()) {
				boundaryPolygon.append(QPointF((*node)[0], (*node)[1]));
			}

			QPointF point = mapToScene(event->pos());
			if (!boundaryPolygon.containsPoint(point, Qt::OddEvenFill) && m_tissue_slicer->MoveCut(point)) {
				m_tissue_slicer->EndCut();
			}
		}
	}
}

void TissueGraphicsView::mouseReleaseEvent(QMouseEvent* event)
{
	if (m_mode == Mode::NODE && m_scene.selectedItems().size() == 1) {
		m_tissue->StopMoveNode();
		m_scene.selectedItems().front()->setFlag(QGraphicsItem::ItemIsMovable);
	}
	PanAndZoomView::mouseReleaseEvent(event);
}

void TissueGraphicsView::mouseDoubleClickEvent(QMouseEvent*) {}

} // namespace
