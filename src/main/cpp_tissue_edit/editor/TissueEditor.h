#ifndef SIMPT_EDITOR_SIMPT_TISSUE_EDITOR_H_INCLUDED
#define SIMPT_EDITOR_SIMPT_TISSUE_EDITOR_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for the TissueEditor.
 */

#include "gui/HasUnsavedChangesPrompt.h"

#include <boost/property_tree/ptree.hpp>
#include <memory>
#include <QAction>
#include <QLabel>
#include <QMainWindow>
#include <string>

namespace SimPT_Editor {

class EditControlLogic;
class EditorActions;
class PTreePanels;
class TissueGraphicsView;
class UndoStack;

/**
 * Main GUI class for cell editor.
 */
class TissueEditor : public QMainWindow, public SimShell::Gui::HasUnsavedChangesPrompt
{
	Q_OBJECT
public:
	/// Constructor with the parent of the window.
	TissueEditor(QWidget* parent = nullptr);

	/// Destructor.
	virtual ~TissueEditor();

	/// True if tissue has been opened, false otherwise.
	virtual bool IsOpened() const;

	/// True iff file has been successfully opened (if another file is open, it is closed first).
	bool OpenPath(const std::string& path);

	/// Receive a message when something is dragged over the window
	void dragEnterEvent(QDragEnterEvent *e);

	/// Recive a message when a file is dropped on the window
	void dropEvent(QDropEvent *e);


private slots:
	/// Override Qt method, closes program (will show dialog for unsaved changes).
	virtual void closeEvent(QCloseEvent* event);

	/// Slot for closing the currently open tissue without exiting program (true if tissue closed).
	bool CloseLeaf();

	/// Slot for generating a regular pattern.
	void GenerateRegularPattern();

	/// Slot for generating a Voronoi pattern.
	void GenerateVoronoiPattern();

	/**
	 * Creates a new tissue borrowing the sim data preamble, the parameters
	 * and (in current implementation) the mesh.cells.chemical_count
	 * form a template file you specify. The mesh (taking chemical_count
	 * into account) is generated (two square cells in current implementation)
	 * and substituted into the tissue. That tissue is then available for editing.
	 */
	void NewLeaf();

	/// Slot for opening an existing tissue
	void OpenLeaf();

	/// Redo the last action.
	void Redo();

	/// Slot for saving the current opened tissue
	void SaveLeaf();

	/// Slot for switching to the 'Cell' mode
	void SetCellMode();

	/// Slot for switching to the 'Edge' mode
	void SetEdgeMode();

	/// Slot to indicate tissue modification.
	/// This function will track this (undo stack and saved changes).
	void SetModified();

	/// Slot to alert about tissue modification.
	/// This function will skip the undo stack (used when node has been dynamically moved).
	void SetModifiedWithoutUndo();

	/// Slot for switching to the 'Node' mode
	void SetNodeMode();

	/// Slot for setting the status bar with info.
	void SetStatusBar(const std::string& info);

	/// Undo the last action.
	void Undo();

private:
	/// Creates the basic elements (menus, actions, toolbar) of the editor.
	void CreateActions();

	/// Create the layout of the main window.
	void CreateLayout();

	/// Update the views with the current tissue (which must be initialized).
	void UpdateViews();

private:
	/// @see PromptOnCLose
	virtual void InternalForceClose();

	/// @see PromptOnCLose
	virtual bool InternalIsClean() const;

	/// @see PromptOnCLose
	virtual bool InternalSave();

private:
	std::shared_ptr<EditControlLogic>   m_tissue;          ///< Currently opened tissue.
	boost::property_tree::ptree         m_ptree;           ///< Currently opened ptree (full).
	UndoStack*                          m_undo_stack;      ///< Undo Stack.
	TissueGraphicsView*                 m_graphics_view;   ///< Graphic workspace.
	PTreePanels*                        m_ptree_panels;    ///< The ptree panels.
	std::string                         m_current_path;    ///< Filename.
	EditorActions*                      m_actions;         ///< The actions of the editor.
	QLabel*                             m_status_info;     ///< Information to show in the status bar.
	bool                                m_modified;        ///< True if there is unsaved modified data.
};

} // namespace

#endif // end_of_include_guard
