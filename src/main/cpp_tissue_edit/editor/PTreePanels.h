#ifndef SIMPT_EDITOR_PTREE_PANELS_H_INCLUDED
#define SIMPT_EDITOR_PTREE_PANELS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTreePanels.
 */

#include "EditControlLogic.h"
#include "gui/PTreeContainer.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Editor {

using namespace SimShell;
using boost::property_tree::ptree;

/**
 * Manages the attribute and geometric ptree panel for a given tissue.
 */
class PTreePanels : public QWidget
{
	Q_OBJECT
public:
	/// Constructor to initialize the panels properly.
	PTreePanels(QWidget* parent);

	/// Destructor.
	virtual ~PTreePanels();

	/// Get the attribute panel.
	Gui::PTreeContainer* AttributePanel() const;

	/// Get the geometric panel.
	Gui::PTreeContainer* GeometricPanel() const;

	/// Initializes the panels with a given tissue.
	void Initialize(std::shared_ptr<EditControlLogic> tissue);

	/// Get the parameters panel.
	Gui::PTreeContainer* ParametersPanel() const;

private slots:
	/**
	 * Update the tissue according to the attribute panel.
	 * @param	pt       The ptree that should be updated in the tissue.
	 */
	void ApplyAttributePanel(const boost::property_tree::ptree& pt);

	/**
	 * Update the tissue according to the geometric panel.
	 * @param	pt       The ptree that should be updated in the tissue.
	 */
	void ApplyGeometricPanel(const boost::property_tree::ptree& pt);

	/**
	 * Update the tissue's parameters according to the parameters panel
	 * @param	pt       The ptree containing parameters from parameters panel.
	 */
	void ApplyParametersPanel(const boost::property_tree::ptree& pt);

	/**
	 * Show or hide the attribute panel.
	 * @param	toggled  The panel will be shown when true.
	 */
	void ToggleAttributePanel(bool toggled);

	/**
	 * Show or hide the geometric panel.
	 * @param	toggled	  The panel will be shown when true.
	 */
	void ToggleGeometricPanel(bool toggled);

	/**
	 * Show or hide the parameters panel.
	 * @param	toggled  The panel will be shown when true.
	 */
	void ToggleParametersPanel(bool toggled);

	/// Update the geometric panel.
	void UpdateGeometricPanel();

	/**
	 * Update the parameters panel.
	 */
	void UpdateParametersPanel();

	/// Update the panels.
	void UpdatePanels();

private:
	/**
	 * Merge join two ptrees.
	 * Each tree should have the exact same structure.
	 * When the data of two (equal) paths are different, it will use '?' as a default value.
	 * @param	pt1			The first ptree.
	 * @param	pt2			The second ptree.
	 * @return	The joined ptree.
	 */
	ptree MergeJoin(ptree pt1, ptree pt2);

	/**
	 * Set the data of this ptree (and of all its children) to unknown.
	 * @param	pt			The given ptree.
	 * @return	The ptree with the unknown data.
	 */
	ptree SetDataToUnknown(const ptree& pt);

	/// Update the attribute panel.
	void UpdateAttributePanel();
private:
	Gui::PTreeContainer*               m_attribute_panel;
	bool                               m_attribute_panel_toggled;
	Gui::PTreeContainer*               m_geometric_panel;
	bool                               m_geometric_panel_toggled;
	std::shared_ptr<EditControlLogic>  m_tissue;
	Gui::PTreeContainer*               m_parameters_panel;
	bool                               m_parameters_panel_toggled;
};

} // namespace

#endif // end_of_include_guard
