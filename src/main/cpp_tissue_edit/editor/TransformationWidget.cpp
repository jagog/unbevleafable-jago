/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for TransformationWidget.
 *
 * @author	Coding with(out) Discipline
 */

#include "TransformationWidget.h"

#include <cmath>
#include <QBoxLayout>
#include <QCheckBox>
#include <QDial>
#include <QFrame>
#include <QSlider>
#include <QSpinBox>
#include <QLabel>

namespace SimPT_Editor {

namespace {

double IntRatioToFactor(int intRatio)
{
	// Mapping
	// First: ratio = slider integer value / 100
	// Then:
	// 	 0 -> 1.0		Not scaled
	// 	+x -> x + 1		Ratio y : 1 magnification (with y = x + 1: for every increase of 100 above 0 of the value of the slider, we increase the y : 1 scaling ratio by one)
	//	-x -> 1 / -(x - 1)	Ratio 1 : y reduction (inverse of previous line)
	double ratio = intRatio / 100.0;
	return (ratio >= 0 ? (ratio + 1) : 1 / (1 - ratio));
}

int FactorToIntRatio(double factor)
{
	// Mapping is the inverse of IntRatioToFactor
	double ratio = factor >= 1.0 ? factor - 1 : 1 - 1 / factor;
	return std::round(ratio * 100);
}

}

TransformationWidget::TransformationWidget(int maxScaleRatio, double maxTranslationX, double maxTranslationY, QWidget *parent)
	: QWidget(parent), m_rotation(0), m_scaling_x(1.0), m_scaling_y(1.0), m_aspect_ratio_maintained(true), m_translation_x(0.0), m_translation_y(0.0), m_updating(false)
{
	SetupGui(maxScaleRatio, maxTranslationX, maxTranslationY);
}

TransformationWidget::~TransformationWidget()
{
}

void TransformationWidget::UpdateRotation(int degrees)
{
	m_rotation = degrees;

	emit TransformationChanged();
}

void TransformationWidget::UpdateScalingX(double factor)
{
	m_scaling_x = factor;
	if (!m_updating) { // To remove annoying mutual signal-slots invocations
		m_updating = true;
		m_scaling_x_slider->setValue(FactorToIntRatio(factor));
		if (m_aspect_ratio_maintained) {
			m_scaling_y_spin_box->setValue(factor);
			m_scaling_y_slider->setValue(m_scaling_x_slider->value());
		}
		m_updating = false;

		emit TransformationChanged();
	}
}

void TransformationWidget::UpdateScalingX(int value)
{
	if (!m_updating) { // To remove annoying mutual signal-slots invocations
		m_updating = true;
		m_scaling_x_spin_box->setValue(IntRatioToFactor(value));
		if (m_aspect_ratio_maintained) {
			m_scaling_y_spin_box->setValue(m_scaling_x_spin_box->value());
			m_scaling_y_slider->setValue(value);
		}
		m_updating = false;

		emit TransformationChanged();
	}
}

void TransformationWidget::UpdateScalingY(double factor)
{
	m_scaling_y = factor;
	if (!m_updating) { // To remove annoying mutual signal-slots invocations
		m_updating = true;
		m_scaling_y_slider->setValue(FactorToIntRatio(factor));
		m_updating = false;

		emit TransformationChanged();
	}
}

void TransformationWidget::UpdateScalingY(int value)
{
	if (!m_updating) { // To remove annoying mutual signal-slots invocations
		m_updating = true;
		m_scaling_y_spin_box->setValue(IntRatioToFactor(value));
		m_updating = false;

		emit TransformationChanged();
	}
}

void TransformationWidget::UpdateAspectRatioMaintained(bool checked)
{
	m_aspect_ratio_maintained = checked;
	m_scaling_y_spin_box->setEnabled(!checked);
	m_scaling_y_slider->setEnabled(!checked);

	emit TransformationChanged();
}

void TransformationWidget::UpdateTranslationX(double position)
{
	m_translation_x = position;
	if (!m_updating) { // To remove annoying mutual signal-slots invocations
		m_updating = true;
		m_translation_x_slider->setValue(std::round(position * 100));
		m_updating = false;

		emit TransformationChanged();
	}
}

void TransformationWidget::UpdateTranslationX(int position)
{
	if (!m_updating) { // To remove annoying mutual signal-slots invocations
		m_updating = true;
		m_translation_x_spin_box->setValue(position / 100.0);
		m_updating = false;

		emit TransformationChanged();
	}
}

void TransformationWidget::UpdateTranslationY(double position)
{
	m_translation_y = position;
	if (!m_updating) { // To remove annoying mutual signal-slots invocations
		m_updating = true;
		m_translation_y_slider->setValue(std::round(position * 100));
		m_updating = false;

		emit TransformationChanged();
	}
}

void TransformationWidget::UpdateTranslationY(int position)
{
	if (!m_updating) { // To remove annoying mutual signal-slots invocations
		m_updating = true;
		m_translation_y_spin_box->setValue(position / 100.0);
		m_updating = false;

		emit TransformationChanged();
	}
}


void TransformationWidget::SetupGui(int maxScaleRatio, double maxTranslationX, double maxTranslationY)
{
	// > GUI Layout
	QVBoxLayout *layout = new QVBoxLayout();

	// 	> Rotation
	QHBoxLayout *rotationLayout = new QHBoxLayout();
	rotationLayout->addWidget(new QLabel("rotation"));
	rotationLayout->addStretch();

	QSpinBox *rotationSpinBox = new QSpinBox();
	rotationSpinBox->setRange(-180, 180);
	rotationSpinBox->setValue(0);
	rotationSpinBox->setSuffix(QString::fromUtf8("��"));
	rotationLayout->addWidget(rotationSpinBox);
	layout->addLayout(rotationLayout);

	QDial *rotationDial = new QDial();
	rotationDial->setRange(-180, 180);
	rotationDial->setWrapping(true);
	rotationDial->setValue(0);
	layout->addWidget(rotationDial);

	connect(rotationSpinBox, SIGNAL(valueChanged(int)), rotationDial, SLOT(setValue(int)));
	connect(rotationDial, SIGNAL(valueChanged(int)), rotationSpinBox, SLOT(setValue(int)));
	connect(rotationDial, SIGNAL(valueChanged(int)), this, SLOT(UpdateRotation(int)));
	// 	< Rotation

	QFrame *line = new QFrame();
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	layout->addWidget(line);

	// 	> Scaling
	QHBoxLayout *scalingXLayout = new QHBoxLayout();
	scalingXLayout->addWidget(new QLabel("x scale"));
	scalingXLayout->addStretch();

	m_scaling_x_spin_box = new QDoubleSpinBox();
	m_scaling_x_spin_box->setRange(1.0 / maxScaleRatio, maxScaleRatio);
	m_scaling_x_spin_box->setValue(m_scaling_x);
	m_scaling_x_spin_box->setDecimals(2);
	m_scaling_x_spin_box->setSingleStep(0.1);
	scalingXLayout->addWidget(m_scaling_x_spin_box);
	layout->addLayout(scalingXLayout);

	m_scaling_x_slider = new QSlider(Qt::Horizontal);
	m_scaling_x_slider->setRange((1 - maxScaleRatio) * 100, (maxScaleRatio - 1) * 100);
	m_scaling_x_slider->setValue(0);
	layout->addWidget(m_scaling_x_slider);

	connect(m_scaling_x_spin_box, SIGNAL(valueChanged(double)), this, SLOT(UpdateScalingX(double)));
	connect(m_scaling_x_slider, SIGNAL(valueChanged(int)), this, SLOT(UpdateScalingX(int)));

	QHBoxLayout *scalingYLayout = new QHBoxLayout();
	scalingYLayout->addWidget(new QLabel("y scale"));
	scalingYLayout->addStretch();

	m_scaling_y_spin_box = new QDoubleSpinBox();
	m_scaling_y_spin_box->setRange(1.0 / maxScaleRatio, maxScaleRatio);
	m_scaling_y_spin_box->setValue(m_scaling_y);
	m_scaling_y_spin_box->setDecimals(2);
	m_scaling_y_spin_box->setSingleStep(0.1);
	scalingYLayout->addWidget(m_scaling_y_spin_box);
	layout->addLayout(scalingYLayout);

	m_scaling_y_slider = new QSlider(Qt::Horizontal);
	m_scaling_y_slider->setRange((1 - maxScaleRatio) * 100, (maxScaleRatio - 1) * 100);
	m_scaling_y_slider->setValue(0);
	layout->addWidget(m_scaling_y_slider);

	connect(m_scaling_y_spin_box, SIGNAL(valueChanged(double)), this, SLOT(UpdateScalingY(double)));
	connect(m_scaling_y_slider, SIGNAL(valueChanged(int)), this, SLOT(UpdateScalingY(int)));

	QCheckBox *aspectRatio = new QCheckBox("Maintain aspect ratio");
	aspectRatio->setChecked(m_aspect_ratio_maintained);
	UpdateAspectRatioMaintained(m_aspect_ratio_maintained);
	connect(aspectRatio, SIGNAL(toggled(bool)), this, SLOT(UpdateAspectRatioMaintained(bool)));

	layout->addWidget(aspectRatio);
	//	< Scaling

	line = new QFrame();
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	layout->addWidget(line);

	// 	> Translation
	QHBoxLayout *translationXLayout = new QHBoxLayout();
	translationXLayout->addWidget(new QLabel("x offset"));
	translationXLayout->addStretch();

	m_translation_x_spin_box = new QDoubleSpinBox();
	m_translation_x_spin_box->setRange(-maxTranslationX, maxTranslationX);
	m_translation_x_spin_box->setValue(m_translation_x);
	m_translation_x_spin_box->setDecimals(2);
	translationXLayout->addWidget(m_translation_x_spin_box);
	layout->addLayout(translationXLayout);

	m_translation_x_slider = new QSlider(Qt::Horizontal);
	m_translation_x_slider->setRange(-std::nearbyint(maxTranslationX * 100), std::nearbyint(maxTranslationX * 100));
	m_translation_x_slider->setValue(0);
	layout->addWidget(m_translation_x_slider);

	connect(m_translation_x_spin_box, SIGNAL(valueChanged(double)), this, SLOT(UpdateTranslationX(double)));
	connect(m_translation_x_slider, SIGNAL(valueChanged(int)), this, SLOT(UpdateTranslationX(int)));

	QHBoxLayout *translationYLayout = new QHBoxLayout();
	translationYLayout->addWidget(new QLabel("y offset"));
	translationYLayout->addStretch();

	m_translation_y_spin_box = new QDoubleSpinBox();
	m_translation_y_spin_box->setRange(-maxTranslationY, maxTranslationY);
	m_translation_y_spin_box->setValue(m_translation_y);
	m_translation_y_spin_box->setDecimals(2);
	translationYLayout->addWidget(m_translation_y_spin_box);
	layout->addLayout(translationYLayout);

	m_translation_y_slider = new QSlider(Qt::Horizontal);
	m_translation_y_slider->setRange(-std::nearbyint(maxTranslationY * 100), std::nearbyint(maxTranslationY * 100));
	m_translation_y_slider->setValue(0);
	layout->addWidget(m_translation_y_slider);

	connect(m_translation_y_spin_box, SIGNAL(valueChanged(double)), this, SLOT(UpdateTranslationY(double)));
	connect(m_translation_y_slider, SIGNAL(valueChanged(int)), this, SLOT(UpdateTranslationY(int)));
	// 	< Translation

	setLayout(layout); // < GUI Layout
}

} // namespace
