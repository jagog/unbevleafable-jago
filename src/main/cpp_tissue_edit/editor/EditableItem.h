#ifndef SIMPT_EDITOR_EDITABLE_ITEM_H_INCLUDED
#define SIMPT_EDITOR_EDITABLE_ITEM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for EditableItem.
 */

#include <QColor>
#include <QObject>

namespace SimPT_Editor {

/**
 * Interface for an editable graphical representation.
 */
class EditableItem : public QObject
{
	Q_OBJECT
public:
	/**
	 * Constructor.
	 */
	EditableItem() {};

	/**
	 * Destructor.
	 */
	virtual ~EditableItem() {};

	/**
	 * Checks whether the item is at the boundary.
	 *
	 * @return	True if the item is at the boundary.
	 */
	virtual bool IsAtBoundary() const = 0;

	/**
	 * Highlights the item in the canvas.
	 *
	 * @param	highlighted	True if the item should be highlighted.
	 */
	virtual void Highlight(bool highlighted) = 0;

	/**
	 * Set the highlight color to a given color.
	 *
	 * @param	color		The given color.
	 */
	virtual void SetHighlightColor(const QColor& color) = 0;

public slots:
	/**
	 * Update this item.
	 */
	virtual void Update() {};
};

} // namespace

#endif // end_of_include_guard
