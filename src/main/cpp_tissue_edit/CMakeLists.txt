#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Build objects, no install.
#============================================================================
#---------------------------- set variables ---------------------------------
set( LIB	simPTlib_tissue_edit )
set( SRC
		editor/BackgroundDialog.cpp
		editor/CopyAttributesDialog.cpp
		editor/EditableCellItem.cpp
		editor/EditableEdgeItem.cpp
		editor/EditableMesh.cpp
		editor/EditableNodeItem.cpp
		editor/EditControlLogic.cpp
		editor/EditorActions.cpp
		editor/PolygonUtils.cpp
		editor/PTreePanels.cpp
		editor/SelectByIDWidget.cpp
		editor/TissueEditor.cpp
		editor/TissueGraphicsView.cpp
		editor/TransformationWidget.cpp
		editor/UndoStack.cpp
		
		generator/RegularGeneratorDialog.cpp
		generator/RegularTiling.cpp
		generator/VoronoiGeneratorDialog.cpp
		generator/VoronoiTesselation.cpp
		generator/tiles/DiamondTile.cpp
		generator/tiles/HexagonalTile.cpp
		generator/tiles/RectangularTile.cpp
		generator/tiles/TriangularTile.cpp
		
		slicer/SliceItem.cpp
		slicer/TissueSlicer.cpp
)
set( MOC_HEADERS
		generator/RegularGeneratorDialog.h
		
		editor/BackgroundDialog.h
		editor/EditableItem.h
		editor/EditableCellItem.h
		editor/EditableEdgeItem.h
		editor/EditableNodeItem.h
		editor/EditControlLogic.h
		editor/EditorActions.h
		editor/PTreePanels.h
		editor/SelectByIDWidget.h
		editor/TissueEditor.h
		editor/TissueGraphicsView.h
		editor/TransformationWidget.h
		
	    slicer/SliceItem.h	
		slicer/TissueSlicer.h
)
set( MOC_OUTFILES )

#----------------- static lib: build, no install ----------------------------
if ( ${SIMPT_QT_VERSION} EQUAL 4 )
	qt4_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
elseif( ${SIMPT_QT_VERSION} EQUAL 5 )
	qt5_wrap_cpp( MOC_OUTFILES ${MOC_HEADERS} )
endif()
add_library( ${LIB} OBJECT ${SRC} ${MOC_OUTFILES} )			 

#-----------------------------unset variables -------------------------------
unset( MOC_HEADERS  )
unset( MOC_OUTFILES )
unset( SRC          )
unset( LIB          )

#############################################################################
