#ifndef SAVECHANGESDIALOG_H_INCLUDED
#define SAVECHANGESDIALOG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SaveChangesDialog.
 */

#include <map>
#include <vector>
#include <boost/property_tree/ptree.hpp>
#include <QDialog>

class QAbstractButton;
class QCheckBox;
class QPushButton;
class QTreeView;
class QVBoxLayout;

namespace SimShell {
namespace Gui {

class CheckableTreeModel;
class HasUnsavedChanges;

/**
 * Dialog window that displays a tree of objects containing unsaved changes.
 * For each object, a checkbox exists such that the user can select which objects to save and which to discard.
 *
 * Typically you want to show this dialog when closing an object.
 */
class SaveChangesDialog : public QDialog
{
    Q_OBJECT
public:
	/// Create and display a dialog from a HasUnsavedChanges interface.
	/// If user clicks "Save", all checked items will be saved.
	/// Unless user clicks "Cancel", all HasUnsavedChanges interfaces will be force-closed.
	/// @param parent      Dialog parent widget.
	/// @param prompt_map  A map containing PromptOnClose interfaces, indexed by some title. If empty, "true" will be returned immediately (no dialog will be shown).
	/// @return true if user clicked "Save" or "Discard", false if user clicked "Cancel".
	static bool Prompt(HasUnsavedChanges* widget, QWidget* parent = nullptr);

private slots:
	void SLOT_Clicked(QAbstractButton*);

private:
	SaveChangesDialog(const boost::property_tree::ptree&, QWidget* parent = 0);

	/// One of these values will be returned by the SaveChangesDialog::exec() method.
	enum Result
	{
		Save, Cancel, Discard
	};

	QPushButton*             m_save_button;
	QPushButton*             m_cancel_button;
	QPushButton*             m_discard_button;
	CheckableTreeModel*      m_model;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
