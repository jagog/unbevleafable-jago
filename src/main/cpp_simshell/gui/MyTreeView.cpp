/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MyTreeView.
 */

#include "MyTreeView.h"
#include "common/PTreeQtState.h"

using namespace std;
using namespace boost::property_tree;

namespace SimShell {
namespace Gui {

MyTreeView::MyTreeView(QWidget* parent)
	: QTreeView(parent)
{
}

ptree MyTreeView::GetPTreeState() const
{
	ptree result;
	if (model()) {
		result.put_child("treeview", PTreeQtState::GetTreeViewState(this));
	}
	//result.put_child("widget", PTreeQtState::GetWidgetState(this));
	return result;
}

void MyTreeView::SetPTreeState(const ptree& state)
{
	if (model()) {
		auto treeview_state = state.get_child_optional("treeview");
		if (treeview_state) {
			PTreeQtState::SetTreeViewState(this, treeview_state.get());
		}
	}

	/*auto widget_state = state.get_child_optional("widget");
	if (widget_state) {
		PTreeQtState::SetWidgetState(this, widget_state.get());
	}*/
}

} // namespace Gui
} // namespace SimShell
