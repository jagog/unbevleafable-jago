#ifndef GUI_HASUNSAVEDCHANGESPROMPT_H_INCLUDED
#define GUI_HASUNSAVEDCHANGESPROMPT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * HasUnsavedChangesPrompt interface.
 */

#include "HasUnsavedChanges.h"

class QWidget;

namespace SimShell {
namespace Gui {

/**
 * HasUnsavedChanges with the ability of displaying a "save-discard-cancel" dialog to the user before closing, if there are unsaved changes.
 */
class HasUnsavedChangesPrompt : public HasUnsavedChanges
{
public:
	HasUnsavedChangesPrompt(std::string&& title);

	/// Display dialog window containing a list (or tree) of widgets that contain unsaved changes.
	/// The user can select which widgets to save and which to discard, or to cancel the close operation.
	/// If there are no unsaved changes (in this widget and in its children), then no dialog is displayed and the method immediately returns true.
	/// @return Whether we are in unopened state, i.e. close operation was successful (regardless whether user saved or discarded changes)
	bool PromptClose(QWidget* parent = nullptr);
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
