#ifndef MY_FINDDIALOG_H_INCLUDED
#define MY_FINDDIALOG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MyFindDialog.
 */

#include <QCheckBox>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

namespace SimShell {
namespace Gui {

/**
 * Dialog window for searching strings.
 *
 * Also has a 'match case' checkbox.
 */
class MyFindDialog : public QDialog
{
	Q_OBJECT
public:
	MyFindDialog(QWidget* parent = 0);

	/**
	 * Sets focus to the text input box so the user can start typing right away.
	 */
	void CorrectFocus();

private slots:
	void Find();

signals:
	/**
	 * Emitted when user performs a search query by clicking 'Search'
	 * or pressing the Enter button.
	 * @param text         Text the user searches for.
	 * @param match_case   Search to be case-sensitive or not.
	 */
	void FindNext(QString const & text, bool match_case);

private:
	QPushButton*  m_close_button;
	QPushButton*  m_find_button;
	QLineEdit*    m_input;
	QLabel*       m_label;
	QCheckBox*    m_match_case;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
