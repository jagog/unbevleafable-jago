#ifndef NEWPROJECTDIALOG_H_INCLUDED
#define NEWPROJECTDIALOG_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for NewProjectDialog.
 */

#include "workspace/IWorkspace.h"

#include <QComboBox>
#include <QDialog>
#include <QFileDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>

#include <memory>

namespace SimShell {

namespace Ws {
	class IWorkspaceFactory;
}

namespace Gui {

/**
 * Dialog that asks user about information for setting up a new project.
 *
 * User has possibility to copy and existing project from the workspace template (see Ws::IWorkspaceFactory::GetWorkspaceTemplatePath()),
 * or using an existing file as basis for the new project.
 */
class NewProjectDialog : public QDialog
{
	Q_OBJECT
public:
	NewProjectDialog(const std::shared_ptr<Ws::IWorkspaceFactory>&, QWidget* parent = nullptr);

	/**
	 * Get project name string entered by user.
	 */
	std::string GetProjectName() const;

	/**
	 * Get path of custom file select by user.
	 */
	std::string GetSrcPath() const;

	/**
	 * Get project selected by user.
	 */
	Ws::IWorkspace::ConstProjectIterator GetSrcProject() const;

	/**
	 * Whether user wants to copy an existing project from template workspace or just copy a single data file.
	 */
	bool IsCopyProject() const;

private slots:
	/**
	 * Called when user clicks Cancel button.
	 * Sets dialog result to QDialog::Rejected.
	 */
	void Cancel();

	/**
	 * Called when user selected another option for data file.
	 * This is used to validate the form input.
	 */
	void FileChanged(bool use_default);

	/**
	 * Called when path to data file is changed by user.
	 * This is used to validate the form input.
	 */
	void FileChanged(QString const& filename);

	/**
	 * Called when project name entered in text box changed.
	 * This is used to validate the form input.
	 */
	void ProjectNameChanged(QString const& name);

	/**
	 * Called when user clicks OK button.
	 * Sets dialog result to QDialog::Accepted.
	 */
	void Ok();

	/**
	 * Displays dialog to browse for a data file.
	 * Called when user clicks browse button.
	 */
	void ShowBrowseDialog();

private:
	/**
	 * Checks if form input is valid and enables/disables Ok button accordingly.
	 */
	void ValidateForm();

private:
	bool             project_name_ok;
	bool             file_ok;

	std::shared_ptr<Ws::IWorkspace>  m_workspace_model;

	QPushButton*     m_button_cancel;
	QPushButton*     m_button_ok;
	QComboBox*       m_combo_models;
	QFileDialog*     m_dialog;
	QLineEdit*       m_edit_custom;
	QLineEdit*       m_edit_name;
	QRadioButton*    m_radio_custom;
	QRadioButton*    m_radio_default;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
