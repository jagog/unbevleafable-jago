/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * SessionController implementation.
 */

#include "SessionController.h"

#include "util/misc/Exception.h"
#include <cpp_logging/logger.h>

#include <QAction>
#include <QApplication>
#include <QMessageBox>
#include <QTimer>

using namespace std;
using namespace SimPT_Sim::Util;

namespace SimShell {
namespace Gui {
namespace Controller {

SessionController::SessionController(QWidget* parent)
	: QWidget(parent)
{
	qRegisterMetaType< std::string >();

	m_action_run = new QAction("Run", this);
	m_action_run->setCheckable(true);
	m_action_run->setEnabled(false);
	m_action_run->setShortcut(QKeySequence("R"));
	m_action_run->setShortcutContext(Qt::ApplicationShortcut);
	connect(m_action_run, SIGNAL(toggled(bool)), this, SLOT(SLOT_ToggleRunning(bool)));

	m_action_single_step = new QAction("Single Step", this);
	m_action_single_step->setEnabled(false);
	m_action_single_step->setShortcut(QKeySequence("S"));
	m_action_single_step->setShortcutContext(Qt::ApplicationShortcut);
	connect(m_action_single_step, SIGNAL(triggered()), this, SLOT(SLOT_TimeStep()));

	m_enabled_actions.Add(m_action_run);
	m_enabled_actions.Add(m_action_single_step);

	// For the shortcut keys to work the action has to be added to active widget also.
	addAction(m_action_run);
	addAction(m_action_single_step);
}

void SessionController::Disable()
{
	m_project.reset();
	m_action_run->setChecked(false);
	m_enabled_actions.Disable();
}

void SessionController::Enable(const shared_ptr<Ws::IProject>& project)
{
	Disable();
	m_project = project;
	if (m_project->IsOpened()) {
		connect(&m_project->Session(),
			SIGNAL(InfoMessage(const std::string&, const InfoMessageReason&)),
			this, SLOT(SimulationInfo(const std::string&, const InfoMessageReason&)));
                connect(&m_project->Session(),
                	SIGNAL(ErrorMessage(const std::string&)),
                	this, SLOT(SimulationError(const std::string&)));
		m_enabled_actions.Enable();
	}
}

QAction* SessionController::GetActionRun() const
{
	return m_action_run;
}

QAction* SessionController::GetActionSingleStep() const
{
	return m_action_single_step;
}

bool SessionController::IsEnabled() const
{
	return (bool) m_project;
}

bool SessionController::IsRunning() const
{
	return m_action_run->isChecked();
}

void SessionController::SetRunning(bool b)
{
	if (m_action_run->isEnabled())
		m_action_run->setChecked(b);
}

void SessionController::SimulationError(const std::string& error)
{
	SimPT_Logging::SimshellLogger::get()->error("{} Exception> {}\n", LOG_TAG_28, error);
	QString qmess = QString("Exception:\n%1\n").arg(error.c_str());
	QMessageBox::information(0, "Critical Error", qmess);
}

void SessionController::SimulationInfo(const std::string& ,
		const Session::ISession::InfoMessageReason& reason)
{
        switch (reason) {
        case Session::ISession::InfoMessageReason::Stopped:
                SetRunning(false);
                break;
        case Session::ISession::InfoMessageReason::Terminated:
                Disable();
                break;
        default:
                break;
        }
}

void SessionController::SLOT_ToggleRunning(bool b)
{
	m_action_single_step->setEnabled(!b);

	if (m_project) {
		if (m_project->IsOpened()) {
			if (b) {
				m_project->Session().StartSimulation();
			} else {
				m_project->Session().StopSimulation();
			}
		}
	}
}

void SessionController::SLOT_TimeStep()
{
	if (m_project->IsOpened()) {
		m_project->Session().TimeStep();
	}
}

} // namespace Controller
} // namespace Gui
} // namespace SimShell
