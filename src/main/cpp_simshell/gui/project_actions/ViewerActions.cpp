/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * ViewerActions implementation.
 */

#include "ViewerActions.h"

#include <QAction>
#include <QMenu>

using namespace std;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

ViewerActions::ViewerActions(const shared_ptr<Viewer::IViewerNode>& root_node)
{
	function<QMenu*(const shared_ptr<Viewer::IViewerNode>&, QAction*)> init_recursive;
	init_recursive = [&](const shared_ptr<Viewer::IViewerNode>& root_node, QAction* parent_action)
		-> QMenu*
	{
		QMenu* result = new QMenu("Viewers");
		for (auto& node : *root_node) {
			result->addSeparator();
			//result->addAction(QString::fromStdString(node.first))->setEnabled(false);
			QAction* enable_action = result->addAction(QString::fromStdString(node.first));
			enable_action->setCheckable(true);
			if (node.second->IsEnabled())
				enable_action->setChecked(true);
			enable_action->setEnabled(node.second->IsParentEnabled());
			m_node_map.insert({enable_action, node.second});
			m_children_map[parent_action].push_back(enable_action);
			connect(enable_action, SIGNAL(toggled(bool)), this, SLOT(SLOT_Toggle(bool)));
			auto children_menu = init_recursive(node.second, enable_action);
			if (children_menu->actions().size())
				result->addMenu(children_menu);
		}
		return result;
	};
	m_menu = init_recursive(root_node, nullptr);
}

ViewerActions::~ViewerActions()
{
}

shared_ptr<ViewerActions> ViewerActions::Create(const shared_ptr<Viewer::IViewerNode>& root_node)
{
	auto result = shared_ptr<ViewerActions>(new ViewerActions(root_node));

	for (auto& val : result->m_node_map) {
		val.second->Register(val.second, [&](const Viewer::Event::ViewerEvent& e) {
			auto type = e.GetType();
			bool checked = type == Viewer::Event::ViewerEvent::Enabled;
			static_cast<QAction*>(val.first)->setChecked(checked);
		});
	}

	return result;
}

QMenu* ViewerActions::GetMenu() const
{
	return m_menu;
}

void ViewerActions::SLOT_Toggle(bool checked)
{
	auto& node = m_node_map[sender()];

	if (checked) {
		node->Enable();
	} else {
		node->Disable();
	}

	function<void(vector<QAction*>& children)> work_on_children;
	work_on_children = [&](vector<QAction*>& children) {
		for (auto a : children) {
			a->setEnabled(m_node_map[a]->IsParentEnabled());
			work_on_children(m_children_map[a]);
		}
	};
	work_on_children(m_children_map[sender()]);
}

} // namespace ProjectActions
} // namespace Gui
} // namespace SimShell
