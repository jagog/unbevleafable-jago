#ifndef GUI_PROJECTACTIONS_VIEWERACTIONS_H_INCLUDED
#define GUI_PROJECTACTIONS_VIEWERACTIONS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * ViewerActions header.
 */

#include "viewer/IViewerNode.h"

#include <QObject>
#include <map>
#include <memory>
#include <vector>

class QAction;
class QMenu;

namespace SimShell {
namespace Gui {
namespace ProjectActions {

/**
 * Given a so-called 'root' viewer instance, constructs a menu representing the hierarchical structure of viewers.
 * The menu has actions for enabling/disabling viewers at any level in the hierarchy.
 * This object maintains ownership of those actions, so a long as they are available to the user (e.g. added to a menu), this object should NOT be destroyed.
 */
class ViewerActions : public QObject
{
	Q_OBJECT
public:
	/// Construct instance of this class.
	/// @param root_viewer   Viewer instance acting as the root of the tree of viewers.
	static std::shared_ptr<ViewerActions> Create(const std::shared_ptr<Viewer::IViewerNode>& root_viewer);

	virtual ~ViewerActions();

	/// Get menu.
	/// This class keeps ownership over returned object.
	QMenu* GetMenu() const;

private slots:
	void SLOT_Toggle(bool);

private:
	ViewerActions(const std::shared_ptr<Viewer::IViewerNode>& root_viewer);

	QMenu*                                                     m_menu;         ///< List of QAction objects associated with Export callbacks.
	std::map<QObject*, std::shared_ptr<Viewer::IViewerNode>>   m_node_map;     ///< Mapping from QAction objects to callbacks.
	std::map<QObject*, std::vector<QAction*>>                  m_children_map; ///< For disabling viewers that depend on other viewers when those viewers are disabled.
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
