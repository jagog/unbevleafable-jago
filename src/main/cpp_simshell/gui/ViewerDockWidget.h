#ifndef GUI_VIEWERDOCKWIDGET_H_INCLUDED
#define GUI_VIEWERDOCKWIDGET_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ViewerDockWidget.
 */

#include "workspace/MergedPreferences.h"

#include <QDockWidget>
#include <functional>

namespace SimShell {
namespace Gui {

/**
 * A dock widget containing visual output of a viewer. (e.g. LOG viewer)
 * A callback can be performed when the window is closed.
 */
class ViewerDockWidget : public QDockWidget
{
	Q_OBJECT
public:
	/**
	 * @param parent    Parent widget.
	 * @param on_close  Callback to be performed when widget is closed.
	 */
	ViewerDockWidget(Qt::DockWidgetArea,
	                 const std::shared_ptr<Ws::MergedPreferences>&,
	                 QWidget* parent = nullptr,
	                 std::function<void()> on_close = std::function<void()>());

	virtual ~ViewerDockWidget();

protected:
	/// overrides QWidget::closeEvent
	/// @see QWidget::closeEvent
	virtual void closeEvent(QCloseEvent*);

	std::shared_ptr<Ws::MergedPreferences>   m_preferences;

private:
	QAction*                                 m_toggle;
	std::function<void()>                    m_on_close;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
