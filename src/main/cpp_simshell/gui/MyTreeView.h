#ifndef GUI_MYTREEVIEW_H_INCLUDED
#define GUI_MYTREEVIEW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MyTreeView.
 */

#include "IHasPTreeState.h"

#include <boost/property_tree/ptree.hpp>
#include <QTreeView>

namespace SimShell {
namespace Gui {

/**
 * QTreeView with methods to import/export widget layout (x,y,width, ...) in ptree format.
 *
 * @see QTreeView IHasPTreeState
 */
class MyTreeView : public QTreeView, public IHasPTreeState
{
	Q_OBJECT
public:
	MyTreeView(QWidget* parent = nullptr);

	/// @see IHasPTreeState::GetPTreeState().
	virtual boost::property_tree::ptree GetPTreeState() const;

	/// @see IHasPTreeState::SetPTreeState().
	virtual void SetPTreeState(const boost::property_tree::ptree&);
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
