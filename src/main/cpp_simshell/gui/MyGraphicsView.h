#ifndef MYGRAPHICSVIEW_H_INCLUDED
#define MYGRAPHICSVIEW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MyGraphicsView.
 */

#include <QGraphicsView>
#include <memory>

class QWheelEvent;

namespace SimShell {
namespace Gui {

/**
 * Scrollable QGraphicsView with zoom functionality. Also takes co-ownership of graphics scene.
 */
class MyGraphicsView: public QGraphicsView
{
	Q_OBJECT
public:
	/**
	 * @param g       Graphics scene. Will take co-ownership.
	 * @param parent  Parent widget.
	 */
	MyGraphicsView(std::shared_ptr<QGraphicsScene> g, QWidget* parent = nullptr);

	/**
	 * @param parent  Parent widget.
	 */
	MyGraphicsView(QWidget* parent = nullptr);

protected:
	void wheelEvent(QWheelEvent* event);

private:
	std::shared_ptr<QGraphicsScene> m_scene;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
