#ifndef PTREE_EDITOR_WINDOW_H_INCLUDED
#define PTREE_EDITOR_WINDOW_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for PTreeEditorWindow.
 */

#include "IHasPTreeState.h"
#include "gui/HasUnsavedChanges.h"

#include <QMainWindow>
#include <boost/property_tree/ptree.hpp>

class QAction;
class QCloseEvent;
class QStatusBar;
class QToolBar;

namespace SimShell {
namespace Gui {

class PTreeView;
class PTreeModel;

/**
 * Small editor widget for a Boost Property Tree object.
 * Each manipulation by the user immediately results in an Apply() signal.
 */
class PTreeEditorWindow : public QMainWindow, public HasUnsavedChanges, public IHasPTreeState
{
	Q_OBJECT
public:
	/// @param parent   Parent widget.
	PTreeEditorWindow(QWidget* parent = nullptr);

	/// Virtual destructor.
	virtual ~PTreeEditorWindow() {}

	/**
	 * Get the current edit path.
	 * The edit path is the subtree that is shown in the editor.
	 * The rest of the ptree is hidden.
	 * If this value is empty, the entire ptree is shown.
	 */
	std::string GetEditPath() const;

	/// @see IHasPTreeState::GetPTreeState().
	virtual boost::property_tree::ptree GetPTreeState() const;

	/**
	 * Test whether the "only edit data" option is set.
	 * If true, the user can only edit values in the 'data' column.
	 * The user cannot add/move/remove/rename keys.
	 */
	bool IsOnlyEditData() const;

	/// @see HasUnsavedChanges
	virtual bool IsOpened() const;

	/// Redo last manipulation by the user.
	void Redo();

	/// Reimplemented from QWidget.
	virtual QSize sizeHint() const;

	/**
	 * Set the "only edit values" option.
	 * @see IsOnlyEditValues()
	 */
	void SetOnlyEditData(bool);

	/// @see IHasPTreeState::SetPTreeState().
	virtual void SetPTreeState(const boost::property_tree::ptree&);

	/// Undo last manipulation by the user.
	void Undo();


public slots:
	/// Write changes to ptree object. Clean state is achieved as a side effect.
	void Apply();

	/**
	 * Set subtree to show in editor.
	 * If the edit path is different from the current one,
	 * a dialog asking to apply unapplied changes will be shown.
	 * @return true if path was set successfully
	 */
	bool OpenPath(const QString & edit_path);

	/**
	 * Set ptree to show in editor.
	 * A confirmation dialog is shown if there are unapplied changes.
	 * @return true if we are in opened state.
	 */
	bool OpenPTree(const boost::property_tree::ptree&, QString const& edit_path = "");

signals:
	/// Emitted when the window has information available, for e.g. a statusbar.
	void StatusChanged(const QString&);

	/// Emitted when changes are applied to ptree.
	void ApplyTriggered(const boost::property_tree::ptree& pt);

protected:
	/// @see HasUnsavedChanges
	virtual void InternalForceClose();

	/// @see HasUnsavedChanges
	virtual bool InternalIsClean() const;

	/// @see HasUnsavedChanges
	virtual bool InternalSave();

private:
	boost::property_tree::ptree    m_pt;
	std::string                    m_edit_path;
	bool                           m_only_edit_data;

private:
	PTreeModel*       m_model;               // equal to zero <=> unopened state
	PTreeView*        m_treeview;
	QToolBar*         m_toolbar_main;

	QAction*          m_action_show_toolbar_main;

private:
	static int const  g_column_width;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
