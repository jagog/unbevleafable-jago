/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for HasUnsavedChanges.
 */

#include "HasUnsavedChanges.h"

using namespace std;

namespace SimShell {
namespace Gui {

HasUnsavedChanges::HasUnsavedChanges(string&& title)
	: m_title(move(title))
{
}

HasUnsavedChanges::~HasUnsavedChanges()
{
}

HasUnsavedChanges::ChildrenType::iterator HasUnsavedChanges::begin()
{
	return m_children.begin();
}

HasUnsavedChanges::ChildrenType::const_iterator HasUnsavedChanges::begin() const
{
	return m_children.begin();
}

HasUnsavedChanges::ChildrenType::iterator HasUnsavedChanges::end()
{
	return m_children.end();
}

HasUnsavedChanges::ChildrenType::const_iterator HasUnsavedChanges::end() const
{
	return m_children.end();
}

void HasUnsavedChanges::ForceClose()
{
	if (IsOpened()) {
		InternalPreForceClose();
		// Force close children first
		for (auto child : m_children) {
			child->ForceClose();
		}
		InternalForceClose();
	}
}

const string& HasUnsavedChanges::GetTitle()
{
	return m_title;
}

bool HasUnsavedChanges::IsClean() const
{
	if (!InternalIsClean()) {
		return false;
	}
	for (auto child : m_children) {
		if (!child->IsClean())
			return false;
	}
	return true;
}

bool HasUnsavedChanges::Save()
{
	// Save children first
	for (auto child : m_children) {
		if (!child->Save())
			return false;
	}
	if (IsOpened()) {
		return InternalSave();
	} else {
		return true;
	}
}

bool HasUnsavedChanges::SaveAndClose()
{
	if (Save()) {
		ForceClose();
		return true;
	} else {
		return false;
	}
}

void HasUnsavedChanges::AddChild(HasUnsavedChanges::ChildrenType::value_type&& v)
{
	m_children.push_back(move(v));
}

void HasUnsavedChanges::SetChildren(HasUnsavedChanges::ChildrenType&& c)
{
	m_children = move(c);
}

} // namespace Gui
} // namespace SimShell
