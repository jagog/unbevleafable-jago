/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PTreeComparison.
 */

#include "PTreeComparison.h"

#include "util/misc/Exception.h"
#include "util/misc/StringUtils.h"
#include <cpp_logging/logger.h>

#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <cmath>
#include <iostream>
#include <map>
#include <string>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using boost::optional;
using namespace SimPT_Sim::Util;

namespace SimPT_Shell {

bool PTreeComparison::CompareRootValues(ptree const& pt1, ptree const& pt2, double acceptable_diff)
{
	std::string pt1_value = pt1.get_value<std::string>();
	std::string pt2_value = pt2.get_value<std::string>();

	if (acceptable_diff > 1.0e-15) {
		bool pt1_value_is_number = false;
		bool pt2_value_is_number = false;

		double pt1_value_parsed = 0.0;
		double pt2_value_parsed = 0.0;

		try {
			pt1_value_parsed = pt1.get_value<double>();
			pt1_value_is_number = true;
		} catch (ptree_bad_data &) {
			pt1_value_is_number = false;
		}

		try {
			pt2_value_parsed = pt2.get_value<double>();
			pt2_value_is_number = true;
		} catch (ptree_bad_data &) {
			pt2_value_is_number = false;
		}

		if (pt1_value_is_number && pt2_value_is_number) {
			if (pt1_value.find('.') == std::string::npos
			                && pt2_value.find('.') == std::string::npos) {
				// both values are integers => must be exactly the same
				return pt1_value == pt2_value;
			} else {
				double const diff = fabs(pt2_value_parsed - pt1_value_parsed)
				                / (1.0 + fabs(pt1_value_parsed));
				bool equal = (diff < acceptable_diff);
				if (!equal) {
					SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareRootValues> Diff too large: {}\n", LOG_TAG_27, diff);
				}
				return equal;
			}
		}
	}

	return pt1_value == pt2_value;
}

bool PTreeComparison::CompareArray(ptree const& pt1, ptree const& pt2, double acceptable_diff)
{

	if (!CompareRootValues(pt1, pt2, acceptable_diff)) {
		SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareArray> Root values not equal\n", LOG_TAG_27);
		return false;
	}

	if (pt1.size() != pt2.size()) {
		SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareArray> Different number of children.\n", LOG_TAG_27);
		return false;
	}

	auto pt1_child_entry = pt1.begin();
	auto pt2_child_entry = pt2.begin();
	while (pt1_child_entry != pt1.end() && pt2_child_entry != pt2.end()) {
		if (pt1_child_entry->first != pt2_child_entry->first) {
			SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareArray> Array elements have different keys.\n", LOG_TAG_27);
			return false;
		}

		if (pt1_child_entry->first.rfind("_array") == pt1_child_entry->first.length()-6) {
			if (!CompareArray(pt1_child_entry->second, pt2_child_entry->second, acceptable_diff)) {
				return false;
			}
		} else {
			if (!CompareNonArray(pt1_child_entry->second, pt2_child_entry->second, acceptable_diff)) {
				SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareNonArray> Subtree: {}\n", LOG_TAG_27, pt1_child_entry->first);
				return false;
			}
		}

		pt1_child_entry++;
		pt2_child_entry++;
	}
	return true;
}

bool PTreeComparison::CompareNonArray(ptree const& pt1, ptree const& pt2, double acceptable_diff)
{

	if (!CompareRootValues(pt1, pt2, acceptable_diff)) {
		SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareNonArray> Root values not equal\n", LOG_TAG_27);
		return false;
	}

	if (pt1.size() != pt2.size()) {
		SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareNonArray> Different number of children.\n", LOG_TAG_27);
		return false;
	}

	for (auto const& pt1_child_entry : pt1) {
		ptree const & pt1_child = pt1_child_entry.second;
		optional<ptree const&> pt2_child = pt2.get_child_optional(pt1_child_entry.first);

		if (!pt2_child) {
			SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareNonArray> Child doesn't exist: {}\n", LOG_TAG_27, pt1_child_entry.first);
			return false;
		}

		if (pt1_child_entry.first.rfind("_array") == pt1_child_entry.first.length()-6) {
			if (!CompareArray(pt1_child, *pt2_child, acceptable_diff)) {
				return false;
			}
		} else {
			if (!CompareNonArray(pt1_child, *pt2_child, acceptable_diff)) {
				SimPT_Logging::ParexLogger::get()->debug("{} LeafTransformer::CompareNonArray> Subtree: {}\n", LOG_TAG_27, pt1_child_entry.first);
				return false;
			}
		}
	}
	return true;
}

} // namespace
