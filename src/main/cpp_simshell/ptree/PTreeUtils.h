#ifndef PTREEUTILS_H_INCLUDED
#define PTREEUTILS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * PTreeUtils interface.
 */

#include <boost/property_tree/ptree_fwd.hpp>

#include <list>
#include <set>
#include <string>
#include <utility>

namespace SimPT_Sim {
namespace Util {

/**
 * A collection of functions for manipulating the structure of ptrees.
 */
class PTreeUtils
{
public:
	/**
	 * Copies every node from 'source' ptree if that node doesn't exist in 'destination' ptree.
	 *
	 * @param src       'source' ptree.
	 * @param dst       'destination' ptree.
	 * @param ignore    A list of node keys not to copy. (they are skipped).
	 */
	static void CopyNonExistingChildren(
		const boost::property_tree::ptree& src,
		boost::property_tree::ptree& dst,
		const std::set<std::string>& ignore = std::set<std::string>());

	static void CopyStructure(
		const boost::property_tree::ptree& src,
		boost::property_tree::ptree& dst,
		const std::string& fill_value,
		const std::set<std::string>& ignore = std::set<std::string>());

	/**
	 * Removes every node in 'destination' ptree if that node doesn't exist in 'source' ptree.
	 *
	 * @param src       'source' ptree.
	 * @param dst       'destination' ptree.
	 * @param ignore    A list of node keys not to remove. (they are skipped).
	 */
	static void RemoveNonExistingChildren(
		const boost::property_tree::ptree& src,
		boost::property_tree::ptree& dst,
		const std::set<std::string>& ignore = std::set<std::string>());

	/**
	 * Fill the first ptree with the second ptree.
	 * The ptrees should have the same structures.
	 *
	 * @param	pt1		The first ptree.
	 * @param	pt2		The second ptree.
	 */
	static void FillPTree(boost::property_tree::ptree& pt1, const boost::property_tree::ptree& pt2);

	/**
	 * Flattens a ptree from a given path.
	 *
	 * @param	pt		The given ptree.
	 * @param	indexedArray	Whether to append indexes '[i]' to children of '*_array' entries of the ptree.
	 * @param	path		The given path (shouldn't be given when the full ptree should be flatten).
	 * @return	A list with for each path in the ptree, the associated value.
	 */
	static std::list<std::pair<std::string, std::string>>
	Flatten(const boost::property_tree::ptree& pt, bool indexedArray = false, const std::string& path = "");

	/**
	 * Returns a reference to the subptree in the given ptree, taking indexes of
	 * array-elements (of PTreeUtils::Flatten function) into account.
	 *
	 * @param	pt		The given ptree.
	 * @param	path		The given path.
	 * @return	The subtree to get.
	 */
	static const boost::property_tree::ptree&
	GetIndexedChild(const boost::property_tree::ptree& pt, const std::string& path);

	/**
	 * Put the child in the given ptree on the given path. The path can contain indexes.
	 * The path can't be empty and when a node doesn't exist, it will only be created when it has
	 * no index or when the index is zero.
	 *
	 * @param	pt		The given ptree.
	 * @param	path		The given path.
	 * @param	child		The given child.
	 */
	static void PutIndexedChild(
		boost::property_tree::ptree& pt,
		const std::string& path,
		const boost::property_tree::ptree& child);
};

} // namespace
} // namespace

#endif // include-guard
