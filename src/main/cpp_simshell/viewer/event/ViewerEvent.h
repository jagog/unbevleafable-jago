#ifndef VIEWEREVENT_H_INCLUDED
#define VIEWEREVENT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for ViewerEvent.
 */

#include <memory>

namespace SimShell {
namespace Viewer {

class IViewerNode;

namespace Event {

/**
 * An event transmitted by viewers to notify whether they are enabled or disabled.
 */
class ViewerEvent {
public:
	using Source = std::shared_ptr<IViewerNode>;

	enum Type { Enabled, Disabled };

	ViewerEvent(Source s, Type t) : m_source(s), m_type(t) {}

	Source GetSource() const { return m_source; }

	Type GetType() const { return m_type; }

private:
	Source   m_source;
	Type     m_type;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
