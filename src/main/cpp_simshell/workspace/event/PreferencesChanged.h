#ifndef WS_EVENT_PREFERENCESCHANGED_H_INCLUDED
#define WS_EVENT_PREFERENCESCHANGED_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Definition for PreferencesChanged.
 */

#include <boost/property_tree/ptree_fwd.hpp>

namespace SimShell {
namespace Ws {
namespace Event {

/**
 * Event used to inform some observer that preferences have changed.
 *
 * Really a POD but packaged to force users to initialize all data members.
 */
class PreferencesChanged
{
public:
	PreferencesChanged(const boost::property_tree::ptree& p)
		: m_preferences(p) {}

	/// Get preferences.
	const boost::property_tree::ptree& GetPreferences() const { return m_preferences;}

private:
	const boost::property_tree::ptree&  m_preferences;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
