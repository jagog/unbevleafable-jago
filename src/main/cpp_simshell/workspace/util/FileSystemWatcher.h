#ifndef WS_UTIL_FILESYSTEMWATCHER_H_INCLUDED
#define WS_UTIL_FILESYSTEMWATCHER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Definition for FileSystemWatcher.
 */

#include <iostream>
#include <functional>
#include <QFileSystemWatcher>

namespace SimShell {
namespace Ws {
namespace Util {

/**
 * Utility class for being informed about changes to a file/directory on the filesystem.
 *
 * Main purpose is to translate Qt signals to a single callback function, so it can be used by
 * the Workspace/Project template classes. We can't use Qt signals/slots in templates.
 */
class FileSystemWatcher : public QObject
{
	Q_OBJECT
public:
	/**
	 * @param  path      Filesystem path to watch.
	 * @param  callback  Callback function to call whenever something changes at the given path.
	 */
	FileSystemWatcher(const std::string& path, std::function<void()> callback)
		: p(path),
		  c(callback),
		  active(false)
	{
		w.addPath(QString::fromStdString(p));

		SetActive(true);
	}

	/**
	 * Move constructor.
	 */
	FileSystemWatcher(FileSystemWatcher&& other)
		: p(move(other.p)),
		  c(move(other.c)),
		  active(false)
	{
		w.addPath(QString::fromStdString(p));

		SetActive(other.active);
	}

	/**
	 * Get 'active' property, i.e. whether the callback function is called when something changes at the path.
	 * Default for this property is true.
	 * @see SetActive
	 */
	bool IsActive() const
	{
		return active;
	}

	/**
	 * Set 'active' property, i.e. whether the callback function is called when something changes at the path.
	 * Default for this property is true.
	 * @see GetActive
	 */
	void SetActive(bool a)
	{
		if (active != a) {
			if (a) {
				connect(&w, SIGNAL(directoryChanged(const QString&)), this, SLOT(SLOT_Changed(const QString&)));
				connect(&w, SIGNAL(fileChanged(const QString&)), this, SLOT(SLOT_Changed(const QString&)));
			} else {
				disconnect(&w, 0, 0, 0); // disconnect everything connected to w's signals.
			}
			active = a;
		}
	}

private slots:
	void SLOT_Changed(const QString& ) {
		c();
	}

private:
	QFileSystemWatcher      w;
	std::string             p;
	std::function<void()>   c;
	bool                    active;
};

} // namespace
} // namespace
} // namespace

#endif // end_of_inclde_guard
