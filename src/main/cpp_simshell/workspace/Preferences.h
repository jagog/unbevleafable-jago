#ifndef WS_PREFERENCES_H_INCLUDED
#define WS_PREFERENCES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Preferences.
 */

#include "IPreferences.h"

#include <string>
#include <boost/property_tree/ptree.hpp>

namespace SimShell {
namespace Ws {

/**
 * Implementation of IPreferences.
 *
 * An object that contains a tree of preferences, and reads/writes those from/to a file.
 */
class Preferences : public virtual IPreferences
{
public:
	virtual ~Preferences() {}

	Preferences(const std::string& file);

	Preferences(Preferences&&);

	virtual const boost::property_tree::ptree& GetPreferences() const;

	virtual void SetPreferences(const boost::property_tree::ptree&);

protected:
	boost::property_tree::ptree   m_preferences;

private:
	/// Forbid copy constructor.
	Preferences(const Preferences&);

	/// Forbid copy assignment.
	Preferences& operator=(const Preferences&);

private:
	const std::string             m_file;
};

} // namespace
} // namespace

#endif // end_of_inclde_guard
