/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep for Blad model.
 */

#include "Blad.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/Mesh.h"

namespace SimPT_Blad {
namespace CellHousekeep {

using boost::property_tree::ptree;

Blad::Blad(const CoreData& cd)
{
	Initialize(cd);
}

void Blad::Initialize(const CoreData& cd)
{
	m_cd = cd;

	const auto& p				= m_cd.m_parameters->get_child("blad");
	m_M_threshold_expansion	        	= p.get<double>("M_threshold_expansion");
	m_relative_growth			= p.get<double>("relative_growth");
}

void Blad::operator()(Cell* cell)
{
	const double chem1      = cell->GetChemical(1);
	const double t_area     = cell->GetTargetArea();
	const double a_area	= cell->GetArea();

	if (cell->GetBoundaryType() == BoundaryType::None) {
		if ( ( chem1 / a_area ) > m_M_threshold_expansion ) {
			const double incr           = m_relative_growth * a_area; //This is for 240 minutes time step!
			const double update_t_area  = t_area + incr;
			cell->SetTargetArea(update_t_area);
		}
	}
}

} // namespace
} // namespace
