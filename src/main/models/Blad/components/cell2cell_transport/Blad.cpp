/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransport for Blad model.
 */

#include "Blad.h"

#include "bio/Cell.h"
#include "bio/Wall.h"
 
namespace SimPT_Blad {
namespace CellToCellTransport {

using namespace std;
using namespace boost::property_tree;

Blad::Blad(const CoreData& cd)
{
        Initialize(cd);
}

void Blad::Initialize(const CoreData& cd)
{
        m_cd    = cd;
        const auto& p            = m_cd.m_parameters;
        m_M_diffusion_constant   = p->get<double>("blad.M_diffusion_constant");
}

void Blad::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	/*if ( !(w->GetC1()->IsBoundaryPolygon()) && !(w->GetC2()->IsBoundaryPolygon()) )
	{
		const double apoplast_thickness = 2.;

		const double phi = (w->GetLength() / apoplast_thickness) * m_d * ( ( w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()) )
				- ( w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()) ) ); //LEVELS!

		dchem_c1[0] += phi; //LEVELS!
		dchem_c2[0] -= phi; //LEVELS!

		// Active fluxes (PIN1 mediated transport) (Transporters measured in moles, here)
		const double k_import = 60.;
		const double k_export = m_transport;

		// transport ((RM: efflux)) from cell 1 to cell 2
		const double trans12 = w->GetLength() * ( w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()) ) * (k_export * w->GetTransporters1(1) + k_import);//LEVELS!

		// transport ((RM: efflux)) from cell 2 to cell 1
		const double trans21 = w->GetLength() * ( w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()) ) * (k_export * w->GetTransporters2(1) + k_import);//LEVELS!

		dchem_c1[0] += (trans21 - trans12);//LEVELS!
		dchem_c2[0] += (trans12 - trans21);//LEVELS!

		//DDV2012: diffusive hormone - chemical '1'
		const double phi2 = (w->GetLength() / ( apoplast_thickness ) ) * 4*(24./60/10) * ( ( w->GetC2()->GetChemical(1) / (w->GetC2()->GetArea()) )
			- ( w->GetC1()->GetChemical(1) / (w->GetC1()->GetArea()) ) ); //LEVELS!

		dchem_c1[1] += phi2 ; //LEVELS!
		dchem_c2[1] -= phi2 ; //LEVELS!
	}*/

	if ( !(w->GetC1()->IsBoundaryPolygon()) && !(w->GetC2()->IsBoundaryPolygon()) )
	{
		const double apoplast_thickness = 2.;

//		const double phi = (w->GetLength() / apoplast_thickness) * m_d * ( ( w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()) )
//				- ( w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()) ) ); //LEVELS!
//
//		dchem_c1[0] += phi; //LEVELS!
//		dchem_c2[0] -= phi; //LEVELS!

		// Active fluxes (PIN1 mediated transport) (Transporters measured in moles, here)
//		const double k_import = 60.;
//		const double k_export = m_transport;

		// transport ((RM: efflux)) from cell 1 to cell 2
//		const double trans12 = w->GetLength() * ( w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()) ) * (k_export * w->GetTransporters1(1) + k_import);//LEVELS!

		// transport ((RM: efflux)) from cell 2 to cell 1
//		const double trans21 = w->GetLength() * ( w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()) ) * (k_export * w->GetTransporters2(1) + k_import);//LEVELS!

//		dchem_c1[0] += (trans21 - trans12);//LEVELS!
//		dchem_c2[0] += (trans12 - trans21);//LEVELS!

		//DDV2012: morphogen - chemical '1'
		const double phi2 = (w->GetLength() / ( apoplast_thickness ) ) * m_M_diffusion_constant * ( ( w->GetC2()->GetChemical(1) / (w->GetC2()->GetArea()) )
			- ( w->GetC1()->GetChemical(1) / (w->GetC1()->GetArea()) ) ); //LEVELS!

		dchem_c1[1] += phi2 ; //LEVELS!
		dchem_c2[1] -= phi2 ; //LEVELS!
	}
}

} // namespace
} // namespace

