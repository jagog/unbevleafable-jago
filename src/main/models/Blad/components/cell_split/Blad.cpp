/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit for Blad model.
 */

#include "Blad.h"

#include "bio/Cell.h"

#include <cmath>
#include <iostream>

namespace SimPT_Blad {
namespace CellSplit {

using namespace std;
using namespace SimPT_Sim::Util;
using namespace boost::property_tree;

Blad::Blad(const CoreData& cd)
{
	Initialize(cd);
}

void Blad::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p			= m_cd.m_parameters->get_child("blad");
        m_M_threshold_division	       	= p.get<double>("M_threshold_division");
        m_size_threshold_division	= p.get<double>("size_threshold_division");
}

std::tuple<bool, bool, std::array<double, 3>> Blad::operator()(Cell* cell)
{
	const double chem1      = cell->GetChemical(1);
	const double a_area	= cell->GetArea();

	bool must_divide        = false;

	if (cell->GetBoundaryType() == BoundaryType::None) {
		if ( ( chem1 / a_area ) >= m_M_threshold_division ) {
			if ( a_area >= m_size_threshold_division) {
				must_divide = true;
			}
		}
	}

	return std::make_tuple(must_divide, false, std::array<double, 3>());
}

} // namespace
} // namespace

