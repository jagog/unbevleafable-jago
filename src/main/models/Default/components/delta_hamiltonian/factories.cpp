/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry component factory map.
 */

#include "factories.h"

#include "ElasticWall.h"
#include "Maxwell.h"
#include "ModifiedGC.h"
#include "PlainGC.h"

#include <boost/functional/value_factory.hpp>
#include <utility>

using namespace std;

namespace SimPT_Default {
namespace DeltaHamiltonian {

const typename ComponentTraits<DeltaHamiltonianTag>::MapType g_component_factories
{{
        std::make_pair("ElasticWall",    boost::value_factory<ElasticWall>()),
        std::make_pair("Maxwell",        boost::value_factory<Maxwell>()),
        std::make_pair("ModifiedGC",     boost::value_factory<ModifiedGC>()),
        std::make_pair("PlainGC",        boost::value_factory<PlainGC>())
}};

} // namespace
} // namespace
