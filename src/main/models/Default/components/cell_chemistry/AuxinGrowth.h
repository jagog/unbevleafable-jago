#ifndef DEFAULT_CELL_CHEMISTRY_AUXIN_GROWTH_H_INCLUDED
#define DEFAULT_CELL_CHEMISTRY_AUXIN_GROWTH_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for AuxinGrowth model.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim {
class Cell;
class Wall;
}

namespace SimPT_Default {
namespace CellChemistry {

using namespace SimPT_Sim;

/**
 * Implements AuxinGrowth cell chemistry.
 */
class AuxinGrowth
{
public:
	/// Initializing constructor.
	AuxinGrowth(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Execute.
	void operator()(Cell* cell, double* dchem);

private:
	/// Helper.
	double Complex_PijAj(Cell* here, Cell* nb, Wall* w);

private:
	CoreData   m_cd;                 ///< Core data (mesh, params, sim_time,...).
	double	   m_aux_breakdown;
	double     m_aux_cons;
	double     m_k1;
	double     m_k2;
	double     m_km;
	double     m_kr;
	double     m_pin_breakdown;
	double     m_pin_production;
	double     m_pin_production_in_epidermis;
	double     m_r;
	double     m_sam_auxin;
	double     m_sam_auxin_breakdown;
};

} // namespace
} // namespace

#endif // end_of_include_guard
