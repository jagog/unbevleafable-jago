/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Wortel model.
 */

#include "Wortel.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"


namespace SimPT_Default {
namespace CellChemistry {

using namespace std;
using boost::property_tree::ptree;

Wortel::Wortel(const CoreData& cd)
{
	Initialize(cd);
}

void Wortel::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters->get_child("auxin_transport");

        m_aux1prod        = p.get<double>("aux1prod");
        m_aux_breakdown   = p.get<double>("aux_breakdown");
}

void Wortel::operator()(Cell* cell, double* dchem)
{
	/// START FIRST CK-AUX-SHY2
	/// auxin and cytokinin dynamics
	double kmauxCK = 100.;
	if (cell->GetIndex() >= 2 && cell->GetIndex() <= 9 )
	{
		dchem[0] = 100000 + (cell->GetArea()) * m_aux1prod - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = 25 + ( 0.01 * (cell->GetArea()) * kmauxCK / ( kmauxCK + (cell->GetChemical(0) / (cell->GetArea())) ) )  + (cell->GetArea()) * 0.0 - 0.0005 * (cell->GetChemical(1)) ;//LEVELS!
	}
	else if ( (cell->GetIndex() >= 0 && cell->GetIndex() <= 1) || (cell->GetIndex() >= 10 && cell->GetIndex() <= 11) )
	{
		dchem[0] = -100000 + (cell->GetArea()) * m_aux1prod - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = -25 + ( 0.01 * (cell->GetArea()) * kmauxCK / ( kmauxCK + (cell->GetChemical(0) / (cell->GetArea())) ) ) + (cell->GetArea()) * 0.0 - 0.0005 * (cell->GetChemical(1)) ;//LEVELS!
	}
	else
	{
		dchem[0] = (cell->GetArea()) * m_aux1prod - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = ( 0.01 * (cell->GetArea()) * kmauxCK / ( 1 * kmauxCK + (cell->GetChemical(0) / (cell->GetArea())) ) ) + (cell->GetArea()) * 0.0 - 0.0005 * (cell->GetChemical(1)) ;//LEVELS!
	}

	/////SHY2 dynamics
	double kmaux = 100;
	if( ( cell->GetChemical(1) / (cell->GetArea()) )  > 2.5 )
	{
	//		dchem[2] = 1 * (c->Area()) * kmaux / ( kmaux + (c->Chemical(0) / (c->Area())) ) - 0.001 * (c->Chemical(2));
		dchem[2] = 1 * (cell->GetArea()) - (cell->GetChemical(2)) * ( 0.001 + 0.1 * ( (cell->GetChemical(0) / cell->GetArea()) / (kmaux + cell->GetChemical(0) / cell->GetArea()) ) );
	}
	else
	{
		dchem[2] = - 0.001 * (cell->GetChemical(2));
	}

	///GA dynamics
	if( cell->GetCellType() > 2 && cell->GetCellType() < 8 )
	{
		dchem[3] = 100 - 0.1 * cell->GetChemical(3);
	}
	else
	{
		dchem[3] = 200 - 0.1 * cell->GetChemical(3);//wider cells have proportionally more GA production
	}
}

} // namespace
} // namespace
