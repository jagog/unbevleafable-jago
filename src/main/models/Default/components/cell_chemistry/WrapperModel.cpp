/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for the Wrapper model.
 */

#include "WrapperModel.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Default {
namespace CellChemistry {

WrapperModel::WrapperModel(const CoreData& cd)
{
	Initialize(cd);
}

void WrapperModel::Initialize(const CoreData& cd)
{
	m_cd = cd;
	const auto& p = m_cd.m_parameters->get_child("wrapper_model");

	m_ch0_production = p.get<double>("ch0_production");
	m_ch0_breakdown = p.get<double>("ch0_breakdown");
	m_ch1_production = p.get<double>("ch1_production");
	m_ch1_breakdown = p.get<double>("ch1_breakdown");
}

void WrapperModel::operator()(Cell* cell, double* dchem)
{
	/// START FIRST CK-AUX-SHY2
	/// auxin and cytokinin dynamics

	const double chem0      = cell->GetChemical(0);
	const double chem1      = cell->GetChemical(1);
	const double a_area	= cell->GetArea();

	dchem[0] = m_ch0_production * a_area - m_ch0_breakdown * chem0;//LEVELS
	dchem[1] = m_ch1_production * a_area - m_ch1_breakdown * chem1;//LEVELS

	// Force upper four cell's du/dt to 0, their values are set from outside by
	// a coupled model.
	if (cell->GetIndex() < 4) {
		dchem[0] = 0.0;
		dchem[1] = 0.0;
	}
}

} // namespace
} // namespace
