/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Source model.
 */

#include "Source.h"

#include "bio/Cell.h"


namespace SimPT_Default {
namespace CellChemistry {

using namespace std;
using boost::property_tree::ptree;

Source::Source(const CoreData& cd)
{
	Initialize(cd);
}

void Source::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters->get_child("auxin_transport");

        m_tip_source = p.get<double>("leaf_tip_source");
}

void Source::operator()(Cell* cell, double* dchem)
{
	if (cell->GetCellType() == 1) {
		dchem[0] = m_tip_source;
	}
}

} // namespace
} // namespace
