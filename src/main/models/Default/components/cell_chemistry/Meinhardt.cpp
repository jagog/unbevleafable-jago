/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Meinhardt model.
 */

#include "Meinhardt.h"

#include "bio/Cell.h"


using namespace std;
using boost::property_tree::ptree;

namespace SimPT_Default {
namespace CellChemistry {

Meinhardt::Meinhardt(const CoreData& cd)
{
	Initialize(cd);
}

void Meinhardt::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters->get_child("meinhardt");

        m_c 		= p.get<double>("c");
        m_c0 		= p.get<double>("c0");
        m_d 		= p.get<double>("d");
        m_e 		= p.get<double>("e");
        m_eps 		= p.get<double>("eps");
        m_f 		= p.get<double>("f");
        m_gamma 	= p.get<double>("gamma");
        m_mu 		= p.get<double>("mu");
        m_nu 		= p.get<double>("nu");
        m_rho0 		= p.get<double>("rho0");
        m_rho1 		= p.get<double>("rho1");
}

void Meinhardt::operator()(Cell* cell, double* dchem)
{
	double const Y = cell->GetChemical(0);
	double const A = cell->GetChemical(1);
	double const H = cell->GetChemical(2);
	double const S = cell->GetChemical(3);

	dchem[0] = (m_d * A - m_e * Y + Y * Y / (1 + m_f * Y * Y));
	dchem[1] = (m_c * A * A * S / H - m_mu * A + m_rho0 * Y);
	dchem[2] = (m_c * A * A * S - m_nu * H + m_rho1 * Y);
	dchem[3] = (m_c0 - m_gamma * S - m_eps * Y * S);
}

} // namespace
} // namespace
