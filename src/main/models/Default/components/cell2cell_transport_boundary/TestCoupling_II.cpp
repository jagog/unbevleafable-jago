/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransportBoundary component for the TestCoupling model.
 */

#include "TestCoupling_II.h"

#include "bio/Cell.h"
#include "bio/Wall.h"
#include "util/misc/Exception.h"
#include "util/misc/log_debug.h"

namespace SimPT_Default {
namespace CellToCellTransportBoundary {

using namespace std;
using namespace SimPT_Sim;
using namespace boost::property_tree;
using namespace SimPT_Sim::Util;



TestCoupling_II::TestCoupling_II(const CoreData& cd)
{
	Initialize(cd);
}

void TestCoupling_II::Initialize(const CoreData& cd)
{
        m_cd                 = cd;
        const auto& p        = m_cd.m_parameters;
        m_chemical_count     = p->get<unsigned int>("model.cell_chemical_count");
        ptree const& arr_D   = p->get_child("auxin_transport.D.value_array");

        unsigned int c = 0;
        m_D.clear();
        for (auto it = arr_D.begin(); it != arr_D.end(); it++) {
                if (c == m_chemical_count) {
                        break;
                }
                m_D.push_back(it->second.get_value<double>());
                c++;
        }
        // Defensive: if fewer than chemical_count parameters were specified, fill up.
        for (unsigned int i = c; i < m_chemical_count; i++) {
                m_D.push_back(0.0);
        }
}

void TestCoupling_II::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	auto &transfer_map = m_cd.m_coupled_sim_transfer_data;
	double transfer_value = 0.0;

	if (w->GetC1()->IsBoundaryPolygon()) {
		auto it = transfer_map->find(w->GetC2()->GetIndex());
		if (it!=transfer_map->end()) {

			transfer_value = get<0>(it->second);
			double D=get<1>(it->second);

			dchem_c2[1] += w->GetLength() * 1 *
			        /* m_D[1] * */ ( transfer_value - ( w->GetC2()->GetChemical(1)) / w->GetC2()->GetArea() ) ;
		}
	} else if (w->GetC2()->IsBoundaryPolygon()) {
		auto it=transfer_map->find(w->GetC1()->GetIndex());
		if (it!=transfer_map->end()) {

			transfer_value = get<0>(it->second);
			double D=get<1>(it->second);

			dchem_c1[1] += w->GetLength() * 1 *
			        /*  m_D[1] * */ ( transfer_value - ( w->GetC1()->GetChemical(1)) / w->GetC1()->GetArea() ) ;
		}
	}

}

} // namespace
} // namespace
