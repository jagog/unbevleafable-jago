/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * WallChemistry for Meinhardt model.
 */

#include "Meinhardt.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

using namespace std;
using namespace boost::property_tree;

namespace SimPT_Default {
namespace WallChemistry {

Meinhardt::Meinhardt()
	: m_chemical_count(0U)
{
}

Meinhardt::Meinhardt(const CoreData& cd)
	: Meinhardt()
{
	Initialize(cd);
}

void Meinhardt::Initialize(const CoreData& cd)
{
        m_cd     = cd;
        auto& p  = m_cd.m_parameters;

        m_chemical_count   = p->get<unsigned int>("model.cell_chemical_count");
}

void Meinhardt::operator()(Wall*, double* dw1, double* dw2)
{
	for (unsigned int c = 0; c < m_chemical_count; c++) {
		dw1[c] = 0.;
		dw2[c] = 0.;
	}
}

} // namespace
} // namespace
