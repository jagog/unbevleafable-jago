#ifndef DEFAULT_CELL_TO_CELL_TRANSPORT_SMITHPHYLLOTAXIS_H_INCLUDED
#define DEFAULT_CELL_TO_CELL_TRANSPORT_SMITHPHYLLOTAXIS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransport for SmithPhyllotaxis model.
 */

#include "sim/CoreData.h"
#include <vector>

namespace SimPT_Sim { class Wall; }

namespace SimPT_Default {
namespace CellToCellTransport {

using namespace SimPT_Sim;

/**
 * Cell to cell passive & active transport of IAA & PIN
 * in the phyllotaxis model of Richard Smith.
 */
class SmithPhyllotaxis {
public:
    /// Initializing constructor.
    SmithPhyllotaxis(const CoreData& cd);

    /// Initialize or re-initialize.
    void Initialize(const CoreData& cd);

    /// Execute.
    void operator()(Wall* w, double* dchem_c1, double* dchem_c2);

private:
    CoreData     m_cd;        ///< Core data (mesh, params, sim_time,...).
    double       m_D;
    double       m_T;
    double       m_k_T;
    double       m_c;
};

} // namespace
} // namespace

#endif // end_of_include_guard
