/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransport for Meinhardt model.
 */

#include "Meinhardt.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

namespace SimPT_Default {
namespace CellToCellTransport {

using namespace std;
using namespace boost::property_tree;

Meinhardt::Meinhardt(const CoreData& cd) 
{
	Initialize(cd);
}

void Meinhardt::Initialize(const CoreData& cd)
{
        m_cd    = cd;
        auto& p = m_cd.m_parameters;

        m_chemical_count   = p->get<unsigned int>("model.cell_chemical_count");
        ptree const& arr_D = p->get_child("auxin_transport.D.value_array");
        unsigned int c = 0;
        m_D.clear();
        for (auto it = arr_D.begin(); it != arr_D.end(); it++) {
                if (c == m_chemical_count) {
                        break;
                }
                m_D.push_back(it->second.get_value<double>());
                c++;
        }
        // Defensive: if fewer than chemical_count parameters were specified, fill up.
        for (unsigned int i = c; i < m_chemical_count; i++) {
                m_D.push_back(0.0);
        }
}

void Meinhardt::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	// No flux boundaries for all chemicals, except activator: boundary is sink
	if (w->GetC1()->IsBoundaryPolygon() || w->GetC2()->IsBoundaryPolygon()) {
		

		if (w->GetC1()->IsBoundaryPolygon()) {
	 		dchem_c2[1] -= w->GetLength() * m_D[1] * (w->GetC2()->GetChemical(1));
	 	} else {
		 	dchem_c1[1] -= w->GetLength() * m_D[1] * (w->GetC1()->GetChemical(1));
		}

		return;
	}

	// Passive fluxes (Fick's law)
	for (unsigned int i = 0; i < m_chemical_count; i++) {
		const double phi = w->GetLength() * m_D[i]
		                     * (w->GetC2()->GetChemical(i) - w->GetC1()->GetChemical(i));
		dchem_c1[i] += phi;
		dchem_c2[i] -= phi;
	}
}

} // namespace
} // namespace
