/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransport for Wortel model.
 */

#include "Wortel.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

namespace SimPT_Default {
namespace CellToCellTransport {

using namespace std;
using namespace SimPT_Sim::Util;
using namespace boost::property_tree;

Wortel::Wortel(const CoreData& cd)
{
        Initialize(cd);
}

void Wortel::Initialize(const CoreData& cd)
{
        m_cd    = cd;
        auto& p = m_cd.m_parameters;

        m_transport    = p->get<double>("auxin_transport.transport");
        m_d            = 1800.;//p->get<double>("auxin_transport.D[0]");//Was 160.0
}

void Wortel::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	if ( !(w->GetC1()->IsBoundaryPolygon()) && !(w->GetC2()->IsBoundaryPolygon()) ) //This line is not relevant any more it appears...
	{
		const double apoplast_thickness = 2.;

		const double phi = (w->GetLength() / apoplast_thickness) * m_d * ( ( w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()) )
				- ( w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()) ) ); //LEVELS!

		dchem_c1[0] += phi; //LEVELS!
		dchem_c2[0] -= phi; //LEVELS!

		// Active fluxes (PIN1 mediated transport) (Transporters measured in moles, here)
		const double k_import = 60.;
		const double k_export = m_transport; //SHY2 inhibits PIN transport

		// transport ((RM: efflux)) from cell 1 to cell 2
		const double kmshy = 0.1;//SHY2
		const double shy12 = 1 * kmshy  / ( kmshy + ( w->GetC1()->GetChemical(2) ) / (w->GetC1()->GetArea()) );//SHY2
		const double trans12 = w->GetLength() * ( w->GetC1()->GetChemical(0) / (w->GetC1()->GetArea()) ) * (k_export * w->GetTransporters1(1) * shy12 + k_import);//LEVELS!

		// transport ((RM: efflux)) from cell 2 to cell 1
		const double shy21 = 1 * kmshy  / ( kmshy + ( w->GetC2()->GetChemical(2) ) / (w->GetC2()->GetArea()) );
		const double trans21 = w->GetLength() * ( w->GetC2()->GetChemical(0) / (w->GetC2()->GetArea()) ) * (k_export * w->GetTransporters2(1) * shy21 + k_import);//LEVELS!

		dchem_c1[0] += (trans21 - trans12);//LEVELS!
		dchem_c2[0] += (trans12 - trans21);//LEVELS!

		//DDV2012:second diffusive hormone - chemical '1'
		const double phi2 = (w->GetLength() / apoplast_thickness) * 24 * ( ( w->GetC2()->GetChemical(1) / (w->GetC2()->GetArea()) )
			- ( w->GetC1()->GetChemical(1) / (w->GetC1()->GetArea()) ) ); //LEVELS!

		dchem_c1[1] += phi2 ; //LEVELS!
		dchem_c2[1] -= phi2 ; //LEVELS!
	}
}

} // namespace
} // namespace
