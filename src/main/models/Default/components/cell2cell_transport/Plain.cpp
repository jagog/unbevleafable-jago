/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellToCellTransport for Plain model.
 */

#include "Plain.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

namespace SimPT_Default {
namespace CellToCellTransport {

using namespace std;
using namespace boost::property_tree;

Plain::Plain(const CoreData& cd)
{
	Initialize(cd);
}

void Plain::Initialize(const CoreData& cd)
{
        m_cd    = cd;
        auto& p = m_cd.m_parameters;

        boost::optional<ptree&> array = p->get_child_optional("auxin_transport.D.value_array");
        if (array) {
                m_cct_factor = array->begin()->second.get_value<double>();
        }
}

void Plain::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
	double const phi = w->GetLength() * m_cct_factor
		* (w->GetC2()->GetChemical(0) - w->GetC1()->GetChemical(0));
	dchem_c1[0] += phi;
	dchem_c2[0] -= phi;
}

} // namespace
} // namespace
