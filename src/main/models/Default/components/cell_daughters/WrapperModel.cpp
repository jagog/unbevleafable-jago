/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughter component for Wrapper model.
 */

#include "WrapperModel.h"

#include "bio/Cell.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Default {
namespace CellDaughters {

using namespace std;
using boost::property_tree::ptree;

WrapperModel::WrapperModel(const CoreData& cd)
{
	Initialize(cd);
}

void WrapperModel::Initialize(const CoreData& cd)
{
	m_cd = cd;
}

void WrapperModel::operator()(Cell* daughter1, Cell* daughter2)
{

	double const area1 = daughter1->GetArea();
	double const area2 = daughter2->GetArea();
	double const tot_area = area1 + area2;

	daughter1->SetChemical(0, daughter1->GetChemical(0) * (area1 / tot_area));
	daughter2->SetChemical(0, daughter2->GetChemical(0) * (area2 / tot_area));
	daughter1->SetChemical(1, daughter1->GetChemical(1) * (area1 / tot_area));
	daughter2->SetChemical(1, daughter2->GetChemical(1) * (area2 / tot_area));

//	daughter1->SetTransporters(1, 1., 1.);
//	daughter2->SetTransporters(1, 1., 1.);
}

} // namespace
} // namespace
