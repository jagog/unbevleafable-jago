/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellDaughters::SmithPhyllotaxis implementation.
 */

#include "SmithPhyllotaxis.h"

#include "bio/Cell.h"

namespace SimPT_Default {
namespace CellDaughters {

using namespace std;
using namespace boost::property_tree;

SmithPhyllotaxis::SmithPhyllotaxis(const CoreData& cd)
{
        Initialize(cd);
}

void SmithPhyllotaxis::Initialize(const CoreData& cd)
{
        m_cd = cd;
}

void SmithPhyllotaxis::operator()(Cell* daughter, Cell* mother)
{
        // IAA (chem_0) is concentration based so daughter inherits mother's
        // concentration which is already the default behaviour in
        // CellDivider::DivideOverAxis().

        // PIN (chem_1) distributes over mother and daughter proportional to area
        // hence we scale the default distribution according to relative areas.
        const double area_dtr = daughter->GetArea();
        const double area_mtr = mother->GetArea();
        const double area_tot = area_dtr + area_mtr;
        mother->SetChemical(1, mother->GetChemical(1) * (area_mtr / area_tot));
        daughter->SetChemical(1, daughter->GetChemical(1) * (area_dtr / area_tot));
}

} // namespace
} // namespace

