/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep component factory map.
 */

#include "factories.h"

#include "Auxin.h"
#include "AuxinGrowth.h"
#include "BasicAuxin.h"
#include "Geometric.h"
#include "Meinhardt.h"
#include "NoOp.h"
#include "SmithPhyllotaxis.h"
#include "Wortel.h"
#include "WrapperModel.h"

#include <boost/functional/value_factory.hpp>
#include <utility>

namespace SimPT_Default {
namespace CellHousekeep {

const typename ComponentTraits<CellHousekeepTag>::MapType g_component_factories
{{
	std::make_pair("Auxin",         	boost::value_factory<Auxin>()),
	std::make_pair("AuxinGrowth",   	boost::value_factory<AuxinGrowth>()),
	std::make_pair("BasicAuxin",    	boost::value_factory<BasicAuxin>()),
	std::make_pair("Geometric",     	boost::value_factory<Geometric>()),
	std::make_pair("Meinhardt",     	boost::value_factory<Meinhardt>()),
	std::make_pair("NoOp",          	boost::value_factory<NoOp>()),
	std::make_pair("SmithPhyllotaxis",  	boost::value_factory<SmithPhyllotaxis>()),
	std::make_pair("Wortel",      	boost::value_factory<Wortel>()),
	std::make_pair("WrapperModel",       boost::value_factory<WrapperModel>())
}};

} // namespace
} // namespace
