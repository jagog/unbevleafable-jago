/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellHousekeep for Wortel model.
 */

#include "Wortel.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "math/RandomEngine.h"

#include <trng/uniform01_dist.hpp>

namespace SimPT_Default {
namespace CellHousekeep {

using namespace std;
using namespace boost::property_tree;

Wortel::Wortel(const CoreData& cd)
{
	Initialize(cd);
}

void Wortel::Initialize(const CoreData& cd)
{
        m_cd = cd;

        const trng::uniform01_dist<double> dist;
        m_uniform_generator = m_cd.m_random_engine->GetGenerator(dist);
}

void Wortel::operator()(Cell* cell)
{
	const double chem2      = cell->GetChemical(2);
	const double chem3      = cell->GetChemical(3);
	const int celltype	= cell->GetCellType();
	const double t_area     = cell->GetTargetArea();
	const double a_area	= cell->GetArea();

//	if (/*sim->GetSimTime() m_mesh*/m_sim->GetSimTime() == 0)
//	    {
//	    	cell->SetChemical(1,0.000);
//	    }

	// DDV: EXPONENTIAL GROWTH WITH CHEMICAL 1 FOR START/STOP
//	if ( ( chem1 / a_area ) > 1./*1.0 0.085*/)
//	{
//		const double incr 	      = 0.1;
//		const double update_t_area    = t_area + incr;
//		cell->SetTargetArea(update_t_area);
//	}

	if (cell->GetBoundaryType() == BoundaryType::None)
	{
		if (celltype != 0 && (chem3 / a_area) >= 0.7) {
			if ((chem2 / a_area) < 0.1) {
				const double incr = 0.02 * a_area;
				const double update_t_area = t_area + incr;
				cell->SetTargetArea(update_t_area);
			} else {
				const double incr = 0.2 * a_area;
				const double update_t_area = t_area + incr;
				cell->SetTargetArea(update_t_area);
			}
		}

/*

	if (cell->GetBoundary() == BoundaryType::Type::None) {
		if (cell->GetArea() > threshold * m_cell_base_area) {
			cell->SetChemical(0, 0);
			must_divide = true;
		}

	// Updates
	const double incr = m_auxin_dependent_growth ? (chem / (1. + chem)) * rate : rate;
	const double update_t_area    = t_area + incr;
	const double update_factor    = update_t_area / t_area;
	const double update_t_length  = t_length * sqrt(update_factor);

	cell->SetTargetArea(update_t_area);
	cell->SetTargetLength(update_t_length);
	}
*/
	}

/*
	static ofstream myfile1;
	//    if ((int)m_mesh->GetSimTime() % 600 == 0)
	//    {
	myfile1.open("datadumpIUAP_1.txt",std::ios::app);
	myfile1 << (cell->GetChemical(0)) / (cell->GetArea()) << "\t";
	myfile1.close();
//
//	auto &transfer_map=m_cd.m_coupled_sim_transfer_data;

//	double transfer_value=0.0;

//	auto it=transfer_map.find(cell->GetIndex());
//	if (it!=transfer_map.end())
//	{
//		transfer_value=it->second;
//	}
*/

}

} // namespace
} // namespace
