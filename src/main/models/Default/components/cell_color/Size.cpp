/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellColor Size scheme.
 */

#include "Size.h"

#include "bio/Cell.h"
#include "util/color/Hsv2Rgb.h"

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <cmath>

namespace SimPT_Default {
namespace CellColor {

using namespace std;
using namespace SimPT_Sim::Color;

Size::Size(const boost::property_tree::ptree& pt)
{
	m_base = pt.get<double>("cell_mechanics.base_area", 293.893);
}

array<double, 3> Size::operator()(SimPT_Sim::Cell* cell)
{
	const double x = min(1.0, abs(cell->GetArea() - m_base) / m_base);
	constexpr double hue = 10.0 / 255.0;
	constexpr double saturation = 50.0 / 255.0;
	return Hsv2Rgb(hue, saturation, x);
}

} // namespace
} // namespace


