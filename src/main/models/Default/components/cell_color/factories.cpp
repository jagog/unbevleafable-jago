/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry component factory map.
 */

#include "factories.h"

#include "AuxinPIN1.h"
#include "ChemBlue.h"
#include "ChemGreen.h"
#include "Meinhardt.h"
#include "Size.h"
#include "Wortel.h"
#include <boost/functional/value_factory.hpp>
#include <functional>
#include <utility>

using namespace std;
using std::placeholders::_1;

namespace SimPT_Default {
namespace CellColor {

const typename ComponentTraits<CellColorTag>::MapType g_component_factories
{{
        std::make_pair("auxinpin1",       boost::value_factory<AuxinPIN1>()),
        std::make_pair("chem_blue_0",     bind(boost::value_factory<ChemBlue>(),   _1, 0U)),
        std::make_pair("chem_blue_1",     bind(boost::value_factory<ChemBlue>(),   _1, 1U)),
        std::make_pair("chem_blue_2",     bind(boost::value_factory<ChemBlue>(),   _1, 2U)),
        std::make_pair("chem_blue_3",     bind(boost::value_factory<ChemBlue>(),   _1, 3U)),
        std::make_pair("chem_blue_4",     bind(boost::value_factory<ChemBlue>(),   _1, 4U)),
        std::make_pair("chem_blue_5",     bind(boost::value_factory<ChemBlue>(),   _1, 5U)),
        std::make_pair("chem_blue_6",     bind(boost::value_factory<ChemBlue>(),   _1, 6U)),
        std::make_pair("chem_green_0",    bind(boost::value_factory<ChemGreen>(),  _1, 0U)),
        std::make_pair("chem_green_1",    bind(boost::value_factory<ChemGreen>(),  _1, 1U)),
        std::make_pair("chem_green_2",    bind(boost::value_factory<ChemGreen>(),  _1, 2U)),
        std::make_pair("chem_green_3",    bind(boost::value_factory<ChemGreen>(),  _1, 3U)),
        std::make_pair("chem_green_4",    bind(boost::value_factory<ChemGreen>(),  _1, 4U)),
        std::make_pair("chem_green_5",    bind(boost::value_factory<ChemGreen>(),  _1, 5U)),
        std::make_pair("chem_green_6",    bind(boost::value_factory<ChemGreen>(),  _1, 6U)),
        std::make_pair("meinhardt",       boost::value_factory<Meinhardt>()),
        std::make_pair("size",            boost::value_factory<Size>()),
        std::make_pair("wortel",          boost::value_factory<Wortel>())
}};

} // namespace
} // namespace
