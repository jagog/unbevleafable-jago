#ifndef SIMPT_DEFAULT_MOVE_GEN_DIRECTED_NORMAL_H_INCLUDED
#define SIMPT_DEFAULT_MOVE_GEN_DIRECTED_NORMAL_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * MoveGenerator component: DirectedNormal.
 */

#include "math/constants.h"
#include "math/RandomEngine.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>
#include <trng/normal_dist.hpp>
#include <array>
#include <cmath>
#include <string>

namespace SimPT_Default {
namespace MoveGenerator {

using namespace SimPT_Sim;
using namespace SimPT_Sim::Util;
using boost::property_tree::ptree;

/**
 * Move Generator with normal distribution and directional bias.
 */
class DirectedNormal
{
public:
	/// Straight initialization.
	DirectedNormal(const CoreData& cd)
	{
		assert( cd.Check() && "CoreData not ok.");
		Initialize(cd);
	}

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd)
	{
	        m_normal_generator  = cd.m_random_engine->GetGenerator(trng::normal_dist<double>(0.0, 1.0));
	        m_mc_step_x_scale   = cd.m_parameters->get<double>("cell_mechanics.mc_step_x_scale");
	        m_mc_step_y_scale   = cd.m_parameters->get<double>("cell_mechanics.mc_step_y_scale");
	}

	/// Generate a random move.
	std::array<double, 3> operator()()
	{
		// Both x and y range [- very large, very large] and are scaled with direction.
		const double x   = m_normal_generator() * m_mc_step_x_scale;
		const double y   = m_normal_generator() * m_mc_step_y_scale;
		return std::array<double, 3>{{ x, y, 0.0 }};
	}

private:
	std::function<double()>   m_normal_generator;   ///< Generator for normal distribution.
	double                    m_mc_step_x_scale;    ///< Step size in x direction scaled.
	double                    m_mc_step_y_scale;    ///< Step size in y direction scaled.
};

} // namespace
} // namespace

#endif // end_of_include_guard
