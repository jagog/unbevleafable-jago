#ifndef SIMPT_DEFAULT_MOVE_GEN_STANDARD_UNIFORM_H_INCLUDED
#define SIMPT_DEFAULT_MOVE_GEN_STANDARD_UNIFORM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * MoveGenerator component: StandardUniform.
 */

#include "math/constants.h"
#include "math/RandomEngine.h"
#include "sim/CoreData.h"

#include <trng/uniform01_dist.hpp>
#include <array>
#include <cmath>

namespace SimPT_Default {
namespace MoveGenerator {

using namespace SimPT_Sim;
using namespace SimPT_Sim::Util;

/**
 * Move Generator with uniform distribution (no directional bias).
 */
class StandardUniform
{
public:
	/// Straight initialization.
	StandardUniform(const CoreData& cd)
	{
		assert( cd.Check() && "CoreData not ok.");
		Initialize(cd);
	}

	/// Straight initialization of empty object.
	void Initialize(const CoreData& cd)
	{
		m_uniform_generator = cd.m_random_engine->GetGenerator(trng::uniform01_dist<double>());
	}

	/// Generate a random move.
	std::array<double, 3> operator()()
	{
		// Angle ranges [0.0, 2*pi] and ampli [0.0, 1.0].
		const double angle  = m_uniform_generator() * two_pi();
		const double ampli  = m_uniform_generator();
		return std::array<double, 3>{{ ampli * std::sin(angle), ampli * std::cos(angle), 0.0 }};
	}

private:
	std::function<double()>   m_uniform_generator;  ///< Generator for uniform distribution.
};

} // namespace
} // namespace

#endif // end_of_include_guard
