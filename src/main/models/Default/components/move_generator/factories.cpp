/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellSplit component factory map.
 */

#include "factories.h"

#include "DirectedNormal.h"
#include "DirectedUniform.h"
#include "StandardNormal.h"
#include "StandardUniform.h"

#include <boost/functional/value_factory.hpp>
#include <utility>

namespace SimPT_Default {
namespace MoveGenerator {

// ------------------------------------------------------------------------------
// Add new classes here (but do not forget to include the header also).
// ------------------------------------------------------------------------------
const typename ComponentTraits<MoveGeneratorTag>::MapType g_component_factories
{{
        std::make_pair("directed_normal",     boost::value_factory<DirectedNormal>()),
        std::make_pair("directed_uniform",    boost::value_factory<DirectedUniform>()),
        std::make_pair("standard_normal",     boost::value_factory<StandardNormal>()),
        std::make_pair("standard_uniform",    boost::value_factory<StandardUniform>())
}};

} // namespace
} // namespace
