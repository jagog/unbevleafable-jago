#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Build & install library; sources gathered from sub dirs.
#============================================================================
set( LIB  simPT_Default )
set( SOURCES
#---------
	ComponentFactory.cpp
#---------
	cell_chemistry/AuxinGrowth.cpp
	cell_chemistry/factories.cpp
	cell_chemistry/Meinhardt.cpp
	cell_chemistry/PINFlux.cpp
	cell_chemistry/PINFlux2.cpp
	cell_chemistry/SmithPhyllotaxis.cpp
	cell_chemistry/Source.cpp
	cell_chemistry/TestCoupling.cpp
	cell_chemistry/Wortel.cpp
	cell_chemistry/WrapperModel	
#---------
	cell_color/AuxinPIN1.cpp
	cell_color/ChemBlue.cpp
	cell_color/ChemGreen.cpp
	cell_color/factories.cpp
	cell_color/Meinhardt.cpp
	cell_color/Size.cpp
	cell_color/Wortel.cpp
#---------
	cell_daughters/Auxin.cpp
	cell_daughters/BasicPIN.cpp
	cell_daughters/factories.cpp
	cell_daughters/Perimeter.cpp
	cell_daughters/PIN.cpp
    cell_daughters/SmithPhyllotaxis.cpp
    cell_daughters/Wortel.cpp
    cell_daughters/WrapperModel.cpp
#---------
	cell_housekeep/Auxin.cpp
	cell_housekeep/AuxinGrowth.cpp
	cell_housekeep/BasicAuxin.cpp
	cell_housekeep/factories.cpp
	cell_housekeep/Geometric.cpp
	cell_housekeep/Meinhardt.cpp
    cell_housekeep/SmithPhyllotaxis.cpp
    cell_housekeep/Wortel.cpp
    cell_housekeep/WrapperModel.cpp
#---------
	cell_split/AreaThresholdBased.cpp
	cell_split/AuxinGrowth.cpp
	cell_split/factories.cpp
	cell_split/Geometric.cpp
	cell_split/NoOp.cpp
	cell_split/Wortel.cpp
	cell_split/WrapperModel
#---------
    hamiltonian/ElasticWall.cpp
    hamiltonian/factories.cpp
    hamiltonian/Maxwell.cpp
    hamiltonian/ModifiedGC.cpp
    hamiltonian/PlainGC.cpp
#---------
    delta_hamiltonian/ElasticWall.cpp
    delta_hamiltonian/factories.cpp
    delta_hamiltonian/Maxwell.cpp
    delta_hamiltonian/ModifiedGC.cpp
    delta_hamiltonian/PlainGC.cpp
#---------
    move_generator/factories.cpp
#---------
	cell2cell_transport/AuxinGrowth.cpp
	cell2cell_transport/Basic.cpp
	cell2cell_transport/factories.cpp
	cell2cell_transport/Meinhardt.cpp
	cell2cell_transport/Plain.cpp
    cell2cell_transport/Source.cpp
    cell2cell_transport/SmithPhyllotaxis.cpp
    cell2cell_transport/TestCoupling.cpp
    cell2cell_transport/Wortel.cpp
    cell2cell_transport/WrapperModel.cpp
#---------
	cell2cell_transport_boundary/factories.cpp
	cell2cell_transport_boundary/TestCoupling_I.cpp	
	cell2cell_transport_boundary/TestCoupling_II.cpp
#---------
	time_evolver/factories.cpp
	time_evolver/Grow.cpp
	time_evolver/Housekeep.cpp
	time_evolver/HousekeepGrow.cpp	
	time_evolver/SimPT.cpp
	time_evolver/Transport.cpp
	time_evolver/VLeaf.cpp
#---------
	wall_chemistry/AuxinGrowth.cpp
	wall_chemistry/Basic.cpp
	wall_chemistry/factories.cpp
	wall_chemistry/Meinhardt.cpp		
)

#----------------------------- build ----------------------------------------
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )
add_library( ${LIB}   SHARED  ${SOURCES} )
if( APPLE )
	set_target_properties( ${LIB}  PROPERTIES INSTALL_NAME_DIR  "@loader_path" )
	target_link_libraries( ${LIB} simPTlib_sim trng4 ${LIBS} )
endif( APPLE )			

#------------------------------ install -------------------------------------
if (NOT ${CMAKE_PROJECT_NAME}_MAKE_PACKAGE)
	install( TARGETS ${LIB}  DESTINATION  ${LIB_INSTALL_LOCATION} )
	install( TARGETS ${LIB}  DESTINATION  ${CMAKE_INSTALL_PREFIX}/${TESTS_INSTALL_LOCATION}/${TESTS_DIR}bin)
else()
	package_target_install( ${LIB} )
endif()

#-------------------------- unset variables ---------------------------------
unset( SOURCES )
unset( LIB     )

#############################################################################
