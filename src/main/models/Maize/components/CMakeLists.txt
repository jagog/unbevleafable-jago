#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Build & install library; sources gathered from sub dirs.
#============================================================================
set( LIB   simPT_Maize )
set( SOURCES
#---------
    ComponentFactory.cpp
#---------
	cell_chemistry/factories.cpp
	cell_chemistry/Maize.cpp
	cell_chemistry/MaizeGRN.cpp
	cell_chemistry/WortelLight.cpp
#---------
    cell_color/factories.cpp
    cell_color/Maize.cpp
    cell_color/MaizeGRN.cpp
#---------
	cell_daughters/factories.cpp
	cell_daughters/Maize.cpp
	cell_daughters/MaizeGRN.cpp
	cell_daughters/WortelLight.cpp
#---------
	cell_housekeep/factories.cpp
	cell_housekeep/Maize.cpp
	cell_housekeep/MaizeExpansion.cpp
	cell_housekeep/MaizeGRN.cpp
	cell_housekeep/Wortel.cpp
	cell_housekeep/WortelCoupled.cpp
#---------
	cell_split/factories.cpp
	cell_split/Geometric.cpp
	cell_split/Maize.cpp
	cell_split/MaizeGRN.cpp
	cell_split/WortelLight.cpp
#---------
	cell2cell_transport/factories.cpp
	cell2cell_transport/MaizeGRN.cpp
	cell2cell_transport/Maize.cpp
	cell2cell_transport/Wortel.cpp
#---------
	cell2cell_transport_boundary/factories.cpp
	cell2cell_transport_boundary/IUAP_W_Coupled.cpp
	cell2cell_transport_boundary/IUAP_M_Coupled.cpp	
)

#----------------------------- build ----------------------------------------
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )
add_library( ${LIB} SHARED ${SOURCES} )
if( APPLE )
	set_target_properties( ${LIB}  PROPERTIES INSTALL_NAME_DIR  "@loader_path" )
	target_link_libraries( ${LIB} simPTlib_sim )
endif( APPLE )			

#------------------------------ install -------------------------------------
if (NOT ${CMAKE_PROJECT_NAME}_MAKE_PACKAGE)
	install( TARGETS ${LIB}  DESTINATION  ${LIB_INSTALL_LOCATION} )
	install( TARGETS ${LIB}  DESTINATION ${CMAKE_INSTALL_PREFIX}/${TESTS_INSTALL_LOCATION}/${TESTS_DIR}bin)
else()
	package_target_install( ${LIB} )
endif()

#-------------------------- unset variables ---------------------------------
unset( SOURCES )
unset( LIB     )

#############################################################################
