/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

/**
 * @file
 * CellToCellTransport for Maize.
 */
#include "Maize.h"

#include "bio/Cell.h"
#include "bio/Wall.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {
namespace CellToCellTransport {

using namespace std;
using namespace boost::property_tree;

Maize::Maize(const CoreData& cd)
{
        Initialize(cd);
}

void Maize::Initialize(const CoreData& cd)
{
        m_cd = cd;
        const auto& p = m_cd.m_parameters;

        m_transport = p->get<double>("auxin_transport.transport");
        m_d = 1800.; //p->get<double>("auxin_transport.D[0]");
}

void Maize::operator()(Wall* w, double* dchem_c1, double* dchem_c2)
{
        if (!(w->GetC1()->IsBoundaryPolygon()) && !(w->GetC2()->IsBoundaryPolygon())) {
                const double apoplast_thickness = 2.; //originally was 20, divided D by 10 to compensate!

                //DDV2012: diffusive hormone - chemical '1'
                const double phi2 = (w->GetLength() / (apoplast_thickness)) * 10
                        * ((w->GetC2()->GetChemical(1) / (w->GetC2()->GetArea()))
                                - (w->GetC1()->GetChemical(1) / (w->GetC1()->GetArea()))); //LEVELS!

                dchem_c1[1] += phi2; //LEVELS!
                dchem_c2[1] -= phi2; //LEVELS!
        }
}

} // namespace
} // namespace
