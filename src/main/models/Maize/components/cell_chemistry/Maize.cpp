/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for Maize model.
 */

#include "Maize.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {
namespace CellChemistry {

using namespace std;
using boost::property_tree::ptree;


Maize::Maize(const CoreData& cd)
{
	Initialize(cd);
}

double Maize::Hill(double Vm, double Km, double h, double S)
{
	double rate = 0;
	return rate = Vm * pow ( S, h ) / ( pow ( S, h ) + pow ( Km, h ) ) ;
}

void Maize::Initialize(const CoreData& cd)
{
	m_cd = cd;
	const auto& p = m_cd.m_parameters->get_child("auxin_transport");

	m_aux1prod        = p.get<double>("aux1prod");
	m_aux_breakdown	  = p.get<double>("aux_breakdown");
}

void Maize::operator()(Cell* cell, double* dchem)
{
	/// START FIRST CK-AUX-SHY2
	/// auxin and cytokinin dynamics

	const double chem0      = cell->GetChemical(0);
	const double chem1      = cell->GetChemical(1);
	const double chem2      = cell->GetChemical(2);
	const double chem3      = cell->GetChemical(3);
	const double chem4      = cell->GetChemical(4);
	const double chem5      = cell->GetChemical(5);
	const double chem6      = cell->GetChemical(6);
	const double chem7      = cell->GetChemical(7);
	const double chem8      = cell->GetChemical(8);
	const double a_area	= cell->GetArea();


/*	if ( ( chem1 / a_area ) >= 5. )
	{
		dchem[0] = m_aux1prod * 10000 - m_aux_breakdown * (cell->GetChemical(0));//LEVELS! Higher production when dividing!
	}
	else
	{
		dchem[0] =  - m_aux_breakdown * (cell->GetChemical(0));
	}

	if( cell->GetIndex() > 15 && cell->GetIndex() < 32 && time_now < (120.* 60.) ) {
		dchem[1] = (10000/60/2) - (0.03/60) * chem1;//Half the production rate w.r.t. reference model
	}
	else {
		dchem[1] = -(0.03/60) * chem1;
	}
*/
//

	if (cell->GetIndex() > 27 && cell->GetIndex() < 32 )
	{
		dchem[1] = 1000 - 0.01 * chem1;
	} else {
		dchem[1] = - 0.01 * chem1 ;
	}

	if ( (cell->GetIndex() < 28 || cell->GetIndex() > 31 ) )
	{


		//added as intermediate step to represent expression cascade
		if ( ( ((cell->GetCentroid()[1]) ) > (128. - 200. ))/*(c->Chemical(1) / c->Area()) > 10.*/ )
		{
			dchem[0] = 0.02 * a_area - 0.0000 * chem0;
		} else {
			dchem[0] = - 0.01 * chem0;
		}


		////GA-a: chemical 2
		dchem[2] = /*(c->Area())*10.*/a_area * Hill(1., 5., 4, chem0 / a_area) - 0.000 * chem2 - 0.02 * chem2/** ( 1 + c->Chemical(3) )*/;
	//    if (( c->Chemical(1) / c->Area() ) > 5. /*&& ( c->Chemical(1) / c->Area() ) < 10.*/)
	//    {
	//    	dchem[2] = /*(c->Area())*10.*/(c->Area()) * Hill(1., 1., 4, c->Chemical(0) / (c->Area())) - 0.000 * (c->Chemical(2)) - 0.002 * ( c->Chemical(2) )/** ( 1 + c->Chemical(3) )*/;
	//    }
	//	else
	//	{
	//		dchem[2] = - 0.00 * (c->Chemical(2)) - 0.002* ( c->Chemical(2) )/** ( 1 + c->Chemical(3) )*/ ;
	//	}

	    ////GA-b: chemical 3
		dchem[3] = 0.02* chem2/** (1 + c->Chemical(3) )*/ - 0.00 * chem3 - 0.02* chem3;

	    ////GA-c: chemical 4
		dchem[4] = 0.02* chem3 - 0.00 * chem4 - 0.02* chem4;

	    ////GA-d: chemical 5
		dchem[5] = 0.02* chem4 - 0.00 * chem5 - 0.02* chem5;

	    ////GA-1: chemical 6
		dchem[6] = 0.02* chem5 - 0.00 * chem6 - 0.02* chem6;

	    ////GA-8: chemical 7
		dchem[7] = 0.02* chem6 - 0.01 * chem7;

	    ////DELLA: chemical 8
		dchem[8] = 0.10* chem6 - 0.0001 * chem8;

    }
}

} // namespace
} // namespace
