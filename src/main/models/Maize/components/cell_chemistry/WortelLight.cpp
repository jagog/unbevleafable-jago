/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for WortelLight model.
 */

#include "WortelLight.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/ReduceCellWalls.h"
#include "sim/CoreData.h"

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Maize {
namespace CellChemistry {

using namespace std;
using boost::property_tree::ptree;

WortelLight::WortelLight(const CoreData& cd)
{
	Initialize(cd);
}

void WortelLight::Initialize(const CoreData& cd)
{
	m_cd = cd;
	const auto& p = m_cd.m_parameters;

	m_aux_breakdown   	= p->get<double>("wortellight.aux_breakdown");
	m_aux_shy2_breakdown   	= p->get<double>("wortellight.aux_shy2_breakdown");
	m_aux_production  	= p->get<double>("wortellight.aux_production");
	m_aux_sink		= p->get<double>("wortellight.aux_sink");
	m_aux_source		= p->get<double>("wortellight.aux_source");
	m_ck_breakdown		= p->get<double>("wortellight.ck_breakdown");
	m_ck_sink		= p->get<double>("wortellight.ck_sink");
	m_ck_source		= p->get<double>("wortellight.ck_source");
	m_ck_threshold		= p->get<double>("wortellight.ck_threshold");
	m_ga_production		= p->get<double>("wortellight.ga_production");
	m_ga_breakdown		= p->get<double>("wortellight.ga_breakdown");
	m_km_aux_ck		= p->get<double>("wortellight.km_aux_ck");
	m_km_aux_shy2		= p->get<double>("wortellight.km_aux_shy2");
	m_shy2_breakdown	= p->get<double>("wortellight.shy2_breakdown");
	m_shy2_production	= p->get<double>("wortellight.shy2_production");
	m_vm_aux_ck		= p->get<double>("wortellight.vm_aux_ck");
}

void WortelLight::operator()(Cell* cell, double* dchem)
{
	/// START FIRST CK-AUX-SHY2
	/// auxin and cytokinin dynamics
//	double kmauxCK = 100.;
	if (cell->GetIndex() == 1 || cell->GetIndex() == 2 )
	{
		dchem[0] = m_aux_source + (cell->GetArea()) * m_aux_production - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = /*250*/m_ck_source + (  m_vm_aux_ck * (cell->GetArea()) * m_km_aux_ck / ( m_km_aux_ck + (cell->GetChemical(0) / (cell->GetArea())) ) )  + (cell->GetArea()) * 0.0 - m_ck_breakdown * (cell->GetChemical(1)) ;//LEVELS!
	}
	else if ( (cell->GetIndex() == 0) || (cell->GetIndex() == 3) )
	{
		dchem[0] = -m_aux_sink + (cell->GetArea()) * m_aux_production - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = /*-250*/-m_ck_sink + ( m_vm_aux_ck * (cell->GetArea()) * m_km_aux_ck / ( m_km_aux_ck + (cell->GetChemical(0) / (cell->GetArea())) ) ) + (cell->GetArea()) * 0.0 - m_ck_breakdown * (cell->GetChemical(1)) ;//LEVELS!
	}
	else
	{
		dchem[0] = (cell->GetArea()) * m_aux_production - m_aux_breakdown * (cell->GetChemical(0)) ;//LEVELS!
		dchem[1] = (m_vm_aux_ck * (cell->GetArea()) * m_km_aux_ck / ( 1 * m_km_aux_ck + (cell->GetChemical(0) / (cell->GetArea())) ) ) + (cell->GetArea()) * 0.0 - m_ck_breakdown * (cell->GetChemical(1)) ;//LEVELS!
	}

	/////SHY2 dynamics
//	double m_km_aux_shy2 = 100;
	if( ( cell->GetChemical(1) / (cell->GetArea()) )  > m_ck_threshold/*2.*/ )
	{
	//		dchem[2] = 1 * (c->Area()) * kmaux / ( kmaux + (c->Chemical(0) / (c->Area())) ) - 0.001 * (c->Chemical(2));
		dchem[2] = m_shy2_production * (cell->GetArea()) - (cell->GetChemical(2)) * ( m_shy2_breakdown + m_aux_shy2_breakdown * ( (cell->GetChemical(0) / cell->GetArea()) / (m_km_aux_shy2 + cell->GetChemical(0) / cell->GetArea()) ) );
	}
	else
	{
		dchem[2] = - m_shy2_breakdown * (cell->GetChemical(2));
	}

	///GA dynamics
//	if( cell->GetCellType() > 2 && cell->GetCellType() < 8 )
//	{
		dchem[3] = m_ga_production - m_ga_breakdown * cell->GetChemical(3);
//	}
//	else
//	{
//		dchem[3] = 2 * m_ga_production - m_ga_breakdown * cell->GetChemical(3);//2 times wider cells have proportionally more GA production
//	}
}

} // namespace
} // namespace
