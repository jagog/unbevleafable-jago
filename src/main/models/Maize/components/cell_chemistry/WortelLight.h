#ifndef MAIZE_CELL_CHEMISTRY_WORTEL_LIGHT_H_INCLUDED
#define MAIZE_CELL_CHEMISTRY_WORTEL_LIGHT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * CellChemistry for WortelLight model.
 */

#include "sim/CoreData.h"

namespace SimPT_Sim {
class Cell;
}

namespace SimPT_Maize {
namespace CellChemistry {

using namespace SimPT_Sim;

/**
 * Cell chemistry for WortelLight model.
 */
class WortelLight
{
public:
	/// Initializing constructor.
	WortelLight(const CoreData& cd);

	/// Initialize or re-initialize.
	void Initialize(const CoreData& cd);

	/// Execute.
	void operator()(Cell* cell, double* dchem);

private:
	CoreData   	m_cd;                  ///< Core data (mesh, params, sim_time,...).
	double     	m_aux_breakdown;       ///<
	double		m_aux_shy2_breakdown;
	double		m_aux_production;
	double		m_aux_sink;
	double		m_aux_source;
	double		m_ck_breakdown;
	double		m_ck_sink;
	double		m_ck_source;
	double		m_ck_threshold;
	double		m_ga_breakdown;
	double		m_ga_production;
	double		m_km_aux_ck;
	double		m_km_aux_shy2;
	double		m_shy2_breakdown;
	double		m_shy2_production;
	double		m_vm_aux_ck;
};

} // namespace
} // namespace

#endif // end_of_include_guard
