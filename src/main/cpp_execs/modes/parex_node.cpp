/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of main for parameter exploration node.
 */

#include "parex_node_mode.h"

#include "parex_node/WorkerNode.h"
#include "parex_node/Simulator.h"

#include <tclap/CmdLine.h>
#include <QApplication>

namespace Modes {

using namespace std;
using namespace SimPT_Parex;

int ParexNodeMode::operator()(int argc, char** argv)
{
	QCoreApplication app(argc, argv, false);

	TCLAP::CmdLine cmdLine("SimPT Parex Node");
	TCLAP::SwitchArg quietArg("q", "quiet", "Quiet mode (no output)", cmdLine, false);
	cmdLine.parse(argc, argv);

	const bool verbose = !quietArg.getValue();
	WorkerNode node(new Simulator(), verbose);

	return app.exec();
}

} // namesppace
