/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of main for parameter exploration client.
 */

#include "parex_client_mode.h"

#include "parex_client/Client.h"

#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <QApplication>
#include <QMessageBox>

#include <cpp_logging/logger.h>

namespace Modes {

using namespace std;
using namespace SimPT_Parex;

int ParexClientMode::operator()(int argc, char** argv)
{
	int exit_status = EXIT_SUCCESS;

	// qApp: closing last window quits app and exits app process.
	QApplication app(argc, argv, true);
	qRegisterMetaType<std::string>("std::string");
	QObject::connect(qApp, SIGNAL(lastWindowClosed()), qApp, SLOT(quit()));

	try {
		Client clientView;
		clientView.setVisible(true);
		exit_status = app.exec();
	}
	catch (exception& e) {
		SimPT_Logging::ExecsLogger::get()->error("{} {}", LOG_TAG_16, e.what());
		QString qmess = QString("Exception: %1").arg(e.what());
		QMessageBox::critical(0, "Critical Error", qmess,
					QMessageBox::Abort, QMessageBox::NoButton, QMessageBox::NoButton);
		exit_status = EXIT_FAILURE;
	}
	catch (...) {
		SimPT_Logging::ExecsLogger::get()->error("{} Unknown exception.", LOG_TAG_16);
		QString qmess = QString("Unknown exception.");
		QMessageBox::critical(0, "Critical Error", qmess);
		exit_status = EXIT_FAILURE;
	}

	return exit_status;
}

} //namespace
