/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of main for parameter exploration.
 */

#include "modes/mode_manager.h"

#include <boost/functional/value_factory.hpp>
#include <iostream>
#include <string>
#include <vector>

using namespace Modes;
using namespace std;

struct parex {
	static const ModeManager<parex>::MapType 	modes;
	static const std::string                        default_mode;
	static const std::string                        application_name;
};

const ModeManager<parex>::MapType parex::modes=
	{{
		make_pair("client", boost::value_factory<ParexClientMode>()),
		make_pair("server", boost::value_factory<ParexServerMode>()),
		make_pair("node",   boost::value_factory<ParexNodeMode>())
	}};

const std::string parex::default_mode     = "client";
const std::string parex::application_name = "simPT_parex";



int main(int argc, char** argv)
{
	return ModeManager<parex>::main(argc,argv);
}
