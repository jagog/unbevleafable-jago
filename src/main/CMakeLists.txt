#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Add subdirs:
#============================================================================
add_subdirectory( cpp_execs        )
add_subdirectory( cpp_parex        )
add_subdirectory( cpp_sim          )
add_subdirectory( cpp_simptshell   )
add_subdirectory( cpp_simshell     )
add_subdirectory( cpp_tissue_edit  )
add_subdirectory( cpp_logging      )
add_subdirectory( models           )
add_subdirectory( resources        )
add_subdirectory( swig_sim         )

#############################################################################
