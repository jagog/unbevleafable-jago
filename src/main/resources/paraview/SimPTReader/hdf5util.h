/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

#ifndef HDF5UTIL_H_INCLUDED
#define HDF5UTIL_H_INCLUDED

#include <list>
#include <string>
#include <stdexcept>
#include <functional>
//#include <tr1/functional>
#include <hdf5.h>

using std::function;
//using std::tr1::function;

template <typename T>
bool read_array_rank1(hid_t loc_id, std::string name, T** data, \
                      hid_t mem_type_id, size_t* dim_1, \
                      bool optional = false) {
    // HDF5 error status. Used to check if bad things are happening.
    herr_t h5_status;

    // Quickly check for existence of a link called 'name'
    if (0 == H5Lexists(loc_id, name.c_str(), H5P_DEFAULT)) {
        if (optional) {
            return false;
        } else {
            std::string errmsg = std::string("No dataset \'") + name \
                                 + std::string("\'");
            throw std::runtime_error(errmsg);
        }
    }

    // Try to open the dataset
    hid_t dset = H5Dopen(loc_id, name.c_str(), H5P_DEFAULT);
    if (dset < 0) {
        if (optional) {
            return false;
        } else {
            std::string errmsg = std::string("Could not open dataset \'") \
                                 + name + std::string("\'");
            throw std::runtime_error(errmsg);
        }
    }

    // Get the associated dataspace
    hid_t dspace = H5Dget_space(dset);
    int ndims = H5Sget_simple_extent_ndims(dspace);
    if (ndims != 1) {
        std::string errmsg = std::string("Dataset \'") + name \
                             + std::string("\' must be rank 1");
        throw std::runtime_error(errmsg);
    }

    // Number of elements in data
    hsize_t _dim_1;
    H5Sget_simple_extent_dims(dspace, &_dim_1, 0);
    *dim_1 = static_cast<size_t>(_dim_1);

    // Allocate space for data
    *data = new T[_dim_1];

    // Read the data
    h5_status = H5Dread(dset, mem_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, \
                        *data);
    if (h5_status < 0) {
        std::string errmsg = std::string("Could not read dataset \'") + name \
                             + std::string("\'");
        throw std::runtime_error(errmsg);
    }

    // TODO: is type-checking feasible?

    // Clean up HDF5 handles
    H5Sclose(dspace);
    H5Dclose(dset);
    return true;
}

template <typename T>
bool read_array_rank2(hid_t loc_id, std::string name, T** data, \
                      hid_t mem_type_id, size_t* dim_1, size_t* dim_2, \
                      bool optional = false) {
    // HDF5 error status. Used to check if bad things are happening.
    herr_t h5_status;

    // Quickly check for existence of a link called 'name'
    if (0 == H5Lexists(loc_id, name.c_str(), H5P_DEFAULT)) {
        if (optional) {
            return false;
        } else {
            std::string errmsg = std::string("No dataset \'") + name \
                                 + std::string("\'");
            throw std::runtime_error(errmsg);
        }
    }

    // Try to open the dataset
    hid_t dset = H5Dopen(loc_id, name.c_str(), H5P_DEFAULT);
    if (dset < 0) {
        if (optional) {
            return false;
        } else {
            std::string errmsg = std::string("Could not open dataset \'") \
                                 + name + std::string("\'");
            throw std::runtime_error(errmsg);
        }
    }

    // Get the associated dataspace
    hid_t dspace = H5Dget_space(dset);
    int ndims = H5Sget_simple_extent_ndims(dspace);
    if (ndims != 2) {
        std::string errmsg = std::string("Dataset \'") + name \
                             + std::string("\' must be rank 2");
        throw std::runtime_error(errmsg);
    }

    // Number of elements in data
    hsize_t _dims[2];
    H5Sget_simple_extent_dims(dspace, _dims, 0);
    *dim_1 = static_cast<size_t>(_dims[0]);
    *dim_2 = static_cast<size_t>(_dims[1]);

    // Allocate space for data
    *data = new T[_dims[0] * _dims[1]];

    // Read the data
    h5_status = H5Dread(dset, mem_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, \
                        *data);
    if (h5_status < 0) {
        std::string errmsg = std::string("Could not read dataset \'") + name \
                             + std::string("\'");
        throw std::runtime_error(errmsg);
    }

    // TODO: is type-checking feasible?

    // Clean up HDF5 handles
    H5Sclose(dspace);
    H5Dclose(dset);
    return true;
}

// Complies to callback signature:
// herr_t (*H5L_iterate_t)( hid_t g_id, const char *name, const H5L_info_t
// *info, void *op_data)
herr_t collect_link_names(hid_t g_id, const char *name, \
                          const H5L_info_t *info, void *op_data) {
    std::list<std::string>* link_names = \
        static_cast<std::list<std::string>*>(op_data);
    link_names->push_back(std::string(name));
    return 0;
}

std::list<std::string> find_matching_links(hid_t loc_id, \
                                           function<bool(std::string)> match) {
    // Collect all link names at location loc_id
    std::list<std::string> link_names;
    H5Literate(loc_id, H5_INDEX_NAME, H5_ITER_NATIVE, 0, &collect_link_names, \
               &link_names);
    // Remove link names that DON'T match the 'match' predicate
    link_names.remove_if([&](std::string name)->bool{ return !match(name); });
    return link_names;
}

#endif /* HDF5UTIL_H_INCLUDED */

