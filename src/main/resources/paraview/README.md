# Requirements

- ParaView 4.1.x
- CMake 2.8.8
- libhdf5 1.8 (ParaView's bundled HDF5 can be used as well)
- Recent C/C++ compiler suite (able to compile ParaView & with C++11 support).
On Mac Os X we have tested with the compilers provided with the XCode framework.

# Compiling

1.  Get the ParaView source from the [ParaView website](http://paraview.org/paraview/resources/software.php)
    (This plugin is tested to work with ParaView 4.1.0)

2.  Untar the ParaView source and copy the whole `SimPTReader` folder to
    `ParaView-v4.1.0-source/Plugins`

3.  Create a build directory for ParaView (outside the source dir):
    `ParaView-v4.1.0-build`

4.  Inside the build directory invoke `ccmake` or `cmake-gui`: `ccmake
    ../ParaView-v4.0.1-source` to configure cmake files & generate makefiles.

5.  Run `make install` in the build directory

# Notes

Depending on the compiler, you might need to tweak the code to switch 
to the `<functional>` header instead of `<tr1/functional>` and change the 
using directives accordingly in the `hdf5util.h` file.

