#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Name of the application and paths inside the application bundle
#============================================================================
SET( APP_BUNDLE_NAME simPT )
SET( APP_BUNDLE_BIN_DIR ${APP_BUNDLE_NAME}.app/Contents/MacOS )
SET( APP_BUNDLE_RESOURCE_DIR ${APP_BUNDLE_NAME}.app/Contents/Resources )

#============================================================================
# Resolves and copies all dependencies of a bundle and fixes all 
# the paths of the dynamic link libraries
# @param PLUGINDIR      A directory containing plugin libraries. These libs 
#                       are not directly referenced in the object files and 
#                       therefore cannot be discovered automatically
#                       by the fix_bundle function.
#============================================================================
function(install_fix_bundle  PLUGINDIR CURRENT_APP_BUNDLE_NAME)

	# The name of the bundle directory  (Application name + .app)
	SET(APPS "\${CMAKE_INSTALL_PREFIX}/${CURRENT_APP_BUNDLE_NAME}.app")
	SET(CURRENT_APP_BUNDLE_BIN_DIR		${CURRENT_APP_BUNDLE_NAME}.app/Contents/MacOS )

	# Look in this directory for library dependencies, other external libs can be added
	SET(QTDIRS ${QT_LIBRARY_DIR})
		
	# Create the installation code that will fix the bundle, this code must we written
	# in a code block because it must be executed after all the targets are installed.
	#
	# GLOB_RECURSE lists all files in a directory and its sub-directories
	# fixup_bundle( BUNDLEPATH LIBS_IN_BUNDLE_TO_FIX DEPENDENCY_DIRS) 
	INSTALL(CODE "
		file(GLOB_RECURSE R_PLUGINS
		\"\${CMAKE_INSTALL_PREFIX}/${CURRENT_APP_BUNDLE_BIN_DIR}/${PLUGINDIR}/*${CMAKE_SHARED_LIBRARY_SUFFIX}\")
		
	include(BundleUtilities)
	SET(BU_CHMOD_BUNDLE_ITEMS ON)
	fixup_bundle(\"${APPS}\" \"\${R_PLUGINS}\" \"${QTDIRS};${CURRENT_APP_BUNDLE_BIN_DIR}\")
		" COMPONENT Runtime)
		
endfunction(install_fix_bundle)

#============================================================================
# Installs the "main" target into the package. This target will be executed when the 
# user double clicks on the application. This macro will set the required properties of
# the target and will install the required dependencies
# @param TARGETNAME The name of the cmake target
# @param ICON_PATH 	The full location of the icon in the source folder eg. src/icons/myicon.ics
#============================================================================
macro(package_main_target TARGETNAME ICON_PATH)

	set( CURRENT_APP_BUNDLE_NAME		${APP_BUNDLE_NAME} )
	set( extra_macro_args ${ARGN})
	list( LENGTH extra_macro_args num_extra_args )
	if (${num_extra_args} GREATER 0)
        	list(GET extra_macro_args 0 optional_arg)
        	set( CURRENT_APP_BUNDLE_NAME		${optional_arg} )
        	unset(optional_arg)
	endif ()
	unset(num_extra_args)
	unset(extra_macro_args)

	set( CURRENT_APP_BUNDLE_BIN_DIR        ${CURRENT_APP_BUNDLE_NAME}.app/Contents/MacOS )
	SET( CURRENT_APP_BUNDLE_RESOURCE_DIR   ${CURRENT_APP_BUNDLE_NAME}.app/Contents/Resources )

	# Rename the main target to the bundle name, this is the name which will shown in Finder
	SET_TARGET_PROPERTIES( ${TARGETNAME} PROPERTIES RUNTIME_OUTPUT_NAME ${CURRENT_APP_BUNDLE_NAME}) 
	
	# Set where in the bundle to put the icns file
	SET_SOURCE_FILES_PROPERTIES(${ICON_PATH} PROPERTIES MACOSX_PACKAGE_LOCATION Resources)
	
	# Install the main target into the bundle
	install( TARGETS        ${TARGETNAME}
		BUNDLE DESTINATION . RENAME ${CURRENT_APP_BUNDLE_NAME})
		
	message("============> QT_PLUGINS_DIR:" ${QT_PLUGINS_DIR})
	# Copy the image format plugins to the bundle, currently this are the only plugin in use	
	
	INSTALL(DIRECTORY "${QT_PLUGINS_DIR}/imageformats" 
		DESTINATION ${CURRENT_APP_BUNDLE_BIN_DIR}/plugins COMPONENT Runtime)

	# Create a qt.conf file. The file can be empty, if there are no settings qt will just look for 
	# directories with predefined names. If the qt.conf file is absent the global configuration
	# will be used, which has wrong plugin directories defined. 
		
	# Create the qt.conf file in the resource directory. This is the default location
	# if the application bundle is launched using Finder
	INSTALL(CODE "
		file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${CURRENT_APP_BUNDLE_RESOURCE_DIR}/qt.conf\" \"\")
		" COMPONENT Runtime)

	# Create the qt.conf file in the resource directory. This is the default location
	# if the application is launched using the terminal	
	INSTALL(CODE "
		file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${CURRENT_APP_BUNDLE_BIN_DIR}/qt.conf\" \"\")
		" COMPONENT Runtime)

	# Fix the qt plugins of the bundle		
	#install_fix_bundle(plugins ${CURRENT_APP_BUNDLE_NAME})
	
	unset(CURRENT_APP_BUNDLE_NAME)
	unset(CURRENT_APP_BUNDLE_BIN_DIR)
	unset(CURRENT_APP_BUNDLE_RESOURCE_DIR)
	
endmacro(package_main_target)

#==============================================================================
# Copies all dependencies of the bundle and fixes the paths of the dependencies
#==============================================================================
function(package_finalize_bundle)
	install_fix_bundle(plugins "simPT")
	install_fix_bundle(plugins "simPT_Editor")
	install_fix_bundle(plugins "simPT_Parex")
endfunction(package_finalize_bundle)

#============================================================================
# Installs the other targets into the package.
# @param ARG0..ARGN	A list of targets
#============================================================================
function(package_target_install)
	install( TARGETS   ${ARGV} DESTINATION    "simPT.app/Contents/MacOS" )	
	install( TARGETS   ${ARGV} DESTINATION    "simPT_Editor.app/Contents/MacOS" )	
	install( TARGETS   ${ARGV} DESTINATION    "simPT_Parex.app/Contents/MacOS" )	
endfunction(package_target_install)


function(package_install_data)
	install( DIRECTORY ${ARGV} DESTINATION  "simPT.app/Contents/data"   )
	install( DIRECTORY ${ARGV} DESTINATION  "simPT_Editor.app/Contents/data"   )
	install( DIRECTORY ${ARGV} DESTINATION  "simPT_Parex.app/Contents/data"   )
endfunction(package_install_data)

#============================================================================
# Installs test files into the package.
# This may be used for packaging Java and Python test files.
# @param ARG0..ARGN	A list of files
#============================================================================
function(package_file_install)
	install( FILES     ${ARGV} DESTINATION    ${APP_BUNDLE_BIN_DIR} )	
endfunction(package_file_install)

#============================================================================
# Installs a link to the documentation for convenience of the user
#============================================================================ 
function(package_create_doc_link)
	# No longer required
endfunction(package_create_doc_link)

#============================================================================
# Installs a link to the cli for convenience of the user
#============================================================================
function(package_create_cli_link)
	# No longer required

	#install( DIRECTORY  "${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging/macosx_files/simPT Commandline.app"	
	#	DESTINATION . USE_SOURCE_PERMISSIONS)
	#install( FILES  "${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging/macosx_files/cli_welcome.txt"	
	#	DESTINATION     ${APP_BUNDLE_RESOURCE_DIR})
endfunction(package_create_cli_link)

#============================================================================
# Configure various variables if the package creation is enabled
#============================================================================
set( DATA_INSTALL_LOCATION		${APP_BUNDLE_NAME}.app/Contents/data )
set( DOC_INSTALL_LOCATION		${APP_BUNDLE_NAME}.app/Contents/doc )
set( TESTS_INSTALL_LOCATION		${APP_BUNDLE_NAME}.app/Contents/tests )
set( BIN_INSTALL_LOCATION 		${APP_BUNDLE_NAME}.app/Contents/MacOS )
	 
set( CPACK_GENERATOR                    "DragNDrop" )
set( CPACK_BUNDLE_NAME                  "simPT" )
set( CPACK_PACKAGE_ICON                 ${CMAKE_SOURCE_DIR}/main/resources/icons/simPT_icon.icns )
set( CPACK_PACKAGE_INSTALL_DIRECTORY    ${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION} )
set( CPACK_INSTALL_PREFIX               "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}" )  
set( CPACK_DMG_DS_STORE                 "${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging/macosx_files/DS_Store" )
set( CPACK_DMG_BACKGROUND_IMAGE         "${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging/macosx_files/background.png" )
	
# set( CPACK_PACKAGING_INSTALL_PREFIX	"")
# set( CPACK_PACKAGING_INSTALL_PREFIX	"/simPT")  #Use this line to place the applications in a folder
	
# The commands that will install the package, some post install commands can be added later
set( CPACK_INSTALL_COMMANDS            "${CMAKE_MAKE_PROGRAM} install")
set( CPACK_SET_DESTDIR                 OFF )
set( CPACK_OUTPUT_FILE_PREFIX          ${CPACK_OUTPUT_FILE_PREFIX} )   
set( CPACK_BINARY_DRAGNDROP            ON )
include( CPack )

#############################################################################
