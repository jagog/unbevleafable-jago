#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Generic CPack info.
#============================================================================
set( CPACK_PACKAGE_VERSION_MAJOR        "1" )
set( CPACK_PACKAGE_VERSION_MINOR        "0" )
set( CPACK_PACKAGE_VERSION_PATCH        "0" )
set( CPACK_PACKAGE_VENDOR               "SIMPT Consortium" ) 
set( CPACK_PACKAGE_NAME                 "SimPT" )
set( CPACK_PACKAGE_VERSION              ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH} )
set( CPACK_RESOURCE_FILE_LICENSE        ${CMAKE_SOURCE_DIR}/doc/txt/gpl3.txt )
set( CPACK_PACKAGE_DESCRIPTION_SUMMARY  ${CMAKE_SOURCE_DIR}/main/resources/txt/summary.txt )
set( CPACK_PACKAGE_DESCRIPTION_FILE     ${CMAKE_SOURCE_DIR}/main/resources/txt/description.txt )
set( CPACK_RESOURCE_FILE_WELCOME        ${CMAKE_SOURCE_DIR}/main/resources/txt/welcome.txt )
set( CPACK_RESOURCE_FILE_README         ${CMAKE_SOURCE_DIR}/main/resources/txt/description.txt )

#============================================================================
# Platform dependent includes.
#============================================================================
if( APPLE )
	include( ${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging/MacOsX.cmake )
elseif( WIN32 )
	include( ${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging//Windows.cmake )
else( APPLE )
	include( ${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging//GenericOS.cmake )	
endif( APPLE )

#############################################################################
