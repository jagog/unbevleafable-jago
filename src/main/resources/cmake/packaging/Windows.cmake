#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

SET(APP_BUNDLE_NAME simPT_sim)

#============================================================================
# Resolves and copies all dependencies of a bundle and fixes all 
# the paths of the dynamic link libraries
# @param PLUGINDIR      A directory containing plugin libraries. These libs 
#                       are not directly referenced in the object files and 
#                       therefore cannot be discovered automatically
#                       by the fix_bundle function.
#============================================================================
function(install_fix_bundle  PLUGINDIR )

	# The name of the bundle directory  (Application name + .exe)
	#SET(APPS "\${CMAKE_INSTALL_PREFIX}/${APP_BUNDLE_NAME}.exe")

	# Look in this directory for library dependencies, other external libs can be added
	#SET(QTDIRS ${QT_LIBRARY_DIR})
		
	# Create the installation code that will fix the bundle, this code must we written
	# in a code block because it must be executed after all the targets are installed.
	#
	# GLOB_RECURSE lists all files in a directory and its sub-directories
	# fixup_bundle( BUNDLEPATH LIBS_IN_BUNDLE_TO_FIX DEPENDENCY_DIRS) 
		
	# The fixup_bundle does not work on windows with mingw. 
	# To check the dependencies the dumpbin tool is used, but this tool
	# is only available with Visual Studio. The cmake code should be
	# patched to also support the objdump tool, which is available with mingw.
		
	# To enable the packaging of dependencies, the install_required_libraries 
	# function can be used to copy the dependencies manually.		
		
	#INSTALL(CODE "
	#file(GLOB_RECURSE R_PLUGINS
	#\"\${CMAKE_INSTALL_PREFIX}/${PLUGINDIR}/*${CMAKE_SHARED_LIBRARY_SUFFIX}\")
		
	#set(gp_tool \"dumpbin\")
	#set(gp_cmd \"objdump -p\")
		
	#include(BundleUtilities)
	#fixup_bundle(\"${APPS}\" \"\${R_PLUGINS}\" \"${QTDIRS};\${CMAKE_INSTALL_PREFIX}\")
	#" COMPONENT Runtime)
		 
endfunction(install_fix_bundle)

#============================================================================
# Resolves all dependencies and copies the dynamic link libraries 
# manually to the installation folder. 
#============================================================================
macro(install_required_libraries)

	INSTALL(FILES ${QT_BINARY_DIR}/QtCore4.dll  DESTINATION ${BIN_INSTALL_LOCATION})
	INSTALL(FILES ${QT_BINARY_DIR}/QtGui4.dll   DESTINATION ${BIN_INSTALL_LOCATION})
	INSTALL(FILES ${QT_BINARY_DIR}/QtSvg4.dll   DESTINATION ${BIN_INSTALL_LOCATION})
	INSTALL(FILES ${QT_BINARY_DIR}/QtXml4.dll   DESTINATION ${BIN_INSTALL_LOCATION})

	find_library( STDCPPLOCATION NAMES  stdc++-6 )
	if( NOT STDCPPLOCATION-NOTFOUND)	
		INSTALL(FILES ${STDCPPLOCATION} DESTINATION ${BIN_INSTALL_LOCATION})	
	endif()
	if( Boost_REGEX_FOUND )
		INSTALL(FILES ${Boost_REGEX_LIBRARY_RELEASE} DESTINATION ${BIN_INSTALL_LOCATION})
	endif()
	if( HDF5_FOUND )
		if ( HDF5_LIBRARIES_RELEASE)
			INSTALL(FILES ${HDF5_LIBRARIES_RELEASE} DESTINATION ${BIN_INSTALL_LOCATION})
		endif()	
	endif()
	
endmacro(install_required_libraries)

#============================================================================
# Installs the "main" target into the package. This target will be executed when the 
# user double clicks on the application. This macro will set the required properties of
# the target and will install the required dependencies
# @param TARGETNAME     The name of the cmake target
# @param ICON_PATH      The full location of the icon in the source folder eg. src/icons/myicon.ics
#============================================================================
macro(package_main_target TARGETNAME ICON_PATH)

	# Install the main target into the bundle
	install( TARGETS   ${TARGETNAME}   DESTINATION  ${BIN_INSTALL_LOCATION})
		
	# Copy the image format plugins to the bundle, currently this are the only plugins in use			 
	INSTALL(DIRECTORY "${QT_PLUGINS_DIR}/imageformats" DESTINATION ${BIN_INSTALL_LOCATION}/plugins COMPONENT Runtime)

	# Create a qt.conf file. The file can be empty, if there are no settings qt will just look for 
	# directories with predefined names. If the qt.conf file is absent the global configuration
	# will be used, which has wrong plugin directories defined. 
		
	# Create the qt.conf file in the resource directory. This is the default location
	# if the application bundle is launched using Finder
	INSTALL(CODE "
		file(WRITE \"\${CMAKE_INSTALL_PREFIX}/qt.conf\" \"\")
		" COMPONENT Runtime)
				
	install_required_libraries()	
	
endmacro(package_main_target)

#============================================================================
# Installs the other targets into the package.
# @param ARG0..ARGN	A list of targets
#============================================================================
function(package_target_install)
	install( TARGETS   ${ARGV}  DESTINATION   ${BIN_INSTALL_LOCATION} )	
endfunction(package_target_install)

function(package_install_data)
	install( DIRECTORY ${ARGV} DESTINATION  ${DATA_INSTALL_LOCATION}  )
endfunction(package_install_data)

#============================================================================
# Installs non-executable files into the package.
# This may be used for packaging Java and Python test files.
# @param ARG0..ARGN	A list of files
#============================================================================
function(package_file_install)
	install( FILES    ${ARGV}   DESTINATION   ${TESTS_INSTALL_LOCATION}/bin )	
endfunction(package_file_install)

#============================================================================
# Installs a link to the documentation for  convenience of the user
#============================================================================
function(package_create_doc_link)
# Do nothing
endfunction(package_create_doc_link)

#============================================================================
# Installs a link to the cli for  convenience of the user
#============================================================================
function(package_create_cli_link)
	#install( FILES  "${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging/windows_files/StartsimPTCommandLine.bat"
	#	DESTINATION      ${BIN_INSTALL_LOCATION})
	#install( FILES  "${CMAKE_SOURCE_DIR}/main/resources/cmake/packaging/windows_files/cli_welcome.txt"
	#	DESTINATION     ${BIN_INSTALL_LOCATION})			
endfunction(package_create_cli_link)

#============================================================================
# Configure various variables if the package creation is enabled
#============================================================================
message("========================================================")
message("= Creating an NSIS installation package                =") 
message("= The NSIS tool is required, if not installed,         =") 
message("= download it from http://nsis.sourceforge.net/        =") 
message("========================================================")
	
set( DATA_INSTALL_LOCATION              data  )
set( DOC_INSTALL_LOCATION               doc   )
set( TESTS_INSTALL_LOCATION             tests )
set( BIN_INSTALL_LOCATION               .     )
	
set( CPACK_GENERATOR                    "NSIS")
set( CPACK_BUNDLE_NAME                  "simPT")
	
set( CPACK_NSIS_MUI_ICON                ${CMAKE_SOURCE_DIR}/main/resources/icons/simPT_icon.ico )
set( CPACK_NSIS_MUI_UNIICON             ${CMAKE_SOURCE_DIR}/main/resources/icons/simPT_icon.ico )
	
set( CPACK_PACKAGE_INSTALL_DIRECTORY    ${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION} )
set( CPACK_INSTALL_PREFIX               "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")  
		
#set( CPACK_PACKAGING_INSTALL_PREFIX    ".")
# set( CPACK_PACKAGING_INSTALL_PREFIX   "/simPT")  #Use this line to place the applications in a folder
	
set( CPACK_PACKAGE_EXECUTABLES          ""   )
	
# The commands that will install the package, some post install commands can be added later
# set( CPACK_INSTALL_COMMANDS           "${CMAKE_MAKE_PROGRAM} install")
	
# Add shortcuts to the startmenu
set( CPACK_NSIS_MENU_LINKS             "${APP_BUNDLE_NAME}.exe" "${CPACK_PACKAGE_NAME}" )
list( APPEND CPACK_NSIS_MENU_LINKS     "simPT_Editor.exe" "Editor" )
list( APPEND CPACK_NSIS_MENU_LINKS     "simPT_Parex" "Parex" )

set( CPACK_OUTPUT_FILE_PREFIX          ${CPACK_OUTPUT_FILE_PREFIX} )   
set( CPACK_SET_DESTDIR                 OFF )   
include( CPack )

#############################################################################
