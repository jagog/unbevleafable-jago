Welcome to the simPT terminal, the following commands are available:

	simPT          : The simulator graphical interface
	simPT_sim      : The simulator command line interface
	simPT_editor   : The tissue Editor
	simPT_parex    : The parameter explorer

=====================================================================
