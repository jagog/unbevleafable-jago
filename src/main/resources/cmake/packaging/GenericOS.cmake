#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Installs the "main" target into the package. This target will be executed when the 
# user double clicks on the application. This macro will set the required properties of
# the target and will install the required dependencies
# @param TARGETNAME     The name of the cmake target
# @param ICON_PATH      The full location of the icon in the source folder eg. src/icons/myicon.ics
#============================================================================
macro(package_main_target TARGETNAME ICON_PATH)
	# Install the main target into the bundle
	install( TARGETS   ${TARGETNAME}   DESTINATION   ${BIN_INSTALL_LOCATION})
endmacro(package_main_target)

#============================================================================
# Installs the other targets into the package.
# @param ARG0..ARGN	A list of targets
#============================================================================
function(package_target_install)
	install( TARGETS  ${ARGV}   DESTINATION   ${BIN_INSTALL_LOCATION} )	
endfunction(package_target_install)

#============================================================================
# Installs test files into the package.
# This may be used for packaging Java and Python test files.
# @param ARG0..ARGN	A list of files
#============================================================================
function(package_file_install)
	install( FILES    ${ARGV}   DESTINATION   ${TESTS_INSTALL_LOCATION}/bin )	
endfunction(package_file_install)

function(package_install_data)
	install( DIRECTORY ${ARGV} DESTINATION  ${DATA_INSTALL_LOCATION}  )
endfunction(package_install_data)

#============================================================================
# Installs a link to the documentation for  convenience of the user
#============================================================================
function(package_create_doc_link)
# Do nothing
endfunction(package_create_doc_link)

#============================================================================
# Installs a link to the documentation for  convenience of the user
#============================================================================
function(package_create_cli_link)
# Do nothing
endfunction(package_create_cli_link)

#============================================================================
# Configure various variables if the package creation is enabled
#============================================================================
set( BIN_INSTALL_LOCATION                 bin   )
set( DATA_INSTALL_LOCATION                data  )
set( DOC_INSTALL_LOCATION                 doc   )
set( TESTS_INSTALL_LOCATION               tests )
set( CPACK_GENERATOR                      "ZIP" )
set( CPACK_BUNDLE_NAME                    "simPT")
set( CPACK_PACKAGE_ICON	                  ${CMAKE_SOURCE_DIR}/main/resources/icons/simPT_icon.icns )
set( CPACK_PACKAGE_INSTALL_DIRECTORY      ${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION} )
set( CPACK_INSTALL_PREFIX                 "${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}")  
# set( CPACK_PACKAGING_INSTALL_PREFIX     "/simPT")  #Use this to put the applications in a folder
	
# The commands that will install the package, some post install commands can be added later
set( CPACK_INSTALL_COMMANDS               "${CMAKE_MAKE_PROGRAM} install")
set( CPACK_SET_DESTDIR                    OFF ) 
set( CPACK_OUTPUT_FILE_PREFIX             ${CMAKE_INSTALL_PREFIX} )  
include( CPack )

#############################################################################
