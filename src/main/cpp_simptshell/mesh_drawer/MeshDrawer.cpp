/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MeshDrawer
 */

#include "MeshDrawer.h"

#include "ArrowItem.h"
#include "NodeItem.h"
#include "WallItem.h"

#include "bio/BoundaryType.h"
#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "bio/Node.h"
#include "bio/ReduceCellWalls.h"
#include "bio/Wall.h"
#include "model/ComponentFactoryProxy.h"
#include "sim/CoreData.h"
#include "sim/SimInterface.h"
#include "util/misc/StringUtils.h"
#include "workspace/MergedPreferences.h"

#include <QApplication>
#include <QGraphicsItem>
#include <QGraphicsPolygonItem>
#include <QToolTip>

#include <cctype>
#include <functional>
#include <vector>

using namespace std;
using namespace SimPT_Sim::Util;
using namespace SimPT_Sim;

static inline array<double, 3> PINdir(Cell* here, Cell* , Wall* w)
{
	assert( (w->GetC1() == here || w->GetC2() == here) && "Error in cell <-> wall!" );
	const double trans = (w->GetC1() == here) ? w->GetTransporters1(1U) : w->GetTransporters2(1U);
	return trans * w->GetInfluxVector(here);
}

namespace SimPT_Shell {

MeshDrawer::MeshDrawer() : m_preferences(make_shared<prefs_type>()) {}

MeshDrawer::MeshDrawer(const std::shared_ptr<prefs_type>& p) : m_preferences(p) {}

void MeshDrawer::Draw(shared_ptr<SimPT_Sim::SimInterface> sim, QGraphicsScene* scene)
{
	const auto mesh = sim->GetCoreData().m_mesh.get();
	const ptree& parameters = *sim->GetCoreData().m_parameters;

        const auto model_group  = parameters.get<string>("model.group", "");
        const auto factory = ComponentFactoryProxy::Create(model_group);
        const auto cell_colorizer = factory->CreateCellColor(m_preferences->m_cell_color, parameters);

	const auto& nodes = mesh->GetNodes();
	const auto loop_nodes = [&nodes](function<void(Node*)> draw) {
		for (const auto& n : nodes) {draw(n);}
	};

	const auto& walls = mesh->GetWalls();
	const auto loop_walls = [&walls](function<void(Wall*)> draw) {
		for (auto const& w : walls) {draw(w);}
	};

	const auto& cells = mesh->GetCells();
	const auto loop_cells = [&cells](function<void(Cell*)> draw) {
		for (auto c : cells) {draw(c);}
	};
	const auto loop_nb_cells = [&cells](function<void(Cell*)> draw) {
		for (auto c : cells) {if (c->GetBoundaryType() == BoundaryType::None) {draw(c);}}
	};

	const auto cell_draw_with_tooltip = [&](Cell* cell) {
		const auto& nodes = cell->GetNodes();
		QPolygonF pa(nodes.size());
		int cc = 0;
		const unsigned int num_chem = mesh->GetNumChemicals();

		// Build QGraphicsPolygonItem (via CellItem)
		for (auto const& n : nodes) {
			auto pos = (m_preferences->m_mesh_offset + *n) * m_preferences->m_mesh_magnification;
			pa[cc++] = QPoint(static_cast<int>(pos[0]), static_cast<int>(pos[1]));
		}
		const auto c = cell_colorizer(cell);
		QColor cell_color = QColor::fromRgbF(get<0>(c), get<1>(c), get<2>(c));

		auto p = scene->addPolygon(pa, m_preferences->m_outline_width >= 0 ?
			QPen(QColor(m_preferences->m_cell_outline_color.c_str()), m_preferences->m_outline_width) : QPen(Qt::NoPen),
			cell_color);
		p->setZValue(1);

		// Build tool tip for the current cell
		// General information of current cell
		QString tool_tip = QString("Cell: %1,  type: %2 \n").arg(cell->GetIndex()).arg(cell->GetCellType());
		tool_tip += QString("Chemicals (count=%1): ").arg(num_chem);
		for (unsigned int i = 0; i < num_chem; i++) {
			tool_tip += QString("%1 ").arg((cell->GetChemical(i))/(cell->GetArea()));
		}
		QString bt = QString::fromStdString(ToString(cell->GetBoundaryType()));
		tool_tip += QString("\nArea: %1\nCircumference: %2\nBoundary type: %3")
					.arg(cell->GetArea()).arg(cell->GetCircumference()).arg(bt);

		// Node information of current cell
		QString nodelist = "Node_list = { ";
		for (auto const& n : nodes) {
			nodelist += QString("%1 ").arg(n->GetIndex());
		}
		nodelist += " } ";
		tool_tip += "\nNodes: " + nodelist;

		// Wall information of current cell
		QString walllist = "Wall_list = { ";
		for (auto const& w : cell->GetWalls()) {
			walllist += QString("%1 ").arg(w->GetIndex());
		}
		walllist += " } ";
		tool_tip += "\nWalls: " + walllist;

		p->setToolTip(tool_tip);

		// Actual drawing
		p->show();
	};

	const auto cell_draw = [&](Cell* cell) {
		const auto& nodes = cell->GetNodes();
		QPolygonF pa(nodes.size());
		int cc = 0;

		// Build QGraphicsPolygonItem
		for (auto const& n : nodes) {
			auto pos = (m_preferences->m_mesh_offset + *n) * m_preferences->m_mesh_magnification;
			pa[cc++] = QPoint(static_cast<int>(pos[0]), static_cast<int>(pos[1]));
		}
                const auto c = cell_colorizer(cell);
                QColor cell_color = QColor::fromRgbF(get<0>(c), get<1>(c), get<2>(c));

		auto p = scene->addPolygon(pa, m_preferences->m_outline_width >= 0 ?
			QPen(QColor(m_preferences->m_cell_outline_color.c_str()), m_preferences->m_outline_width) : QPen(Qt::NoPen),
			cell_color);
		p->setZValue(1);

		// Actual drawing
		p->show();
	};

	const auto cell_draw_center = [&](Cell* cell) {
		QRectF rect(-1*m_preferences->m_node_magnification, -1*m_preferences->m_node_magnification,
				2*m_preferences->m_node_magnification, 2*m_preferences->m_node_magnification);
		auto disk = scene->addEllipse(rect, QPen(), QColor("forest green"));
		disk->setZValue(5);
		disk->show();
		const auto pos = (m_preferences->m_mesh_offset
			+ cell->GetCentroid()) * m_preferences->m_mesh_magnification;
		disk->setPos(pos[0], pos[1]);
	};

	const auto cell_draw_fluxes = [&](Cell* cell) {
		// get the mean flux through this cell
		auto reduce_cell_walls = [](Cell* cell){
				std::array<double,3> sum {{0.0, 0.0, 0.0}};
				for (auto const& wall : cell->GetWalls()) {
					sum += (wall->GetC1() == cell) ?
						PINdir(wall->GetC1(), wall->GetC2(), wall)
						: PINdir(wall->GetC2(), wall->GetC1(), wall);
				}
				return sum;
			};
		const auto vec_flux = Normalize(reduce_cell_walls(cell));

		ArrowItem* arrow     = new ArrowItem();
		scene->addItem(arrow);
		const auto centroid  = cell->GetCentroid();
		const auto from      = (m_preferences->m_mesh_offset + centroid
			- vec_flux * 0.5 * m_preferences->m_arrow_size) * m_preferences->m_mesh_magnification;
		const auto to        = (m_preferences->m_mesh_offset + centroid
			+ vec_flux * 0.5 * m_preferences->m_arrow_size) * m_preferences->m_mesh_magnification;

		arrow->setPen( QPen(QColor(m_preferences->m_arrow_color.c_str()), m_preferences->m_outline_width));
		arrow->setLine(from[0], from[1], to[0], to[1]);
		arrow->setZValue(20);
		arrow->show();
	};

	const auto cell_draw_text = [&](Cell* cell, const QString& text) {
		const auto pos = (m_preferences->m_mesh_offset
			+ cell->GetCentroid()) * m_preferences->m_mesh_magnification;
		auto ctext = scene->addText(text, QFont( "Helvetica",
			m_preferences->m_cell_number_size, QFont::Bold));
		ctext->setDefaultTextColor( QColor(m_preferences->m_text_color.c_str()) );
		ctext->setZValue(20);
		ctext->show();
		ctext->setPos(pos[0],pos[1]);
	};

	const auto cell_draw_index = [&](Cell* cell) {
		cell_draw_text(cell, QString::number(cell->GetIndex()));
	};

	const auto cell_draw_axis = [&](Cell* cell) {
		const auto& tup         = cell->GetGeoData().GetEllipseAxes();
		const auto long_axis    = Normalize(get<1>(tup));
		const auto short_axis   = Orthogonalize(long_axis);
		const auto centroid     = cell->GetCentroid();
		const auto delta        = 0.5 * get<2>(tup) * short_axis;
		const auto from         = (m_preferences->m_mesh_offset + centroid - delta) * m_preferences->m_mesh_magnification;
		const auto to           = (m_preferences->m_mesh_offset + centroid + delta) * m_preferences->m_mesh_magnification;

		auto line = scene->addLine(QLineF(from[0], from[1], to[0], to[1]),
		                                QPen(QColor(m_preferences->m_arrow_color.c_str()),2));
		line->setZValue(2);
		line->setZValue(10);
		line->show();
	};

	const auto node_draw = [&](Node* node) {
		const auto pos = (m_preferences->m_mesh_offset + *node) * m_preferences->m_mesh_magnification;
		NodeItem* item = new NodeItem(node, m_preferences->m_node_magnification);
		scene->addItem(item);
		item->setColor();
		item->setZValue(5);
		item->show();
		item->setPos(pos[0], pos[1]);
	};

	const auto node_draw_index = [&](Node* node) {
		const auto pos = (m_preferences->m_mesh_offset + *node) * m_preferences->m_mesh_magnification;
		auto number =  scene->addSimpleText(QString::number(node->GetIndex()),
		                        QFont( "Helvetica", m_preferences->m_node_number_size, QFont::Bold));
		//QGraphicsSimpleTextItem* number = new QGraphicsSimpleTextItem ( QString::number(node->GetIndex()), 0, scene);
		//number->setFont( QFont( "Helvetica", m_preferences->m_node_number_size, QFont::Bold) );
		number->setPen( QPen (m_preferences->m_text_color.c_str()) );
		number->setZValue(20);
		number->show();
		number->setPos(pos[0], pos[1]);
	};

	const auto node_draw_with_tooltip = [&](Node* node) {
		// Build tool tip for the current node
		const auto pos = (m_preferences->m_mesh_offset + *node) * m_preferences->m_mesh_magnification;
		NodeItem* item = new NodeItem(node, m_preferences->m_node_magnification);
		scene->addItem(item);
		item->setColor();
		item->setZValue(5);
		item->show();
		item->setPos(pos[0], pos[1]);

		// General information of current node
		QString tool_tip = QString("Node: %1\n").arg(node->GetIndex());
		tool_tip += QString("Fixed: %1\n").arg(node->IsFixed());
		tool_tip += QString("Sam: %1\n").arg(node->IsSam());


		// Cell information of current node
		tool_tip += "Cells: Cell_list = { ";
		for (auto const& c : mesh->GetCells()) {
			if (std::find_if(c->GetNodes().begin(), c->GetNodes().end(),
				[&node](Node* n){return n->GetIndex() == node->GetIndex();}) != c->GetNodes().end()) {
				tool_tip += QString("%1 ").arg(c->GetIndex());
			}
		}
		tool_tip += "}\n";

		// Wall information of current node
		tool_tip += "Walls: Wall_list = { ";
		for (auto const& w : mesh->GetNodeOwningWalls(node)) {
			tool_tip += QString("%1 ").arg(w->GetIndex());
		}
		tool_tip += " }";

		item->setToolTip(tool_tip);

		// Actual drawing
		item->show();
	};

	const auto wall_draw = [&](Wall* wall) {
		// Each wall connects two cells and needs to be visualized inside both cells.
		// Therefore, each wall needs two WallItems for visualization.
		WallItem* wi1 = new WallItem(wall, 1, scene, m_preferences->m_outline_width,
		                m_preferences->m_mesh_magnification, m_preferences->m_mesh_offset);
		WallItem* wi2 = new WallItem(wall, 2, scene, m_preferences->m_outline_width,
		                m_preferences->m_mesh_magnification, m_preferences->m_mesh_offset);
		scene->addItem(wi1);
		scene->addItem(wi2);
		wi1->show();
		wi2->show();
	};

	const auto wall_draw_with_tooltip = [&](Wall* wall) {
		// Each wall connects two cells and needs to be visualized inside both cells.
		// Therefore, each wall needs two WallItems for visualization.
		WallItem* wi1 = new WallItem(wall, 1, scene, m_preferences->m_outline_width,
		                m_preferences->m_mesh_magnification, m_preferences->m_mesh_offset);
		WallItem* wi2 = new WallItem(wall, 2, scene, m_preferences->m_outline_width,
		                m_preferences->m_mesh_magnification, m_preferences->m_mesh_offset);
		scene->addItem(wi1);
		scene->addItem(wi2);
		// Since the tooltip of a wall only displays the two neighboring cell indices
		// both WallItems share the same tooltip.
		QString wall_tt = QString("Wall %1").arg(wall->GetIndex());
		wall_tt += QString("\nType: %1")
			.arg(QString::fromStdString(WallType::ToString(wall->GetType())));
		wall_tt += QString("\nCells: { %1 %2 }").arg(wall->GetC1()->GetIndex()).arg(wall->GetC2()->GetIndex());
		wall_tt += QString("\nNodes: { %1 %2 }").arg(wall->GetN1()->GetIndex()).arg(wall->GetN2()->GetIndex());
		wall_tt += QString("\nLength: %1").arg(wall->GetLength());
		wall_tt += QString("\nRestLength: %1").arg(wall->GetRestLength());
		wall_tt += QString("\nStrength: %1").arg(wall->GetStrength());
		wi1->setToolTip(wall_tt);
		wi2->setToolTip(wall_tt);
		wi1->show();
		wi2->show();
	};

	const auto boundary_polygon_draw = [&](Cell* boundary_polygon) {
		QPolygonF pa(boundary_polygon->GetNodes().size());

		int cc = 0;
		for (const auto& n : boundary_polygon->GetNodes()) {
			const auto pos = (m_preferences->m_mesh_offset + *n) * m_preferences->m_mesh_magnification;
			pa[cc++] = QPoint(static_cast<int>(pos[0]), static_cast<int>(pos[1]));
		}
		auto p = scene->addPolygon(pa,
		        m_preferences->m_outline_width >= 0 ? QPen(QColor(m_preferences->m_cell_outline_color.c_str()), m_preferences->m_outline_width) : QPen(Qt::NoPen),
			Qt::NoBrush);
		//p->setPolygon(pa);
		//p->setPen(m_preferences->m_outline_width >= 0 ? QPen(QColor(m_preferences->m_cell_outline_color.c_str()), m_preferences->m_outline_width) : QPen(Qt::NoPen));
		//p->setBrush(Qt::NoBrush);
		p->setZValue(1);
		p->show();
	};

	const auto mesh_draw_boundary = [&, mesh]() {
		boundary_polygon_draw(mesh->GetBoundaryPolygon());
	};

	// Plotting
	if (!m_preferences->m_only_tissue_boundary && m_preferences->m_cells && m_preferences->m_tooltips) {
		if (m_preferences->m_border_cells)
			loop_cells(cell_draw_with_tooltip);
		else
			loop_nb_cells(cell_draw_with_tooltip);
	}
	if (!m_preferences->m_only_tissue_boundary && m_preferences->m_cells && !m_preferences->m_tooltips) {
		if (m_preferences->m_border_cells)
			loop_cells(cell_draw);
		else
			loop_nb_cells(cell_draw);
	}
	if (m_preferences->m_cell_centers) {
		if (m_preferences->m_border_cells)
			loop_cells(cell_draw_center);
		else
			loop_nb_cells(cell_draw_center);
	}
	if (m_preferences->m_fluxes) {
		if (m_preferences->m_border_cells)
			loop_cells(cell_draw_fluxes);
		else
			loop_nb_cells(cell_draw_fluxes);
	}
	if (m_preferences->m_cell_numbers) {
		loop_cells(cell_draw_index);
	}
	if (m_preferences->m_cell_axes) {
		loop_cells(cell_draw_axis);
	}
	if (m_preferences->m_cell_strain) {
		// draw strain not implemented yet!
	}
	if (m_preferences->m_node_numbers) {
		loop_nodes(node_draw_index);
	}
	if (m_preferences->m_walls) {
		if(m_preferences->m_tooltips) {
			loop_walls(wall_draw_with_tooltip);
		} else {
			loop_walls(wall_draw);
		}
	}
	if (m_preferences->m_nodes) {
		if (m_preferences->m_tooltips) {
			loop_nodes(node_draw_with_tooltip);
		} else {
			loop_nodes(node_draw);
		}
	}
	if (m_preferences->m_only_tissue_boundary) {
		mesh_draw_boundary();
	}
}

} // namespace
