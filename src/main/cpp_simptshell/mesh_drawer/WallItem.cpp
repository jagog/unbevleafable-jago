/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for WallItem.
 */

#include "../../cpp_simptshell/mesh_drawer/WallItem.h"

#include "bio/Cell.h"
#include "bio/Node.h"
#include "math/math.h"

#include <QGraphicsScene>
#include <QGraphicsEllipseItem>
#include <QPainter>

namespace SimPT_Shell {

using namespace std;
using namespace SimPT_Sim::Util;
using namespace SimPT_Sim;

WallItem::WallItem(Wall* w, int wallnumber, QGraphicsScene* canvas, double outlinewidth,
	double magnification, array<double, 3> offset)
       : m_wall(w)
{
	m_wall_number = wallnumber;
	QPolygonF polygon;
	const Cell* c   = (m_wall_number == 1) ? w->GetC1() : w->GetC2();
	const Node& N1  = *(w->GetN1());
	const Node& N2  = *(w->GetN2());

	if (c->IsBoundaryPolygon()) {
		// line with "PIN1"is a bit inside the cell wall
		const auto edgevec   = Normalize(N2 - N1);
		const auto perp      = Orthogonalize(edgevec) * outlinewidth * 0.5;
		const auto fromPlus  = (N1 + offset + perp) * magnification;
		const auto fromMin   = (N1 + offset - perp) * magnification;
		const auto toPlus    = (N2 + offset + perp) * magnification;
		const auto toMin     = (N2 + offset - perp) * magnification;

		polygon << QPointF(fromPlus[0], fromPlus[1])
			<< QPointF(toPlus[0],   toPlus[1])
			<< QPointF(toMin[0],    toMin[1])
			<< QPointF(fromMin[0],  fromMin[1])
			<< QPointF(fromPlus[0], fromPlus[1]);

		// Place a circle at the far end of each boundary polygon wallitem
		const auto center = (N1 + offset) * magnification;
		QRectF rect(0, 0, outlinewidth, outlinewidth);
		rect.moveCenter(QPointF(center[0], center[1]));
		QGraphicsEllipseItem* circle = new QGraphicsEllipseItem(rect);
		circle->setPen(Qt::NoPen);
		circle->setBrush(QColor("Purple"));
		circle->setZValue(256);
		canvas->addItem(circle);
	} else {
		const auto centroid  = c->GetCentroid();
		const auto middle1   = (offset + 0.5 * (centroid + N1)) * magnification;
		const auto middle2   = (offset + 0.5 * (centroid + N2)) * magnification;
		const auto middle11  = (offset + 0.5 * (middle1  + N1)) * magnification;
		const auto middle22  = (offset + 0.5 * (middle2  + N2)) * magnification;

		polygon << QPointF(middle1[0],  middle1[1])
			<< QPointF(middle2[0],  middle2[1])
			<< QPointF(middle22[0], middle22[1])
			<< QPointF(middle11[0], middle11[1])
			<< QPointF(middle1[0],  middle1[1]);
	}
	setPolygon(polygon);
	setColor();//DDV
//	setColor4Test();//DDV
//	setColorWallStrength();//DDV
	setZValue(120);
	show();
}

void WallItem::setColor()
{
	QColor diffcolor(0, 0, 0);

	if (m_wall->GetTransporters1().size() >= 2) {
		const double tr = m_wall_number == 1 ? m_wall->GetTransporters1(1) : m_wall->GetTransporters2(1);
		diffcolor.setRgb(static_cast<int>((tr / (1 + tr)) * 255.0), 0, 0);
	}

	const Cell* c = (m_wall_number == 1) ? m_wall->GetC1() : m_wall->GetC2();
	if (m_wall->IsAuxinSource() && c->IsBoundaryPolygon()) {
		setBrush(QColor("Purple"));
	} else {
		if (m_wall->IsAuxinSink() && c->IsBoundaryPolygon()) {
			setBrush(QColor("Blue"));
		} else {
			setBrush(diffcolor);
		}
	}
}

void WallItem::setColorWallStrength()
{
	QColor diffcolor(0, 0, 0);

	double wallstr = m_wall->GetStrength();
	if (wallstr >= 2) {
		diffcolor.setRgb(255., 0, 0);
	} else {
		diffcolor.setRgb(0., 0, 255.);
	}
	setBrush(diffcolor);
}


void WallItem::setColor4Test()//DDV2012: alternative method for coloring walls, i.e. based on strain rates
{
    QColor diffcolor;
    QColor const purple("Purple");
    QColor const blue("blue");

//    Wall* const w = &getWall();
//    double const tr = wn==1?w->Transporters1(1):w->Transporters2(1);//DDV2012: redundant code ...
//    diffcolor.setRgb( (int)( ( log2(1+tr) / (1 + log2(1+tr)) )*255.), 0, 0);//DDV2012
//    CellBase* const c = wn==1?m_wall->C1():m_wall->C2();
//    diffcolor.setRgb( (int)( ( tr / (1 + tr) )*255.), 0, 0);//DDV2012

    //DDV2012: alternative method for coloring walls, i.e. based on strain rates
    std::array<double, 3> ref{{1., 0., 0.}};
    std::array<double, 3> wa = *(m_wall->GetN2()) - *(m_wall->GetN1());
    double sa = SignedAngle(wa, ref);
    if (sa >= 0 )//DDV2012: linear range for small positive interval
    {
    	diffcolor.setRgb( 255 , 0, 0);
    }
    else if ( sa < 0. )
    {
    	diffcolor.setRgb(  0 , 0, 255);
    }

    if ((sa > (-pi() / 4)) && (sa <= (pi() / 4)))
    { //UPPER WALL: I
	diffcolor.setRgb( 255 , 0, 0);
    }
    else if ((sa <= (-pi() * 3 / 4)) || (sa > (pi() * 3 / 4)))
    { //LOWER WALL: II
	diffcolor.setRgb( 0 , 0, 255.);
    }
    else if ((sa > (-pi() * 3 / 4)) && (sa <= (-pi() / 4)))
    { //RIGHT WALL?: III
	diffcolor.setRgb( 0 , 255., 0);
    }
    else if ((sa > (pi() / 4)) && (sa <= (pi() * 3 / 4)))
    { //LEFT WALL?: IV
	diffcolor.setRgb( 255 , 0, 255.);
    }

    setBrush(diffcolor);

    //DDV2012 - END

//    if (w->AuxinSource() && c->BoundaryPolP())
//    {
//        setBrush(purple);
//    }
//    else
//    {
//        if (w->AuxinSink() && c->BoundaryPolP())
//        {
//            setBrush(blue);
//        }
//        else
//        {
//            setBrush(diffcolor);
//        }
//    }
}


} // namespace
