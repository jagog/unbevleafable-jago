/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for RootViewerNode.
 */

#include "RootViewerNode.h"

#include "viewers/Hdf5Viewer.h"
#include "viewers/LogViewer.h"
#include "viewers/LogWindowViewer.h"
#include "viewers/QtViewer.h"
#include "viewers/XmlViewer.h"
#include "gui/controller/AppController.h"
#include "sim/CoupledSim.h"
#include "viewer/SubjectNode.h"
#include "viewer/ViewerNode.h"

namespace SimShell { namespace Ws { class MergedPreferences; } }

using namespace std;
using namespace SimShell::Viewer;

namespace SimShell {
namespace Viewer {

template class SubjectNode<SimPT_Sim::Sim>;

template <>
struct viewer_is_widget<SimPT_Shell::LogWindowViewer>
{
	static const bool value = true;
};

template <>
struct viewer_is_widget<SimPT_Shell::QtViewer>
{
	static const bool value = true;

};

} // namespace Viewer
} // namespace SimShell


namespace SimPT_Shell {
namespace Viewer {

template <typename SubjectType>
RootViewerNode<SubjectType>::RootViewerNode(std::shared_ptr<SimShell::Ws::MergedPreferences> p,
                               std::shared_ptr<SubjectType> subject,
                               SimShell::Gui::Controller::AppController* a)
      : SubjectNode<SubjectType>(p, subject,
	   {
		{"HDF5 File Viewer",
			make_shared<ViewerNode<Hdf5Viewer, SubjectType>>(p->GetChild("hdf5"))},
		{"Log Console",
			make_shared<ViewerNode<LogViewer, SubjectType>>(p->GetChild("log"))},
		{"Log Dock Window",
			make_shared<ViewerNode<LogWindowViewer, SubjectType>>(p->GetChild("logwindow"), a)},
		{"Qt Window",
			make_shared<ViewerNode<QtViewer, SubjectType>>(p->GetChild("qt"), a)},
		{"XML File Viewer",
			make_shared<ViewerNode<XmlViewer, SubjectType>>(p->GetChild("xml"))}
	   })
{
}

template <>
RootViewerNode<SimPT_Sim::CoupledSim>::RootViewerNode(std::shared_ptr<SimShell::Ws::MergedPreferences> p,
                               std::shared_ptr<SimPT_Sim::CoupledSim> subject,
                               SimShell::Gui::Controller::AppController* a)
	: SubjectNode<SimPT_Sim::CoupledSim>(p, subject,
	   { // Children viewer nodes
		{"Log Dock Window",
			make_shared<ViewerNode<LogWindowViewer, SimPT_Sim::CoupledSim>>(p->GetChild("logwindow"), a)}
	   })
{
}

// Explicit template instantiations
template class RootViewerNode<SimPT_Sim::Sim>;
template class RootViewerNode<SimPT_Sim::CoupledSim>;

} // namespace
} // namespace
