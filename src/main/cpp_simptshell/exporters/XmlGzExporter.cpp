/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for XmlGzExporter.
 */

#include "../../cpp_simptshell/exporters/XmlGzExporter.h"

#include <fstream>
#include "fileformats/PTreeFile.h"
#include "sim/Sim.h"

namespace SimPT_Shell {

using namespace std;
using namespace SimPT_Sim::Util;
using namespace SimPT_Sim;

bool XmlGzExporter::Export(shared_ptr<Sim> sim, const string& file_path, bool overwrite)
{
	if (!overwrite && ifstream(file_path).good()) {
		return false; // Don't overwrite
	}

	bool status = false;
	try {
		PTreeFile::WriteXmlGz(file_path, sim->ToPtree());
	}
	catch(...) {
		status = false;
	}
	return status;
}

string XmlGzExporter::GetFileExtension()
{
	return "xml.gz";
}

} // namespace
