#ifndef VIEWERS_VECTOR_GRAPHICS_PREFERENCES_H_
#define VIEWERS_VECTOR_GRAPHICS_PREFERENCES_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface/Implementation for VectorGraphicsPreferences.
 */

#include "exporters/common/GraphicsPreferences.h"
#include "util/misc/Exception.h"

namespace SimPT_Shell
{

/**
 * Preferences for a graphic viewer.
 */
struct VectorGraphicsPreferences : GraphicsPreferences
{
	enum Format {
		Pdf, Eps
	};

	Format       m_format;

	VectorGraphicsPreferences()
	{
		m_format               = Pdf;
	}

	void Update(const SimShell::Ws::Event::MergedPreferencesChanged& e)
	{
		GraphicsPreferences::Update(e);

		const auto f = e.source->Get<std::string>("format", "");
		if (!f.empty()) {
			if (f == "pdf") {
				m_format = Pdf;
			} else if (f == "eps") {
				m_format = Eps;
			} else {
				throw SimPT_Sim::Util::Exception("Unknown vector_graphics format: " + f);
			}
		}
	}
};

} // namespace

#endif
