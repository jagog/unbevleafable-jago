/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for PlyExporter.
 */

#include "PlyExporter.h"

#include "bio/Cell.h"
#include "bio/Edge.h"
#include "bio/Mesh.h"
#include "sim/CoreData.h"
#include "sim/SimInterface.h"

#include <fstream>
#include <QFile>
#include <QString>
#include <QTextStream>

namespace SimPT_Shell {

using namespace std;
using namespace SimPT_Sim;

bool PlyExporter::Export(shared_ptr<SimInterface> sim, string const& file_path, bool overwrite)
{
	if (ifstream(file_path).good()) {
		if (overwrite) {
			// Qt doesn't overwrite by default, must delete file first
			QFile::remove(QString::fromStdString(file_path));
		} else {
			return false; // Don't overwrite
		}
	}

	bool status = false;
	QFile file(QString::fromStdString(file_path));
	if (file.open(QIODevice::WriteOnly)) {
	        const auto mesh = sim->GetCoreData().m_mesh;
		QTextStream stream(&file);
		StreamHeader(*mesh, stream);
		StreamBody(*mesh, stream);
		file.close();
		status = true;
	}
	return status;
}

string PlyExporter::GetFileExtension()
{
	return "ply";
}

void PlyExporter::StreamHeader(const Mesh& mesh, QTextStream& stream)
{
	stream << "ply" << endl;
	stream << "format ascii 1.0" << endl;
	stream << "element vertex " << mesh.GetNodes().size() << endl;
	stream << "property double x" << endl;
	stream << "property double y" << endl;
	stream << "property double z" << endl;
	stream << "property uint attributes.fixed" << endl; //use uint for boolean
	stream << "element face " << mesh.GetCells().size() << endl;
	stream << "property list uchar int vertex_indices" << endl;
	stream << "property list uchar int wall_indices" << endl;
	stream << "property list uchar double attributes.chemical_array" << endl;
	stream << "element boundary 1" << endl;
	stream << "property list uchar int vertex_indices" << endl;
	stream << "property list uchar int wall_indices" << endl;
	stream << "element wall " << mesh.GetWalls().size() << endl;
	stream << "property int node1" << endl;
	stream << "property int node2" << endl;
	stream << "property int cell1" << endl;
	stream << "property int cell2" << endl;
	stream << "property double attributes.rest_length" << endl;
	stream << "property double attributes.rest_length_init" << endl;
	stream << "element edge " << mesh.GetCells().size() + mesh.GetNodes().size() - 1 << endl;
	stream << "property int vertex1" << endl;
	stream << "property int vertex2" << endl;
	stream << "end_header" << endl;
}

void PlyExporter::StreamBody(const Mesh& mesh, QTextStream& stream)
{
	//Stream nodes (order matters for indexing when streaming cells).
	for (const auto& n : mesh.GetNodes()) {
		stream << (*n)[0] << " " << (*n)[1] << " " << (*n)[2] << " "  << (unsigned int)n->IsFixed() << endl;
	}

	//Stream cells.
	for (auto c : mesh.GetCells()) {
		stream << c->GetNodes().size();
		for (Node* n : c->GetNodes()) {
			stream << " " << n->GetIndex();
		}
		stream << " " << c->GetWalls().size();
		for (Wall* w : c->GetWalls()) {
			stream << " " << w->GetIndex();
		}
		auto chemicals = c->GetChemicals();
		stream << " " << chemicals.size();
		for (double chemical : chemicals) {
			stream << " " << chemical;
		}
		stream << endl;
	}

	//Stream boundary polygon.
	stream << mesh.GetBoundaryPolygon()->GetNodes().size();
	for (Node* n : mesh.GetBoundaryPolygon()->GetNodes()) {
		stream << " " << n->GetIndex();
	}
	stream << " " << mesh.GetBoundaryPolygon()->GetWalls().size();
	for (Wall* w : mesh.GetBoundaryPolygon()->GetWalls()) {
		stream << " " << w->GetIndex();
	}
	stream << endl;

	//Stream walls and collect edges.
	std::list<Edge> edges;
	for (const auto& w : mesh.GetWalls()) {
		stream << w->GetN1()->GetIndex() << " " << w->GetN2()->GetIndex() << " ";
		stream << w->GetC1()->GetIndex() << " " << w->GetC2()->GetIndex() << " ";
		stream << w->GetRestLength() << endl;

		auto newEdges = w->GetEdges();
		edges.insert(edges.end(), newEdges.begin(), newEdges.end());
	}
	edges.unique();

	//Stream edges.
	for (const auto& e : edges) {
		stream << e.GetFirst()->GetIndex() << " " << e.GetSecond()->GetIndex() << endl;
	}
}

} // namespace
