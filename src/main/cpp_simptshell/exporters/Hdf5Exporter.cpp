/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Hdf5 Exporter.
 */

#include "../../cpp_simptshell/exporters/Hdf5Exporter.h"

#include "fileformats/Hdf5File.h"
#include "sim/Sim.h"

#include <fstream>

namespace SimPT_Shell {

using namespace std;
using SimPT_Sim::Sim;

bool Hdf5Exporter::Export(shared_ptr<Sim> sim, const string& file_path, bool overwrite)
{
	if (!overwrite && ifstream(file_path).good()) {
		return false;
	}

	bool status = false;
	Hdf5File   file(file_path);
	if (file.IsOpen()) {
		file.Write(sim);
		status = file.Close();
	}
	return status;
}

string Hdf5Exporter::GetFileExtension()
{
	return "h5";
}


} // namespace
