#ifndef VIEW_FILE_VIEWER_H_INCLUDED
#define VIEW_FILE_VIEWER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for FileViewer.
 */

#include "../../../cpp_simptshell/viewers/common/FileViewerPreferences.h"
#include "../../../cpp_simptshell/viewers/common/PreferencesObserver.h"
#include "workspace/MergedPreferences.h"
#include <string>
#include <sstream>

namespace SimPT_Shell {

/**
 * Viewer that writes to file and needs to maintain
 * some state info w.r.t. file it is writing to.
 */
template <typename FileType>
class FileViewer
{
public:
	FileViewer(const std::shared_ptr<SimShell::Ws::MergedPreferences>&);

	FileViewer(const std::shared_ptr<SimShell::Ws::MergedPreferences>&, std::string const & dir_path);

	/// Destructor must be virtual.
	virtual ~FileViewer() {}

	/// Check whether export should happen and then do so.
	template <typename EventType>
	void Update(const EventType& e);

private:
	using prefs_type = PreferencesObserver<FileViewerPreferences>;

private:
	/// Utility function builds file path.
	std::string MakeFilepath(const std::string& file_name) const;

private:
	std::shared_ptr<prefs_type>   m_preferences;
	std::string                   m_dir_path;
	FileType                      m_exporter;
};

//-----------------------------------------------------------------------------------
// Primary implementations
//-----------------------------------------------------------------------------------

template <typename FileType>
FileViewer<FileType>::FileViewer(const std::shared_ptr<SimShell::Ws::MergedPreferences>& p)
	: m_preferences(prefs_type::Create(p)),
	  m_dir_path(p->GetPath()),
	  m_exporter(MakeFilepath(m_preferences->m_file))
{
}

template <typename FileType>
std::string FileViewer<FileType>::MakeFilepath(const std::string& file_name) const
{
	std::string const postfix = FileType::GetFileExtension();
	std::stringstream file_path;
	file_path << m_dir_path << '/' << file_name << "." << postfix;
	return file_path.str();
}

template <typename FileType>
template <typename EventType>
void FileViewer<FileType>::Update(const EventType& e)
{
	// Unwrap event data
	typename EventType::Source es   = e.GetSource();
	const int step                  = e.GetStep();
	typename EventType::Type et     = e.GetType();

	const bool check = (et == EventType::Type::Forced)
		|| (et == EventType::Type::Done && m_preferences->m_stride == 0)
		|| (et == EventType::Type::Stepped && ((m_preferences->m_stride != 0) && (step % m_preferences->m_stride == 0)));

	// Check whether writing is required
	if (m_exporter.IsOpen() && check) {
		m_exporter.Write(es);
	}
}

} // namespace

#endif // end_of_include_guard
