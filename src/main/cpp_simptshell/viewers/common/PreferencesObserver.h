#ifndef VIEWERS_PREFERENCES_OBSERVER_H_INCLUDED
#define VIEWERS_PREFERENCES_OBSERVER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Preferences observer.
 */

#include "workspace/MergedPreferences.h"
#include <memory>

namespace SimPT_Shell {

/**
 * Listener for SimShell::Event::MergedPreferencesChanged events.
 * Reads preferences from SimShell::Preferences object and stores them in member fields.
 */
template <typename PreferencesType>
class PreferencesObserver : public PreferencesType
{
public:
	using self_type = PreferencesObserver<PreferencesType>;

	static std::shared_ptr<self_type> Create(const std::shared_ptr<SimShell::Ws::MergedPreferences>& p)
	{
		auto result = std::shared_ptr<self_type>(new self_type(p));
		p->Register(result, std::bind(&self_type::Update, result.get(), std::placeholders::_1));
		return result;
	}

private:
	/// Private constructor, use Create().
	PreferencesObserver(const std::shared_ptr<SimShell::Ws::MergedPreferences>& p)
	{
		PreferencesType::Update({p});
	}
};

} // namespace

#endif // end_of_include_guard
