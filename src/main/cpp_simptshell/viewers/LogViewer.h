#ifndef LOG_VIEWER_H_INCLUDED
#define LOG_VIEWER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Log Viewer.
 */

#include "sim/event/SimEvent.h"
#include "workspace/MergedPreferences.h"
#include <cpp_logging/logger.h>
#include <sstream>

namespace SimPT_Shell {

/**
 * A viewer that logs sim events to console.
 */
class LogViewer {
public:
	LogViewer(const std::shared_ptr<SimShell::Ws::MergedPreferences>&) {}

	template <typename EventType>
	void Update(const EventType&);
};

template <typename EventType>
inline void LogViewer::Update(const EventType& e)
{
	auto state = e.GetSource()->GetState();
	SimPT_Logging::SimptshellLogger::get()->debug("{} === [LOG] ===", LOG_TAG_14);
	std::stringstream temp;
	state.PrintToStream(temp);
	SimPT_Logging::SimptshellLogger::get()->debug("{} {}\n", LOG_TAG_14, temp.str());
}

}// namespace

#endif // end-of-include-guard

