/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ConversionList
 */

#include "gui/ConversionList.h"

#include "converter/StepSelection.h"
#include "gui/qtmodel/StepFilterProxyModel.h"

#include <QApplication>
#include <QBoxLayout>
#include <QCheckBox>
#include <QFileInfo>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QMessageBox>
#include <QRegExp>
#include <QRegExpValidator>
#include <QScrollBar>
#include <QStandardItemModel>
#include <QTableView>

#include <cassert>
#include <set>

using namespace std;
using namespace SimPT_Sim::Util;

namespace SimPT_Shell {

ConversionList::ConversionList(QWidget *parent)
	: QWidget(parent),
	  m_model(new QStandardItemModel(this)),
	  m_filter_model(new StepFilterProxyModel(this))
{
	connect(m_model, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(UpdateSelectAllCheckState()));
	m_filter_model->setSourceModel(m_model);
	m_filter_model->setFilterKeyColumn(1);

	SetupGui();
	Clear();
}

ConversionList::~ConversionList()
{
}

void ConversionList::AddEntry(const EntryType& entry)
{
	QList<QStandardItem*> row;

	auto timestep_item = new QStandardItem();
	auto filename_item = new QStandardItem();

	timestep_item->setCheckable(true);
	timestep_item->setData(entry.timestep, Qt::DisplayRole);
	timestep_item->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
	timestep_item->setEditable(false);
	row.append(timestep_item);

	filename_item->setData(QString::fromStdString(entry.files), Qt::DisplayRole);
	filename_item->setTextAlignment(Qt::AlignLeft | Qt::AlignVCenter);
	filename_item->setEditable(false);
	row.append(filename_item);

	m_model->appendRow(row);
}

void ConversionList::Clear()
{
	m_model->clear();
	m_model->setHorizontalHeaderLabels(QStringList() << "Step" << "File(s)");
	m_select_all_check_box->setChecked(false);
}

void ConversionList::EmitSelectionChanged()
{
	emit SelectionChanged(!m_table_view->selectionModel()->selection().isEmpty());
}

vector<ConversionList::EntryType> ConversionList::GetCheckedEntries() const
{
	vector<EntryType> result;
	for (int i = 0; i < m_model->rowCount(); ++i) {
		auto timestep_item = m_model->item(i, 0);
		if (timestep_item->checkState() == Qt::Checked) {
			auto filename_item = m_model->item(i, 1);
			bool ok;
			int timestep = timestep_item->data(Qt::DisplayRole).toInt(&ok);
			assert(ok && "Couldn't get int from checked row");
			string filename = filename_item->data(Qt::DisplayRole).toString().toStdString();
			result.push_back(EntryType({timestep, filename}));
		}
	}
	return result;
}

bool ConversionList::HasCheckedEntries() const
{
	return m_model->rowCount() > 0 && m_select_all_check_box->checkState() != Qt::Unchecked;
}

void ConversionList::UpdateStepFilter()
{
	if (m_step_filter->hasAcceptableInput())
		m_filter_model->SetStepRanges(m_step_filter->text());

	ResizeColumns();
	UpdateSelectAllCheckState();
}

void ConversionList::UpdateFileFilter()
{
	m_filter_model->setFilterWildcard(m_file_filter->text());

	ResizeColumns();
	UpdateSelectAllCheckState();
}

void ConversionList::UpdateSelectAllCheckState()
{
	Qt::CheckState checkState = Qt::Unchecked;
	if (m_filter_model->rowCount() > 0) {
		QModelIndex index = m_filter_model->mapToSource(m_filter_model->index(0, 0));
		checkState = m_model->itemFromIndex(index)->checkState();
		for (int i = 1; i < m_filter_model->rowCount(); ++i) {
			index = m_filter_model->mapToSource(m_filter_model->index(i, 0));
			if (checkState != m_model->itemFromIndex(index)->checkState()) {
				checkState = Qt::PartiallyChecked;
				break;
			}
		}
	}

	// Possibly set true on setCheckState(Qt::PartiallyChecked)CheckedSimStatesChanged();
	m_select_all_check_box->setTristate(false);
	m_select_all_check_box->setCheckState(checkState);
	emit CheckedChanged();
}

void ConversionList::SelectAll()
{
	Qt::CheckState checkState = m_select_all_check_box->checkState();
	for (int i = 0; i < m_filter_model->rowCount(); ++i) {
		QModelIndex index = m_filter_model->mapToSource(m_filter_model->index(i, 0));
		m_model->itemFromIndex(index)->setCheckState(checkState);
	}
	// Possibly set true on setCheckState(Qt::PartiallyChecked)CheckedSimStatesChanged();
	m_select_all_check_box->setTristate(false);
	ResizeColumns();
}

void ConversionList::SetupGui()
{
	// > Main layout
	QVBoxLayout* layout = new QVBoxLayout();
	layout->setMargin(0);

	// 	> Table view
	m_table_view = new QTableView();
	m_table_view->setModel(m_filter_model);
	m_table_view->setShowGrid(false);
	m_table_view->verticalHeader()->setHidden(true);
	m_table_view->setSelectionBehavior(QAbstractItemView::SelectRows);
	m_table_view->setHorizontalScrollMode(QTableView::ScrollPerPixel);
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	m_table_view->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	m_table_view->horizontalHeader()->setSectionsClickable(false);
#else
	m_table_view->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
	m_table_view->horizontalHeader()->setClickable(false);
#endif
	m_table_view->horizontalHeader()->setStretchLastSection(true);
	m_table_view->horizontalHeader()->setHighlightSections(false);
	layout->addWidget(m_table_view);
	connect(m_table_view->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
		this, SLOT(EmitSelectionChanged()));
	// 	< Table view

	// 	> Filter & Select all
	QHBoxLayout* selectionLayout = new QHBoxLayout();
	selectionLayout->addWidget(new QLabel("Filter:"));

	// 		> Step filter
	m_step_filter = new QLineEdit();
	m_step_filter->setPlaceholderText("steps");
	m_step_filter->setSizePolicy(QSizePolicy::Ignored, m_step_filter->sizePolicy().verticalPolicy());
	m_step_filter->setValidator(new QRegExpValidator(StepSelection::g_repeated_regex, this));
	selectionLayout->addWidget(m_step_filter, 1);
	connect(m_step_filter, SIGNAL(textChanged(const QString&)), this, SLOT(UpdateStepFilter()));
	// 		< Step filter

	selectionLayout->addWidget(new QLabel("in"));

	// 		> File filter
	m_file_filter = new QLineEdit();
	m_file_filter->setPlaceholderText("files");
	m_file_filter->setSizePolicy(QSizePolicy::Ignored, m_file_filter->sizePolicy().verticalPolicy());
	selectionLayout->addWidget(m_file_filter, 1);
	connect(m_file_filter, SIGNAL(textChanged(const QString&)), this, SLOT(UpdateFileFilter()));
	// 		< File filter

	m_select_all_check_box = new QCheckBox("Select All");
	connect(m_select_all_check_box, SIGNAL(clicked()), this, SLOT(SelectAll()));
	selectionLayout->addWidget(m_select_all_check_box);
	layout->addLayout(selectionLayout);
	// 	< Filter & Select all

	setLayout(layout);
	// < Main layout

}

void ConversionList::ResizeColumns()
{
	// To resize the first column to the longest number (the last)
	int scrollPos = m_table_view->verticalScrollBar()->value();
	m_table_view->scrollToBottom();
	m_table_view->resizeColumnsToContents();
	m_table_view->horizontalHeader()->setStretchLastSection(true);
	m_table_view->verticalScrollBar()->setValue(scrollPos);
}

} // namespace
