/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for ConverterWindow
 */

#include "ConverterWindow.h"

#include "converter/FileConversion.h"
#include "gui/ConversionList.h"
#include "workspace/StartupFileBase.h"
#include "util/misc/Exception.h"
#include "util/misc/StringUtils.h"

#include <QApplication>
#include <QBoxLayout>
#include <QCloseEvent>
#include <QComboBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMessageBox>
#include <QProgressDialog>
#include <QPushButton>
#include <QSignalMapper>
#include <QString>

#include <algorithm>
#include <cassert>


using namespace std;
using namespace SimPT_Sim::Util;

namespace SimPT_Shell {

const QRegExp ConverterWindow::g_export_name_regex("[^\\x0-\\x1F<>:/\\\\|]*"); // The allowed characters are anything except 0x0-0x1F, '<', '>', ':', '"', '/', '\', and '|'. (Windows file naming)

ConverterWindow::ConverterWindow(
	Ws::Project* project,
	std::vector<IConverterFormat*> formats,
	shared_ptr<SimShell::Ws::MergedPreferences> prefs,
	const string& window_title,
	QWidget* parent)
	: QDialog(parent),
	  m_project(project),
	  m_formats(formats),
	  m_preferences(prefs)
{
	setWindowModality(Qt::WindowModal);
	setWindowTitle(QString::fromStdString(window_title));
	setMinimumSize(400, 500);

	// GUI layout
	QVBoxLayout *layout = new QVBoxLayout();

	// Conversion list & loading
	m_conversion_list = new ConversionList();
	connect(m_conversion_list, SIGNAL(CheckedChanged()), this, SLOT(CheckConversionEnabled()));
	layout->addWidget(m_conversion_list);

	// Separator line
	QFrame *line = new QFrame();
	line->setFrameShape(QFrame::HLine);
	line->setFrameShadow(QFrame::Sunken);
	layout->addWidget(line);

	// Exporters
	QFormLayout *exportLayout = new QFormLayout();

	// Formats ComboBox
	QHBoxLayout *comboLayout = new QHBoxLayout();
	m_export_format = new QComboBox();
	for (auto format : m_formats) {
		QString formatName = QString::fromStdString(format->GetName());
		m_export_format->addItem(formatName);
	}
	connect(m_export_format, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(SLOT_ExportFormatChanged(const QString&)));
	comboLayout->addWidget(m_export_format);
	m_options_button = new QPushButton("Options...");
	m_options_button->setEnabled(false);
	connect(m_options_button, SIGNAL(clicked()), this, SLOT(SLOT_OptionsTriggered()));
	comboLayout->addWidget(m_options_button);
	comboLayout->addStretch();
	exportLayout->addRow(new QLabel("Export Format"), comboLayout);

	// Export path
	QHBoxLayout *exportPathLayout = new QHBoxLayout();

	m_export_path = new QLineEdit();
	m_export_path->setReadOnly(true);
	m_export_path->setText(QString::fromStdString(m_project->GetPath()));
	exportPathLayout->addWidget(m_export_path);
	connect(m_export_path, SIGNAL(textChanged(const QString&)), this, SLOT(CheckConversionEnabled()));

	QPushButton *browseExportPathButton = new QPushButton("Browse...");
	exportPathLayout->addWidget(browseExportPathButton);
	connect(browseExportPathButton, SIGNAL(clicked()), this, SLOT(BrowseExportPath()));

	exportLayout->addRow("Export Path", exportPathLayout);

	// Export name
	m_export_name = new QLineEdit();
	m_export_name->setText("leaf");
	exportLayout->addRow("Export Prefix", m_export_name);
	m_export_name->setValidator(new QRegExpValidator(g_export_name_regex));
	connect(m_export_name, SIGNAL(textChanged(const QString&)), this, SLOT(CheckConversionEnabled()));

	layout->addLayout(exportLayout);

	// Convert & Close buttons
	QHBoxLayout *buttonLayout = new QHBoxLayout();
	buttonLayout->addStretch();

	QPushButton *closeButton = new QPushButton("Close");
	connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	buttonLayout->addWidget(closeButton);

	m_convert_button = new QPushButton("Convert");
	m_convert_button->setEnabled(false);
	m_convert_button->setDefault(true);
	connect(m_convert_button, SIGNAL(clicked()), this, SLOT(Convert()));
	buttonLayout->addWidget(m_convert_button);


	layout->addLayout(buttonLayout);

	setLayout(layout);

	// Get list of input files
	Refresh();
}

ConverterWindow::~ConverterWindow()
{
}

void ConverterWindow::CheckConversionEnabled()
{
	bool enabled = m_conversion_list->HasCheckedEntries() && !m_export_path->text().isEmpty() && QDir(m_export_path->text()).exists() && !m_export_name->text().isEmpty();
	m_convert_button->setEnabled(enabled);
}

void ConverterWindow::BrowseExportPath()
{
	QString exportPath = QFileDialog::getExistingDirectory(this, "Select export directory", !m_export_path->text().isEmpty() ? m_export_path->text() : QDir::homePath());
	if (!exportPath.isEmpty()) {
		m_export_path->setText(exportPath);
	}
}

void ConverterWindow::Convert()
{
	auto checked = m_conversion_list->GetCheckedEntries();

	vector<int> timesteps;
	for (auto& checked_entry : checked) {
		timesteps.push_back(checked_entry.timestep);
	}

	auto format = *find_if(m_formats.begin(), m_formats.end(), [this] (const IConverterFormat *format) -> bool { return format->GetName() == m_export_format->currentText().toStdString(); });

	QProgressDialog progress("Converting...", "Cancel", 0, checked.size(), this);
	progress.setWindowTitle("Please wait");
	progress.setMinimumDuration(0);
	progress.setWindowModality(Qt::WindowModal);

	FileConversion conversion(
		m_project,
		timesteps,
		m_preferences,
		format,
		m_export_path->text().toStdString(),
		m_export_name->text().toStdString());

	try {
		conversion.Run([&](int i) {
			QApplication::processEvents();
			progress.setValue(i);
			if (progress.wasCanceled()) {
				throw 0;
			}
		});
	}
	catch (exception &e) {
		QMessageBox::critical(this, "Error exporting steps", e.what(), QMessageBox::Ok);
	}
	catch (...) {
		QMessageBox::critical(this, "Error exporting steps", "Unknown exception occurred", QMessageBox::Ok);
	}
}

void ConverterWindow::Refresh()
{
	m_conversion_list->Clear();
	m_step_to_name_map.clear();
	m_step_to_file_map.clear();

	for (auto& file : *m_project) {
		auto sim_file = static_pointer_cast<Ws::StartupFileBase>(file.second);
		for (auto step : sim_file->GetTimeSteps()) {
			m_step_to_name_map[step].push_back(file.first);
			m_step_to_file_map[step] = sim_file;
		}
	}

	for (auto& it : m_step_to_name_map) {
		string files;
		for (auto file_it = it.second.begin(); file_it != it.second.end(); file_it++) {
			if (file_it != it.second.begin()) {
				files += ", ";
			}
			files += *file_it;
		}
		m_conversion_list->AddEntry({it.first, files});
	}

	m_convert_button->setEnabled(false);
}

void ConverterWindow::SLOT_ExportFormatChanged(const QString&)
{
	//auto format = *find_if(m_formats.begin(), m_formats.end(), [this] (IGuiFormat *format) -> bool { return format->GetName() == m_export_format->currentText().toStdString(); });
	//m_options_button->setEnabled(format->HasOptions());
}

void ConverterWindow::SLOT_OptionsTriggered()
{
	/*auto format = *find_if(m_formats.begin(), m_formats.end(), [this] (IGuiFormat *format) -> bool { return format->GetName() == m_export_format->currentText().toStdString(); });
	auto options = format->ShowOptions(this);
	options->exec();
	delete options;*/
}

} // namespace
