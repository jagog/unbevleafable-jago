/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for StepFilterProxyModel
 */

#include "gui/qtmodel/StepFilterProxyModel.h"

#include <cassert>

namespace SimPT_Shell {

StepFilterProxyModel::StepFilterProxyModel(QObject *parent)
	: QSortFilterProxyModel(parent)
{
}

StepFilterProxyModel::~StepFilterProxyModel()
{
}

void StepFilterProxyModel::SetStepRanges(const QString &ranges)
{
	assert(QRegExp(StepSelection::g_repeated_regex).exactMatch(ranges) && "Ranges string doesn't match StepSelection::g_repeated_regex");
	m_step_selection.SetSelectionText(ranges.toStdString());
	invalidateFilter();
}

bool StepFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
	QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
	bool ok;
	int step = sourceModel()->data(index).toInt(&ok);

	assert(ok && "Couldn't get int from item in first column (of sourceModel)");

	return QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent) && m_step_selection.Contains(step);
}

} // namespace
