/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of AppCustomizer.
 */

#include "AppCustomizer.h"

#include "util/revision/RevisionInfo.h"
#include "workspace/WorkspaceFactory.h"


namespace SimPT_Shell {
namespace Gui {

using namespace std;

using namespace SimPT_Sim::Util;
using namespace SimPT_Shell::Ws;


shared_ptr<IWorkspaceFactory> AppCustomizer::CreateWorkspaceFactory() const
{
	return make_shared<WorkspaceFactory>();
}

string AppCustomizer::GetAbout() const
{
	string s = "<h3>SimPT, version 1.0</h3>";
	s.append("<p><b> Revision id: " + RevisionInfo::HashId() + "<br>");
	s.append("Commit date: " + RevisionInfo::CommitDate() + "</b></p>");
	s.append("<p>\
		Simulator for Plant Tissue a.k.a SimPT or simPT is an Open Source framework \
                for cell-based modeling of plant tissue growth and development. \
		</p>\
		<p>\
		Copyright 2011-2016 Universiteit Antwerpen\
		<Licensed under the EUPL, Version 1.1 or  as soon they will be approved by \
		the European Commission - subsequent versions of the EUPL (the \"Licence\"); \
		You may not use this work except in compliance with the Licence.\
		You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5 \
		Unless required by applicable law or agreed to in writing, software \
		distributed under the Licence is distributed on an \"AS IS\" basis, \
		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. \
		See the Licence for the specific language governing \
		permissions and limitations under the Licence. \
		</p>");
	return s;
}

string AppCustomizer::GetApplicationName() const
{
	return "SimPT_Sim";
}

string AppCustomizer::GetOrganizationName() const
{
	return "SimPT Consortium";
}

} // namespace
} // namespace
