#ifndef SIMPT_SHELL_WORKSPACE_FACTORY_H_INCLUDED
#define SIMPT_SHELL_WORKSPACE_FACTORY_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for WorkspaceFactory.
 */

#include "workspace/IWorkspaceFactory.h"

namespace SimPT_Shell {
namespace Ws {

/**
 * SimPT-specific implementation of workspace factory.
 */
class WorkspaceFactory : public SimShell::Ws::IWorkspaceFactory
{
public:
	/// Virtual destructor.
	virtual ~WorkspaceFactory() {}

	std::shared_ptr<SimShell::Ws::IWorkspace> CreateCliWorkspace(const std::string& path);

	/// @see SimShell::Ws::IWorkspaceFactory
	virtual std::shared_ptr<SimShell::Ws::IWorkspace> CreateWorkspace(const std::string& path);

	/// @see SimShell::Ws::IWorkspaceFactory
	virtual std::string GetDefaultWorkspaceName() const;

	/// @see SimShell::Ws::IWorkspaceFactory
	virtual std::string GetWorkspaceTemplatePath() const;

	/// @see SimShell::Ws::IWorkspaceFactory
	virtual void InitWorkspace(const std::string& path);

	static const std::string   g_project_index_file;
	static const std::string   g_workspace_index_file;
};

} // namespace
} // namespace

#endif // end_of_include_guard
