#ifndef SIMPT_WS_SIMPT_FILE_XMLGZ_H_INCLUDED
#define SIMPT_WS_SIMPT_FILE_XMLGZ_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for StartupFileXmlGz.
 */
#include "../../cpp_simptshell/workspace/util/Compressor.h"
#include "../../cpp_simptshell/workspace/StartupFilePtree.h"

namespace SimPT_Shell {
namespace Ws {


/**
 * A file containing a gzip'ed XML representation of a project.
 */
class StartupFileXmlGz : public StartupFilePtree
{
public:
	/// Constructor.
	/// @param path  Path to file.
	StartupFileXmlGz(const std::string& path);

	/// @see IFile
	virtual std::vector<QAction*> GetContextMenuActions() const;

	/// @see StartupFilePtree
	virtual boost::property_tree::ptree ToPtree() const;

private:
	Util::Compressor   m_decompressor;
};

} // namespace
} // namespace

#endif // end_of_include_guard
