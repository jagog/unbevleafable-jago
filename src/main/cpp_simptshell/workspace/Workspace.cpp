/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for Workspace.
 */

#include "workspace/Workspace.h"

#include <cpp_sim/util/misc/InstallDirs.h>
#include "workspace/CliWorkspace.h"
#include "workspace/GuiWorkspace.h"
#include "workspace/Workspace_def.h"
#include "util/misc/XmlWriterSettings.h"

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::Util;

namespace SimShell { namespace Ws {
	template class Workspace<SimPT_Shell::Ws::Project,
		SimPT_Shell::Ws::WorkspaceFactory::g_workspace_index_file>;
}}

namespace SimPT_Shell {
namespace Ws {

using namespace SimShell;
using namespace SimShell::Ws;

Workspace::Workspace(const string& path,
                               const string& prefs_file)
	: SimShell::Ws::Workspace<Project, WorkspaceFactory::g_workspace_index_file>(path, prefs_file)
{
	string prefs_path = path + '/' + prefs_file;

	if (!QFile::exists(QString::fromStdString(prefs_path))) {
		ptree default_prefs;

		WorkspaceFactory f;
		const string default_prefs_file = f.GetWorkspaceTemplatePath() + '/' + prefs_file;
		read_xml(default_prefs_file, default_prefs, trim_whitespace);

		SetPreferences(default_prefs.get_child("preferences"));
	}
}

void Workspace::Init(const string& p)
{
	string path = p;

	// make sure path doesn't end with a '/'
	if (path.length() > 0 && path.rfind("/") == path.length() - 1) {
		path = path.erase(path.length() - 1);
	}

	// Overwrite preferences files
	string cli_prefs_filename = path + '/' + CliWorkspace::g_preferences_file;
	string gui_prefs_filename = path + '/' + GuiWorkspace::g_preferences_file;

	if (QFile::exists(QString::fromStdString(cli_prefs_filename))) {
		if (!QFile::remove(QString::fromStdString(cli_prefs_filename))) {
			throw Exception("Unable to overwrite \"" + cli_prefs_filename + '"');
		}
	}
	if (QFile::exists(QString::fromStdString(gui_prefs_filename))) {
		if (!QFile::remove(QString::fromStdString(gui_prefs_filename))) {
			throw Exception("Unable to overwrite \"" + gui_prefs_filename + '"');
		}
	}

	WorkspaceFactory f;
	string default_cli_prefs_filename = f.GetWorkspaceTemplatePath()
					+ '/' + CliWorkspace::g_preferences_file;
	string default_gui_prefs_filename = f.GetWorkspaceTemplatePath()
					+ '/' + GuiWorkspace::g_preferences_file;

	if (!QFile::copy(QString::fromStdString(default_cli_prefs_filename),
					QString::fromStdString(cli_prefs_filename))) {
		throw Exception("Unable to copy file \"" + default_cli_prefs_filename
					+ "\". Your installation could be corrupted.");
	}

	if (!QFile::copy(QString::fromStdString(default_gui_prefs_filename),
				QString::fromStdString(gui_prefs_filename))) {
		throw Exception("Unable to copy file \"" + default_gui_prefs_filename
					+ "\". Your installation could be corrupted.");
	}

	// Create empty workspace index file
	string index_filename = path + '/' + WorkspaceFactory::g_workspace_index_file;
	if (!QFile::exists(QString::fromStdString(index_filename))) {
		ptree pt_workspace;
		pt_workspace.put("workspace.projects", "");

		try {
			write_xml(index_filename, pt_workspace, std::locale(), XmlWriterSettings::GetTab());
		}
		catch (xml_parser_error & e) {
			throw Exception("Could not initialize workspace \"" + path + '"');
		}
	}
}

} // namespace
} // namespace
