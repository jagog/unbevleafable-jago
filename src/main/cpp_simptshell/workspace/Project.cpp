/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for SimPT projects.
 */

#include "Project.h"

#include <cpp_sim/util/misc/InstallDirs.h>
#include "converter/FileConverterFormats.h"
#include "gui/ConverterWindow.h"
#include "ptree/PTreeUtils.h"
#include "workspace/Project_def.h"

#include <QAction>

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::Util;

namespace SimShell {
namespace Ws {
	template class Project<SimPT_Shell::Ws::StartupFileBase,
		SimPT_Shell::Ws::WorkspaceFactory::g_project_index_file>;
} // Ws
} // SimShell


namespace SimPT_Shell {
namespace Ws {

Project::Project(const string& path,
                           const string& prefs_file,
                           const shared_ptr<SimShell::Ws::IWorkspace>& w)
	: SimPTProjectBase(path, prefs_file, w)
{
	auto prefs_path = path + '/' + prefs_file;

	if (!QFile::exists(QString::fromStdString(prefs_path))) {
		ptree default_preferences;
		ptree new_preferences;

		WorkspaceFactory f;
		const string default_prefs_file = f.GetWorkspaceTemplatePath() + '/' + prefs_file;
		read_xml(default_prefs_file, default_preferences, trim_whitespace);
		default_preferences = default_preferences.get_child("preferences");

		// Make sure this workspace's preferences contains all the keys from default-preferences.xml.
		PTreeUtils::CopyStructure(default_preferences, new_preferences, "$WORKSPACE$", {"<xmlcomment>"});

		SetPreferences(new_preferences);
	}
}

Project::~Project()
{
}

const Project::ConstructorType Project::Constructor({
	[](const string& type, const string& path, const string& prefs_file, const shared_ptr<SimShell::Ws::IWorkspace>& w)
		-> shared_ptr<SimShell::Ws::IProject>
	{
		if (type == "project") {
			return make_shared<Project>(path, prefs_file, w);
		} else {
			return nullptr;
		}
	}
});

std::vector<QAction*> Project::GetContextMenuActions() const
{
	return {};
}

std::vector<Project::WidgetCallback> Project::GetWidgets()
{
	auto create_window = [this](vector<IConverterFormat*> formats, const string& title,
	                                shared_ptr<SimShell::Ws::MergedPreferences> p, QWidget* parent) {
		auto conv_window = make_shared<ConverterWindow>(this, formats, p, title, parent);
		conv_window->show();
		m_converter_window = conv_window; // keep weak_ptr to created widget
		return conv_window;
	};
	return {{"Convert...", bind(create_window, FileConverterFormats::GetConverterFormats(),
	                                        "Convert Project", placeholders::_1, placeholders::_2)},
		{"Postprocess...", bind(create_window, FileConverterFormats::GetExportFormats(),
		                                "Postprocess Project", placeholders::_1, placeholders::_2)}};
}

void Project::Refresh()
{
	SimPTProjectBase::Refresh();
	if (auto c = m_converter_window.lock()) {
		c->Refresh();
	}
}


} // namespace
} // namespace
