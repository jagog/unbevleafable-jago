#ifndef SIMPT_SHELL_GUI_WORKSPACE_H_INCLUDED
#define SIMPT_SHELL_GUI_WORKSPACE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for workspace.
 */

#include "Workspace.h"

#include <string>

namespace SimPT_Shell {
namespace Ws {

	/// Gui Workspace for the simulator.
	class GuiWorkspace : public Workspace
	{
	public:
		GuiWorkspace(const std::string& path);

		static const std::string g_preferences_file;
	};

} // namepsace
} // namespace

#endif // end_of_include_guard
