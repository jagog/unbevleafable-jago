#ifndef SIMPT_WS_SIMPT_FILE_PTREE_H_INCLUDED
#define SIMPT_WS_SIMPT_FILE_PTREE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for StartupFilePtree.
 */

#include "../../cpp_simptshell/workspace/StartupFileBase.h"

namespace SimPT_Shell {
namespace Ws {

/**
 * Base class for files containing a Sim snapshot that can be read as a ptree.
 */
class StartupFilePtree : public StartupFileBase
{
public:
	/// Constructor.
	/// @param path  Path to file.
	StartupFilePtree(const std::string& path);

	/// @see SimShell::Ws::IFile::CreateSession()
	virtual std::shared_ptr<SimShell::Session::ISession>
	CreateSession(
		std::shared_ptr<SimShell::Ws::IProject> proj,
		std::shared_ptr<SimShell::Ws::IWorkspace> ws) const;

	/// @see StartupFileBase
	virtual SimPT_Sim::SimState GetSimState(int timestep) const;

	/// @see StartupFileBase
	virtual std::vector<int> GetTimeSteps() const;

	/// Returns file content in a ptree format.
	virtual boost::property_tree::ptree ToPtree() const = 0;
};

} // namespace
} // namespace

#endif // end_of_include_guard
