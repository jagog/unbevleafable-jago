#ifndef CLI_CONTROLLER_H_
#define CLI_CONTROLLER_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CliController.
 */

#include "cli/CliConverterTask.h"
#include <cpp_logging/logger.h>
#include "CliSimulationTask.h"
#include "adapt2rfp/adapt2rfp.h"
#include "session/SimSession.h"
#include "util/clock_man/Timeable.h"
#include "workspace/CliWorkspace.h"

#include <QMessageBox>
#include <QObject>
#include <string>

namespace SimPT_Shell {

/**
 * Command line interface application controller.
 */
class CliController : public QObject, public SimPT_Sim::ClockMan::Timeable<>
{
	Q_OBJECT
public:
	/**
	 * Virtual destructor.
	 */
	virtual ~CliController();

	/**
	 * Create controller.
	 * @param workspace   work space that it operates in
	 * @param quiet       mode (verbosity)
	 */
	static std::shared_ptr<CliController> Create(const std::string& workspace, bool quiet);

	/**
	 * Create controller.
	 * @param workspace   work space that it operates in
	 * @param quiet       mode (verbosity)
	 */
	static std::shared_ptr<CliController> Create(std::shared_ptr<Ws::CliWorkspace> workspace, bool quiet);

	/**
	 * Execute converter task
	 * @return    status indicates whether task ran OK.
	 */
	int Execute(CliConverterTask task);

	/**
	 * Execute simulation task.
	 * @return    status indicates whether task ran OK.
	 */
	int Execute(CliSimulationTask task);

	/**
	 * Execution timings in duration units specified in Timeable base class.
	 * @return      records with timing info
	 */
	Timings GetTimings() const;

	/**
	 * Request termination of the simulation
	 */
	void Terminate();

signals:
	/**
	 * Private signal, to be used for signaling that the simulation should stop
	 */
	void TerminationRequested();

private:
	/**
	 * Constructor (use Create method).
	 * @param workspace_model   work space that it operates in
	 * @param quiet             mode (verbosity)
	 */
	CliController(std::shared_ptr<Ws::CliWorkspace> workspace_model, bool quiet);

	/**
	 * Query whether operating in quiet mode.
	 */
	bool IsQuiet() const;

	/**
	 * Let simulator run until termination condition is met.
	 * @return EXIT_SUCCESS/EXIT_FAILURE.
	 */
	int SimulatorRun(CliSimulationTask task);

	/**
	 * Handler for SIGINT interrupt signal.
	 */
	void SigIntHandler(int sig);

	/**
	 * Handler for Qt errors.
	 */
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	void SigQtHandler(QtMsgType type, const QMessageLogContext&, const QString&);
#else
	void SigQtHandler(QtMsgType type, const char* msg);
#endif

	/**
	 * Display error to user.
	 */
	static void UserError(const std::string& msg);

	/**
	 * Display message to user.
	 */
	static void UserMessage(const std::string& msg);

private:
	// Because of Qt wanting to match parameter types of signals and slots by string comparison
	using InfoMessageReason = SimShell::Session::ISession::InfoMessageReason;

private slots:
	/// Slot to receive the simulation info message from the project
	void SimulationInfo(const std::string& message, const InfoMessageReason& reason);

	/// Slot to receive an simulation error from the project
	void SimulationError(const std::string& error);

private:
	/// Prototype for SIGINT interrupt handler.
	//typedef void (SigIntHandlerType)(int);
	using SigIntHandlerType = void (int);

	/// Adaptor turns std::function into SigIntHandlerType raw function type.
	using SigIntAdaptorType = UA_CoMP_Adapt2rfp::Adaptor<SigIntHandlerType>;

	/// Prototype for Qt error handler.
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	using SigQtHandlerType = void (QtMsgType, const QMessageLogContext&, const QString&);
#else
	using SigQtHandlerType = void (QtMsgType, const char*);
#endif

	/// Adaptor turns std::function into SigQtHandlerType raw function type.
	using SigQtAdaptorType = UA_CoMP_Adapt2rfp::Adaptor<SigQtHandlerType>;

private:
	/// Work space with working directories for projects.
	std::shared_ptr<Ws::CliWorkspace>   m_workspace_model;

	/// Controller in quiet mode.
	bool m_quiet;

	/// Adapt controller method to raw function pointer for interrupt handler.
	SigIntAdaptorType  m_sig_int_adaptor;

	/// Adapt controller method to raw function pointer for qt error handler.
	SigQtAdaptorType  m_sig_qt_adaptor;

        /// Timing records.
	Timings  m_timings;
};

} // namespace

#endif // end_of_include_guard
