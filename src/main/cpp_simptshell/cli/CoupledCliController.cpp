/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CoupledCliController.
 */


#include "cli/CoupledCliController.h"

#include "session/SimSession.h"
#include "session/SimSessionCoupled.h"
#include "workspace/CliWorkspace.h"
#include "ptree/PTreeUtils.h"
#include "util/misc/log_debug.h"
#include "util/misc/Exception.h"
#include "workspace/MergedPreferences.h"

#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QApplication>
#include <QFileInfo>

#include <cassert>
#include <csignal>
#include <cstdlib>
#include <sstream>
#include <stdexcept>
#include <string>

namespace SimPT_Shell {

using namespace std;
using namespace std::chrono;
using namespace std::placeholders;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::ClockMan;
using namespace SimPT_Sim::Util;
using namespace UA_CoMP_Adapt2rfp;


CoupledCliController::CoupledCliController(shared_ptr<Ws::CliWorkspace> workspace_model, bool quiet)
	: m_workspace_model(move(workspace_model)), m_quiet(quiet),
	  m_sig_int_adaptor(bind(&CoupledCliController::SigIntHandler, this, placeholders::_1)),
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	  m_sig_qt_adaptor(bind(&CoupledCliController::SigQtHandler, this, placeholders::_1, placeholders::_2, placeholders::_3))
#else
	  m_sig_qt_adaptor(bind(&CoupledCliController::SigQtHandler, this, placeholders::_1, placeholders::_2))
#endif
{
	// register interrupt handler (adaptor auto-converts to function pointer).
	signal(SIGINT, m_sig_int_adaptor);

	// Register Qt error handler (adaptor auto-converts to function pointer).
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	qInstallMessageHandler(m_sig_qt_adaptor);
#else
	qInstallMsgHandler(m_sig_qt_adaptor);
#endif
}

CoupledCliController::~CoupledCliController()
{
	signal(SIGINT, SIG_DFL);
#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
	qInstallMessageHandler(0);
#else
	qInstallMsgHandler(0);
#endif
}

std::shared_ptr<CoupledCliController> CoupledCliController::Create(const string& workspace, bool quiet)
{
	const string here = string(VL_HERE) + " exception:\n";
	shared_ptr<CoupledCliController> ptr;

	try {
		// QApplication must have been instantiated for the Qt services we use.
		// QApp i.o. QCoreApp because of production of graphics output.
		if (static_cast<QApplication*>(QApplication::instance()) == nullptr) {
			throw Exception(here + "No QApplication instantiated in calling context.");
		}

		// Create workspace model.
		auto workspace_model = make_shared<Ws::CliWorkspace>(workspace);
		if (!quiet) {
			UserMessage("Successfully opened workspace " + workspace_model->GetPath());
		}
		ptr = shared_ptr<CoupledCliController>(new CoupledCliController(workspace_model, quiet));
		if (ptr == nullptr) {
			throw Exception(here + "Produced nullptr for " + workspace);
		}
	} catch (Exception& e) {
		UserError(here + e.what());
		throw;
	} catch(...) {
		UserError(here + "Unknown exception!");
	}

	return ptr;
}

int CoupledCliController::Execute(CliSimulationTask task)
{
	int exit_status   = EXIT_SUCCESS;
	Session::SimSession::Stopclock  chrono_total("total", true);

	try {
		bool status = SimulatorRun(task);
		if (status == false) {
			exit_status = EXIT_FAILURE;
		}
	}
	catch (exception& e) {
		exit_status = EXIT_FAILURE;
		throw Exception(e.what());
	}

	m_timings.Record(chrono_total.GetName(), chrono_total.Get());
	return exit_status;
}

CoupledCliController::Timings CoupledCliController::GetTimings() const
{
	return m_timings.GetRecords();
}

bool CoupledCliController::IsQuiet() const
{
	return m_quiet;
}

shared_ptr<Session::SimSessionCoupled>
CoupledCliController::OpenProject(const string& project_name, const string& file_name)
{
	// TODO Refactor the opening of the project, so that the CoupledCliController isn't
	// needed anymore and the CliController can handle both a normal and a coupled project.

	const string here = string(VL_HERE);
	shared_ptr<Session::SimSessionCoupled> session;

	try {
		auto project_it = m_workspace_model->Find(project_name); // may throw
		auto file_it = project_it->second->Find(file_name); // may throw

		// Initialize project
		auto file = file_it->second->GetPath();
		const string extension = QFileInfo(QString::fromStdString(file)).suffix().toStdString();
		if (extension == "xml") {
			ptree sim_pt;
			read_xml(file, sim_pt, trim_whitespace); // may throw
			session = make_shared<Session::SimSessionCoupled>(
					SimShell::Ws::MergedPreferences::Create(
						m_workspace_model, project_it->second.Project()), sim_pt, m_workspace_model);
		}

		if (!IsQuiet()) {
			UserMessage("Successfully read coupled project from file " + file_it->second->GetPath());
		}
	}
	catch (exception& e) {
		throw Exception(here + " exception:\n" + e.what());
	}

	return session;
}

void CoupledCliController::SigIntHandler(int )
{
	signal(SIGINT, SIG_DFL);
	emit TerminationRequested();
}

#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
void CoupledCliController::SigQtHandler(QtMsgType type, const QMessageLogContext& /*context*/, const QString& msg)
{
	switch (type) {
	case QtDebugMsg:
		UserMessage("Qt debug: " + msg.toStdString());
		break;
	#if (QT_VERSION >= QT_VERSION_CHECK(5,5,0))
	case QtInfoMsg:
		UserMessage("Qt info: " + msg.toStdString());
		break;
	#endif
	case QtWarningMsg:
		UserMessage("Qt warning: " + msg.toStdString());
		break;
	case QtCriticalMsg:
		UserError("Qt critical: " + msg.toStdString());
		break;
	case QtFatalMsg:
		UserError("Qt fatal: " + msg.toStdString());
		abort();
		break;
	default:
		UserError("Qt fatal: " + msg.toStdString());
		abort();
		break;
	}
}
#else
void CoupledCliController::SigQtHandler(QtMsgType type, const char* msg)
{
	switch (type) {
	case QtDebugMsg:
		UserMessage("Qt debug: " + string(msg));
		break;
	case QtWarningMsg:
		UserMessage("Qt warning: " + string(msg));
		break;
	case QtCriticalMsg:
		UserError("Qt critical: " + string(msg));
		break;
	case QtFatalMsg:
		UserError("Qt fatal: " + string(msg));
		abort();
		break;
	default:
		UserError("Qt fatal: " + string(msg));
		abort();
		break;
	}
}
#endif

void CoupledCliController::SimulationError(const std::string &error)
{
	UserError(error);
	emit TerminationRequested();
}

void CoupledCliController::SimulationInfo(
	const std::string &message, const Session::ISession::InfoMessageReason &reason)
{
	switch (reason) {
	case Session::ISession::InfoMessageReason::Stepped:
		if (!IsQuiet()) {
			UserMessage(message);
		}
		break;
	case Session::ISession::InfoMessageReason::Stopped:
		emit TerminationRequested();
		break;
	case Session::ISession::InfoMessageReason::Terminated:
		emit TerminationRequested();
		break;
	default:
		break;
	}
}

int CoupledCliController::SimulatorRun(CliSimulationTask task)
{
	bool exit_status = EXIT_FAILURE;
	const string here = string(VL_HERE) + "> ";
	const string project_name = task.GetProjectName();
	auto ws_project = m_workspace_model->Get(project_name);
	const string file = task.IsLeafSet() ? task.GetLeaf() : (--ws_project->end())->first;

	auto session = OpenProject(project_name, file);
	if (session) {
		// Create root viewer and initialize its subviewers
		auto rootViewer = session->CreateRootViewer();

		connect(session.get(), SIGNAL(InfoMessage(const std::string&, const InfoMessageReason&)),
			this, SLOT(SimulationInfo(const std::string&, const InfoMessageReason&)));
		connect(session.get(), SIGNAL(ErrorMessage(const std::string&)),
			this, SLOT(SimulationError(const std::string&)));

		if (!IsQuiet()) {
			UserMessage("Opened project " + project_name + " and " + file);
		}

		// Run simulation inside an event loop, so this thread can receive signals
		// from possibly other threads
		// If TerminationRequested() would for some reason already be emitted during
		// StartSimulation, then the connection to quit will be queued, so that the quit()
		// slot of eventLoop will not be called before the call to exec()
		QEventLoop eventLoop(this);
		connect(this, SIGNAL(TerminationRequested()), &eventLoop, SLOT(quit()), Qt::QueuedConnection);
		session->StartSimulation(task.IsNumStepsSet() ? task.GetNumSteps() : -1);
		eventLoop.exec();

		exit_status = EXIT_SUCCESS;
		// Get timings from simulation.
		m_timings.Merge(session->GetTimings());
	} else {
		exit_status = EXIT_FAILURE;
		UserError(here + "Failed to open project " + project_name + " and " + file);
	}

	return exit_status;
}

void CoupledCliController::Terminate()
{
	emit TerminationRequested();
}

void CoupledCliController::UserError(const string& msg)
{
	SimPT_Logging::SimptshellLogger::get()->error("{} {}", LOG_TAG_2, msg);
}

void CoupledCliController::UserMessage(const string& msg)
{
	SimPT_Logging::SimptshellLogger::get()->debug("{} {}", LOG_TAG_2, msg);
}

} // namespace
