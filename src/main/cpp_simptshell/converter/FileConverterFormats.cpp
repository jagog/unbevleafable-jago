/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of File Converter Formats.
 */

#include "FileConverterFormats.h"

using namespace std;
using namespace SimPT_Sim;
using SimShell::Ws::MergedPreferences;

namespace SimPT_Shell {

BitmapFormat     FileConverterFormats::g_bmp_format(BitmapFormat::FileFormat::Bmp);
CsvFormat        FileConverterFormats::g_csv_format;
CsvGzFormat      FileConverterFormats::g_csv_gz_format;
Hdf5Format       FileConverterFormats::g_hdf5_format;
BitmapFormat     FileConverterFormats::g_jpeg_format(BitmapFormat::FileFormat::Jpeg);
PlyFormat        FileConverterFormats::g_ply_format;
VectorFormat     FileConverterFormats::g_pdf_format(VectorFormat::FileFormat::Pdf);
BitmapFormat     FileConverterFormats::g_png_format(BitmapFormat::FileFormat::Png);
XmlFormat        FileConverterFormats::g_xml_format;
XmlGzFormat      FileConverterFormats::g_xml_gz_format;

vector<IConverterFormat*> FileConverterFormats::GetFormats() {
	return {&g_hdf5_format, &g_xml_format, &g_xml_gz_format,
		&g_bmp_format, &g_csv_format, &g_csv_gz_format,
		&g_jpeg_format, &g_ply_format, &g_pdf_format, &g_png_format};
}
vector<IConverterFormat*> FileConverterFormats::GetConverterFormats() {
	return {&g_hdf5_format, &g_xml_format, &g_xml_gz_format};
}

vector<IConverterFormat*> FileConverterFormats::GetExportFormats() {
	return {&g_bmp_format, &g_csv_format, &g_csv_gz_format,
		&g_jpeg_format, &g_ply_format, &g_pdf_format, &g_png_format};
}

} // namespace
