/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for StepSelection
 */

#include "StepSelection.h"

#include <algorithm>
#include <iterator>
#include <QString>

namespace SimPT_Shell {

// Regex: start[-stop[:step]] separated by , or ; (number not equal to 0)
const QRegExp StepSelection::g_range_regex("(\\d+)(?:\\s*-\\s*(\\d+)(?:\\s*:\\s*(\\d*[1-9]\\d*))?)?");
const QRegExp StepSelection::g_repeated_regex("\\s*(?:\\s*" + StepSelection::g_range_regex.pattern() + "\\s*(?:[,;](?!$)|$))*");

StepSelection::StepSelection() : m_all_selected(true)
{
}

StepSelection::~StepSelection()
{
}

void StepSelection::SetSelectionText(const std::string &ranges)
{
	QRegExp regex(g_range_regex);
	m_selected_steps.clear();

	QString rangesString = QString::fromStdString(ranges);

	if (regex.indexIn(rangesString, 0) == -1) {
		m_all_selected = true;
	}
	else {
		m_all_selected = false;

		int pos = 0;
		while ((pos = regex.indexIn(rangesString, pos)) != -1) {
			int start = regex.cap(1).toInt();

			if (!regex.cap(2).isEmpty()) {
				int stop = regex.cap(2).toInt();
				int step = 1;
				if (!regex.cap(3).isEmpty()) {
					step = regex.cap(3).toInt();
				}

				for (int i = start; i <= stop; i += step) {
					m_selected_steps.insert(i);
				}
			} else {
				m_selected_steps.insert(start);
			}

			pos += regex.matchedLength();
		}
	}
}

std::set<int> StepSelection::ApplySelection(const std::set<int> &available) const
{
	if (m_all_selected) {
		return available;
	} else {
		std::set<int> intersected;
		std::set_intersection(available.begin(), available.end(), m_selected_steps.begin(), m_selected_steps.end(), std::inserter(intersected, intersected.begin()));
		return intersected;
	}
}

bool StepSelection::Contains(int step) const
{
	return m_all_selected || m_selected_steps.find(step) != m_selected_steps.end();
}

} // namespace
