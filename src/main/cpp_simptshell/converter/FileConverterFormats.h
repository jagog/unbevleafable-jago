#ifndef SIMPT_SHELL_CONVERTER_FORMATS_H_INCLUDED
#define SIMPT_SHELL_CONVERTER_FORMATS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of file converter formats.
 */

#include "BitmapFormat.h"
#include "IConverterFormat.h"
#include "ExporterFormat.h"
#include "CsvFormat.h"
#include "CsvGzFormat.h"
#include "Hdf5Format.h"
#include "PlyFormat.h"
#include "VectorFormat.h"
#include "XmlFormat.h"
#include "XmlGzFormat.h"

namespace SimPT_Shell {

/**
 * Bundles file converter formats  specifications.
 */
class FileConverterFormats
{
public:
	static std::vector<IConverterFormat*> GetFormats();
	static std::vector<IConverterFormat*> GetConverterFormats();
	static std::vector<IConverterFormat*> GetExportFormats();

private:
	static BitmapFormat    g_bmp_format;
	static CsvFormat       g_csv_format;
	static CsvGzFormat     g_csv_gz_format;
	static Hdf5Format      g_hdf5_format;
	static BitmapFormat    g_jpeg_format;
	static PlyFormat       g_ply_format;
	static VectorFormat    g_pdf_format;
	static BitmapFormat    g_png_format;
	static XmlFormat       g_xml_format;
	static XmlGzFormat     g_xml_gz_format;
};

} // namespace

#endif // end_of_include_guard
