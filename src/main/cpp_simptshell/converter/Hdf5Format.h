#ifndef SIMPT_SHELL_CONVERTER_HDF5_FORMAT_H_INCLUDED
#define SIMPT_SHELL_CONVERTER_HDF5_FORMAT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of File Converter Formats.
 */

#include "converter/IConverterFormat.h"
#include "sim/Sim.h"
#include "fileformats/Hdf5File.h"

namespace SimPT_Shell {

/**
 * Hdf5 converter format specifications.
 */
class Hdf5Format : public IConverterFormat
{
public:
	virtual bool AppendTimeStepSuffix() const { return false; };

	virtual bool IsPostProcessFormat() const { return false; }

	virtual std::string GetExtension() const
	{
		return "h5";
	}

	virtual std::string GetName() const
	{
		return "HDF5";
	}

	virtual void PreConvert(const std::string& target_path,
		std::shared_ptr<SimShell::Ws::MergedPreferences> p);

	virtual void Convert(const SimPT_Sim::SimState& src);

	virtual void PostConvert();

private:
	Hdf5File  m_file;
};

} // namespace

#endif // end_of_include_guard
