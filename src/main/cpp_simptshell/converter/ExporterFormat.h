#ifndef SIMPT_SHELL_CONVERTER_EXPORTER_FORMAT_H_INCLUDED
#define SIMPT_SHELL_CONVERTER_EXPORTER_FORMAT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of File Converter Formats.
 */

#include "TimeStepPostfixFormat.h"
#include "sim/Sim.h"

namespace SimPT_Shell {

/**
 * Utility template for generating exporter formats.
 */
template <class Exporter>
class ExporterFormat : public TimeStepPostfixFormat
{
public:
	virtual bool AppendTimeStepSuffix() const { return true; };

	virtual std::string GetExtension() const
	{
		return Exporter::GetFileExtension();
	}

	virtual void Convert(const SimPT_Sim::SimState& s)
	{
		auto sim = std::make_shared<SimPT_Sim::Sim>();
		sim->Initialize(s);
		Exporter::Export(sim, GeneratePath(s), true);
	}
};

} // namespace

#endif // end_of_include_guard
