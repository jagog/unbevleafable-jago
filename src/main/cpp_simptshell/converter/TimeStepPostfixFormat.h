#ifndef SIMPT_SHELL_TOME_STEP_POSTFIX_FORMAT_H_INCLUDED
#define SIMPT_SHELL_TOME_STEP_POSTFIX_FORMAT_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of TimeStepPostfixFormat.
 */

#include "IConverterFormat.h"
#include "util/misc/StringUtils.h"

namespace SimPT_Shell {

/**
 * Manages time labels int file names.
 */
class TimeStepPostfixFormat : public IConverterFormat
{
public:
	virtual bool AppendTimeStepSuffix() const { return true; };

	virtual void PreConvert(const std::string& target_path,
		std::shared_ptr<SimShell::Ws::MergedPreferences> p)
	{
		m_target_path = target_path;
		m_prefs = p;
	}

	virtual void PostConvert()
	{
		m_target_path.clear();
		m_prefs.reset();
	}

protected:
	std::string GeneratePath(const SimPT_Sim::SimState& sim) const
	{
		return m_target_path
			+ '_'
			+ SimPT_Sim::Util::ToString(sim.GetTimeStep(), 6, '0')
			+ '.' + GetExtension();
	}

protected:
	std::string                                         m_target_path;
	std::shared_ptr<SimShell::Ws::MergedPreferences>    m_prefs;
};

} // namespace

#endif // end_of_include_guard
