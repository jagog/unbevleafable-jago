#ifndef DEFAULT__COMPONENT_FACTORY_H_INCLUDED
#define DEFAULT__COMPONENT_FACTORY_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Proxy for dealing with the factories.
 */

#include "ComponentFactoryBase.h"
#include "model/ComponentTraits.h"
#include "PluginLoader.h"

#include <memory>
#include <string>

namespace SimPT_Sim {

using boost::property_tree::ptree;
using std::shared_ptr;
using std::string;

struct CoreData;
class ComponentFactoryBase;

class ComponentFactoryProxy
{
public:
        /// Create a factory proxy.
        static std::shared_ptr<ComponentFactoryProxy> Create(const string& group_name, bool throw_ok = true);

        /// Create a CellChemistry component.
        CellChemistryComponent CreateCellChemistry(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a CellColor component.
        CellColorComponent CreateCellColor(const string& select, const ptree& pt, bool default_ok = true, bool throw_ok = true) const;

        /// Create a CellDaughters component.
        CellDaughtersComponent CreateCellDaughters(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a CellHouskeep component.
        CellHousekeepComponent CreateCellHousekeep(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a CellSplit component.
        CellSplitComponent CreateCellSplit(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a CellToCellTransport component.
        CellToCellTransportComponent CreateCellToCellTransport(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a DeltaHamiltonian component.
        DeltaHamiltonianComponent CreateDeltaHamiltonian(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a Hamiltonian component.
        HamiltonianComponent CreateHamiltonian(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a CellSplit component.
        MoveGeneratorComponent CreateMoveGenerator(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a TimeEvolver component.
        TimeEvolverComponent CreateTimeEvolver(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

        /// Create a WallChemistry component.
        WallChemistryComponent CreateWallChemistry(const CoreData& cd, bool default_ok = true, bool throw_ok = true) const;

public:
        /// List components for CellChemistry.
        std::vector<std::string>  ListCellChemistry(bool default_ok = true) const;

        /// List components for CellColor.
        std::vector<std::string>  ListCellColor(bool default_ok = true) const;

        /// List components for CellDaughters.
        std::vector<std::string>  ListCellDaughters(bool default_ok = true) const;

        /// List components for CellHouseKeep.
        std::vector<std::string>  ListCellHousekeep(bool default_ok = true) const;

        /// List components for CellSplit.
        std::vector<std::string>  ListCellSplit(bool default_ok = true) const;

        /// List components for CellToCellTransport.
        std::vector<std::string>  ListCellToCellTransport(bool default_ok = true) const;

        /// List components for DeltaHamiltonian.
        std::vector<std::string>  ListDeltaHamiltonian(bool default_ok = true) const;

        /// List components for Hamiltonian.
        std::vector<std::string>  ListHamiltonian(bool default_ok = true) const;

        /// List components for MoveGenerator.
        std::vector<std::string>  ListMoveGenerator(bool default_ok = true) const;

        /// List components for TimeEvolver.
        std::vector<std::string>  ListTimeEvolver(bool default_ok = true) const;

        /// List components for WallChemistry.
        std::vector<std::string>  ListWallChemistry(bool default_ok = true) const;

private:
        ///
        ComponentFactoryProxy(shared_ptr<ComponentFactoryBase>, shared_ptr<ComponentFactoryBase>);

        ///
        template<typename T>
        typename ComponentTraits<T>::ComponentType CreateHelper(const CoreData& cd, bool default_ok, bool throw_ok) const;

        ///
        template<typename T>
        vector<string> ListHelper(bool default_ok) const;

private:
        shared_ptr<ComponentFactoryBase>  m_default;
        shared_ptr<ComponentFactoryBase>  m_factory;
};

} // namespace

#endif // end_of_include_guard
