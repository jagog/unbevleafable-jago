#include "PluginLoader.h"
#include <boost/filesystem.hpp>
#include <cpp_sim/util/misc/InstallDirs.h>
#include <cpp_logging/logger.h>

namespace SimPT_Sim {

namespace dll = boost::dll;

std::shared_ptr<ComponentFactoryBase> PluginLoader::Load(std::string group_name)
{
    boost::filesystem::path lib_path(Util::InstallDirs::GetRootDir().c_str());

    lib_path /= "bin";
    lib_path /= ("libsimPT_" + group_name);

    boost::shared_ptr<ComponentFactoryBase> plugin;

    plugin = dll::import<ComponentFactoryBase>(lib_path, "plugin", dll::load_mode::append_decorations);

    if(!plugin.get()) {
        SimPT_Logging::SimLogger::get()->error("{} Plugin at {} not found, returning null pointer.\n", LOG_TAG_29, lib_path.c_str());
    }

    return toStdSharedPtr(plugin);
}



} //end_of_namespace
