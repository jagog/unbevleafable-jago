#ifndef SIMPT_PLUGINLOADER_H
#define SIMPT_PLUGINLOADER_H
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for plugin loader
 */

#include <model/ComponentFactoryBase.h>
#include <boost/dll/import.hpp>
#include <memory>

namespace SimPT_Sim {

class PluginLoader
{
public:

    std::shared_ptr<ComponentFactoryBase> Load(std::string group_name);

private:

    /// Convert a boost::shared_ptr to an std::shared_ptr in a safe way.
    template<typename T>
    std::shared_ptr<T> toStdSharedPtr( boost::shared_ptr<T>& pointer )
    {
        std::shared_ptr<T> ret(pointer.get(), [pointer](T*) mutable {pointer.reset();});
        return ret;
    }
};

}

#endif //SIMPT_PLUGINLOADER_H
