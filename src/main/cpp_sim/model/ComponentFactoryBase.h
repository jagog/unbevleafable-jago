#ifndef MODEL_COMPONENT_ABSTRACT_FACTORY_H_INCLUDED
#define MODEL_COMPONENT_ABSTRACT_FACTORY_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of Component Factory.
 */

#include "ComponentInterfaces.h"
#include "ComponentTraits.h"

#include <string>
#include <vector>

namespace SimPT_Sim { struct CoreData; }

namespace SimPT_Sim {

using SimPT_Sim::CoreData;
using namespace std;
using boost::property_tree::ptree;

/// Interface for ComponentFactory.
class ComponentFactoryBase
{
public:
        /// Destructor must be virtual.
        virtual	~ComponentFactoryBase() =default;

        /// Create a component for CellChemistry.
        virtual CellChemistryComponent  CreateCellChemistry(const CoreData&) const { return nullptr; }

        /// Create a component for CellColor.
        virtual CellColorComponent  CreateCellColor(const string&, const ptree&) const { return nullptr; }

        /// Create a component for CellDaughters.
        virtual CellDaughtersComponent  CreateCellDaughters(const CoreData&) const { return nullptr; }

        /// Create a component for CellHouseKeep.
        virtual CellHousekeepComponent  CreateCellHousekeep(const CoreData&) const { return nullptr; }

        /// Create a component for CellSplit.
        virtual CellSplitComponent  CreateCellSplit(const CoreData&) const { return nullptr; }

        /// Create a component for ReactionTransport.
        virtual CellToCellTransportComponent  CreateCellToCellTransport(const CoreData&) const { return nullptr; }

        /// Create a component for DeltaHamitonian.
        virtual DeltaHamiltonianComponent  CreateDeltaHamiltonian(const CoreData&) const { return nullptr; }

        /// Create a component for Hamitonian.
        virtual HamiltonianComponent  CreateHamiltonian(const CoreData&) const { return nullptr; }

        /// Create a component for MoveGenerator.
        virtual MoveGeneratorComponent  CreateMoveGenerator(const CoreData&) const { return nullptr; }

        /// Create a component for TimeEvolver.
        virtual TimeEvolverComponent  CreateTimeEvolver(const CoreData&) const { return nullptr; }

        /// Create a component for WallChemistry.
        virtual WallChemistryComponent  CreateWallChemistry(const CoreData&) const { return nullptr; }

public:
        /// List components for CellChemistry.
        virtual std::vector<std::string>  ListCellChemistry() const { return std::vector<std::string>(); }

        /// List components for CellColor.
        virtual std::vector<std::string>  ListCellColor() const { return std::vector<std::string>(); }

        /// List components for CellDaughters.
        virtual std::vector<std::string>  ListCellDaughters() const { return std::vector<std::string>(); }

        /// List components for CellHouseKeep.
        virtual std::vector<std::string>  ListCellHousekeep() const { return std::vector<std::string>(); }

        /// List components for CellSplit.
        virtual std::vector<std::string>  ListCellSplit() const { return std::vector<std::string>(); }

        /// List components for CellToCellTransport.
        virtual std::vector<std::string>  ListCellToCellTransport() const { return std::vector<std::string>(); }

        /// List components for DeltaHamiltonian.
        virtual std::vector<std::string>  ListDeltaHamiltonian() const { return std::vector<std::string>(); }

        /// List components for Hamiltonian.
        virtual std::vector<std::string>  ListHamiltonian() const { return std::vector<std::string>(); }

        /// List components for MoveGenrator.
        virtual std::vector<std::string>  ListMoveGenerator() const { return std::vector<std::string>(); }

        /// List components for TimeEvolver.
        virtual std::vector<std::string>  ListTimeEvolver() const { return std::vector<std::string>(); }

        /// List components for WallChemistry.
        virtual std::vector<std::string>  ListWallChemistry() const { return std::vector<std::string>(); }

public:
        /// Utility method with tag forwarding for use in templates.
        virtual CellChemistryComponent  Create(const CoreData& cd, CellChemistryTag) const
        {
                return CreateCellChemistry(cd);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual CellColorComponent  Create(
                const std::string& select, const boost::property_tree::ptree& pt, CellColorTag) const
        {
                return CreateCellColor(select, pt);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual CellDaughtersComponent  Create(const CoreData& cd, CellDaughtersTag) const
        {
                return CreateCellDaughters(cd);
        }

        /// Utility method with tag forwarding or use in templates.
        virtual CellHousekeepComponent  Create(const CoreData& cd, CellHousekeepTag) const
        {
                return CreateCellHousekeep(cd);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual CellSplitComponent  Create(const CoreData& cd, CellSplitTag) const
        {
                return CreateCellSplit(cd);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual CellToCellTransportComponent  Create(const CoreData& cd, CellToCellTransportTag) const
        {
                return CreateCellToCellTransport(cd);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual DeltaHamiltonianComponent  Create(const CoreData& cd, DeltaHamiltonianTag) const
        {
                return CreateDeltaHamiltonian(cd);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual HamiltonianComponent  Create(const CoreData& cd, HamiltonianTag) const
        {
                return CreateHamiltonian(cd);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual MoveGeneratorComponent  Create(const CoreData& cd, MoveGeneratorTag) const
        {
                return CreateMoveGenerator(cd);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual TimeEvolverComponent  Create(const CoreData& cd, TimeEvolverTag) const
        {
                return CreateTimeEvolver(cd);
        }

        /// Utility method with tag forwarding for use in templates.
        virtual WallChemistryComponent  Create(const CoreData& cd, WallChemistryTag) const
        {
                return CreateWallChemistry(cd);
        }

public:
        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string>  List(CellChemistryTag) const
        {
                return ListCellChemistry();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string>  List(CellColorTag) const
        {
                return ListCellColor();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string>  List(CellDaughtersTag) const
        {
                return ListCellDaughters();
        }

        /// Utility method with tag forwarding or use in templates.
        virtual std::vector<std::string> List(CellHousekeepTag) const
        {
                return ListCellHousekeep();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string> List(CellSplitTag) const
        {
                return ListCellSplit();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string> List(CellToCellTransportTag) const
        {
                return ListCellToCellTransport();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string> List(DeltaHamiltonianTag) const
        {
                return ListDeltaHamiltonian();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string> List(HamiltonianTag) const
        {
                return ListHamiltonian();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string> List(MoveGeneratorTag) const
        {
                return ListMoveGenerator();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string> List(TimeEvolverTag) const
        {
                return ListTimeEvolver();
        }

        /// Utility method with tag forwarding for use in templates.
        virtual std::vector<std::string> List(WallChemistryTag) const
        {
                return ListWallChemistry();
        }
};

} // namespace

#endif // end_of_include_guard
