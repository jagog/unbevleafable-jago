#ifndef SIM_SIMULATOR_INTERFACE_H_INCLUDED
#define SIM_SIMULATOR_INTERFACE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Simulator interface.
 */

#include "sim/SimPhase.h"
#include "util/clock_man/Timeable.h"

#include <boost/property_tree/ptree_fwd.hpp>
#include <string>


namespace SimPT_Sim {

struct CoreData;

/**
 * Simulator interface.
 */
class SimInterface : public SimPT_Sim::ClockMan::Timeable<>
{
public:
	/// Virtual destructor.
	virtual ~SimInterface() {}

        /// Access to CoreData.
        virtual CoreData& GetCoreData() =0;

	/// Provide reference to the parameters.
	virtual const boost::property_tree::ptree& GetParameters() const = 0;

	/// Return name that identifies this project.
	virtual std::string GetProjectName() const = 0;

	/// Return start time/date of the current simulation run.
	virtual std::string GetRunDate() const = 0;

	/// Get number of simulation steps taken in this simulation history.
	virtual int GetSimStep() const = 0;

	/// Execution timings in duration units specified in milliseconds.
	virtual Timings GetTimings() const = 0;

	/// Query whether simulation has met termination condition.
	virtual bool IsAtTermination() const = 0;

	///  Returns true if the simulation is in a stationary state.
	virtual bool IsStationary() const = 0;

	/// Reinitialize with parameters; executed any time the parameters change.
	virtual void Reinitialize(const boost::property_tree::ptree& parameters) = 0;

	/// Let simulator take a time step.
	virtual void TimeStep() = 0;
};

} // namespace

#endif // end_of_include_guard
