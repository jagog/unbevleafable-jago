/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for CoupledSim.
 */

#include "CoupledSim.h"

#include "CoreData.h"
#include "coupler/CouplerFactories.h"
#include "TimeSlicer.h"
#include "Sim.h"
#include "util/clock_man/TimeStamp.h"
#include "util/misc/Exception.h"

#include <iostream>
#include <thread>

using namespace std;
using boost::property_tree::ptree;
using namespace SimPT_Sim::ClockMan;
using namespace SimPT_Sim::Util;

namespace SimPT_Sim {

CoupledSim::CoupledSim()
	: m_sim_step(0), m_sim_ODE_steps(0)
{
}

const ptree& CoupledSim::GetParameters() const
{
	return *m_cd.m_parameters;
}

string CoupledSim::GetProjectName() const
{
	return m_project_name;
}

string CoupledSim::GetRunDate() const
{
	return m_run_date;
}

int CoupledSim::GetSimStep() const
{
	return m_sim_step;
}

CoupledSim::Timings CoupledSim::GetTimings() const
{
	Timings timings = m_timings;
	for (const auto& simPair : m_simulators) {
		timings.Merge(simPair.second->GetTimings());
	}
	return timings;
}

void CoupledSim::Initialize(const ptree& pt, const std::vector<std::shared_ptr<Sim>>& simulators)
{
	for (const auto &sim : simulators) {
		if (m_simulators.count(sim->GetProjectName()) == 0) {
			m_simulators[sim->GetProjectName()] = sim;
		} else {
			throw Exception("Subsimulations with the same name!");
		}
	}

	const ptree& sim_pt         = pt.get_child("vleaf2");
	m_project_name              = sim_pt.get<std::string>("project");
	m_run_date                  = TimeStamp().ToString();
	m_sim_step                  = sim_pt.get<unsigned int>("sim_step", 0U);
	m_sim_ODE_steps             = sim_pt.get<unsigned int>("sim_ODE_coupling_steps", 1U);
	m_cd.m_parameters           = make_shared<ptree>();
	Reinitialize(sim_pt.get_child("parameters"));
}

bool CoupledSim::IsAtTermination() const
{
        bool status = true;
	for (const auto& simPair : m_simulators) {
		status = status && simPair.second->IsAtTermination();
		if (!status) break;
	}
	return status;
}

bool CoupledSim::IsStationary() const
{
        bool status = true;
	for (const auto& simPair : m_simulators) {
	        status = status && simPair.second->IsStationary();
	        if (!status) break;
	}
	return status;
}

void CoupledSim::Reinitialize(const ptree& parameters)
{
	// Read couplers from parameters
	SimPT_Sim::CouplerFactories factory;
	const auto& coupler_array_pt = parameters.get_child("coupler_array");
	for (const auto& pair : coupler_array_pt) {

		const auto type = pair.second.get<std::string>("type");
		const auto from = pair.second.get<std::string>("from");
		const auto to   = pair.second.get<std::string>("to");

		if (!factory.IsValid(type))
			throw Exception("Couldn't find coupler type: " + type);

		auto fromIt = m_simulators.find(from);
		if (fromIt == m_simulators.end()) {
			throw Exception("Couldn't find 'from'-simulation used in coupler: " + from);
		}
		auto toIt = m_simulators.find(to);
		if (toIt == m_simulators.end()) {
			throw Exception("Couldn't find 'to'-simulation used in coupler: " + to);
		}
		auto coupler = factory.Get(type)();
		coupler->Initialize(pair.second.get_child("parameters"),
		                        fromIt->second->GetCoreData(), toIt->second->GetCoreData());
		m_couplers.push_back(coupler);
	}
}

void CoupledSim::TimeStep()
{
	// Compute the time slice.
	const int n = m_sim_ODE_steps;
        const auto params = m_simulators.begin()->second->GetCoreData().m_parameters;
        const double time_slice = params->get<double>("model.time_step")/static_cast<double>(n);

        // Produce the TimeSlicers.
        vector<unique_ptr<TimeSlicer>>  t_slicers;
        for (const auto& sim : m_simulators) {
                t_slicers.push_back(sim.second->TimeSlicing());
        }

	// Transport ODE's using time slices interleaved by coupling.
        vector<std::thread> sim_threads;
	for (int i = 0; i < n; ++i) {
		for (const auto& t_slicer : t_slicers) {
			sim_threads.push_back(thread(
			        [&t_slicer, time_slice] () {t_slicer->Go(time_slice, SimPhase::_1);}
			));
		}

		// Join all threads, then clear thread vector.
		for (auto& thread : sim_threads) {
			thread.join();
		}
		sim_threads.clear();

		// Couple all sub-simulations
		Stopclock couplerChrono("couplers", true);
		for (auto coupler : m_couplers) {
			coupler->Exec();
		}
		m_timings.Record(couplerChrono.GetName(), couplerChrono.Get());
	}

	// Mechanics and Houskeeping (sim_threads has been cleared, so it's ready for use).
	for (const auto& t_slicer : t_slicers) {
	        sim_threads.push_back(thread(
	                [&t_slicer] () {
	                        t_slicer->Go(0.0, SimPhase::_2);
	                        t_slicer->IncrementStepCount();     // Time step completely finished.
	                }
	        ));
	}
	for (auto& thread : sim_threads) {
	        thread.join();
	}

	// Timestep updating for coupled sim.
	IncrementStepCount();
}

} // namespace
