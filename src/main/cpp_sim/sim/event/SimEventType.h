#ifndef SIM_EVENT_TYPE_H_INCLUDED
#define SIM_EVENT_TYPE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Definition for SimEventType.
 */

namespace SimPT_Sim {
namespace Event {

/**
 * Type of events triggered by sim or on behalf of sim by project.
 *
 * Initialized:
 * 	notification effected by root viewer to indicate initialization of simulation.
 * Stepped:
 * 	notification effected by project to indicate that a step has been completed.
 * Done:
 * 	notification effected by project to indicate that it is closing.
 * Forced:
 * 	notification effected by project e.g. to indicate a parameter update
 */
enum struct SimEventType { Initialized, Stepped, Done, Forced };

} // namespace
} // namespace

#endif
