#ifndef SIMWRAPPER_H_INCLUDED
#define SIMWRAPPER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for SimWrapper.
 */
#include <string>
#include <memory>

namespace SimPT_Sim {

class Sim;
class SimState;

/**
 * Shows whether a time step was a success or not.
 */
enum SimWrapperStatus { SUCCESS, FAILURE };

/**
 * All exceptions must be dealt with internally, so
 * each method returns an object of this type.
 */
template <typename T>
struct SimWrapperResult {
	SimWrapperStatus  status;
	std::string       message;
	T                 value;
};

/**
 * Specialization of SimWrapperResult template for type void.
 */
template<>
struct SimWrapperResult<void> {
	SimWrapperStatus  status;
	std::string     message;
};


/**
 * Interface exposing the simulator to Java, Python, and C++ programs.
 */
class SimWrapper {
public:
	SimWrapper();

	/**
	 * Provide sim state in format suitable for i/o.
	 */
	SimWrapperResult<SimState> GetState() const;

	/**
	 * Provide sim state in XML format serialized to string.
	 */
	SimWrapperResult<std::string> GetXMLState() const;

	/**
	 * Set sim state.
	 */
	SimWrapperResult<void> Initialize(SimState state);

	/**
	 * Initialize (path to the input file) with full configuration (global
	 * info, parameters, random engine, mesh). This refers to the one-time
	 * complete setup prior to first use.
	 */
	SimWrapperResult<void> Initialize(const std::string& path);

	/**
	 * Let simulator take a time step.
	 * @throws Exception if something goes wrong.
	 */
	SimWrapperResult<void> TimeStep();

private:
	std::shared_ptr<Sim> m_sim;
};

} // namespace

#endif // include guard
