/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of OdeintFactories2Map.
 */

#include "OdeintFactories2Map.h"

#include <boost/functional/value_factory.hpp>
#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric::odeint;
using boost::value_factory;

namespace {

/**
 * Template to generate odeint solvers with adaptive step size.
 */
template<typename System, typename State, typename Stepper>
class Solver2
{
public:
	Solver2(double& eps_abs, double& eps_rel)
	: m_eps_abs(eps_abs), m_eps_rel(eps_rel)
	{}

	void operator()(System system, State& start_state,
		double start_time, double end_time, double dt)
	{
		integrate_adaptive(make_controlled<Stepper>(m_eps_abs, m_eps_rel),
			system, start_state, start_time, end_time, dt);
	}

private:
	double m_eps_abs;
	double m_eps_rel;
};

} // anonymous namespace

namespace SimPT_Sim {

OdeintFactories2Map::OdeintFactories2Map()
: MapType({
	make_pair("runge_kutta_cash_karp54",
		value_factory<Solver2<System, State, runge_kutta_cash_karp54<State>>>()),
	make_pair("runge_kutta_dopri5",
		value_factory<Solver2<System, State, runge_kutta_dopri5<State>>>()),
	make_pair("runge_kutta_fehlberg78",
		value_factory<Solver2<System, State, runge_kutta_fehlberg78<State>>>())
})
{}

} // namespace
