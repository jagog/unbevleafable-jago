#ifndef SIMPT_MATH_RANDOM_ENGINE_H_INCLUDED
#define SIMPT_MATH_RANDOM_ENGINE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of RandomEngine.
 */
#include "RandomEngineType.h"

#include <trng/lcg64_shift.hpp>
#include <trng/lcg64.hpp>
#include <trng/mrg2.hpp>
#include <trng/mrg3.hpp>
#include <trng/yarn2.hpp>
#include <trng/yarn3.hpp>
#include <boost/property_tree/ptree.hpp>
#include <functional>
#include <string>
#include <vector>

namespace SimPT_Sim {

/**
 * Creates a random generator based on a seed.
 * Internally, TRNG is used since:
 * "However, there is a crucial difference between TRNG distributions and C++11
 *  distri butions. TRNG distributions consume exactly one random number from a
 *  random number engine to generated a random number from a desired
 *  distribution. With C++11 distributions the number of consumed random numbers
 *  may be larger or may even vary. Thus, C++11 random number distributions
 *  should not be utilized in parallel Monte Carlo simulations. In particular,
 *  it is not possible to write parallel Monte Carlo simulations that play fair,
 *  see section 2.3."
 */
class RandomEngine
{
public:
	/**
	 * Initializes to default engine( trng::mrg2 with seed 1).
	 */
	RandomEngine();

	/**
	 * Return the state of the random engine.
	 */
	RandomEngineType::Info GetInfo() const;

	/**
	 * Produce a random generator out of a distribution bound to sim's random engine.
	 * @param   d   The distribution object.
	 * @param   i   Which thread is requesting the generator, 0 by default.
	 * @return      The random generator g that can be called as g()
	 */
	template<typename D>
	std::function<typename D::result_type()> GetGenerator(const D& d, int i = 0)
	{
		using namespace RandomEngineType;

		std::function<typename D::result_type()>  gen;
		switch (m_type_id) {
		case TypeId::lcg64 :
			gen = std::bind(d, std::ref(m_lcg64[i])); break;
		case TypeId::lcg64_shift  :
			gen = std::bind(d, std::ref(m_lcg64_shift[i])); break;
		case TypeId::mrg2 :
			gen = std::bind(d, std::ref(m_mrg2[i])); break;
		case TypeId::mrg3 :
			gen = std::bind(d, std::ref(m_mrg3[i])); break;
		case TypeId::yarn2 :
			gen = std::bind(d, std::ref(m_yarn2[i])); break;
		case TypeId::yarn3 :
			gen = std::bind(d, std::ref(m_yarn3[i])); break;
		}
		return gen;
	}

	/**
	 * Reset to the default engine (trng::mrg2 with seed 1).
	 */
	bool Reinitialize();

	/**
	 * Reset to engine defined by the data packed in ptree.
	 */
	bool Reinitialize(const boost::property_tree::ptree& ptree);

private:
	/// Set the state via seed.
	void SetState(unsigned long);

	/// Set the state via string -> sstream --> state.
	void SetState(const std::string& state);

	unsigned long               m_seed;    ///< Actual seed used with random engine.
	RandomEngineType::TypeId    m_type_id; ///< Identifies type of random engine.

private:
	std::vector<trng::lcg64>             m_lcg64;
	std::vector<trng::lcg64_shift>       m_lcg64_shift;
	std::vector<trng::mrg2>              m_mrg2;
	std::vector<trng::mrg3>              m_mrg3;
	std::vector<trng::yarn2>             m_yarn2;
	std::vector<trng::yarn3>             m_yarn3;
};

} // namespace

#endif // end-of-include-guard
