#ifndef SIMPT_MATH_MESH_GEOMETRY_H_INCLUDED
#define SIMPT_MATH_MESH_GEOMETRY_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MeshGeometry.
 */

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <tuple>

namespace SimPT_Sim {

using boost::property_tree::ptree;
class Mesh;

/**
 * Helper functions for mesh geometry.
 */
class MeshGeometry
{
public:
	/**
	 * Calculate a bounding box around the mesh.
	 */
	static std::tuple<std::array<double, 3>, std::array<double, 3>> BoundingBox(std::shared_ptr<Mesh> mesh);

	/**
	 * Calculate the convex hull of the cells in the mesh and
	 * returns respectively the ratio of the areas of the mesh and the hull,
	 * the area of the hull and the circumference of the hull.
	 */
	static std::tuple<double, double, double> Compactness(const Mesh* mesh);
};

} // namespace

#endif // end_of_include_guard

