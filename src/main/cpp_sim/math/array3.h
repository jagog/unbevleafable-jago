#ifndef MATH_ARRAY3_H_INCLUDED
#define MATH_ARRAY3_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Extending std with arithmetic for std::array<double, 3>. Extending std to
 * make functions available through namespace name (Koenig) lookup.
 */

#include "constants.h"
#include "signum.h"

#include <array>
#include <cmath>
#include <ostream>

namespace std {


// ---------------------------------------------------------------------------------------------------
// Array's norm.
// ---------------------------------------------------------------------------------------------------

/// The array's norm.
inline double Norm(const std::array<double, 3>& u)
{
	return std::sqrt( u[0] * u[0] + u[1] * u[1] + u[2] * u[2] );
}

/// The array's norm squared.
inline double Norm2(const std::array<double, 3>& u)
{
	return u[0] * u[0] + u[1] * u[1] + u[2] * u[2];
}

// ---------------------------------------------------------------------------------------------------
// Unary, basic arithmetic.
// ---------------------------------------------------------------------------------------------------

/// Unary addition.
inline std::array<double, 3>& operator+=(std::array<double, 3>& u, const std::array<double, 3>& v)
{
	u[0] += v[0];
	u[1] += v[1];
	u[2] += v[2];
	return u;
}

/// Unary subtraction.
inline std::array<double, 3>& operator-=(std::array<double, 3>& u, const std::array<double, 3>& v)
{
	u[0] -= v[0];
	u[1] -= v[1];
	u[2] -= v[2];
	return u;
}

/// Unary multiplication by scalar.
inline std::array<double, 3>& operator*=(std::array<double, 3>& u, double m)
{
	u[0] *= m;
	u[1] *= m;
	u[2] *= m;
	return u;
}

/// Unary division by scalar.
inline std::array<double, 3>& operator/=(std::array<double, 3>& u, double m)
{
	u[0] /= m;
	u[1] /= m;
	u[2] /= m;
	return u;
}

// ---------------------------------------------------------------------------------------------------
// Binary, basic arithmetic.
// ---------------------------------------------------------------------------------------------------

/// Binary addition.
inline std::array<double, 3> operator+(const std::array<double, 3>& u, const std::array<double, 3>& v)
{
	return {{ u[0] + v[0], u[1] + v[1], u[2] + v[2] }};
}

/// Binary subtraction.
inline std::array<double, 3> operator-(const std::array<double, 3>& u, const std::array<double, 3>& v)
{
	return {{ u[0] - v[0], u[1] - v[1], u[2] - v[2] }};
}

/// Multiplication scalar * array.
inline std::array<double, 3> operator* (const std::array<double, 3>& u, double m)
{
	return {{u[0]*m, u[1]*m, u[2]*m}};
}

/// Multiplication array * scalar.
inline std::array<double, 3> operator* (double m, const std::array<double, 3>& v)
{
	return v * m;
}

/// Division by a scalar.
inline std::array<double, 3> operator/ (const std::array<double, 3>& u, double d)
{
	return {{u[0]/d, u[1]/d, u[2]/d}};
}

// ---------------------------------------------------------------------------------------------------
// Geometric ops.
// ---------------------------------------------------------------------------------------------------

/// Normalized copy.
inline std::array<double, 3> Normalize(const std::array<double, 3>& u)
{
	return u / Norm(u);
}


/// Cross product with another vector.
inline std::array<double, 3> CrossProduct(const std::array<double, 3>& u, const std::array<double, 3>& v)
{
	return {{ u[1]*v[2] - u[2]*v[1], u[2]*v[0] - u[0]*v[2], u[0]*v[1] - u[1]*v[0] }};
}

/// Inner product.
inline double InnerProduct(const std::array<double, 3>& u, const std::array<double, 3>& v)
{
	return (u[0] * v[0] + u[1] * v[1] + u[2] * v[2]);
}

/// Perpendicular vector in 2D (in x,y i.e. 0,1 plane)
inline std::array<double, 3> Orthogonalize(const std::array<double, 3>& u)
{
	return {{ u[1], -u[0], 0.0 }};
}

/// Angle between two vectors  within range of [0, pi] radians.
inline double Angle(const std::array<double, 3>& u, const std::array<double, 3>& v)
{
	return std::acos(std::min(1.0, std::max(-1.0, InnerProduct(u, v) / (Norm(u) * Norm(v)))));
}

/// Return true iff the two vectors point in the same direction.
inline bool IsSameDirection(const std::array<double, 3>& u, const std::array<double, 3>& v)
{
	return (Angle(u, v) < 0.5 * SimPT_Sim::Util::pi());
}

/// Signed angle between two vectors (within range of [-pi,pi] radians).
inline double SignedAngle(const std::array<double, 3>& u, const std::array<double, 3>& v)
{
	double cos_angle = InnerProduct(u, v) / (Norm(u) * Norm(v));
	double angle = 0.0;
	// check for computational inaccuracies
	if (cos_angle <= -1.0) {
		angle = SimPT_Sim::Util::pi();
	}
	else if (cos_angle >= 1.0) {
		angle = 0.0;
	} else {
		angle = acos(cos_angle) * SimPT_Sim::Util::signum(InnerProduct(Orthogonalize(u), v));
	}
	return angle;
}

// ---------------------------------------------------------------------------------------------------
// I/O.
// ---------------------------------------------------------------------------------------------------

/// ostream insertion.
inline std::ostream& operator<<(std::ostream& os, const std::array<double, 3>& v)
{
	os << "(" << v[0] << ", " << v[1] << ", " << v[2] << ")";
	return os;
}

} // namespace std

#endif // include guard
