#ifndef ODEINT_FACTORY2_H_INCLUDED
#define ODEINT_FACTORY2_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for OdeintFactory2.
 */

#include "OdeintFactories2Map.h"
#include "OdeintTraits.h"

#include <list>
#include <string>
#include <vector>

namespace SimPT_Sim {

/**
 * Produces factory function for odeint integrator algorithm.
 */
class OdeintFactory2
{
public:
	using State  = OdeintTraits<>::State;
	using System = OdeintTraits<State>::System;
	using Solver = OdeintTraits<State>::Solver;

public:
	Solver Create(const std::string& name, double eps_abs, double eps_rel)
	{
		return m_f2_map.Get(name)(eps_abs, eps_rel);
	}

	/**
	 * Returns true iff this factory can provide the requested solver.
	 */
	bool ProvidesSolver(const std::string& name) const
	{
		return m_f2_map.IsValid(name);
	}

	std::list<std::string> List() const
	{
		return m_f2_map.List();
	}

private:
	OdeintFactories2Map  m_f2_map;
};

} // VLEaf2_Sim

#endif // end_of_include_guard
