#ifndef MATH_BINARY_CSR_MATRIX_H_INCLUDED
#define MATH_BINARY_CSR_MATRIX_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Data strucuture for generalized node/cell incidence relations
 */

#include <cstddef>
#include <vector>

namespace SimPT_Sim {

/**
 * Represents a binary matrix with n rows and m columns in a Compressed Sparse
 * Row representation. All nonzero values are implicitly one (or true).
 *
 * - col_ind stores indices (j) of matrix columns that have nonzero values.
 *   Size of col_ind is equal to the number of nonzeros in the matrix.
 * - row_ptr keeps indices (offests) to col_ind elements where matrix rows
 *   start. The last entry of row_ptr is the number of nonzeros in the matrix
 *   (= length(col_ind)). The size of row_ptr is n + 1, where n is the number
 *   of matrix rows.
 *
 * Example:
 *   A = [.x..]    where . is a zero (false) and x is a nonzero (1 or true)
 *       [..x.]
 *       [.x.x]
 *       [x..x]
 *       [xxx.]
 *
 *   #rows, #columns = 5, 4
 *   number of nonzeros = 9
 *   col_ind = [1, 2, 1, 3, 0, 3, 0, 1, 2]  length(col_ind) = 9 (= #nonzeros)
 *   row_ptr = [0, 1, 2, 4, 6, 9]           length(row_ptr) = 6 (= #rows + 1)
 *
 * Note that the number of nonzeros of a row i equals row_ptr[i+1] - row_ptr[i]
 */
struct CSRMatrix
{
        std::vector<int>            col_ind;   ///< Indices of columns that have nonzeros in a row.
        std::vector<std::size_t>    row_ptr;   ///< Indices to col_ind elements where rows start.
};

} // namespace

#endif // end_of_include_guard
