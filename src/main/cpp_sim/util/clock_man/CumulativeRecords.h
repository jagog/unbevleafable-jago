#ifndef UTIL_CLOCK_MAN_CUMULATIVE_RECORDS_H_INCLUDED
#define UTIL_CLOCK_MAN_CUMULATIVE_RECORDS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for CumulativeRecords.
 */

#include "Utils.h"

#include <chrono>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <list>
#include <map>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

namespace SimPT_Sim {
namespace ClockMan {

/**
 * Utility class to record durations in a cumulative manner.
 * Records durations associated with a name.
 * Template parameter is duration type (default std::chrono::seconds).
 */
template<typename T = std::chrono::seconds>
class CumulativeRecords
{
public:
	using Duration = T;

	/// Clear the entire set of records.
	void Clear()
	{
		m_map.clear();
	}

	/// Return count for records with name.
	unsigned int GetCount(const std::string& name) const
	{
		return IsPresent(name) ? m_map.at(name).first : 0U;
	}

	/// Return cumulative time for records with name
	Duration GetCumulative(const std::string& name) const
	{
		return IsPresent(name) ? m_map.at(name).second : T::duration::zero();
	}

	/// Return cumulative time for records with name.
	Duration GetMean(const std::string& name) const
	{
		const auto count = GetCount(name);
		return (count > 0U) ? GetCumulative(name) / count : T::duration::zero();
	}

	/// Return list of names.
	std::list<std::string> GetNames() const
	{
		std::list<std::string> l;
		for (auto const& p : m_map) {
			l.push_back(p.first);
		}
		return l;
	}

	/// Return records for all names in duration unit U.
	template <typename U = std::chrono::seconds>
	CumulativeRecords<U> GetRecords() const
	{
		CumulativeRecords<U>  rec;
		for (auto const& p : m_map) {
			rec.Record(p.first, GetCumulative(p.first));
		}
		return rec;
	}

	/// Return records for all names in durations Duration (so no casting).
	CumulativeRecords<Duration> GetRecords() const
	{
		return *this;
	}

	/// Return whether there are records associated with a given name.
	bool IsPresent(const std::string& name) const
	{
		return (m_map.find(name) != m_map.end());
	}

	/// Merge an extra set of records (casting durations if required).
	template<typename U>
	void Merge(const CumulativeRecords<U>& extra)
	{
		Merge(extra.template GetRecords<Duration>());
	}

	/// Merge an extra set of records of same Duration (no casting).
	void Merge(const CumulativeRecords<Duration>& extra)
	{
		for (const auto& p : extra.m_map) {
			const auto it = m_map.find(p.first);
			if (it != m_map.end()) {
				(it->second).first  += (p.second).first;
				(it->second).second += (p.second).second;
			} else {
				m_map[p.first] = p.second;
			}
		}
	}

	/// Record the duration for the given name.
	template<typename R, typename P>
	void Record(const std::string& name, const std::chrono::duration<R, P>& duration)
	{
		const auto it = m_map.find(name);
		if (it != m_map.end()) {
			(it->second).first++;
			(it->second).second += std::chrono::duration_cast<T>(duration);
		} else {
			m_map[name] = make_pair(1U, std::chrono::duration_cast<T>(duration));
		}
	}

	/// Record the duration for the given name.
	void Record(const std::string& name, const Duration& duration)
	{
		const auto it = m_map.find(name);
		if (it != m_map.end()) {
			(it->second).first++;
			(it->second).second += duration;
		} else {
			m_map[name] = make_pair(1U, duration);
		}
	}

private:
	std::map<std::string, std::pair<unsigned int, Duration>> m_map;
};

/**
 * Nicely formated output with nanoseconds.
 */
inline std::ostream& operator<<(std::ostream& os,
	CumulativeRecords<std::chrono::nanoseconds> const& dr)
{
	using namespace std;
	using namespace std::chrono;

	os << right << "timings:   " << endl
			<< setw(14) << "name" << " | "
			<< setw(36) << " (hh:mm:ss: ms: us: ns) |     (ms) | "
			<< setw(7) << "count" << " | "
			<< setw(36) << "(hh:mm:ss:ms:us:ns) |     (ms) | " << endl
			<< string(100, '-') << endl;

	auto name_list = dr.GetNames();
	name_list.sort();
	for (auto const& name : name_list) {
		auto const count      = dr.GetCount(name);
		auto const cumul_val  = dr.GetCumulative(name);
		auto const cumul      = duration_cast<nanoseconds>(cumul_val);
		auto const avg_val    = (count != 0) ? (cumul_val / count) : nanoseconds::zero();
		auto const avg        = duration_cast<nanoseconds>(avg_val);

		os << right
			<< setw(14) << name << " | "
			<< setw(23) << Utils::ToColonString(cumul) << " | "
			<< setw(8) << scientific << setprecision(4) << cumul.count()/1000000 << " | "
			<< setw(7) << count << " | "
			<< setw(23) << Utils::ToColonString(avg) << " | "
			<< setw(8) << scientific << setprecision(4) << avg.count()/1000000 << " | "<< endl;
	}
	return os;
}

/**
 * Nicely formated output with microseconds.
 */
inline std::ostream& operator<<(std::ostream& os,
	CumulativeRecords<std::chrono::microseconds> const& dr)
{
	using namespace std;
	using namespace std::chrono;

	os << right << "timings:   " << endl
			<< setw(14) << "name" << " | "
			<< setw(32) << " (hh:mm:ss: ms: us) |     (ms) | "
			<< setw(7) << "count" << " | "
			<< setw(32) << " (hh:mm:ss: ms: us) |     (ms) | " << endl
			<< string(100, '-') << endl;

	auto name_list = dr.GetNames();
	name_list.sort();
	for (auto const& name : name_list) {
		auto const count      = dr.GetCount(name);
		auto const cumul_val  = dr.GetCumulative(name);
		auto const cumul      = duration_cast<microseconds>(cumul_val);
		auto const avg_val    = (count != 0) ? (cumul_val / count) : microseconds::zero();
		auto const avg        = duration_cast<microseconds>(avg_val);

		os << right
			<< setw(14) << name << " | "
			<< setw(19) << Utils::ToColonString(cumul) << " | "
			<< setw(8) << scientific << setprecision(4) << cumul.count()/1000000 << " | "
			<< setw(7) << count << " | "
			<< setw(19) << Utils::ToColonString(avg) << " | "
			<< setw(8) << scientific << setprecision(4) << avg.count()/1000000 << " | "<< endl;
	}
	return os;
}

/**
 * Nicely formated output with milliseconds.
 */
inline std::ostream& operator<<(std::ostream& os,
	CumulativeRecords<std::chrono::milliseconds> const& dr)
{
	using namespace std;
	using namespace std::chrono;

	os << right << "timings:   " << endl
			<< setw(14) << "name" << " | "
			<< setw(21) << "(hh:mm:ss:ms) |     (ms) | "
			<< setw(7) << "count" << " | "
			<< setw(21) << "(hh:mm:ss:ms) |     (ms) | " << endl
			<< string(80, '-') << endl;

	auto name_list = dr.GetNames();
	name_list.sort();
	for (auto const& name : name_list) {
		auto const count      = dr.GetCount(name);
		auto const cumul_val  = dr.GetCumulative(name);
		auto const cumul      = duration_cast<milliseconds>(cumul_val);
		auto const avg_val    = (count != 0) ? (cumul_val / count) : milliseconds::zero();
		auto const avg        = duration_cast<milliseconds>(avg_val);

		os << right
			<< setw(14) << name << " | "
			<< setw(13) << Utils::ToColonString(cumul) << " | "
			<< setw(8) << scientific << setprecision(4) << cumul.count() << " | "
			<< setw(7) << count << " | "
			<< setw(13) << Utils::ToColonString(avg) << " | "
			<< setw(8) << scientific << setprecision(4) << avg.count() << " | "<< endl;
	}
	return os;
}

/**
 * Nicely formated output with seconds.
 */
inline std::ostream& operator<<(std::ostream& os,
	CumulativeRecords<std::chrono::seconds> const& dr)
{
	using namespace std;
	using namespace std::chrono;

	os << right << "timings:   " << endl
			<< setw(14) << "name" << " | "
			<< setw(20) << " (hh:mm:ss)  |    (s)  | "
			<< setw(7) << "count" << " | "
			<< setw(20) << " (hh:mm:ss)  |     (s) | " << endl
			<< string(76, '-') << endl;

	auto name_list = dr.GetNames();
	name_list.sort();
	for (auto const& name : name_list) {
		auto const count      = dr.GetCount(name);
		auto const cumul_val  = dr.GetCumulative(name);
		auto const cumul      = duration_cast<seconds>(cumul_val);
		auto const avg_val    = (count != 0) ? (cumul_val / count) : seconds::zero();
		auto const avg        = duration_cast<seconds>(avg_val);

		os << right
			<< setw(14) << name << " | "
			<< setw(12) << Utils::ToColonString(cumul) << " | "
			<< setw(7) << scientific << setprecision(4) << cumul.count() << " | "
			<< setw(7) << count << " | "
			<< setw(12) << Utils::ToColonString(avg) << " | "
			<< setw(7) << scientific << setprecision(4) << avg.count() << " | "<< endl;
	}
	return os;
}

/**
 * Pretty print of chrono recordings on output stream.
 */
template<typename T>
inline std::ostream& operator<<(std::ostream& os, CumulativeRecords<T> const& dr)
{
	using namespace std;

	os << right << "timings:  name  |           total       |    count   |           mean       |" << endl;
	os << right << "-----------------------------------------------------------------------------" << endl;

	auto name_list = dr.GetNames();
	name_list.sort();
	for (auto const& name : name_list) {
		os << right << setw(14) << name << "  |  "
			<< setw(20) << Utils::ToColonString(dr.GetCumulative(name)) << " | "
			<< setw(10) << dr.GetCount(name) << " | "
			<< setw(20) << Utils::ToColonString(dr.GetMean(name)) << " | "
		    << endl;
	}
	return os;
}

} // namespace
} // namespace

#endif // end-of-include-guard
