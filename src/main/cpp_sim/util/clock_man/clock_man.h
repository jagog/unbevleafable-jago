#ifndef UTIL_CLOCK_MAN_H_INCLUDED
#define UTIL_CLOCK_MAN_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence. You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Utility header file for clock related classes.
 */

#include "ClockCLib.h"
#include "CumulativeRecords.h"
#include "IndividualRecords.h"
#include "Stopwatch.h"
#include "Timeable.h"
#include "TimeStamp.h"
#include "Utils.h"

#include <chrono>

#endif // end-of-include-guard
