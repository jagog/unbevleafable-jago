/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * HSV color def to RBG color def: r,g,b values are from 0 to 1
 * and  h = [0,360], s = [0,1], v = [0,1] (if s == 0, then h = -1)
 */

#include "Hsv2Rgb.h"

#include <array>
#include <cmath>

namespace SimPT_Sim {
namespace Color {

using namespace std;

std::array<double, 3> Hsv2Rgb(double h, double s, double v)
{
        array<double, 3> ret_val {{v, v, v}};            // achromatic (grey)
        h /= 60.0;                                     // sector 0 to 5
        const int    i = floor(h);
        const double f = h - i;                        // factorial part of h
        const double p = v * (1.0 - s);
        const double q = v * (1.0 - s * f);
        const double t = v * (1.0 - s * (1.0 - f));

        switch( i ) {
                case 0:  ret_val = {{v, t, p}}; break;
                case 1:  ret_val = {{q, v, p}}; break;
                case 2:  ret_val = {{p, v, t}}; break;
                case 3:  ret_val = {{p, q, v}}; break;
                case 4:  ret_val = {{t, p, v}}; break;
                default: ret_val = {{v, p, q}}; break;
        }

        return ret_val;
}

} // namespace
} // namespace
