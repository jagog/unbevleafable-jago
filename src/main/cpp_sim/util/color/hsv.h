#ifndef SIMPT_CELL_COLOR_HSV_H_
#define SIMPT_CELL_COLOR_HSV_H_
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Header file for HSV colors.
 */

#include <cassert>
#include <iostream>
#include <tuple>

namespace SimPT_Sim {
namespace Color {

using hsv_t = std::tuple<double, double, double>;

// -----------------------------------------------------------
// see http://www.rapidtables.com/convert/color/hsv-to-rgb.htm
// -----------------------------------------------------------
const hsv_t black   {  0.0 / 360.0, 0.0, 0.0 };
const hsv_t white   {180.0 / 360.0, 0.0, 1.0 };
const hsv_t red     {  0.0 / 360.0, 1.0, 1.0 };
const hsv_t lime    {120.0 / 360.0, 1.0, 1.0 };
const hsv_t blue    {240.0 / 360.0, 1.0, 1.0 };
const hsv_t yellow  { 60.0 / 360.0, 1.0, 1.0 };
const hsv_t cyan    {180.0 / 360.0, 1.0, 1.0 };
const hsv_t magenta {300.0 / 360.0, 1.0, 1.0 };
const hsv_t gray    {  0.0 / 360.0, 0.0, 0.5 };
const hsv_t teal    {180.0 / 360.0, 1.0, 0.5 };
const hsv_t navy    {240.0 / 360.0, 1.0, 0.5 };

inline hsv_t interpolate(double x, hsv_t t_a, double x_a, hsv_t t_b, double x_b)
{
	assert(x_b < x_a);
	assert(x_a <= x && x <= x_b);

	const double s = (x - x_a) / (x_b - x_a);
	return std::make_tuple(
		(1.0 - s) * std::get<0>(t_a) + s * std::get<0>(t_b),
		(1.0 - s) * std::get<1>(t_a) + s * std::get<1>(t_b),
		(1.0 - s) * std::get<2>(t_a) + s * std::get<2>(t_b));
}

inline std::ostream& operator<<(std::ostream& os , hsv_t t)
{
	os << std::get<0>(t) << " - " << std::get<1>(t) << " - " << std::get<2>(t);
	return os;
}

} // namespace
} // namespace

#endif // end_of_include_guard
