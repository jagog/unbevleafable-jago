#ifndef SIMPT_CONFIG_INFO_H_INCLUDED
#define SIMPT_CONFIG_INFO_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Info on configuration through compile time constants i.o. macros.
 */

#include <string>

namespace SimPT_Sim {
namespace Util {

/**
 * Info on revision id and commit date.
 */
class ConfigInfo
{
public:
	///
	static constexpr bool HaveOpenMP()
	{
#ifdef _OPENMP
	        return true;
#else
	        return false;
#endif
	}
};

} // namespace
} // namespace

#endif // end-of-include-guard
