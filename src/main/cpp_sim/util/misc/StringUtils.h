#ifndef INC_STRINGUTILS_H
#define INC_STRINGUTILS_H
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file.
 * String manipulation utilities.
 */

#include <algorithm>
#include <cctype>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

namespace SimPT_Sim {
namespace Util {

// ================================
// Utilities to manipulate strings.
// ================================

/// Builds a value of type T representation from a string.
template<typename T>
inline static T FromString(const std::string& s)
{
	std::stringstream ss(s);
	T t;
	ss >> t;
	return t;
}

/// Tokenize a string (in order of occurence) by splitting it on the given delimiters.
static std::vector<std::string> Tokenize(const std::string& str, const std::string& delimiters)
{
	std::vector<std::string> tokens;

	// Skip delimiters at beginning.
	std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
	// Find first non-delimiter.
	std::string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (std::string::npos != pos || std::string::npos != lastPos) {
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));
		// Skip delimiters.
		lastPos = str.find_first_not_of(delimiters, pos);
		// Find next non-delimiter.
		pos = str.find_first_of(delimiters, lastPos);
	}

	return tokens;
}

/// Builds a string representation of a value of type T.
template<typename T>
inline static std::string ToString(const T& value)
{
	std::stringstream ss;
	ss << value;
	return ss.str();
}

/// Builds a string representation with minimum width of a value of type T.
template<typename T>
inline static std::string ToString(const T& value, int width, char fill = ' ')
{
	std::stringstream ss;
	ss << std::setw(width) << std::setfill(fill) << value;
	return ss.str();
}

/// Builds a string with upper case characters only.
static std::string ToUpper(const std::string& source)
{
	auto upper = [](int c)->int {return std::toupper(c);};
	std::string copy;
	std::transform(source.begin(), source.end(), std::back_inserter(copy), upper);
	return copy;
}

/// Trim characters at right end of string.
static std::string TrimRight(const std::string& source, const std::string& t = " ")
{
	std::string str = source;
	return str.erase(str.find_last_not_of(t) + 1);
}

/// Trim characters at left end of string.
static std::string TrimLeft(std::string const& source, const std::string& t = " ")
{
	std::string str = source;
	return str.erase(0, source.find_first_not_of(t));
}

/// Trim characters at both ends of string.
static std::string Trim(const std::string& source, const std::string& t = " ")
{
	std::string str = source;
	return TrimLeft(TrimRight(str, t), t);
}

} // namespace
} // namespace

#endif // end-of-include-guard
