/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for FileSys class.
 */
#include "InstallDirs.h"

#include <string>
#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

using boost::filesystem::path;
using boost::filesystem::initial_path;

using namespace std;

namespace SimPT_Sim {
namespace Util {

std::string 	InstallDirs::g_data_dir;
std::string 	InstallDirs::g_root_dir;
std::string 	InstallDirs::g_workspace_dir;
bool            InstallDirs::g_initialized = false;


inline void InstallDirs::Check()
{
	if (!g_initialized) {
		Initialize();
	}
}

#if defined(__WIN32__)

#include <string>
#include <windows.h>

std::string InstallDirs::getExecutablePath()
{
  char result[1024];
  return std::string(result, GetModuleFileName(NULL, result, 1024) );
}

#elif defined(__APPLE__)

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <libproc.h>

std::string InstallDirs::getExecutablePath()
{
	int ret;
	pid_t pid;
	char pathbuf[PROC_PIDPATHINFO_MAXSIZE];
	pid = getpid();
	ret = proc_pidpath (pid, pathbuf, sizeof(pathbuf));
	if ( ret <= 0 ) {
		return std::string();
	} else {
		return std::string(pathbuf);
	}
}

#else

#include <string>
#include <limits.h>
#include <unistd.h>

std::string InstallDirs::getExecutablePath()
{
	char result[ 1024 ];
	ssize_t count = readlink( "/proc/self/exe", result, 1024 );
	return std::string( result, (count > 0) ? count : 0 );
}

#endif

void InstallDirs::Initialize()
{
	//------- Retrieving Install Root Path
	{
		path dir = path(getExecutablePath()).parent_path();

		std::string dirname = dir.filename().string();
		boost::to_lower(dirname);
	#if defined(__APPLE__)
		if (dirname == "macos") {
			// app
			//	-Contents		<-Root Path
			//		-MacOS
			//		     -binaries
			dir = dir.parent_path();
		} else
	#endif
		if (dirname == "debug" or dirname == "release") {
			//x/exec		<-Root Path
			//	-bin
			//		-release/debug
			//			-binaries
			dir = dir.parent_path();
			dir = dir.parent_path();
		} else
	#if defined(__WIN32__)
		if (dirname != "bin") {
				// Do Nothing, binaries in root folder
		} else
	#endif
		{
			//x/exec		<-Root Path
			//	-bin
			//		-binaries
			dir = dir.parent_path();
		}

		g_root_dir = "";
		const path* dir_copy = new path(dir);
		if(boost::filesystem::exists(*dir_copy))
			g_root_dir = dir.string();
	}
	//------- Retrieving Data Dir
	{
		path dir(g_root_dir);
		dir /= "data";
		g_data_dir = "";

		const path* dir_copy = new path(dir);
		if(boost::filesystem::exists(*dir_copy))
			g_data_dir = dir.string();
	}
	//------- Retrieving Workspace
	{
		path dir(g_data_dir);
		dir /= "simPT_Default_workspace";
		g_workspace_dir = "";

		const path* dir_copy = new path(dir);
		if(boost::filesystem::exists(*dir_copy))
			g_workspace_dir = dir.string();
	}

	g_initialized = true;
}

string InstallDirs::GetDataDir()
{
	Check();
	return g_data_dir;
}

string InstallDirs::GetRootDir()
{
    Check();
    return g_root_dir;
}

string InstallDirs::GetWorkspaceDir()
{
	Check();
	return g_workspace_dir;
}

} // namespace Util
} // namespace SimPT_Sim
