/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of CBMBBuilder.
 */

#include "CBMBuilder.h"

#include "Cell.h"
#include "Mesh.h"
#include "Node.h"

#include <iostream>

namespace SimPT_Sim {

using namespace std;
using namespace SimPT_Sim;

CBMBuilder::CBMBuilder()
{
}

shared_ptr<Mesh> CBMBuilder::Build(const Mesh& src)
{
	//Initialize.
	auto target = make_shared<Mesh>(src.m_num_chemicals);

	vector<Wall*> newWalls; //Will be filled with the copied walls later for efficiency reasons.


	//Lambdas.
	auto find_cell = [&target](Cell* c) {
		return (c->GetIndex() == -1 ?
			target->m_boundary_polygon : target->m_cells[c->GetIndex()]);
	};
	auto find_node = [&target](Node* n) {
		return target->m_nodes[n->GetIndex()];
	};
	auto find_wall = [&newWalls](Wall* w) {
		return newWalls[w->GetIndex()];
	};

	//Copy nodes.
	for (const auto& n : src.m_nodes) {
		target->BuildNode(n->GetIndex(), *n, n->IsAtBoundary())->NodeAttributes::operator=(*n);
	}

	//Copy cells (without the walls set).
	for (auto c : src.m_cells) {
		vector<unsigned int> node_ids;
		transform(
			c->GetNodes().begin(),
			c->GetNodes().end(),
			back_inserter(node_ids),
			[](Node* n){return n->GetIndex();});
		target->BuildCell(c->GetIndex(), node_ids)->CellAttributes::operator=(*c);
	}

	//Copy boundary polygon (without the walls set).
	if (src.m_boundary_polygon != nullptr) {
		vector<unsigned int> node_ids;
		transform(
			src.m_boundary_polygon->GetNodes().begin(),
			src.m_boundary_polygon->GetNodes().end(),
			back_inserter(node_ids),
			[](Node* n){return n->GetIndex();});
		target->BuildBoundaryPolygon(node_ids)->CellAttributes::operator=(*src.m_boundary_polygon);
	}

	//Copy walls.
	for (const auto& w : src.m_walls) {
		Node* n1 = find_node(w->GetN1());
		Node* n2 = find_node(w->GetN2());
		Cell* c1 = find_cell(w->GetC1());
		Cell* c2 = find_cell(w->GetC2());
		target->BuildWall(w->GetIndex(), n1, n2, c1, c2)->WallAttributes::operator=(*w);
	}

	//Copy walls in a local vector (for an efficient copy).
	copy(target->m_walls.begin(), target->m_walls.end(), back_inserter(newWalls));

	//Set the walls in the cells and boundary polygon.
	for (auto c : src.m_cells) {
		transform(
			c->GetWalls().begin(),
			c->GetWalls().end(),
			back_inserter(find_cell(c)->GetWalls()),
			find_wall);
	}
	if (src.m_boundary_polygon != nullptr) {
		transform(
			src.m_boundary_polygon->GetWalls().begin(),
			src.m_boundary_polygon->GetWalls().end(),
			back_inserter(target->m_boundary_polygon->GetWalls()),
			find_wall);
	}

	//Node owning neighbors are already set (see Mesh::BuildCell).

	//Set node owning walls.
	for (const auto& node_owning_wall : src.m_node_owning_walls) {
		auto pair = target->m_node_owning_walls.insert(
			make_pair(find_node(node_owning_wall.first), list<Wall*>()));
		assert(pair.second && "node_owning_wall");
		transform(
			node_owning_wall.second.begin(),
			node_owning_wall.second.end(),
			back_inserter((pair.first)->second),
			find_wall);
	}

	//Set wall neighbors.
	for (const auto& wall_neighbor : src.m_wall_neighbors) {
		auto pair = target->m_wall_neighbors.insert(
			make_pair(find_cell(wall_neighbor.first), list<Cell*>()));
		assert(pair.second && "m_wall_neighbors");
		transform(
			wall_neighbor.second.begin(),
			wall_neighbor.second.end(),
			back_inserter((pair.first)->second),
			find_cell);
	}

	return target;
}

}
