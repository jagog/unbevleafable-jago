/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for AttributeStore.
 */

#include "AttributeStore.h"
#include "util/misc/Exception.h"

#include <cassert>
#include <iostream>

using namespace std;
using boost::property_tree::ptree;
using SimPT_Sim::Util::Exception;


namespace SimPT_Sim {

AttributeStore::AttributeStore(const boost::property_tree::ptree& pt) : m_ptree(pt)
{
	const string xcpt = "AttributeStoreIndex::AttributeStoreIndex(const ptree&) : for type ";

	for(auto it = m_ptree.begin(); it != m_ptree.end(); it++){
		const auto pt2 = it->second;
		const auto name = pt2.get<string>("name");
		const auto type = pt2.get<string>("type");

		if( type == "bool" ){
			if ( IsAttribute<bool>(name) ) {
				throw std::runtime_error(xcpt + "bool, name already in index: " + name);
			} else{
				m_map_b[name] = make_pair(std::vector<bool>(), pt2.get<bool>("default"));
			}
		} else if( type == "int" ){
			if ( IsAttribute<int>(name) ) {
				throw std::runtime_error(xcpt + "int, name already in index: " + name);
			} else{
				m_map_i[name] = make_pair(std::vector<int>(), pt2.get<int>("default"));
			}
		} else if( type == "double" ) {
			if ( IsAttribute<int>(name) ) {
				throw std::runtime_error(xcpt + "double, name already in index: " + name);
			} else{
				m_map_d[name] = make_pair(std::vector<double>(), pt2.get<double>("default"));
			}
		} else if ( type == "string" ) {
			if ( IsAttribute<string>(name) ) {
				throw std::runtime_error(xcpt + "string, name already in index: " + name);
			} else{
				m_map_s[name] = make_pair(std::vector<string>(), pt2.get<string>("default"));
			}
		} else {
			throw Exception( xcpt + type + " is not a supported attribute type.");
		}
	}
}

ptree AttributeStore::GetAttributeValues(size_t pos) const
{
	ptree result;

	for(const auto& name : GetAttributeNames<bool>()) {
		result.put(name, Container<bool>(name).at(pos));
	}
	for(const auto& name : GetAttributeNames<int>()) {
		result.put(name, Container<int>(name).at(pos));
	}
	for(const auto& name : GetAttributeNames<double>()) {
		result.put(name, Container<double>(name).at(pos));
	}
	for(const auto& name : GetAttributeNames<string>()) {
		result.put(name, Container<string>(name).at(pos));
	}

	return result;
}

ptree AttributeStore::GetIndexDump() const
{
	ptree result;

	for(const auto& name : GetAttributeNames<bool>()) {
		ptree child;
		child.put("name", name);
		child.put("type", "bool");
		child.put("default", GetDefaultValue<bool>(name));
		result.add_child("attribute", child);
	}
	for(const auto& name : GetAttributeNames<int>()) {
		ptree child;
		child.put("name", name);
		child.put("type", "int");
		child.put("default", GetDefaultValue<int>(name));
		result.add_child("attribute", child);
	}
	for(const auto& name : GetAttributeNames<double>()) {
		ptree child;
		child.put("name", name);
		child.put("type", "double");
		child.put("default", GetDefaultValue<double>(name));
		result.add_child("attribute", child);
	}
	for(const auto& name : GetAttributeNames<string>()) {
		ptree child;
		child.put("name", name);
		child.put("type", "string");
		child.put("default", GetDefaultValue<double>(name));
		result.add_child("attribute", child);
	}

	return result;
}

ostream& operator<<(ostream& os, const AttributeStore& a)
{
	os << "AttributeStore index:" << endl;
	const ptree pt = a.GetIndexPtree();
	for(auto it = pt.begin(); it != pt.end(); it++){
		const auto pt2 = it->second;
		const auto name = pt2.get<string>("name");
		const auto type = pt2.get<string>("type");

		if( type == "bool" ){
			os << "name: " << name << "   type: bool  " << "    default value: " << pt2.get<bool>("default") << endl;
		} else if( type == "int" ){
			os << "name: " << name << "   type: int   " << "    default value: " << pt2.get<int>("default") << endl;
		} else if( type == "double" ) {
			os << "name: " << name << "   type: double" << "    default value: " << pt2.get<double>("default") << endl;
		} else if ( type == "string" ) {
			os << "name: " << name << "   type: string" << "    default value: " << pt2.get<string>("default") << endl;
		} else {
			throw Exception( string("operator<<(ostream& os, const AttributeStore& a)") + type + " is not a supported attribute type.");
		}
	}

	os << "AttributeStore values for bool attributes:" << endl;
	for (const auto& name : a.GetAttributeNames<bool>()) {
		os << "name: " << name << endl;
		for (const auto e : a.Container<bool>(name)) {
			os << e << endl;
		}
		os << endl;
	}

	os << "AttributeStore values for int attributes:" << endl;
	for (const auto& name : a.GetAttributeNames<int>()) {
		os << "name: " << name << endl;
		for (const auto e : a.Container<int>(name)) {
			os << e << endl;
		}
		os << endl;
	}

	os << "AttributeStore values for double attributes:" << endl;
	for (const auto& name : a.GetAttributeNames<double>()) {
		os << "name: " << name << endl;
		for (const auto e : a.Container<double>(name)) {
			os << e << endl;
		}
		os << endl;
	}

	os << "AttributeStore values for string attributes:" << endl;
	for (const auto& name : a.GetAttributeNames<string>()) {
		os << "name: " << name << endl;
		for (const auto e : a.Container<string>(name)) {
			os << e << endl;
		}
		os << endl;
	}

	return os;
}

} // namespace
