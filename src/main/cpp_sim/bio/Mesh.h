#ifndef MESH_H_INCLUDED
#define MESH_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for Mesh.
 */

#include "Cell.h"
#include "NeighborNodes.h"
#include "Node.h"
#include "util/container/SegmentedVector.h"

#include <boost/property_tree/ptree.hpp>
#include <array>
#include <list>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <tuple>
#include <vector>

namespace SimPT_Editor {
	class EditableMesh;
}

namespace SimPT_Sim {

class Edge;
class Node;
class MeshState;
class Wall;


/**
 * Structure of cells; key data structure.
 *
 * The mesh consists of nodes, walls and cells. For more information,
 * see Node.h, Wall.h and Cell.h. There are some structures provided
 * (that must be kept consistent) to increase efficiency.
 * - A boundary polygon that contains the boundary of the whole cell complex is being kept.
 * - For every cell, their neighbor cells are provided (see Mesh::GetNeighbors).
 * - For every node, their neighbors are provided (see Mesh::GetNodeOwningNeighbors).
 *   For more information about neighbors, see Neighbor.h.
 * - For every node, the wall to which the node belongs are provided (see Mesh::NodeOwningWalls).
 *   This information is updated by Mesh::UpdateNodeOwningWalls.
 */
class Mesh
{
public:
	/// Constructs empty Mesh.
	Mesh(unsigned int num_chemicals);

	/// Add node to cell between existing neighbor nodes.
	void AddNodeToCell(Cell* c, Node* n, Node* nb1, Node* nb2);

	Cell* BuildBoundaryPolygon(const std::vector<unsigned int>& node_ids);

	Cell* BuildCell();

	Cell* BuildCell(const std::vector<unsigned int>& node_ids);

	Cell* BuildCell(unsigned int id, const std::vector<unsigned int>& node_ids);

	Node* BuildNode(const std::array<double, 3>& vec, bool at_boundary);

	Node* BuildNode(unsigned int id, const std::array<double, 3>& vec, bool at_boundary);

	Wall* BuildWall(Node* n1, Node* n2, Cell* c1, Cell* c2);

	Wall* BuildWall(unsigned int id, Node* n1, Node* n2, Cell* c1, Cell* c2);

	/// Constructs neighbor list by looping over walls and registering adjoining cell
	/// for that wall, and then eliminating all instances of boundary polygon.
	void ConstructNeighborList(Cell* cell);

	/// Constructs neighbor list by looping over walls and registering
	//void ConstructNeighborList(Cell* cell) { ConstructNeighborList(cell); }

	/// Calculate derivatives of all variables to pass to ODESolver.
	void Derivatives(double* derivs);

	/// Find the neighbor to this cell with respect to the given edge.
	/// This neighbor can be the boundary polygon.
	Cell* FindEdgeNeighbor(Cell* cell, Edge edge) const;

	/// Find the wall including the edge.
	Wall* FindWall(const Edge& edge) const;

	/// Get the boundary polygon of the mesh.
	Cell* GetBoundaryPolygon() const { return m_boundary_polygon; }

	/// The cells of this mesh, EXCLUDING the boundary polygon.
	const std::vector<Cell*>& GetCells() const { return m_cells; }

	/// The cells of this mesh, EXCLUDING the boundary polygon.
	std::vector<Cell*>& GetCells() { return m_cells; }

	/// Find the intersecting neighbors of the end points of the edge. Every edge has
	/// two neighbor cells, so there will be four of neighbors (two for every end point).
	std::vector<NeighborNodes> GetEdgeOwners(const Edge& edge) const;

	/// Get the adjacent cells to the given cell (i.e. sharing a wall).
	std::list<Cell*> const& GetNeighbors(Cell* c) const { return m_wall_neighbors.at(c); }

	/// Get the adjacent cells to the given cell (i.e. sharing a wall).
	std::list<Cell*>& GetNeighbors(Cell* c) { return m_wall_neighbors[c]; }

	/// Get the neighbors associated with this node.
	std::list<NeighborNodes>& GetNodeOwningNeighbors(Node* n) { return m_node_owning_neighbors[n]; }

	/// Get the neighbors associated with this node.
	const std::list<NeighborNodes>& GetNodeOwningNeighbors(Node* n) const { return m_node_owning_neighbors.at(n); }

	/// The walls that contain the given node somewhere on the wall.
	std::list<Wall*>& GetNodeOwningWalls(Node* n) { return m_node_owning_walls[n]; }

	/// The walls that contain the given node somewhere on the wall.
	const std::list<Wall*>& GetNodeOwningWalls(Node* n) const { return m_node_owning_walls.at(n); }

	/// The nodes of the mesh.
	const std::vector<Node*>& GetNodes() const { return m_nodes; }

	/// The nodes of the mesh.
	std::vector<Node*>& GetNodes() { return m_nodes; }

	// The chemical count.
	unsigned int GetNumChemicals() const { return m_num_chemicals; }

	/// The current state of the mesh as a self-contained object.
	MeshState GetState() const;

	/// The walls of this mesh.
	const std::vector<Wall*>& GetWalls() const { return m_walls; }

	/// The walls of this mesh.
	std::vector<Wall*>& GetWalls() { return m_walls; }

	/// True iff the edge is on the mesh boundary.
	bool IsAtBoundary(const Edge* e) const;

	/// True iff one of cells owning the wall is the boundary polygon.
	bool IsAtBoundary(Wall* w) const;

	/// True iff the node is one of the nodes of the boundary polygon.
	bool IsInBoundaryPolygon(Node* n) const;

	/// Solely for debugging purposes
	std::ostream& Print(std::ostream& os) const;

	/// Removes the cell from the neighbor list (only do this when deleting this cell).
	void RemoveFromNeighborList(Cell* cell);

	/// Removes the node from the 'node owning neighbors' and 'node owning walls' lists.
	/// Only do this when deleting this node.
	void RemoveNodeFromOwnerLists(Node* node);

	/// Convert the mesh to a ptree.
	boost::property_tree::ptree ToPtree() const;

	/// Set ownerships of node <-> cell or update them after cell division.
	void UpdateNodeOwningNeighbors(Cell* cell);

	/// Set ownerships of node <-> cell or update them after cell division.
	//void UpdateNodeOwningNeighbors(Cell* cell) { UpdateNodeOwningNeighbors(cell); }

	/// Sets and/or corrects the list of node owning walls of the nodes on this wall.
	/// Completely corrects the node owning walls of the nodes in between of the end points.
	/// Removes wrong entries and adds this wall in the node owning wall list of
	/// the end points of the wall. (So if another wall isn't contained in the list of node
	/// owning walls of an end point, it won't be added)
	void UpdateNodeOwningWalls(Wall *wall);

	/// @see void Mesh::UpdateNodeOwningWalls(Wall *wall);
	//void UpdateNodeOwningWalls(Wall* wall) { UpdateNodeOwningWalls(wall); }

public:
	/// Used for interacting with ODE-solvers.
	void getValues(double* values);

	/// Used for interacting with ODE-solvers.
	void setValues(double x, double* y);

private:
	friend class MeshCheck;
	friend class CBMBuilder;
	friend class SimPT_Editor::EditableMesh;

private:
	/// At a node at the end of the list of nodes in the cell. (Only when building a cell.)
	/// @param    c      The given cell.
	/// @param    n      The node that should be added.
	/// @param    nb1    The first neighbor of the node.
	/// @param    nb2    The second neighbor of the node.
	void AddConsecutiveNodeToCell(Cell* c, Node* n, Node* nb1, Node* nb2);

	/// Find the boundary node next to a given node. The next boundary
	/// is the next node in the list of nodes of the boundary polygon.
	Node* FindNextBoundaryNode(Node*) const;

	/// Fix the IDs of all the walls. The IDs should start at 0 and be subsequent.
	/// The order of the walls by ID will be kept.
	void FixWallIDs();

	/// Fix the IDs of all the nodes. The IDs should start at 0 and be subsequent.
	/// The order of the nodes by ID will be kept.
	void FixNodeIDs();

	/// Fix the IDs of all the cells. The IDs start at 0 (except boundary polygon which
	/// must be -1) and must be consecutive. The order of the cells by ID will be kept.
	void FixCellIDs();

private:
	/// Outer boundary of the mesh.
	Cell*  m_boundary_polygon;

	/// Contiguous storage for cells (for locality)
	SimPT_Sim::Container::SegmentedVector<Cell>  m_cell_storage;

	/// Cells that are part of this mesh.
	std::vector<Cell*>  m_cells;

	/// Next id to be handed out (excludes boundary_polygon).
	unsigned int m_cell_next_id;

	/// Contiguous storage for nodes (for locality)
	SimPT_Sim::Container::SegmentedVector<Node>  m_node_storage;

	/// Nodes in the mesh. The id should be equal to the index of the vector.
	std::vector<Node*>  m_nodes;

	/// Next id to be handed out.
	unsigned int m_node_next_id;

	/// Owners of each of the nodes.
	std::map<Node*, std::list<NeighborNodes>> m_node_owning_neighbors;

	/// Walls to which each of the nodes belong.
	std::map<Node*, std::list<Wall*>> m_node_owning_walls;

	/// Number of chemicals in each cell (needed here to dimension vectors).
	const unsigned int m_num_chemicals;

	/// Contiguous storage for walls (for locality)
	SimPT_Sim::Container::SegmentedVector<Wall>  m_wall_storage;

	/// Walls defined in the mesh.
	std::vector<Wall*>   m_walls;

	/// Next id to be handed out.
	unsigned int m_wall_next_id;

	/// Cells (excluding boundary_polygon and yourself) you share a wall with.
	std::map<Cell*, std::list<Cell*>>  m_wall_neighbors;
};

}

#endif // end_of_include_guard
