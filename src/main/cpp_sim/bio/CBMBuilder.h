#ifndef CBMBUILDER_H_INCLUDED
#define CBMBUILDER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of CBMBuilder.
 */

#include <memory>

namespace SimPT_Sim {

class Mesh;

/**
 * The Copy-Based Mesh Builder directs the Mesh building process using another mesh.
 * This will result in a deep copy of the given mesh.
 */
class CBMBuilder {
public:
	CBMBuilder();

	/**
	 * Build a mesh exactly like the given mesh.
	 */
	std::shared_ptr<Mesh> Build(const Mesh& mesh);
};

} //namespace

#endif // end_of_include_guard

