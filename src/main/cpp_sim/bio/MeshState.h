#ifndef MESHSTATE_H_INCLUDED
#define MESHSTATE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of MeshState.
 */

////////////////////////////////////////////////////////////////////////////
// WARNING: FILE ALSO GETS PROCESSED BY SWIG. SWIG (VERSIONS EARLY 2013)  //
// DOES NOT ACCEPT TWO CONSECUTIVE >> TEMPLATE TYPE PARAMETERLIST ENDING  //
// BRACKETS WITHOUT INTERVENING BLANK (EVEN THOUGH C++11 DOES ACCPT THIS) //
////////////////////////////////////////////////////////////////////////////

#include <map>
#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>
#include "AttributeContainer.h"

namespace SimPT_Sim {

/**
 * Contains the state of the whole Mesh at a given simulation step.
 * This class is mainly supposed to be a data object to pass around
 * mesh state information for saving and loading.
 */
class MeshState
{
public:
	/**
	 * Creates a meshstate that represents an empty mesh with no nodes / cells / walls.
	 */
	MeshState();

	/** @name Nodes
	 * Methods for handling nodes in the mesh.
	 * Note that the collective node operations assume the passed/retrieved
	 * containers to be ordered by node index and NOT by node ID!
	 */
	///@{
	/**
	 * Returns the number of nodes in the mesh.
	 */
	size_t GetNumNodes() const;

	/**
	 * Sets the number of nodes in the mesh.
	 * This method must be called with the requested number of nodes prior to setting
	 * any node properties since it preallocates space for all node related props.
	 */
	void SetNumNodes(size_t n);

	/**
	 * Returns the ID of a particular node.
	 */
	int GetNodeID(size_t node_idx) const;

	/**
	 * Returns the x coordinate of a particular node.
	 */
	double GetNodeX(size_t node_idx) const;

	/**
	 * Returns the y coordinate of a particular node.
	 */
	double GetNodeY(size_t node_idx) const;

	/**
	 * Returns the IDs of all nodes.
	 */
	std::vector<int> GetNodesID() const;

	/**
	 * Returns the x coordinates of all nodes.
	 */
	std::vector<double> GetNodesX() const;

	/**
	 * Returns the y coordinates of all nodes.
	 */
	std::vector<double> GetNodesY() const;

	/**
	 * Adds a new named attribute of type T for the nodes.
	 */
	AttributeContainer& NodeAttributeContainer()
	{
		return m_nodes_attr;
	}

	/**
	 * Adds a new named attribute of type T for the nodes.
	 */
	const AttributeContainer& NodeAttributeContainer() const
	{
		return m_nodes_attr;
	}

	/**
	 * Sets the ID of a particular node.
	 */
	void SetNodeID(size_t node_idx, int value);

	/**
	 * Sets the x coordinate of a particular node.
	 */
	void SetNodeX(size_t node_idx, double value);

	/**
	 * Sets the y coordinate of a particular node.
	 */
	void SetNodeY(size_t node_idx, double value);

	/**
	 * Sets the IDs of all nodes.
	 */
	void SetNodesID(std::vector<int> values);

	/**
	 * Sets the x coordinates of all nodes.
	 */
	void SetNodesX(std::vector<double> values);

	/**
	 * Sets the y coordinates of all nodes.
	 */
	void SetNodesY(std::vector<double> values);

	///@}

	/** @name Cells
	 * Methods for handling cells in the mesh.
	 * Note that the collective cell operations assume the passed/retrieved
	 * containers to be ordered by cell index and NOT by cell ID!
	 */
	///@{
	/**
	 * Returns the number of cells in the mesh.
	 */
	size_t GetNumCells() const;

	/**
	 * Sets the number of cells in the mesh.
	 * This method must be called with the requested number of cells prior to setting
	 * any cell properties since it preallocates space for all cell related props.
	 */
	void SetNumCells(size_t n);

	/**
	 * Adds a new named attribute of type T for the nodes.
	 */
	AttributeContainer& CellAttributeContainer()
	{
		return m_cells_attr;
	}

	/**
	 * Adds a new named attribute of type T for the nodes.
	 */
	const AttributeContainer& CellAttributeContainer() const
	{
		return m_cells_attr;
	}

	/**
	 * Returns the ID of a particular cell.
	 */
	int GetCellID(size_t cell_idx) const;

	/**
	 * Returns the number of nodes that define the polygon of a particular cell.
	 */
	size_t GetCellNumNodes(size_t cell_idx) const;

	/**
	 * Returns the IDs of the nodes that form the polygon of a particular cell.
	 */
	std::vector<int> GetCellNodes(size_t cell_idx) const;

	/**
	 * Returns the number of walls of a particular cell.
	 */
	size_t GetCellNumWalls(size_t cell_idx) const;

	/**
	 * Returns the IDs of walls of a particular cell.
	 */
	std::vector<int> GetCellWalls(size_t cell_idx) const;

	/**
	 * Sets the ID of a particular cell.
	 */
	void SetCellID(size_t cell_idx, int value);

	/**
	 * Sets the IDs of the nodes that form the polygon of a particular cell.
	 */
	void SetCellNodes(size_t cell_idx, std::vector<int> node_ids);

	/**
	 * Sets the IDs of walls of a particular cell.
	 */
	void SetCellWalls(size_t cell_idx, std::vector<int> wall_ids);

	/**
	 * Returns the IDs of all cells.
	 */
	std::vector<int> GetCellsID() const;

	/**
	 * Returns the number of nodes for all cells.
	 */
	std::vector<size_t> GetCellsNumNodes() const;

	/**
	 * Returns the number of walls for all cells.
	 */
	std::vector<size_t> GetCellsNumWalls() const;

	/**
	 * Sets the IDs of all cells.
	 */
	void SetCellsID(std::vector<int> values);

	/**
	 * Returns the IDs of the nodes that form the boundary polygon of the tissue.
	 */
	std::vector<int> GetBoundaryPolygonNodes() const;

	/**
	 * Returns the IDs of the walls that form the boundary polygon of the tissue.
	 */
	std::vector<int> GetBoundaryPolygonWalls() const;

	/**
	 * Sets the IDs of the nodes that form the boundary polygon of the tissue.
	 */
	void SetBoundaryPolygonNodes(std::vector<int> node_ids);

	/**
	 * Sets the IDs of the walls that form the boundary polygon of the tissue.
	 */
	void SetBoundaryPolygonWalls(std::vector<int> wall_ids);
	///@}

	/** @name Walls
	 * Methods for handling walls in the mesh.
	 */
	///@{
	/**
	 * Returns the number of walls in the mesh.
	 */
	size_t GetNumWalls() const;

	/**
	 * Sets the number of walls in the mesh.
	 * This method must be called with the requested number of walls prior to setting
	 * any wall properties since it preallocates space for all wall related props.
	 */
	void SetNumWalls(size_t n);

	/**
	 * Adds a new named attribute of type T for the nodes.
	 */
	AttributeContainer& WallAttributeContainer()
	{
		return m_walls_attr;
	}

	/**
	 * Adds a new named attribute of type T for the nodes.
	 */
	const AttributeContainer& WallAttributeContainer() const
	{
		return m_walls_attr;
	}

	/**
	 * Returns the ID of a particular wall.
	 */
	int GetWallID(size_t wall_idx) const;

	/**
	 *
	 */
	std::pair<int, int> GetWallNodes(size_t wall_idx) const;

	/**
	 *
	 */
	std::pair<int, int> GetWallCells(size_t wall_idx) const;

	/**
	 * Sets the ID of a particular wall.
	 */
	void SetWallID(size_t wall_idx, int value);

	/**
	 *
	 */
	void SetWallNodes(size_t wall_idx, std::pair<int, int> nodes_ids);

	/**
	 *
	 */
	void SetWallCells(size_t wall_idx, std::pair<int, int> cells_ids);

	/**
	 * Returns the IDs of all walls.
	 */
	std::vector<int> GetWallsID() const;

	/**
	 * Sets the IDs of all walls.
	 */
	void SetWallsID(std::vector<int> values);

	///@}

	/**
	 * Prints a textual representation of this MeshState to output stream.
	 * @param[in,out]      out The ouput stream to use for printing.
	 * @note This method is mainly used for debug purposes.
	 */
	void PrintToStream(std::ostream& out) const;

private:
	size_t                              m_num_nodes;                ///< Number of nodes in the tissue
	std::vector<int>                    m_nodes_id;                 ///< IDs of nodes
	std::vector<double>                 m_nodes_x;                  ///< X coordinates of nodes
	std::vector<double>                 m_nodes_y;                  ///< Y coordinates of nodes
	AttributeContainer                  m_nodes_attr ;              ///< Node attributes

private:
	size_t                              m_num_cells;                ///< Number of cells in the tissue
	std::vector<int>                    m_cells_id;                 ///< IDs of cells
	std::vector< std::vector<int> >     m_cells_nodes;              ///< Node IDs of the cells
	std::vector< std::vector<int> >     m_cells_walls;              ///< Wall IDs of the cells
	std::vector<int>                    m_boundary_polygon_nodes;   ///< Node IDs of the boundary polygon
	std::vector<int>                    m_boundary_polygon_walls;   ///< Wall IDs of the boundary polygon
	AttributeContainer                  m_cells_attr;               ///< Cell attributes

private:
	size_t                              m_num_walls;                ///< Number of walls in the tissue
	std::vector<int>                    m_walls_id;                 ///< IDs of walls
	std::vector<std::pair<int, int> >   m_walls_cells;              ///< IDs of cells connected by the walls
	std::vector<std::pair<int, int> >   m_walls_nodes;              ///< IDs of end-nodes of the walls
	AttributeContainer                  m_walls_attr;               ///< Wall attributes
};

} // namespace

#endif // include guard

