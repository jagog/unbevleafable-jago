#ifndef BOUNDARY_TYPE_H_INCLUDED
#define BOUNDARY_TYPE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * BoundaryType enumeration class.
 */

#include "util/misc/StringUtils.h"

#include <istream>
#include <map>
#include <ostream>
#include <string>

namespace SimPT_Sim {

/**
 * Enumerates cell boundary types.
 */
enum class BoundaryType
{
		None, NoFlux, SourceSink, SAM
};

inline std::ostream& operator<<(std::ostream& os, BoundaryType b)
{
	static const std::map<BoundaryType, std::string> m {
		std::make_pair(BoundaryType::None,        "None"),
		std::make_pair(BoundaryType::NoFlux,      "NoFlux"),
		std::make_pair(BoundaryType::SourceSink,  "SourceSink"),
		std::make_pair(BoundaryType::SAM,         "SAM")
	};

	const auto search = m.find(b);
	os << ((search != m.end()) ? search->second : "Unknown");

	return os;
}

inline std::istream& operator>>(std::istream& is, BoundaryType& b)
{
	static const std::map<std::string, BoundaryType> m {
		std::make_pair("NONE",        BoundaryType::None),
		std::make_pair("NOFLUX",      BoundaryType::NoFlux),
		std::make_pair("SOURCESINK",  BoundaryType::SourceSink),
		std::make_pair("SAM",         BoundaryType::SAM)
	};

	std::string s;
	is >> s;
	const auto search = m.find(SimPT_Sim::Util::ToUpper(s));
	b = (search != m.end()) ? search->second : BoundaryType::None;

	return is;
}

} // namespace

#endif // end-of-include-guard
