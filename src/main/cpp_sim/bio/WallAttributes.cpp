/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for WallAttributes.
 */

#include "WallAttributes.h"

#include "WallType.h"
#include "util/misc/Exception.h"
#include "util/misc/log_debug.h"

#include <boost/optional.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

using namespace std;
using namespace boost::property_tree;
using boost::optional;
using namespace SimPT_Sim;
using namespace SimPT_Sim::Util;

namespace SimPT_Sim {

WallAttributes::WallAttributes(unsigned int num_chem)
	: m_rest_length(0.0), m_rest_length_init(0.0), m_strength(0.0),
	  m_transporters1(vector<double>(num_chem)), m_transporters2(vector<double>(num_chem)),
	  m_wall_type(WallType::Type::Normal)
{
}

WallAttributes::WallAttributes(double rest_length, double rest_length_init, double strength,
	const vector<double>& transporters1, const vector<double>& transporters2, WallType::Type wall_type)
	: m_rest_length(rest_length), m_rest_length_init(rest_length_init), m_strength(strength),
	  m_transporters1(transporters1), m_transporters2(transporters2), m_wall_type(wall_type)
{
}

void WallAttributes::CycleWallType()
{
	if (m_wall_type == WallType::Type::Normal) {
		m_wall_type = WallType::Type::AuxinSource;
	} else if (m_wall_type == WallType::Type::AuxinSource) {
		m_wall_type = WallType::Type::AuxinSink;
	} else if (m_wall_type == WallType::Type::AuxinSink) {
		m_wall_type = WallType::Type::Normal;
	}
}

void WallAttributes::Swap(WallAttributes& src)
{
	swap(m_rest_length,       src.m_rest_length);
	swap(m_rest_length_init,  src.m_rest_length_init);
	swap(m_transporters1,     src.m_transporters1);
	swap(m_transporters2,     src.m_transporters2);
	swap(m_wall_type,         src.m_wall_type);
}

void WallAttributes::ReadPtree(boost::property_tree::ptree const& wall_pt)
{
	try {
		m_rest_length       = wall_pt.get<double>("rest_length");
		m_rest_length_init  = wall_pt.get<double>("rest_length_init");
		m_strength          = wall_pt.get<double>("strength");
		m_transporters1.assign(m_transporters1.size(), 0.0);
		m_transporters2.assign(m_transporters2.size(), 0.0);
		m_wall_type         = WallType::FromString(wall_pt.get<string>("type", "normal"));

		optional<ptree const&> tp1_array_pt = wall_pt.get_child_optional("transporter1_array");
		if (tp1_array_pt) {
			assert(tp1_array_pt->size() <= m_transporters1.size());
			int tp1_i = 0;
			for (auto const& tp1_v : *tp1_array_pt) {
				m_transporters1[tp1_i++] = tp1_v.second.get_value<double>();
			}
		}

		optional<ptree const&> tp2_array_pt = wall_pt.get_child_optional("transporter2_array");
		if (tp2_array_pt) {
			assert(tp2_array_pt->size() <= m_transporters1.size());
			int tp2_i = 0;
			for (auto const& tp2_v : *tp2_array_pt) {
				m_transporters2[tp2_i++] = tp2_v.second.get_value<double>();
			}
		}
	}
	catch(exception& e) {
		const string here = string(VL_HERE) + " exception:\n";
		throw Exception(here + e.what());
	}
}

ptree WallAttributes::ToPtree() const
{
	ptree ret;

	ret.put("rest_length", GetRestLength());
	ret.put("rest_length_init", GetRestLengthInit());
	ret.put("strength", GetStrength());
	ret.put("type", WallType::ToString(m_wall_type));

	ptree tp1_pt;
	for (const auto& t : m_transporters1) {
		tp1_pt.add("transporter1", t);
	}
	ret.put_child("transporter1_array", tp1_pt);

	ptree tp2_pt;
	for (const auto& t : m_transporters2) {
		tp2_pt.add("transporter2", t);
	}
	ret.put_child("transporter2_array", tp2_pt);

	return ret;
}

} // namespace
