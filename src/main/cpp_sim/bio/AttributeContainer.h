#ifndef ATTRIBUTE_CONTAINER_H_INCLUDED
#define ATTRIBUTE_CONTAINER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface of AttributeContainer.
 */

////////////////////////////////////////////////////////////////////////////
// WARNING: FILE ALSO GETS PROCESSED BY SWIG. SWIG (VERSIONS EARLY 2013)  //
// DOES NOT ACCEPT TWO CONSECUTIVE >> TEMPLATE TYPE PARAMETERLIST ENDING  //
// BRACKETS WITHOUT INTERVENING BLANK (EVEN THOUGH C++11 DOES ACCPT THIS) //
////////////////////////////////////////////////////////////////////////////

#include <map>
#include <string>
#include <vector>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Sim {

/**
 * Manages attributes.
 *
 * NOTE that the collective node operations assume the passed/retrieved
 * containers to be ordered by node index and NOT by node ID!
 */
class AttributeContainer
{
public:
	/**
	 * Creates an empty AttributeContainer.
	 */
	AttributeContainer(size_t n = 0)
		: m_num(n)
	{
		SetNum(n);
	}

	/**
	 * Adds a named attribute of type T.
	 */
	template<typename T> void Add(const std::string& name);

	/**
	 * Returns the value of a named attribute.
	 */
	template<typename T> T Get(size_t index, const std::string& name) const;

	/**
	 * Returns all values of a named attribute.
	 */
	template<typename T> std::vector<T> GetAll(const std::string& name) const;

	/**
	 * Returns a vector containing the names of attributes of type T.
	 */
	template<typename T> std::vector<std::string> GetNames() const;

	/**
	 * Returns size of each of the attribute containers.
	 */
	size_t GetNum() const
	{
		return m_num;
	}

	/**
	 * Returns true if there is an attribute of type T with a specific name.
	 */
	template<typename T> bool IsName(const std::string& name) const;

	/**
	 * Prints a textual representation of this AttributeContainer.
	 */
	void Print(std::ostream& out) const;

	/**
	 * Sets the value of a named attribute.
	 */
	template<typename T> void Set(size_t index, const std::string& name, T value);

	/**
	 * Sets all values of a named attribute.
	 */
	template<typename T> void SetAll(const std::string& name, const std::vector<T>& values);

	/**
	 * Set size of each of the attribute containers.
	 */
	void SetNum(size_t n)
	{
		m_num = n;
		for (auto attr : m_attri) {
			attr.second.resize(n);
		}
		for (auto attr : m_attrd) {
			attr.second.resize(n);
		}
		for (auto attr : m_attrs) {
			attr.second.resize(n);
		}
	}

private:
	size_t m_num;
	template<typename T> std::map<std::string, std::vector<T> >& Attributes();
	template<typename T> const std::map<std::string, std::vector<T> >& Attributes() const;

private:
	std::map<std::string, std::vector<int> >          m_attri;   ///< Attributes of type int
	std::map<std::string, std::vector<double> >       m_attrd;   ///< Attributes of type double
	std::map<std::string, std::vector<std::string> >  m_attrs;   ///< Attributes of type std::string
};


/////////////////////////////////////////////////////////////////////////////////////////////////////
// Implementation.
/////////////////////////////////////////////////////////////////////////////////////////////////////
template<> inline
std::map<std::string, std::vector<int> >& AttributeContainer::Attributes<int>()
{
	return m_attri;
}

template<> inline
const std::map<std::string, std::vector<int> >& AttributeContainer::Attributes<int>() const
{
	return m_attri;
}

template<> inline
std::map<std::string, std::vector<double> >& AttributeContainer::Attributes<double>()
{
	return m_attrd;
}

template<> inline
const std::map<std::string, std::vector<double> >& AttributeContainer::Attributes<double>() const
{
	return m_attrd;
}

template<> inline
std::map<std::string, std::vector<std::string> >& AttributeContainer::Attributes<std::string>()
{
	return m_attrs;
}

template<> inline
const std::map<std::string, std::vector<std::string> >& AttributeContainer::Attributes<std::string>() const
{
	return m_attrs;
}

template<typename T>
inline void AttributeContainer::Add(const std::string& name)
{
	Attributes<T>()[name] = std::vector<T>(m_num);
}

template<typename T>
inline T AttributeContainer::Get(size_t node_idx, const std::string& name) const
{
	auto attr_kv = Attributes<T>().find(name);
	if (attr_kv != Attributes<T>().end()) {
		return attr_kv->second.at(node_idx);
	} else {
		throw std::runtime_error("AttributeContainer::Get(): attribute not found: " + name);
	}
}
template<typename T>
inline std::vector<std::string> AttributeContainer::GetNames() const
{
	std::vector<std::string> names;
	for (auto attr : Attributes<T>()) {
		names.push_back(attr.first);
	}
	return names;
}

template<typename T>
inline bool AttributeContainer::IsName(const std::string& name) const
{
	return Attributes<T>().find(name) != Attributes<T>().end();
}

template<typename T>
inline void AttributeContainer::Set(size_t node_idx, const std::string& name, T value)
{
	auto attr_kv = Attributes<T>().find(name);
	if (attr_kv != Attributes<T>().end()) {
		attr_kv->second.at(node_idx) = value;
	} else {
		throw std::runtime_error("AttributeContainer::Set(): attribute not found: " + name);
	}
}

template<typename T>
inline std::vector<T> AttributeContainer::GetAll(const std::string& name) const
{
	auto attr_kv = Attributes<T>().find(name);
	if (attr_kv != Attributes<T>().end()) {
		return attr_kv->second;
	} else {
		throw std::runtime_error("AttributeContainer::GetAll(): attribute not found: " + name);
	}
}

template<typename T>
inline void AttributeContainer::SetAll(const std::string& name, const std::vector<T>& values)
{
	if (values.size() != GetNum()) {
		throw std::runtime_error("AttributeContainer::SetAll(): Number attribute values doesn't match number of values.");
	} else {
		auto attr_kv = Attributes<T>().find(name);
		if (attr_kv != Attributes<T>().end()) {
			attr_kv->second = values;
		} else {
			throw std::runtime_error("AttributeContainer::SetAll(): attribute not found: " + name);
		}
	}
}

} // namespace

#endif // include guard

