/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of WallType.
 */

#include "WallType.h"

#include <boost/algorithm/string.hpp>
#include <map>
#include <string>

using namespace std;
using boost::to_upper;
using namespace SimPT_Sim::WallType;

namespace {

std::map<Type, string> g_wall_type_name {
                make_pair(Type::AuxinSource, "aux_source"),
                make_pair(Type::AuxinSink, "aux_sink"),
                make_pair(Type::Normal, "normal")
};

std::map<string, Type> g_wall_name_type {
                make_pair("AUX_SOURCE", Type::AuxinSource),
                make_pair("AUX_SINK", Type::AuxinSink),
                make_pair("NORMAL", Type::Normal)
};

}

namespace SimPT_Sim {
namespace WallType {

string ToString(Type w)
{
	string ret = "Unknown";
	if (g_wall_type_name.count(w) == 1)
	                {
		ret = g_wall_type_name[w];
	}
	return ret;
}

Type FromString(string s)
{
	Type ret = Type::Normal;
	to_upper(s);
	if (g_wall_name_type.count(s) == 1)
	                {
		ret = g_wall_name_type[s];
	}
	return ret;
}

} // namespace
} // namespace
