#ifndef NODE_ATTRIBUTES_H_INCLUDED
#define NODE_ATTRIBUTES_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for NodeAttributes.
 */

#include <boost/property_tree/ptree.hpp>

namespace SimPT_Sim {

/**
 * Attributes associated with a node.
 */
class NodeAttributes
{
public:
	///
	NodeAttributes() : m_fixed(false), m_sam(false) {}

	///
	virtual ~NodeAttributes() {}

	///
	bool IsFixed() const { return m_fixed; }

	///
	bool IsSam() const { return m_sam; }

	/// Read the given ptree into the attributes.
	virtual void ReadPtree(const boost::property_tree::ptree& node_pt);

	///
	void SetFixed(bool b){ m_fixed = b; }

	///
	void SetSam(bool b) { m_sam = b; }

	/**
 	 * Convert the node attributes to a ptree.
	 * The format of a node attributes ptree is as follows:
	 *
	 * <fixed>[true if the node is fixed]</fixed>
	 * <sam>[true if the node is connected to the shoot]</sam>
	 */
	virtual boost::property_tree::ptree ToPtree() const;

private:
	bool m_fixed;         ///< Fixed nodes cannot move. E.g. to represent the petiole
	bool m_sam;           ///< True if node is connected to the shoot
};

}

#endif // end_of_include_guard
