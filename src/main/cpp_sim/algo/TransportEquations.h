#ifndef RDAT_EQUATIONS_H_INCLUDED
#define RDAT_EQUATIONS_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Equations for diffusion and transport.
 */

#include "model/ComponentInterfaces.h"
#include "sim/CoreData.h"

#include <vector>

namespace SimPT_Sim {

class Mesh;
class Sim;

/**
 * Equations for reaction and transport processes. Two equations per chemical
 * for each wall, and one equation per chemical for each cell. This is for generality.
 * For a specific model you may optimize this by removing superfluous (empty) equations.
 * Layout of derivatives is as follows:
 * cells[chem1...chem n] walls[[w1(chem 1)...w1(chem n)][w2(chem 1)...w2(chem n)]]
 */
class TransportEquations
{
public:
	/// Initializing constructor.
	TransportEquations(const CoreData& cd);

	/// Interact with the mesh to calculate values of derivatives,
	void GetDerivatives(std::vector<double> &derivs) const;

	/// Two equations per chemical for each walls, and one equation per chemical
	/// for each cell. This is for generality. For a specific model you may optimize
	/// this by removing superfluous (empty) equations.
	int GetEquationCount() const;

	/// Interact with the mesh to get values of transport variables.
	void GetVariables(std::vector<double> &values) const;

	/// Straight initialization or re-initialization.
	void Initialize(const CoreData& cd);

	/// Interact with mesh to set values for transport variables.
	void SetVariables(std::vector<double> const &values) const;

private:
	CoreData                          m_cd;                    ///< Core data (mesh, params, sim_time,...).
	CellChemistryComponent            m_cell_chemistry;
	int                               m_effective_nchem;
	int 			          m_equation_count;
	Mesh* 			          m_mesh;                  ///< Bare pointer to mesh for convenience.
	CellToCellTransportComponent      m_cell2cell_transport;
	WallChemistryComponent            m_wall_chemistry;
};

} // namespace

#endif // end-of-include-guard
