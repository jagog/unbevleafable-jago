#ifndef PTREE2HDF5_H_INCLUDED
#define PTREE2HDF5_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Header for ptree2hdf5.
 */

// Code only available on platforms with libhdf5
#ifdef USE_HDF5

#include <boost/property_tree/ptree.hpp>
#include <hdf5.h>

namespace SimPT_Shell {

/**
 * Write the contents of a boost property tree to a HDF5 group
 */
void ptree2hdf5(const boost::property_tree::ptree& pt, hid_t h5);

/**
 * Return the ptree that represents the groups of attributes in HDF5
 */
boost::property_tree::ptree hdf52ptree(hid_t h5);

} // namespace

#endif /* USE_HDF5 */

#endif // end_of_include_guard

