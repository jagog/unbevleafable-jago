#ifndef HDF5FILE_H_INCLUDED
#define HDF5FILE_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for VirtualLeaf::Hdf5File.
 */

#include "sim/SimState.h"

#ifdef USE_HDF5
#include <list>
#include <string>
#include <vector>
#include <hdf5.h>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Sim {
	class Sim;
}

namespace SimPT_Shell {

/// Contains facilities for storing the simulation Mesh to HDF5 files.
class Hdf5File
{
public:
	/// Creates an empty HDF5 mesh exporter with no file association. In order to
	/// create a new HDF5 file a call to HDF5MeshFile::Open is necessary.
	Hdf5File();

	/// Creates a HDF5 mesh a with a file association, provided the HDF5 file has
	/// been successfully opened (IsOpen checks this). Otherwise the exporter is empty.
	/// @param[in] file_path        path name of the HDF5 file used for storage.
	Hdf5File(const std::string& file_path);

	/// Cleans up HDF5 storage: makes sure files are closed properly.
	virtual ~Hdf5File();

	/// Closes the HDF5 file associated with exporter, dissociates it from this
	/// exporter object and returns true. Return false if no open file was associated
	/// with exporter object.
	/// @return 	Status of operation.
	bool Close();

	/// Return file extension for HDF5 format
	static std::string GetFileExtension();

	/// Return file path for HDF5 file associated with the exporter object, if any.
	std::string GetFilePath();

	/// To avoid littering the code with preprocessor conditionals
	static bool HDF5Available() { return true; }

	/// Returns true if exporter is associated with a HDF5 file that has
	/// been successfully opened.
	bool IsOpen();

	/// Creates an HDF5 file and binds the exporter object to it. If the exporter is
	/// currently associated with an open file, that file gets closed first.
	/// @param[in] file_path        path name of the HDF5 file used for storage.
	bool Open(const std::string& file_path);

	// TODO: document!
	SimPT_Sim::SimState Read(int step_idx = -1);

	/// Reads the time steps contained in the HDF5 file.
	/// @return 	Pairs of time step numbers and time stamps.
	std::vector<std::pair<int, double>> TimeSteps();

	/// Writes the mesh state to the HDF5 file currently associated with exporter object.
	/// Does nothing if no open file is currently associated with exporter object.
	/// @param[in] sim       The simulation instance whose mesh and state will be written to file
	void Write(std::shared_ptr<const SimPT_Sim::Sim> sim);

private:
	/// Reads boundary polygon nodes from file and fills up the MeshState, overwriting previous data
	void ReadBPNodes(hid_t loc_id, SimPT_Sim::MeshState& mstate);

	/// Reads boundary polygon walls from file and fills up the MeshState, overwriting previous data
	void ReadBPWalls(hid_t loc_id, SimPT_Sim::MeshState& mstate);

	/// Reads cell information from file and fills up the MeshState, overwriting previous data
	void ReadCells(hid_t loc_id, SimPT_Sim::MeshState& mstate);

	/// Reads node information from file and fills up the MeshState, overwriting previous data
	void ReadNodes(hid_t loc_id, SimPT_Sim::MeshState& mstate);

	/// Reads parameters from file and returns them in a ptree
	boost::property_tree::ptree ReadParameters(hid_t loc_id);

	/// Reads the whole ptree as a serialized XML version of the ptree
	/// from an attribute of 'loc_id' called 'attr_name'
	boost::property_tree::ptree ReadPtreeFromStringAttribute(hid_t loc_id, const std::string& attr_name);

	/// Reads wall information from file and fills up the MeshState, overwriting previous data
	void ReadWalls(hid_t loc_id, SimPT_Sim::MeshState& mstate);

	void WriteBPNodes(hid_t loc_id, const SimPT_Sim::MeshState& mstate);

	void WriteBPWalls(hid_t loc_id, const SimPT_Sim::MeshState& mstate);

	/// Writes Cell information as data sets in a HDF5 file or group.
	/// @param[in,out] loc_id  The file or group id used to locate where the relevant data sets
	///                        will be created. Most probably this will be the id of the '/step_n'
	///                        group we're currently saving to.
	/// @param[in] mesh        The mesh that contains cell information to be written to HDF5.
	/// @note
	/// This method is called by HDF5MeshFile::WriteMesh as part of writing
	/// the Mesh to a HDF5 file. It should not be used on its own.
	void WriteCells(hid_t loc_id, const SimPT_Sim::MeshState& mstate);

	/// Writes tissue information as data sets in a HDF5 file or group.
	/// @param[in,out] loc_id  The file or group id used to locate where the relevant datasets
	///                        will be created. Most probably this will be the id of the '/step_n'
	///                        group we're currently saving to.
	/// @param[in] mesh        The mesh that contains node/cell/wall information to be written to HDF5.
	/// @note
	/// This method is called by HDF5MeshFile::WriteState as part of writing
	/// the Mesh to a HDF5 file. As such, it should not be used on its own.
	void WriteMesh(hid_t loc_id, const SimPT_Sim::MeshState& mstate);

	/// Writes Node information as data sets in a HDF5 file or group.
	/// @param[in,out] loc_id  The file or group id used to locate where the relevant datasets
	///                        will be created. Most probably this will be the id of the '/step_n'
	///                        group we're currently saving to.
	/// @param[in] mesh        The mesh that contains node information to be written to HDF5.
	/// @note
	/// This method is called by HDF5MeshFile::WriteMesh as part of writing
	/// the Mesh to a HDF5 file. It should not be used on its own.
	void WriteNodes(hid_t loc_id, const SimPT_Sim::MeshState& mstate);

	/// Writes simulation parameters contained in params to a structure of HDF5 subtrees with attributes.
	/// @param[in,out] loc_id  The file or group id used to locate where the relevant datasets
	///                        will be created. Most probably this will be the id of the '/step_n'
	///                        group we're currently saving to.
	/// @param[in] params      The parameter property tree to save.
	/// This method is called by HDF5MeshFile::WriteState as part of writing
	/// the Mesh to a HDF5 file. As such, it should not be used on its own.
	void WriteParameters(hid_t loc_id, const boost::property_tree::ptree& params);

	/// Writes the provided ptree as a serialized XML version to an attribute of 'loc_id' called attr_name
	/// @param[in,out] loc_id  The file or group id used to locate where the attribute
	///                        will be created. Most probably this will be the id of the '/step_n'
	///                        group we're currently saving to.
	/// @param[in] attr_name   How the attribute should be called.
	/// @param[in] pt          The property tree to save in serialized XML version.
	/// This method is called by HDF5MeshFile::WriteState as part of writing
	/// the Mesh to a HDF5 file. As such, it should not be used on its own.
	void WritePtreeToStringAttribute(hid_t loc_id, const std::string& attr_name, boost::property_tree::ptree const& pt);

	/// Writes simulation state information as data sets in a HDF5 file or group.
	/// @param[in] SimState    The simulation state to be written to HDF5.
	/// @note
	/// This method is called by HDF5MeshFile::Write as part of writing
	/// the Mesh to a HDF5 file. As such, it should not be used on its own.
	void WriteState(const SimPT_Sim::SimState& s);

	/// Writes Wall information as data sets in a HDF5 file or group.
	/// @param[in,out] loc_id  The file or group id used to locate where the relevant datasets
	///                        will be created. Most probably this will be the id of the '/step_n'
	///                        group we're currently saving to.
	/// @param[in] mesh        The mesh that contains wall information to be written to HDF5.
	/// @note
	/// This method is called by HDF5MeshFile::WriteMesh as part of writing
	/// the Mesh to a HDF5 file. As such, it should not be used on its own.
	void WriteWalls(hid_t loc_id, const SimPT_Sim::MeshState& mstate);

private:
	hid_t           m_file_id;              ///< ID of the HDF5 file as used by libhdf5
	std::string     m_file_path;            ///< Path name of the HDF5 file used as storage
};

} // end namespace VirtualLeaf

// Fallback code in case libhdf5 is not available on this platform
#else

#include "bio/Mesh.h"
#include <iostream>
#include <string>
#include <cpp_logging/logger.h>

#pragma GCC diagnostic ignored "-Wunused-parameter"

namespace SimPT_Sim {
	class Sim;
}

namespace SimPT_Shell {

class Hdf5File
{
public:
	Hdf5File()
	{
		SimPT_Logging::SimLogger::get()->warn("{} Warning: HDF5 must be enabled at compile time.", LOG_TAG_9);
	}
	Hdf5File(std::string const&)
	{
		SimPT_Logging::SimLogger::get()->warn("{} Warning: HDF5 must be enabled at compile time.", LOG_TAG_9)
	}
	virtual ~Hdf5File() {}

	bool Close() { return true; }

	std::string static GetFileExtension() {return "h5";}

	std::string GetFilePath() {return "";}

	static bool HDF5Available() { return false; }

	bool IsOpen() { return true; }

	bool Open(const std::string& ) { return true; }

	void Write(std::shared_ptr<const SimPT_Sim::Sim> ) {}

	SimPT_Sim::SimState Read(int step = -1) {return SimPT_Sim::SimState(); }

	std::vector<std::pair<int, double>> TimeSteps() { return {}; }
};

} // namespace

#endif /* USE_HDF5 */

#endif // end_of_include guard

