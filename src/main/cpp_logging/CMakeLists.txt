#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Build & install library; sources gathered from sub dirs.
#============================================================================
set( LIB  simPTlib_logging )
set( SOURCES
#---------	
	logger.cpp;
)

#----------------------------- build ----------------------------------------
add_library( ${LIB} ${SOURCES} )

target_link_libraries( ${LIB} ${Boost_SYSTEM_LIBRARY} ${Boost_FILESYSTEM_LIBRARY})


if( APPLE )
    if( NOT ${CMAKE_PROJECT_NAME}_MAKE_PACKAGE )
	    set_target_properties( ${LIB}  PROPERTIES INSTALL_NAME_DIR  "@rpath" )
    else()
        set_target_properties( ${LIB}  PROPERTIES INSTALL_NAME_DIR  "@loader_path" )
    endif()
endif( APPLE )		


#------------------------------ install -------------------------------------
if (NOT ${CMAKE_PROJECT_NAME}_MAKE_PACKAGE)
	install( TARGETS ${LIB}  DESTINATION  ${LIB_INSTALL_LOCATION} )
else()
	package_target_install( ${LIB} )
endif()

#-------------------------- unset variables ---------------------------------
unset( SOURCES )
unset( LIB     )

#############################################################################
