/*
 * File holding all the logtag macros defined and used in logging
 * We define them all here so we can ensure unique naming as well as
 * a single location to modify a filename if that is desired
 *
 * If no more changes happen in the naming of files this system is
 * good enough to do the logging with seperate tags to show in which
 * class an error originated
 */

#define LOG_TAG_1  "[SimPT_Shell::CliController]"
#define LOG_TAG_2  "[SimPT_Shell::CoupledCliController]"
#define LOG_TAG_3  "[SimPT_Parex::ExplorationStatusOverview]"
#define LOG_TAG_4  "[SimPT_Parex::SendPage]"
#define LOG_TAG_5  "[SimPT_Parex::ExplorationSubscriptionManager]"
#define LOG_TAG_6  "[SimPT_Parex::ClientHandler]"
#define LOG_TAG_7  "[SimPT_Parex::Status]"
#define LOG_TAG_8  "[SimPT_Parex::Client]"
#define LOG_TAG_9  "[SimPT_Sim::Hdf5File]"
#define LOG_TAG_10 "[SimPT_Sim::ptree2hdf5]"
#define LOG_TAG_11 "[SimPT_Sim::Mesh]"
#define LOG_TAG_12 "[SimPT_Execs::sim_cli]"
#define LOG_TAG_13 "[SimPT_Execs::mode_manager]"
#define LOG_TAG_14 "[SimPT_Simptshell::LogViewer]"
#define LOG_TAG_15 "[SimPT_Simshell::ViewerNode]"
#define LOG_TAG_16 "[SimPT_Execs::parex_client]"
#define LOG_TAG_17 "[SimPT_Parex::SecureShell]"
#define LOG_TAG_18 "[SimPT_Parex::WorkerNode]"
#define LOG_TAG_19 "[SimPT_Parex::ServerClientProtocol]"
#define LOG_TAG_20 "[SimPT_Parex::ExplorationManager]"
#define LOG_TAG_21 "[SimPT_Parex::ServerOverview]"
#define LOG_TAG_22 "[SimPT_Parex::WorkerPool]"
#define LOG_TAG_23 "[SimPT_Parex::Server]"
#define LOG_TAG_24 "[SimPT_Parex::WorkerRepresentative]"
#define LOG_TAG_25 "[SimPT_Parex::Simulator]"
#define LOG_TAG_26 "[SimPT_Execs::simPT_editor]"
#define LOG_TAG_27 "[SimPT_Simshell::PTreeComparison]"
#define LOG_TAG_28 "[SimPT_Simshell::SessionController]"
#define LOG_TAG_29 "[SimPT_Sim::PluginLoader]"
#define LOG_TAG_30 "[SimPT_Sim::Hdf5File]"
#define LOG_TAG_31 "[SimPT_Parex::NodeAdvertiser]"
