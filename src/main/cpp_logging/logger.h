#ifndef LOGGER_H
#define LOGGER_H

#include <spdlog/spdlog.h>
#include "logtags.h"
#include <stdio.h>
#include <cpp_sim/util/misc/InstallDirs.h>
#include <memory>
#include <string>
#include <fstream>
#include <sstream>
#include <boost/filesystem.hpp>

namespace SimPT_Logging {

/*
	Class intended as an abstract base class. Used to create your own logger specific to
	an environment.
*/
class Logger
{
protected:
	/*
		Utility method to register a named logger. Reused by all child logger classes.

		@param name The name of the logger (should be unique)
		@return std::shared_ptr<spdlog::logger> A shared pointer to the logger. 
	*/
	static std::shared_ptr<spdlog::logger> getNamed(std::string name){
		auto logger = spdlog::get(name);
		if(logger == nullptr){
			std::string workspace = SimPT_Sim::Util::InstallDirs::GetWorkspaceDir();
			boost::filesystem::path logging_dir(workspace + "/" + "logging");
			// Workaround for a boost beta bug
			const boost::filesystem::path* dir_copy = new boost::filesystem::path(logging_dir);
			// Check if the logging directory exists
			if(not boost::filesystem::exists(*dir_copy)){
				// Create the directory
				boost::filesystem::create_directory(logging_dir);
			}

			// Get the file paths for the logging destination file
			// We have a file specific to the current logging env and a combined logger that shows all logs
			std::string specific_log_path = logging_dir.string() + "/" + "combined_logs";
			std::string combined_log_path = logging_dir.string() + "/" + name + "_logs";
			
			// Register a stdout sink and both file sinks
			std::vector<spdlog::sink_ptr> sinks;
	        sinks.push_back(std::make_shared<spdlog::sinks::stdout_sink_st>());
	        // File sinks create a new file every 24h
	        sinks.push_back(std::make_shared<spdlog::sinks::daily_file_sink_st>(combined_log_path, "txt", 23, 59));
	        sinks.push_back(std::make_shared<spdlog::sinks::daily_file_sink_st>(specific_log_path, "txt", 23, 59));
	        // Create the combined logger with all 3 sinks
	        logger = std::make_shared<spdlog::logger>(name, begin(sinks), end(sinks));

	        // Get the logging level from the config file
			logger->set_level(getSettings(name));
	        spdlog::register_logger(logger);
		}
		return logger;
	}

	/* data */

private:
	Logger(){};
	~Logger(){};

	/*
		Helper method to get the logging level for the specified environment.

		@param name The name of the current logging environment.
		@return The logging level.
	*/
	static spdlog::level::level_enum getSettings(std::string name){
		std::string workspace = SimPT_Sim::Util::InstallDirs::GetWorkspaceDir();
		std::string setting_file_path = workspace + "/" + "logging.ini";
		// open the file
		std::ifstream setting_file(setting_file_path);
		std::stringstream contents;
		// Set the default file values (in case the file doesn't exists)
		contents << "parex=0" 		<< std::endl <<
					"sim=0" 		<< std::endl <<
					"simptshell=0" 	<< std::endl <<
					"simshell=0" 	<< std::endl <<
					"execs=0" 		<< std::endl <<
					"models=0" 		<< std::endl;

		// Check if file exists
		if(setting_file.good()){
			// Clear the defaults
			contents.str(std::string());
			// Load the file as string
			contents << setting_file.rdbuf();
			setting_file.close();
		}else{
			// Save the defaults to a new file
			std::ofstream temp(setting_file_path);
			temp << contents.str();
			temp.close();
		}

		// Loop over every line in the file
		std::string buffer;
		while(std::getline(contents, buffer, '\n')){
			// Find the '=' char in the line to split on
			size_t eq_pos = buffer.find("=");
			if(eq_pos != std::string::npos){
				// if found get the name as a substring
				std::string comp = buffer.substr(0, eq_pos);
				// If the name is a match with the one we're looking for and there is a char behind the '='
				if(comp == name and buffer.size() > eq_pos + 1){
					// Convert the char after the '=' to an int and convert that to the corresponding
					// logging level enum
					const char* level_as_str = buffer.c_str() + eq_pos + 1;
					return intToEnum(std::atoi(level_as_str));
				}
			}
		}
		// If anything went wrong log everything
		return spdlog::level::trace;
	}

	/*
		Logging levels are stored as an integer inside config file. This method translates
		the ints to their respective logging level enums.

		@param level The found logging level int.
		@param default_val The logging level that should be returned when no valid level is found.
		
		@return The logging level enum.
	*/
	static spdlog::level::level_enum intToEnum(int level, spdlog::level::level_enum default_val = spdlog::level::trace){
	    switch(level){
		    case 0: return spdlog::level::trace;
		    case 1: return spdlog::level::debug;
		    case 2: return spdlog::level::info;
		    case 3: return spdlog::level::notice;
		    case 4: return spdlog::level::warn;
		    case 5: return spdlog::level::err;
		    case 6: return spdlog::level::critical;
		    case 7: return spdlog::level::alert;
		    case 8: return spdlog::level::emerg;
		    case 9: return spdlog::level::off;
		    default: break;
	    }
	    return default_val;
	}
};

// trace = 0,
// debug = 1,
// info = 2,
// notice = 3,
// warn = 4,
// err = 5,
// critical = 6,
// alert = 7,
// emerg = 8,
// off = 9

class ParexLogger : public Logger {
public:
	static std::shared_ptr<spdlog::logger> get(){
		auto logger = Logger::getNamed("parex");
		return logger;
	}
};

class SimLogger : public Logger {
public:
	static std::shared_ptr<spdlog::logger> get(){
		auto logger = Logger::getNamed("sim");
		return logger;
	}
};

class SimptshellLogger : public Logger {
public:
	static std::shared_ptr<spdlog::logger> get(){
		auto logger = Logger::getNamed("simptshell");
		return logger;
	}
};

class SimshellLogger : public Logger {
public:
	static std::shared_ptr<spdlog::logger> get(){
		auto logger = Logger::getNamed("simshell");
		return logger;
	}
};

class ExecsLogger : public Logger {
public:
	static std::shared_ptr<spdlog::logger> get(){
		auto logger = Logger::getNamed("execs");
		return logger;
	}
};

}
#endif
