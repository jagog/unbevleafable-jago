beautifulsoup4==4.4.1
Cython==0.23.4
h5py==2.5.0
lxml==3.6.0
numpy==1.10.4
PyPTS==0.2.5
scipy==0.17.0
six==1.10.0
