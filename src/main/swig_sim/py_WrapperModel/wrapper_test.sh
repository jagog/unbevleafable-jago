#!/bin/sh
#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
set +x

CMAKE_INSTALL_PREFIX=../..

#----------------------------------------------------
# Set up virtual environment for python
#----------------------------------------------------
cd $CMAKE_INSTALL_PREFIX/tests/data/py_WrapperModel_data
virtualenv pypts
source pypts/bin/activate
pip install -r requirements.txt

#----------------------------------------------------
# Execute the simulations
#----------------------------------------------------
$CMAKE_INSTALL_PREFIX/bin/run_coupled_sim.py \
	--leaf $CMAKE_INSTALL_PREFIX/data/py_WrapperModel_data/leaf.h5 \
	--root $CMAKE_INSTALL_PREFIX/data/simPT_Default_workspace/WrapperModel/leaf_000000.xml \
	--out coupled --num_steps 1 --blind_mode

#----------------------------------------------------
# Cleanup
#----------------------------------------------------
deactivate
rm -rf pypts

#############################################################################