# SimPT - Python coupled simulations

Make sure SimPT is compiled with `SIMPT_MAKE_PYTHON_WRAPPER=ON` and that the
`PYTHONPATH` environment variable includes the location of `simPT.py` and
`_simPT.so` files (which is most probably `{PATH TO SIMPT}/bin`).

## Requirements

- Python 2.7
- The requirements listed in the file requirements.txt in the py_WrapperModel_data directory.

See also `src/main/swig_sim/py_WrapperModel/requirements.txt` for a full list
for automated installation with:

	pip install -r requirements.txt

## Running the code

From within the `bin` subdirectory run the following command:

	./run_coupled_sim.py --leaf ../data/py_WrapperModel_data/leaf.h5                         \
						 --root ../data/simPT_Default_workspace/WrapperModel/leaf_000000.xml \
					     --out output                                                       \
						 --num_steps 100 [--blind_mode]

to perform a coupled simulation of 100 steps using the provided tissue geometry
description files for the root and the leaf. The "blind mode" can be triggered
to disable live plotting with Matplotlib.

The output will be written to two separate files that represent, repsectively
the leaf and the root: `output_simpt.h5` and `output_pypts.h5`.

During the coupled simulation run a figure with simulation results is shown and
a detailed description of the process is shown on the terminal:

    INFO - tissue.py:186 - Loading tissue from file 'leaf.h5'
    INFO - tissue.py:219 - Using tissue data from '/step_28'
    INFO - tissue.py:1520 - Boundary polygon nodes & walls updated
    INFO - run_coupled_sim.py:185 - Number of equations: 276
    INFO - tissue.py:1533 - Saving tissue to file 'out_pypts.h5' as '/step_0'
    INFO - run_coupled_sim.py:62 - Running SimPT simulation step
    INFO - tissue.py:1520 - Boundary polygon nodes & walls updated
    INFO - run_coupled_sim.py:96 - Transfer values SimPT -> PyPTS
    INFO - run_coupled_sim.py:105 - Running PyPTS simulation step
    INFO - run_coupled_sim.py:50 - t = 0.1, |du/dt| = 3592.28584676
    INFO - run_coupled_sim.py:50 - t = 0.2, |du/dt| = 3338.63051113
    INFO - run_coupled_sim.py:50 - t = 0.3, |du/dt| = 3230.74674363
    INFO - run_coupled_sim.py:50 - t = 0.4, |du/dt| = 3172.24366071
    INFO - run_coupled_sim.py:50 - t = 0.5, |du/dt| = 3134.32221629
    INFO - run_coupled_sim.py:50 - t = 0.6, |du/dt| = 3106.62098231
    INFO - run_coupled_sim.py:50 - t = 0.7, |du/dt| = 3084.71954611
    INFO - run_coupled_sim.py:50 - t = 0.8, |du/dt| = 3066.44574624
    INFO - run_coupled_sim.py:50 - t = 0.9, |du/dt| = 3050.60437669
    INFO - run_coupled_sim.py:50 - t = 1.0, |du/dt| = 3036.47750807
    INFO - run_coupled_sim.py:50 - t = 1.1, |du/dt| = 3023.60435445
    INFO - run_coupled_sim.py:113 - Transfer values PyPTS -> SimPT
    INFO - tissue.py:1533 - Saving tissue to file 'out_pypts.h5' as '/step_1'
    INFO - tissue.py:1533 - Saving tissue to file 'out_simpt.h5' as '/step_1'
    INFO - run_coupled_sim.py:62 - Running SimPT simulation step
    INFO - tissue.py:1520 - Boundary polygon nodes & walls updated
    INFO - run_coupled_sim.py:96 - Transfer values SimPT -> PyPTS
    INFO - run_coupled_sim.py:105 - Running PyPTS simulation step
    INFO - run_coupled_sim.py:50 - t = 0.1, |du/dt| = 3234.33893513
    INFO - run_coupled_sim.py:50 - t = 0.2, |du/dt| = 3125.54936894
    INFO - run_coupled_sim.py:50 - t = 0.3, |du/dt| = 3073.55480871
    INFO - run_coupled_sim.py:50 - t = 0.4, |du/dt| = 3041.21812376
    ...

