#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

import unittest
import simPT

class UnitMeshState(unittest.TestCase):
    def setUp(self):
        self.mesh = simPT.MeshState()
        
    def testNodes(self):
        id = [5, 3, 2]
        x = [1.0, 2.0, 3.0]
        y = [9.0, 8.0, 7.0]
        
        self.mesh.SetNumNodes(len(id))
        self.mesh.SetNodesID(id)
        self.mesh.SetNodesX(x)
        self.mesh.SetNodesY(y)
        
        self.assertEqual(self.mesh.GetNumNodes(), len(id))
        self.assertSequenceEqual(self.mesh.GetNodesID(), id)
        self.assertSequenceEqual(self.mesh.GetNodesX(), x)
        self.assertSequenceEqual(self.mesh.GetNodesY(), y)
        
        for i in range(self.mesh.GetNumNodes()):
            self.assertEqual(self.mesh.GetNodeID(i), id[i])
            self.assertEqual(self.mesh.GetNodeX(i), x[i])
            self.assertEqual(self.mesh.GetNodeY(i), y[i])
            
    def testCells(self):
        nodes = [1, 2, 3]
        walls = [7, 8, 9]
        
        self.mesh.SetNumCells(1)
        self.mesh.SetCellNodes(0, nodes)
        self.mesh.SetCellWalls(0, walls)
        
        self.mesh.SetBoundaryPolygonNodes(nodes)
        self.mesh.SetBoundaryPolygonWalls(walls)
        
        self.assertEqual(self.mesh.GetNumCells(), 1)
        self.assertSequenceEqual(self.mesh.GetCellNodes(0), nodes)
        self.assertSequenceEqual(self.mesh.GetCellWalls(0), walls)
        
        self.assertSequenceEqual(self.mesh.GetBoundaryPolygonNodes(), nodes)
        self.assertSequenceEqual(self.mesh.GetBoundaryPolygonWalls(), walls)
        
    def testWalls(self):
        nodes = (1, 2)
        cells = (8, 9)
        
        self.mesh.SetNumWalls(1)
        self.mesh.SetWallNodes(0, nodes)
        self.mesh.SetWallCells(0, cells)
        
        self.assertEqual(self.mesh.GetNumWalls(), 1)
        self.assertSequenceEqual(self.mesh.GetWallNodes(0), nodes)
        self.assertSequenceEqual(self.mesh.GetWallCells(0), cells)
        
    def testNodeAttributes(self):
        self.mesh.SetNumNodes(1)
        
        self.mesh.NodeAttributeContainer().AddInt("i")
        self.mesh.NodeAttributeContainer().AddDouble("d")
        self.mesh.NodeAttributeContainer().AddString("s")
        
        self.assertTrue(self.mesh.NodeAttributeContainer().IsNameInt("i"))
        self.assertTrue(self.mesh.NodeAttributeContainer().IsNameDouble("d"))
        self.assertTrue(self.mesh.NodeAttributeContainer().IsNameString("s"))
        
        self.assertSequenceEqual(self.mesh.NodeAttributeContainer().GetNamesInt(), ["i"])
        self.assertSequenceEqual(self.mesh.NodeAttributeContainer().GetNamesDouble(), ["d"])
        self.assertSequenceEqual(self.mesh.NodeAttributeContainer().GetNamesString(), ["s"])
        
        self.mesh.NodeAttributeContainer().SetInt(0, "i", 5)
        self.mesh.NodeAttributeContainer().SetDouble(0, "d", 2.0)
        self.mesh.NodeAttributeContainer().SetString(0, "s", "test")
        
        self.assertEqual(self.mesh.NodeAttributeContainer().GetInt(0, "i"), 5)
        self.assertEqual(self.mesh.NodeAttributeContainer().GetDouble(0, "d"), 2.0)
        self.assertEqual(self.mesh.NodeAttributeContainer().GetString(0, "s"), "test")
        
        self.mesh.NodeAttributeContainer().SetAllInt("i", [5])
        self.mesh.NodeAttributeContainer().SetAllDouble("d", [2.0])
        self.mesh.NodeAttributeContainer().SetAllString("s", ["test"])
        
        self.assertSequenceEqual(self.mesh.NodeAttributeContainer().GetAllInt("i"), [5])
        self.assertSequenceEqual(self.mesh.NodeAttributeContainer().GetAllDouble("d"), [2.0])
        self.assertSequenceEqual(self.mesh.NodeAttributeContainer().GetAllString("s"), ["test"])
        
    def testCellAttributes(self):
        self.mesh.SetNumCells(1)
        
        self.mesh.CellAttributeContainer().AddInt("i")
        self.mesh.CellAttributeContainer().AddDouble("d")
        self.mesh.CellAttributeContainer().AddString("s")
        
        self.assertTrue(self.mesh.CellAttributeContainer().IsNameInt("i"))
        self.assertTrue(self.mesh.CellAttributeContainer().IsNameDouble("d"))
        self.assertTrue(self.mesh.CellAttributeContainer().IsNameString("s"))
        
        self.assertSequenceEqual(self.mesh.CellAttributeContainer().GetNamesInt(), ["i"])
        self.assertSequenceEqual(self.mesh.CellAttributeContainer().GetNamesDouble(), ["d"])
        self.assertSequenceEqual(self.mesh.CellAttributeContainer().GetNamesString(), ["s"])
        
        self.mesh.CellAttributeContainer().SetInt(0, "i", 5)
        self.mesh.CellAttributeContainer().SetDouble(0, "d", 2.0)
        self.mesh.CellAttributeContainer().SetString(0, "s", "test")
        
        self.assertEqual(self.mesh.CellAttributeContainer().GetInt(0, "i"), 5)
        self.assertEqual(self.mesh.CellAttributeContainer().GetDouble(0, "d"), 2.0)
        self.assertEqual(self.mesh.CellAttributeContainer().GetString(0, "s"), "test")
        
        self.mesh.CellAttributeContainer().SetAllInt("i", [5])
        self.mesh.CellAttributeContainer().SetAllDouble("d", [2.0])
        self.mesh.CellAttributeContainer().SetAllString("s", ["test"])
        
        self.assertSequenceEqual(self.mesh.CellAttributeContainer().GetAllInt("i"), [5])
        self.assertSequenceEqual(self.mesh.CellAttributeContainer().GetAllDouble("d"), [2.0])
        self.assertSequenceEqual(self.mesh.CellAttributeContainer().GetAllString("s"), ["test"])
        
    def testWallAttributes(self):
        self.mesh.SetNumWalls(1)
        
        self.mesh.WallAttributeContainer().AddInt("i")
        self.mesh.WallAttributeContainer().AddDouble("d")
        self.mesh.WallAttributeContainer().AddString("s")
        
        self.assertTrue(self.mesh.WallAttributeContainer().IsNameInt("i"))
        self.assertTrue(self.mesh.WallAttributeContainer().IsNameDouble("d"))
        self.assertTrue(self.mesh.WallAttributeContainer().IsNameString("s"))
        
        self.assertSequenceEqual(self.mesh.WallAttributeContainer().GetNamesInt(), ["i"])
        self.assertSequenceEqual(self.mesh.WallAttributeContainer().GetNamesDouble(), ["d"])
        self.assertSequenceEqual(self.mesh.WallAttributeContainer().GetNamesString(), ["s"])
        
        self.mesh.WallAttributeContainer().SetInt(0, "i", 5)
        self.mesh.WallAttributeContainer().SetDouble(0, "d", 2.0)
        self.mesh.WallAttributeContainer().SetString(0, "s", "test")
        
        self.assertEqual(self.mesh.WallAttributeContainer().GetInt(0, "i"), 5)
        self.assertEqual(self.mesh.WallAttributeContainer().GetDouble(0, "d"), 2.0)
        self.assertEqual(self.mesh.WallAttributeContainer().GetString(0, "s"), "test")
        
        self.mesh.WallAttributeContainer().SetAllInt("i", [5])
        self.mesh.WallAttributeContainer().SetAllDouble("d", [2.0])
        self.mesh.WallAttributeContainer().SetAllString("s", ["test"])
        
        self.assertSequenceEqual(self.mesh.WallAttributeContainer().GetAllInt("i"), [5])
        self.assertSequenceEqual(self.mesh.WallAttributeContainer().GetAllDouble("d"), [2.0])
        self.assertSequenceEqual(self.mesh.WallAttributeContainer().GetAllString("s"), ["test"])
        
