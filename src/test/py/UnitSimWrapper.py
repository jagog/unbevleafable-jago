#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

import unittest
import simPT
from xml.etree import ElementTree

class UnitSimWrapper(unittest.TestCase):
    def setUp(self):
        self.sim  = simPT.SimWrapper()
        self.path = "../data/simPT_Default_workspace/"
        self.file = "leaf_000000.xml"

    def testTimeStep(self):
        r1 = self.sim.Initialize(self.path + "Geometric/" + self.file)
        self.assertTrue(r1.status == simPT.SUCCESS)
        for i in range(10):
            self.assertTrue(self.sim.TimeStep().status == simPT.SUCCESS)
        r2 = self.sim.GetState();
        self.assertTrue(r2.status == simPT.SUCCESS)
        self.assertEqual(r2.value.GetTimeStep(), 10)
        
    def testXMLState(self):
        r1 = self.sim.Initialize(self.path + "Geometric/" + self.file)
        self.assertTrue(r1.status == simPT.SUCCESS)
        for i in range(10):
            self.assertTrue(self.sim.TimeStep().status == simPT.SUCCESS)
        r2 = self.sim.GetXMLState()
        self.assertTrue(r2.status == simPT.SUCCESS)
        xml = ElementTree.fromstring(r2.value)
        self.assertEqual(xml.findtext("project"), "Geometric")
        self.assertEqual(xml.findtext("parameters/cell_mechanics/target_node_distance"), "3.09017")
