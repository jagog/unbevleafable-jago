#!/bin/sh
#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#------------------------------------------------------------------------
# Generates build configurations for use in Jenkins CI.
#------------------------------------------------------------------------

#------------------------------------------------------------------------
# All options OFF, Release
#------------------------------------------------------------------------
NAME=NO_ALL_Release
echo "\nConfig_$NAME in BuildConfig_$NAME.txt"
tee BuildConfig_$NAME.txt << ENDOFCONFIG_$NAME
CONFIG=CONFIG_$NAME
SIMPT_BUILD_TYPE=Release
SIMPT_FORCE_NO_HDF5=OFF
SIMPT_FORCE_NO_BOOST_GZIP=OFF
SIMPT_FORCE_NO_ZLIB=OFF
SIMPT_FORCE_NO_OPENMP=OOFF
ENDOFCONFIG_$NAME

#------------------------------------------------------------------------
# All options OFF, Debug
#------------------------------------------------------------------------
NAME=NO_ALL_Debug
echo "\nConfig_$NAME in BuildConfig_$NAME.txt"
tee BuildConfig_$NAME.txt << ENDOFCONFIG_$NAME
CONFIG=CONFIG_$NAME
SIMPT_BUILD_TYPE=Debug
SIMPT_FORCE_NO_HDF5=OFF
SIMPT_FORCE_NO_BOOST_GZIP=OFF
SIMPT_FORCE_NO_ZLIB=OFF
SIMPT_FORCE_NO_OPENMP=OFF
ENDOFCONFIG_$NAME

#------------------------------------------------------------------------
# NO_HDF5 ON, others OFF
#------------------------------------------------------------------------
NAME=NO_HDF5_Release
echo "\nConfig_$NAME in BuildConfig$NAME.txt"
tee BuildConfig_$NAME.txt << ENDOFCONFIG_$NAME
CONFIG=CONFIG_$NAME
SIMPT_BUILD_TYPE=Release
SIMPT_FORCE_NO_HDF5=ON  
SIMPT_FORCE_NO_BOOST_GZIP=OFF
SIMPT_FORCE_NO_ZLIB=OFF
SIMPT_FORCE_NO_OPENMP=OFF
ENDOFCONFIG_$NAME

#------------------------------------------------------------------------
# NO_BOOST_GZIP=ON ON, others OFF
#------------------------------------------------------------------------
NAME=NO_GZIP_Release
echo "\nConfig_$NAME in BuildConfig_$NAME.txt"
tee BuildConfig_$NAME.txt << ENDOFCONFIG_$NAME
CONFIG=CONFIG_$NAME
SIMPT_BUILD_TYPE=Release
SIMPT_FORCE_NO_HDF5=OFF  
SIMPT_FORCE_NO_BOOST_GZIP=ON
SIMPT_FORCE_NO_ZLIB=OFF
SIMPT_FORCE_NO_OPENMP=OFF
ENDOFCONFIG_$NAME

#------------------------------------------------------------------------
# NO_BOOST_GZIP=ON, NO_ZLIB=ON, others OFF
#------------------------------------------------------------------------
NAME=NO_GZIP_NO_ZLIB_Release
echo "\nConfig_$NAME in BuildConfig_$NAME.txt"
tee BuildConfig_$NAME.txt << ENDOFCONFIG_$NAME
CONFIG=CONFIG_$NAME
SIMPT_BUILD_TYPE=Release
SIMPT_FORCE_NO_HDF5=OFF  
SIMPT_FORCE_NO_BOOST_GZIP=ON
SIMPT_FORCE_NO_ZLIB=ON
SIMPT_FORCE_NO_OPENMP=OFF
ENDOFCONFIG_$NAME

#------------------------------------------------------------------------
# NO_OPENMP=ON, others OFF, Release
#------------------------------------------------------------------------
NAME=NO_OPENMP_Release
echo "\nConfig_$NAME in BuildConfig_$NAME.txt"
tee BuildConfig_$NAME.txt << ENDOFCONFIG_$NAME
CONFIG=CONFIG_$NAME
SIMPT_BUILD_TYPE=Release
SIMPT_FORCE_NO_HDF5=OFF
SIMPT_FORCE_NO_BOOST_GZIP=OFF
SIMPT_FORCE_NO_ZLIB=OFF
SIMPT_FORCE_NO_OPENMP=ON
ENDOFCONFIG_$NAME

#------------------------------------------------------------------------
# NO_OPENMP=ON, others OFF, Debug
#------------------------------------------------------------------------
NAME=NO_OPENMP_Debug
echo "\nConfig_$NAME in BuildConfig_$NAME.txt"
tee BuildConfig_$NAME.txt << ENDOFCONFIG_$NAME
CONFIG=CONFIG_$NAME
SIMPT_BUILD_TYPE=Debug
SIMPT_FORCE_NO_HDF5=OFF
SIMPT_FORCE_NO_BOOST_GZIP=OFF
SIMPT_FORCE_NO_ZLIB=OFF
SIMPT_FORCE_NO_OPENMP=ON
ENDOFCONFIG_$NAME

