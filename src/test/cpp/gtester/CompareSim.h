#ifndef COMPARE_SIM_H_INCLUDED
#define COMPARE_SIM_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Compare simulation data (hdf5, ptree and xml format).
 */

#include <string>
#include <boost/property_tree/ptree.hpp>

namespace SimPT_Shell {

using boost::property_tree::ptree;

/**
 * Methods to compare sims.
 */
class CompareSim
{
public:
	/**
	 * Test equality of sims in HDF5 files.
	 * Some values (such as run_date) are ignored.
	 * @param f1             Name of first file with hdf5 in it.
	 * @param f2             Name of second file with hdf5 in it.
	 * @param accept_diff    Largest tolerated difference for floats.
	 */
	static bool Hdf5Files(const std::string& f1, const std::string& f2, double accept_diff = 1.0e-4);

	/**
	 * Test if ptree's of sim objects are equal.
	 * Some values (such as run_date) are ignored.
	 * @param sim1               Sim 1.
	 * @param sim2               Sim 2.
	 * @param acceptable_diff    Largest tolerated difference in case of float values.
	 */
	static bool PTrees(const ptree& sim1, const ptree& sim2, double acceptable_diff = 1.0e-4);

	/**
	 * Test equality of sim ptree's in (possibly gzipped) XML files.
	 * Some values (such as run_date) are ignored.
	 * @param f1             First file with ptree in it.
	 * @param f2             Second file with ptree in it.
	 * @param accept_diff    Largest tolerated difference for floats.
	 */
	static bool XmlFiles(const std::string& f1, const std::string& f2, double accept_diff = 1.0e-4);
};

} // namespace


#endif // include-guard
