/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of UnitSimPtree.
 */

#include "CompareSim.h"

#include <cpp_sim/util/misc/InstallDirs.h>
#include "bio/Mesh.h"
#include "sim/Sim.h"
#include "util/misc/XmlWriterSettings.h"
#include "workspace/CliWorkspace.h"
#include "workspace/StartupFilePtree.h"

#include <boost/property_tree/xml_parser.hpp>
#include <gtest/gtest.h>
#include <QDir>
#include <memory>

namespace SimPT_Sim {
namespace Tests {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Shell;
using namespace SimPT_Sim::Util;

class UnitSimPtree: public ::testing::TestWithParam<string>
{
protected:
	virtual ~UnitSimPtree() {}

	virtual void SetUp()
	{
		m_sim             = make_shared<Sim>();
		m_output_prefix   = "UnitSimPtree_";
	}

	virtual void TearDown()
	{
	}

	string                       m_output_prefix;
	shared_ptr<Sim>  m_sim;
};

TEST_P( UnitSimPtree, PtreeToSimToPtree )
{
	// Assert that work space exists and define its workspace_model.
	QDir dir(QString::fromStdString(InstallDirs::GetDataDir()));
	ASSERT_EQ(true, dir.cd("simPT_Default_workspace"));
	Ws::CliWorkspace ws_model(dir.absolutePath().toStdString());

	// Prelim stuff.
	const string project = GetParam();

	// Assert that the sim data file exists and read it into ptree.
	const auto file_ptr = ws_model.Get(project)->Back();
	ASSERT_EQ(true, dir.exists(QString::fromStdString(file_ptr->GetPath())));
	auto expected_pt = static_pointer_cast<Ws::StartupFilePtree>(file_ptr)->ToPtree();

	// PTree to Sim and back to new ptree
	m_sim->Initialize(expected_pt);
	ptree actual_pt = m_sim->ToPtree();

	// Check equality.
	EXPECT_EQ(true, CompareSim::PTrees(expected_pt, actual_pt, 1.0e-4));
}

TEST_P( UnitSimPtree, XMLToSimToXML )
{
	// Assert that work space exists and define its workspace_model.
	QDir dir(QString::fromStdString(InstallDirs::GetDataDir()));
	ASSERT_EQ(true, dir.cd("simPT_Default_workspace"));
	Ws::CliWorkspace ws_model(dir.absolutePath().toStdString());

	// Prelim stuff.
	const string project = GetParam();

	// Assert that the sim data file exists and read it into ptree.
	const auto file_ptr = ws_model.Get(project)->Back();
	ASSERT_EQ(true, dir.exists(QString::fromStdString(file_ptr->GetPath())));
	auto expected_pt = static_pointer_cast<Ws::StartupFilePtree>(file_ptr)->ToPtree();

	// Write a copy of expected_pt in test output dir.
	const string expected_file = m_output_prefix + project + ".expected.xml";
	write_xml(expected_file, expected_pt, std::locale(), XmlWriterSettings::GetTab());

	// Ptree to Sim to xml file
	m_sim->Initialize(expected_pt);
	const string actual_file   = m_output_prefix + project + ".actual.xml";
	write_xml(actual_file, m_sim->ToPtree(), std::locale(), XmlWriterSettings::GetTab());

	// Check equality.
	EXPECT_TRUE(CompareSim::XmlFiles(expected_file, actual_file, 1.0e-4));
}

namespace {

string scenarios[] {
	"AuxinGrowth",
	"Geometric",
	"Meinhardt",
	"NoGrowth",
	"TipGrowth",
	"Wortel"
	};

}

INSTANTIATE_TEST_CASE_P( UnitSimPtree_a, UnitSimPtree, ::testing::ValuesIn(scenarios) );

} // namespace
} // namespace
