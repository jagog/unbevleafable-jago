/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of UnitPBMBuilder
 */

#include <cpp_sim/util/misc/InstallDirs.h>
#include "bio/Mesh.h"
#include "bio/PBMBuilder.h"
#include "ptree/PTreeComparison.h"
#include "sim/Sim.h"
#include "workspace/CliWorkspace.h"
#include "workspace/StartupFilePtree.h"

#include <QDir>
#include <QString>
#include <boost/property_tree/xml_parser.hpp>
#include <gtest/gtest.h>

#include <iostream>
#include <memory>
#include <string>

namespace SimPT_Sim {
namespace Tests {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using boost::optional;

using namespace SimPT_Shell;
using namespace SimPT_Shell::Ws;
using namespace SimPT_Sim::Util;

class UnitPBMBuilder: public ::testing::TestWithParam<string>
{
protected:
	virtual ~UnitPBMBuilder() {}

	virtual void SetUp()
	{
		m_output_prefix   = SimPT_Sim::Util::InstallDirs::GetDataDir() + "/MeshPtree_";
	}

	virtual void TearDown()
	{
	}

	string                     m_output_prefix;
};

TEST_P( UnitPBMBuilder, PtreeToMeshToPtree )
{
	// Assert that work space exists and define its workspace_model.
	string const data_dir = InstallDirs::GetDataDir();
	QDir dir(QString::fromStdString(data_dir));
	ASSERT_EQ(true, dir.cd("simPT_Default_workspace"));
	CliWorkspace ws_model(dir.absolutePath().toStdString());

	// Input data from xml file.
	const string project = GetParam();
	const auto project_it = ws_model.Find(project);
	const auto file_ptr = project_it->second->Back();
	ASSERT_EQ(true, dir.exists(QString::fromStdString(file_ptr->GetPath())));

	auto sim_pt = static_pointer_cast<StartupFilePtree>(file_ptr)->ToPtree();

	// Ptree to mesh with PBMBuilder.
	PBMBuilder build_director;
	shared_ptr<Mesh> mesh = build_director.Build(sim_pt.get_child("vleaf2.mesh"));

	// And from mesh back to ptree.
	ptree mesh_out_pt(mesh->ToPtree());

	// Define expected_pt.
	ptree expected_pt;
	expected_pt.put_child("mesh", sim_pt.get_child("vleaf2.mesh"));

	// Define actual_pt.
	ptree actual_pt;
	actual_pt.put_child("mesh", mesh_out_pt);

	// Un-comment if you want written output for verification.
	// xml_writer_settings<char> w('\t', 1);
	// const string expected_fn = m_output_prefix + project + ".expected.xml";
	// write_xml(expected_fn, expected_pt, std::locale(), w);
	// const string actual_fn = m_output_prefix + project + ".actual.xml";
	// write_xml(actual_fn, actual_pt, std::locale(), w);

	// Check equality.
	EXPECT_TRUE(PTreeComparison::CompareNonArray(expected_pt, actual_pt, 1.0e-5));
}

namespace {

string scenarios[] {
	"AuxinGrowth",
	"Geometric",
	"Meinhardt",
	"NoGrowth",
	"TipGrowth",
	"Wortel"
	};

}

INSTANTIATE_TEST_CASE_P(UnitPBMBuilder_a, UnitPBMBuilder, ::testing::ValuesIn(scenarios));

} // namespace
} // namespace
