/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of UnitWallType.
 */

#include "bio/WallType.h"
#include <gtest/gtest.h>
#include <string>

using namespace std;
using namespace SimPT_Sim::WallType;

namespace SimPT_Sim {
namespace Tests {

class UnitWallType: public ::testing::Test
{
protected:
	/// Set up for the test fixture.
	virtual void SetUp() {}

	/// Tearing down the test fixture.
	virtual void TearDown() {}
};

TEST_F( UnitWallType, Types )
{
	ASSERT_EQ( true, Type::AuxinSource  ==  FromString(ToString(Type::AuxinSource)) );
	ASSERT_EQ( true, Type::AuxinSink    ==  FromString(ToString(Type::AuxinSink)) );
	ASSERT_EQ( true, Type::Normal       ==  FromString(ToString(Type::Normal)) );
}

TEST_F( UnitWallType, Names )
{
	ASSERT_EQ( true, string("aux_source")  ==  ToString(FromString("aux_source")) );
	ASSERT_EQ( true, string("aux_sink")    ==  ToString(FromString("aux_sink")) );
	ASSERT_EQ( true, string("normal")      ==  ToString(FromString("normal")) );
}

} //namespace
} //namespace
