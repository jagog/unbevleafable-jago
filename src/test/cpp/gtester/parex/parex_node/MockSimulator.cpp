/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MockSimulator.
 */

#include "MockSimulator.h"

namespace SimPT_Parex {
namespace Tests {

MockSimulator::MockSimulator()
	: m_testSucceeded(false), m_currentTest(stests::acceptTcp)
{

}

MockSimulator::~MockSimulator()
{
}

void MockSimulator::SolveTask(const SimPT_Parex::SimTask&)
{
	SimResult r;
	emit TaskSolved(r);
	if (m_currentTest == stests::startSim) {
		m_testSucceeded = true;
		emit Done();
	}
}

bool MockSimulator::TestSucceeded()
{
	return m_testSucceeded;
}

} // namespace
} // namespace
