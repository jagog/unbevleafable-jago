#ifndef MOCK_SERVER_H_INCLUDED
#define MOCK_SERVER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MockServer.
 */

#include "parex_protocol/ServerNodeProtocol.h"

#include <QObject>
#include <QUdpSocket>
#include <QTcpSocket>

namespace SimPT_Parex {
namespace Tests {

enum stests
{
	broadcast, acceptTcp, startSim
};

/**
 * A dummy server used to test the node
 */
class MockServer: public QObject
{
	Q_OBJECT
public:
	///
	MockServer(QObject *parent = 0);

	///
	virtual ~MockServer();

	///
	bool SetUp();

	///
	bool TestSucceeded();

	///
	void setCurrentTest(stests t) { m_currentTest = t; }

signals:
	void Done();

private slots:
	void HandleDatagrams();
	void SucceedTest();
	void Connected();

private:
	QUdpSocket*            m_socket;
	bool                   m_testSucceeded;
	stests                 m_currentTest;
	ServerNodeProtocol*    m_protocol;
	QTcpSocket*            m_tsocket;
};

} // namespace
} // namespace

#endif // end-of-include-guard
