/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MockServer
 */

#include "MockServer.h"
#include "parex_server/QHostAnyAddress.h"

#include <QTcpSocket>

namespace SimPT_Parex {
namespace Tests {

MockServer::MockServer(QObject* parent)
	: QObject(parent), m_socket(new QUdpSocket(this)), m_testSucceeded(false),
	  m_currentTest(stests::broadcast), m_protocol(nullptr), m_tsocket(nullptr)
{
}

MockServer::~MockServer()
{
}

bool MockServer::SetUp()
{
	if (!m_socket->bind(QHostAnyAddress(), 45678, QUdpSocket::ShareAddress)) {
		return false;
	}

	if (!m_socket->joinMulticastGroup(QHostAddress("239.13.13.13"))) {
		return false;
	}
	connect(m_socket, SIGNAL(readyRead()), this, SLOT(HandleDatagrams()));
	return true;
}

bool MockServer::TestSucceeded()
{
	return m_testSucceeded;
}

void MockServer::HandleDatagrams()
{
	if (m_currentTest == stests::broadcast) {
		SucceedTest();
		return;
	}

	// We have to process all pending datagrams or the
	// readyRead() signal will not be emitted again.
	while (m_socket->hasPendingDatagrams()) {
		QByteArray datagram;
		datagram.resize(m_socket->pendingDatagramSize());

		// Don't know where the node is located, so check sender address of the UDP packet
		QHostAddress *senderAddr = new QHostAddress();
		m_socket->readDatagram(datagram.data(), datagram.size(), senderAddr);

		QString s(datagram);
		int port = 0;

		if (s.startsWith("Job Application")) {
			QString msg("Job Application");
			s.remove(0, msg.length() + 1); //remove the server message + " "
			port = s.toInt();
		}

		if (!port)
			return;

		m_tsocket = new QTcpSocket();
		connect(m_tsocket, SIGNAL(connected()), this, SLOT(Connected()));
		m_tsocket->connectToHost(*senderAddr, port);
	}
}

void MockServer::SucceedTest()
{
	m_testSucceeded = true;
	emit Done();
}

void MockServer::Connected()
{
	if (m_currentTest == stests::acceptTcp) {
		SucceedTest();
		return;
	}
	m_protocol = new ServerNodeProtocol(m_tsocket, this);
	SimTask task;
	m_protocol->SendTask(task);
}

} // namespace
} // namespace
