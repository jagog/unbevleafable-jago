#ifndef TEST_WORKER_H
#define TEST_WORKER_H

#include <QObject>
#include <QString>
#include <QThread>
#include <functional>

namespace SimPT_Parex {
namespace Tests {

class TestWorker : public QObject {
    Q_OBJECT
 
public:
    TestWorker(std::function<void(void)> payload) : m_payload(payload) {
    	thread = new QThread();
		this->moveToThread(thread);
		
		connect(this, SIGNAL(error(QString)), this, SLOT(errorString(QString)));
		connect(thread, SIGNAL(started()), this, SLOT(process()));
		connect(this, SIGNAL(finished()), thread, SLOT(quit()));
		connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
		connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

		thread->start();
    }

    ~TestWorker(){

    }
 
public slots:
    void process(){
    	// Do the work
    	m_payload();
    	// Signal the thread to end
    	//emit finished();
    	//printf("[TestWorker:process] Signaling thread to end\n");
    }

    void errorString(QString error){

    }
 
signals:
    void finished();
    void error(QString err);
 
private:
	std::function<void(void)> m_payload;
    QThread* thread;
};

}
}

#endif