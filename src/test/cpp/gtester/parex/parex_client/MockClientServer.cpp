/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation for MockClientServer.
 */

#include "MockClientServer.h"

#include "parex_protocol/Exploration.h"
#include "parex_server/QHostAnyAddress.h"
#include <cstdio>

namespace SimPT_Parex {
namespace Tests {

MockClientServer::MockClientServer(QObject* parent)
	: QObject(parent), m_testSucceeded(false), m_currentTest(stests::connect),
	  m_protocol(nullptr), m_tsocket(nullptr), m_server(nullptr)
{

}

MockClientServer::~MockClientServer()
{
	delete m_protocol;
	delete m_server;
}

bool MockClientServer::SetUp()
{
	m_server = new QTcpServer(this);

	// Port=0: Let Qt pick an empty port.
	if (!m_server->listen(QHostAnyAddress(), 0)) {
		return false;
	}

	connect(m_server, SIGNAL(newConnection()), this, SLOT(HandleConnection()));

	return true;
}

int MockClientServer::getPort()
{
	return m_server->serverPort();
}

void MockClientServer::HandleConnection()
{
	m_tsocket = m_server->nextPendingConnection();
	if (m_tsocket) {
		m_protocol = new ServerClientProtocol(m_tsocket);
		if (m_currentTest == stests::connect) {
			SucceedTest();
		}
		QObject::connect(m_protocol, SIGNAL(ExplorationReceived(const Exploration*, std::vector<int>, std::string)), this,
				SLOT(ExplorationReceived(const Exploration*, std::vector<int>, std::string)));
		QObject::connect(m_protocol, SIGNAL(Subscribe(std::string)),
			this, SLOT(SubscribeReceived(std::string)));
		QObject::connect(m_protocol, SIGNAL(Ping()), this, SLOT(PingReceived()));
		QObject::connect(m_protocol, SIGNAL(RequestExplorationStatus(const std::string& )),
						this, SLOT(RequestExplorationStatus(std::string)));
	}
	else
		emit Done();
}

void MockClientServer::ExplorationReceived(const Exploration* exploration, std::vector<int> task_list, std::string parking_ip)
{
	if (m_currentTest == stests::sendExploration && parking_ip == "mock_ip" && task_list.size() != 0)
		SucceedTest();
}

void MockClientServer::SubscribeReceived(std::string name)
{
	if (m_currentTest == stests::requestUpdates && name == "TestName")
		SucceedTest();
}

void MockClientServer::PingReceived()
{
	if (m_currentTest == stests::ping)
		SucceedTest();

}

void MockClientServer::RequestExplorationStatus(std::string name)
{
	if (m_currentTest == stests::GetExplorationProgress && name == "TestName")
		SucceedTest();
}

bool MockClientServer::TestSucceeded()
{
	return m_testSucceeded;
}

void MockClientServer::SucceedTest()
{
	m_testSucceeded = true;
	emit Done();
}
} // namespace
} // namespace
