#ifndef MOCK_CLIENT_SERVER_H_INCLUDED
#define MOCK_CLIENT_SERVER_H_INCLUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Interface for MockClientServer.
 */

#include "parex_protocol/ServerClientProtocol.h"

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

namespace SimPT_Parex {
namespace Tests {

enum stests
{
	connect, sendExploration, requestUpdates, ping, GetExplorationProgress
};

/**
 * A dummy server used to test the client
 */
class MockClientServer: public QObject
{
	Q_OBJECT
public:
	MockClientServer(QObject* parent = 0);

	virtual ~MockClientServer();

	bool SetUp();

	bool TestSucceeded();

	int getPort();

	void setCurrentTest(stests t) { m_currentTest = t; }

signals:
	void Done();

private slots:
	void HandleConnection();

	void SucceedTest();

	void ExplorationReceived(const Exploration* exploration, std::vector<int> task_list, std::string parking_ip);

	void SubscribeReceived(std::string name);

	void PingReceived();

	void RequestExplorationStatus(std::string name);

private:
	bool                      m_testSucceeded;
	stests                    m_currentTest;
	ServerClientProtocol*     m_protocol;
	QTcpSocket*               m_tsocket;
	QTcpServer*               m_server;
};

} // namespace
} // namespace

#endif // end-of-include-guard
