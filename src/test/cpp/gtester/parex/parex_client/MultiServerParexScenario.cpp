/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of test of parameter exploration client.
 */

#include "parex/parex_client/MockClientServer.h"

#include "parex_protocol/RangeSweep.h"
#include "parex_server/Server.h"
#include <cpp_sim/util/misc/InstallDirs.h>
#include "parex_protocol/ClientProtocol.h"
#include "parex_protocol/Exploration.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/ServerInfo.h"
#include "workspace/MergedPreferences.h"
#include "parex_server/QHostAnyAddress.h"

#include "workspace/StartupFilePtree.h"
#include "workspace/Workspace.h"
#include "workspace/WorkspaceFactory.h"

#include <gtest/gtest.h>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <QEventLoop>
#include <QTimer>
#include <QFile>
#include <vector>
#include <QDateTime>

namespace SimPT_Parex {
namespace Tests {

using SimPT_Sim::Util::InstallDirs;
using namespace SimPT_Shell;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

class MultiServerParexScenario : public testing::Test
{
protected:
	virtual void SetUp()
	{
		m_server_first = new Server(1, 0);
		m_server_second = new Server(1, 0);

		std::string Name1 = "Server1";
		std::string Name2 = "Server2";

		s1 = new ServerInfo(Name1, std::string("127.0.0.1"), m_server_first->serverPort());
		s2 = new ServerInfo(Name2, std::string("127.0.0.1"), m_server_second->serverPort());

		QTimer::singleShot(3000, &m_event_loop, SLOT(quit()));
		m_event_loop.exec();


		m_client_first  = new ClientProtocol(s1);
		m_client_second = new ClientProtocol(s2);
	}

	virtual void TearDown()
	{
		delete m_server_first;
		delete m_server_second;
		delete m_client_first;
		delete m_client_second;
		delete s1;
		delete s2;
		//Clean up
		try {
			QFile::remove("exploration_backup.xml");
		} catch (...) { }
	}

	// Data members of the test fixture
	ServerInfo* 			s1;
	ServerInfo* 			s2;

	Server* 				m_server_first;
	Server* 				m_server_second;
	ClientProtocol* 		m_client_first;
	ClientProtocol* 		m_client_second;
	QEventLoop				m_event_loop;
};

TEST_F( MultiServerParexScenario, TestTaskDivisionSplit)
{
	ASSERT_EQ(true, m_client_first->Ping())<< "SetUp failed for first server, we can't continue testing...";
	ASSERT_EQ(true, m_client_second->Ping())<< "SetUp failed for second server, we can't continue testing...";

	// Construct workspace in workspace template directory.
	Ws::WorkspaceFactory f;
	const std::string template_path = f.GetWorkspaceTemplatePath();

	auto workspace                  = f.CreateCliWorkspace(template_path);
	auto project                    = workspace->Get("SimpleExploration");
	auto preferences                = workspace->GetPreferences();
	auto exploration = std::static_pointer_cast<Ws::StartupFilePtree>(project->Back())->ToPtree();

	std::string exploration_name = "SimpleExploration" + QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate).toStdString();
	ParameterExploration* expl = new ParameterExploration(exploration_name, exploration, preferences);
	expl->SetSweep("cell_mechanics.base_area", new RangeSweep(1,5,1));

	std::vector<int> t;
	t.push_back(0);
	t.push_back(1);

	std::vector<int> tt;
	tt.push_back(2);
	tt.push_back(3);
	tt.push_back(4);

	ASSERT_EQ(true, m_client_first->SendExploration(expl, "127.0.0.1", t, "/home/uauser/exploration_results"))
		<< "Sending the exploration to m_server_first was unsuccesful";
	ASSERT_EQ(true, m_client_second->SendExploration(expl, "127.0.0.2", tt, "/home/uauser/exploration_results"))
		<< "Sending the exploration to m_server_second was unsuccesful";

	std::string name1;
	std::string name2;
	QTimer::singleShot(15000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ClientProtocol::GetFullExplorationName(s1, exploration_name, [&name1, exploration_name, this](std::string full_name){
	  	ClientProtocol::GetSession( this->s1, [&name1, full_name, this, exploration_name](ClientProtocol& client){
		  name1 = full_name;
	  });
	});

	ClientProtocol::GetFullExplorationName(s2, exploration_name, [&name2, exploration_name, this](std::string full_name){
		ClientProtocol::GetSession( this->s2, [&name2, full_name, this, exploration_name](ClientProtocol& client){
		  name2 = full_name;
	  });
	});

	QTimer::singleShot(15000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(2, m_server_first->GetNumberOfAssignedTasks(name1)) << "Assigning of the tasks failed for server 1";
	ASSERT_EQ(3, m_server_second->GetNumberOfAssignedTasks(name2)) << "Assigning of the tasks failed for server 2";

	//Clean up
	try {
		QFile::remove("exploration_backup.xml");
	} catch (...) {	}
}

TEST_F( MultiServerParexScenario, TestTaskDivisionStacked)
{
	ASSERT_EQ(true, m_client_first->Ping())<< "SetUp failed for first server, we can't continue testing...";
	ASSERT_EQ(true, m_client_second->Ping())<< "SetUp failed for second server, we can't continue testing...";

	// Construct workspace in workspace template directory.
	Ws::WorkspaceFactory f;
	const std::string template_path = f.GetWorkspaceTemplatePath();

	auto workspace                  = f.CreateCliWorkspace(template_path);
	auto project                    = workspace->Get("SimpleExploration");
	auto preferences                = workspace->GetPreferences();
	auto exploration = std::static_pointer_cast<Ws::StartupFilePtree>(project->Back())->ToPtree();

	std::string exploration_name = "SimpleExploration" + QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate).toStdString();
	ParameterExploration* expl = new ParameterExploration(exploration_name, exploration, preferences);
	expl->SetSweep("cell_mechanics.base_area", new RangeSweep(1,5,1));

	std::vector<int> t;
	t.push_back(0);
	t.push_back(1);
	t.push_back(2);
	t.push_back(3);

	std::vector<int> tt;
	tt.push_back(4);

	ASSERT_EQ(true, m_client_first->SendExploration(expl, "127.0.0.1", t, "/home/uauser/exploration_results"))
		<< "Sending the exploration to m_server_first was unsuccesful";
	ASSERT_EQ(true, m_client_second->SendExploration(expl, "127.0.0.2", tt, "/home/uauser/exploration_results"))
		<< "Sending the exploration to m_server_second was unsuccesful";

	std::string name1;
	std::string name2;
	QTimer::singleShot(15000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ClientProtocol::GetFullExplorationName(s1, exploration_name, [&name1, exploration_name, this](std::string full_name){
	  	ClientProtocol::GetSession( this->s1, [&name1, full_name, this, exploration_name](ClientProtocol& client){
		  name1 = full_name;
	  });
	});

	ClientProtocol::GetFullExplorationName(s2, exploration_name, [&name2, exploration_name, this](std::string full_name){
		ClientProtocol::GetSession( this->s2, [&name2, full_name, this, exploration_name](ClientProtocol& client){
		  name2 = full_name;
	  });
	});

	QTimer::singleShot(15000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(4, m_server_first->GetNumberOfAssignedTasks(name1)) << "Assigning of the tasks failed for server 1";
	ASSERT_EQ(1, m_server_second->GetNumberOfAssignedTasks(name2)) << "Assigning of the tasks failed for server 2";

	//Clean up
	try {
		QFile::remove("exploration_backup.xml");
	} catch (...) {	}
}

TEST_F( MultiServerParexScenario, TestDeleteExplorationSingleServer)
{
	ASSERT_EQ(true, m_client_first->Ping())<< "SetUp failed for first server, we can't continue testing...";

	// Construct workspace in workspace template directory.
	Ws::WorkspaceFactory f;
	const std::string template_path = f.GetWorkspaceTemplatePath();

	auto workspace                  = f.CreateCliWorkspace(template_path);
	auto project                    = workspace->Get("SimpleExploration");
	auto preferences                = workspace->GetPreferences();
	auto exploration = std::static_pointer_cast<Ws::StartupFilePtree>(project->Back())->ToPtree();

	std::string exploration_name = "SimpleExploration" + QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate).toStdString();
	ParameterExploration* expl = new ParameterExploration(exploration_name, exploration, preferences);
	expl->SetSweep("cell_mechanics.base_area", new RangeSweep(1,5,1));

	std::vector<int> t;
	t.push_back(0);
	t.push_back(1);
	t.push_back(2);
	t.push_back(3);
	t.push_back(4);

	ASSERT_EQ(true, m_client_first->SendExploration(expl, "127.0.0.1", t, "/home/uauser/exploration_results"))
		<< "Sending the exploration to m_server_first was unsuccesful";

	std::string name1;
	QTimer::singleShot(15000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ClientProtocol::GetFullExplorationName(s1, exploration_name, [&name1, exploration_name, this](std::string full_name){
	  	ClientProtocol::GetSession( this->s1, [&name1, full_name, this, exploration_name](ClientProtocol& client){
		  name1 = full_name;
	  });
	});


	QTimer::singleShot(15000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_client_first->DeleteExploration(name1))<< "Deleting the exploration failed on server1";

	QTimer::singleShot(10000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	//returns -1 if it cant find the exploration
	ASSERT_EQ(-1, m_server_first->GetNumberOfAssignedTasks(name1)) << "deleting of the exploration failed for server 1";

	//Clean up
	try {
		QFile::remove("exploration_backup.xml");
	} catch (...) {
		std::cout << "PROBLEM" << std::endl;
	}
}

TEST_F( MultiServerParexScenario, TestDeleteExplorationMultiServer)
{
	ASSERT_EQ(true, m_client_first->Ping())<< "SetUp failed for first server, we can't continue testing...";
	ASSERT_EQ(true, m_client_second->Ping())<< "SetUp failed for second server, we can't continue testing...";

	// Construct workspace in workspace template directory.
	Ws::WorkspaceFactory f;
	const std::string template_path = f.GetWorkspaceTemplatePath();

	auto workspace                  = f.CreateCliWorkspace(template_path);
	auto project                    = workspace->Get("SimpleExploration");
	auto preferences                = workspace->GetPreferences();
	auto exploration = std::static_pointer_cast<Ws::StartupFilePtree>(project->Back())->ToPtree();

	std::string exploration_name = "SimpleExploration" + QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate).toStdString();
	ParameterExploration* expl = new ParameterExploration(exploration_name, exploration, preferences);
	expl->SetSweep("cell_mechanics.base_area", new RangeSweep(1,5,1));

	std::vector<int> t;
	t.push_back(0);
	t.push_back(1);
	t.push_back(2);

	std::vector<int> tt;
	tt.push_back(3);
	tt.push_back(4);

	ASSERT_EQ(true, m_client_first->SendExploration(expl, "127.0.0.1", t, "/home/uauser/exploration_results"))
		<< "Sending the exploration to m_server_first was unsuccesful";
	ASSERT_EQ(true, m_client_second->SendExploration(expl, "127.0.0.2", tt, "/home/uauser/exploration_results"))
		<< "Sending the exploration to m_server_second was unsuccesful";

	std::string name1;
	std::string name2;
	QTimer::singleShot(15000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ClientProtocol::GetFullExplorationName(s1, exploration_name, [&name1, exploration_name, this](std::string full_name){
	  	ClientProtocol::GetSession( this->s1, [&name1, full_name, this, exploration_name](ClientProtocol& client){
		  name1 = full_name;
	  });
	});

	ClientProtocol::GetFullExplorationName(s2, exploration_name, [&name2, exploration_name, this](std::string full_name){
		ClientProtocol::GetSession( this->s2, [&name2, full_name, this, exploration_name](ClientProtocol& client){
		  name2 = full_name;
	  });
	});

	QTimer::singleShot(15000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_client_first->DeleteExploration(name1))<< "Deleting the exploration failed on server1";
	ASSERT_EQ(true, m_client_second->DeleteExploration(name2))<< "Deleting the exploration failed on server2";

	QTimer::singleShot(10000, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	//returns -1 if it cant find the exploration
	ASSERT_EQ(-1, m_server_first->GetNumberOfAssignedTasks(name1)) << "deleting of the exploration failed for server 1";
	ASSERT_EQ(-1, m_server_second->GetNumberOfAssignedTasks(name2)) << "deleting of the exploration failed for server 2";

	//Clean up
	try {
		QFile::remove("exploration_backup.xml");
	} catch (...) {
		std::cout << "PROBLEM" << std::endl;
	}
}


 } // namespace
 } // namespace
