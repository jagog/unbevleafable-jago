/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Implementation of test of parameter exploration client.
 */

#include "MockClientServer.h"

#include <cpp_sim/util/misc/InstallDirs.h>
#include "parex_protocol/ClientProtocol.h"
#include "parex_protocol/Exploration.h"
#include "parex_protocol/ParameterExploration.h"
#include "parex_protocol/ServerInfo.h"
#include "workspace/MergedPreferences.h"

#include "workspace/StartupFilePtree.h"
#include "workspace/Workspace.h"
#include "workspace/WorkspaceFactory.h"

#include <gtest/gtest.h>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <QEventLoop>
#include <QTimer>
#include <vector>

namespace SimPT_Parex {
namespace Tests {

using SimPT_Sim::Util::InstallDirs;
using namespace SimPT_Shell;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;

class UnitParexClient : public testing::Test
{
protected:
	virtual void SetUp()
	{
		m_server = new MockClientServer();
		ASSERT_EQ(true, m_server->SetUp())<< "SetUp failed, we can't continue testing...";
		std::string name = "Test";
		m_client = new ClientProtocol(new ServerInfo(name,"127.0.0.1", m_server->getPort()));
	}

	virtual void TearDown()
	{
		delete m_server;
		delete m_client;
	}

	// Data members of the test fixture
	MockClientServer* 	m_server;
	ClientProtocol* 	m_client;
	QEventLoop		m_event_loop;
};

TEST_F( UnitParexClient, ConnectClient)
{
	m_server->setCurrentTest(stests::connect);
	QTimer::singleShot(2 * 1000, &m_event_loop, SLOT(quit()));
	QTimer::connect(m_server, SIGNAL(Done()), &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_server->TestSucceeded()) << "Connection timed out.";
}

TEST_F( UnitParexClient, SendExploration)
{
	ASSERT_EQ(true, m_client->IsConnected()) << "SetUp failed, not connected";

	m_server->setCurrentTest(stests::sendExploration);
	QTimer::singleShot(500, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	// Construct workspace in workspace template directory.
	Ws::WorkspaceFactory f;
	const std::string template_path = f.GetWorkspaceTemplatePath();
	auto workspace                  = f.CreateCliWorkspace(template_path);
	auto project                    = workspace->Get("Geometric");
	auto preferences                = workspace->GetPreferences();
	std::vector<int> testvec = {0};
	auto exploration = std::static_pointer_cast<Ws::StartupFilePtree>(project->Back())->ToPtree();

	Exploration* expl = new ParameterExploration("Test", exploration, preferences);
	ASSERT_EQ(true, m_client->SendExploration(expl, "mock_ip", testvec, "/home/exploration_results")) << "Could not send request";

	QTimer::singleShot(500, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_server->TestSucceeded()) << "Connection timed out.";
}

TEST_F( UnitParexClient, RequestUpdates)
{
    ASSERT_EQ(true, m_client->IsConnected()) << "SetUp failed, not connected";

	m_server->setCurrentTest(stests::requestUpdates);
	QTimer::singleShot(500, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_client->SubscribeUpdates("TestName")) << "Could not send request";

	QTimer::singleShot(500, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_EQ(true, m_server->TestSucceeded()) << "Connection timed out.";
}

TEST_F( UnitParexClient, Ping)
{
	ASSERT_EQ(true, m_client->IsConnected()) << "SetUp failed, not connected";

	m_server->setCurrentTest(stests::ping);
	QTimer::singleShot(500, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_TRUE(m_client->Ping()) << "Ping unsuccesful";

	QTimer::singleShot(500, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_TRUE(m_server->TestSucceeded()) << "connection timed out ";
}

TEST_F( UnitParexClient, GetExplorationProgress)
{
	ASSERT_EQ(true, m_client->IsConnected()) << "SetUp failed, not connected";

	m_server->setCurrentTest(stests::GetExplorationProgress);

	QTimer::singleShot(500, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_TRUE(m_client->GetExplorationProgress("TestName")) << "Exploration status request unsuccesful";

	QTimer::singleShot(500, &m_event_loop, SLOT(quit()));
	m_event_loop.exec();

	ASSERT_TRUE(m_server->TestSucceeded()) << "connection timed out";
}

} // namespace
} // namespace
