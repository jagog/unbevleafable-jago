/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests of PluginLoader.cpp
 */

#include <model/ComponentFactoryBase.h>
#include <cpp_sim/model/PluginLoader.h>
#include <cpp_sim/util/misc/InstallDirs.h>

#include <boost/filesystem.hpp>
#include <boost/dll/import.hpp>

#include <gtest/gtest.h>

namespace SimPT_Sim {
namespace Tests {

boost::filesystem::path getPluginPath(const std::string plugin_name) {
    boost::filesystem::path lib_path(Util::InstallDirs::GetRootDir().c_str());

    lib_path = lib_path / "bin" / ("libsimPT_" + plugin_name);
    return lib_path;
}

TEST( UnitPluginLoader, Default ) {
    boost::filesystem::path path = getPluginPath("Default");

    boost::shared_ptr<ComponentFactoryBase> plugin;
    plugin = boost::dll::import<ComponentFactoryBase>(path, "plugin", boost::dll::load_mode::append_decorations);
    ASSERT_TRUE( plugin.get() != nullptr );
}

TEST( UnitPluginLoader, Blad ){
    boost::filesystem::path path = getPluginPath("Blad");

    boost::shared_ptr<ComponentFactoryBase> plugin;
    plugin = boost::dll::import<ComponentFactoryBase>(path, "plugin", boost::dll::load_mode::append_decorations);
    ASSERT_TRUE( plugin.get() != nullptr );
}

TEST( UnitPluginLoader, Maize ){
    boost::filesystem::path path = getPluginPath("Maize");

    boost::shared_ptr<ComponentFactoryBase> plugin;
    plugin = boost::dll::import<ComponentFactoryBase>(path, "plugin", boost::dll::load_mode::append_decorations);
    ASSERT_TRUE( plugin.get() != nullptr );
}

TEST( UnitPluginLoader, MissingPlugin ){
    boost::filesystem::path path = getPluginPath("fjh5982yog57j9go9j5y5oui928jyr2q88");
    ASSERT_FALSE(boost::filesystem::exists(path) );

    try {
        boost::shared_ptr<ComponentFactoryBase> plugin;
        plugin = boost::dll::import<ComponentFactoryBase>(path, "plugin", boost::dll::load_mode::append_decorations);
    } catch (std::exception& e) {
        std::string error = "boost::dll::shared_library::load() failed (dlerror system message: " + path.string()  + ": cannot open shared object file: No such file or directory): Bad file descriptor";
        ASSERT_EQ( error, e.what() );
    }
}

} // namespace
} // namespace
