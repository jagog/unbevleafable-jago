/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests for PolygonUtils.
 */

#include "editor/PolygonUtils.h"

#include <gtest/gtest.h>
#include <QLineF>
#include <QPointF>
#include <QPolygonF>
#include <QRectF>
#include <QVector>
#include <cmath>

using namespace std;

namespace SimPT_Editor {
namespace Tests {

class UnitPolygonUtils : public ::testing::Test
{
};

TEST(UnitPolygonUtils, TestIsClockwise)
{
	//Setup1
	QVector<QPointF> points1(3);
	points1[0] = QPointF(0,0);
	points1[1] = QPointF(1,1);
	points1[2] = QPointF(1,0);
	QPolygonF polygon1 = QPolygonF(points1);

	//Test1: simple test with small polygon, points are given clockwise
	EXPECT_TRUE(PolygonUtils::IsClockwise(polygon1)) << "Polygon should be clockwise";

	//Setup2
	QVector<QPointF> points2(3);
	points2[0] = QPointF(0,0);
	points2[1] = QPointF(1,0);
	points2[2] = QPointF(1,1);
	QPolygonF polygon2 = QPolygonF(points2);

	//Test2: simple test with small polygon, points are given counterclockwise
	EXPECT_FALSE(PolygonUtils::IsClockwise(polygon2)) << "Polygon should be counterclockwise";

	//Setup3
	QVector<QPointF> points3(8);
	points3[0] = QPointF(0,0);
	points3[1] = QPointF(0,1);
	points3[2] = QPointF(0,2);
	points3[3] = QPointF(1,2);
	points3[4] = QPointF(2,2);
	points3[5] = QPointF(2,1);
	points3[6] = QPointF(2,0);
	points3[7] = QPointF(1,0);
	QPolygonF polygon3 = QPolygonF(points3);

	//Test3: test with a greater polygon (8 points), points are given clockwise
	EXPECT_TRUE(PolygonUtils::IsClockwise(polygon3)) << "Polygon should be clockwise";

	//Setup4
	QVector<QPointF> points4(8);
	points4[0] = QPointF(0,0);
	points4[1] = QPointF(1,0);
	points4[2] = QPointF(2,0);
	points4[3] = QPointF(2,1);
	points4[4] = QPointF(2,2);
	points4[5] = QPointF(1,2);
	points4[6] = QPointF(0,2);
	points4[7] = QPointF(0,1);
	QPolygonF polygon4 = QPolygonF(points4);

	//Test4: test with a greater polygon (8 points), points are given counterclockwise
	EXPECT_FALSE(PolygonUtils::IsClockwise(polygon4)) << "Polygon should be counterclockwise";
}

TEST(UnitPolygonUtils,TestIsSimplePolygon)
{
	//We expect that the points can be given both clockwise and counterclockwise
	//Setup1
	QVector<QPointF> points1(3);
	points1[0] = QPointF(0,0);
	points1[1] = QPointF(1,1);
	points1[2] = QPointF(1,0);
	QPolygonF polygon1 = QPolygonF(points1);

	//Test1: simple test with small polygon, points are given clockwise
	EXPECT_TRUE(PolygonUtils::IsSimplePolygon(polygon1)) << "Polygon should be simple";

	//Setup2
	QVector<QPointF> points2(3);
	points2[0] = QPointF(0,0);
	points2[1] = QPointF(1,0);
	points2[2] = QPointF(1,1);
	QPolygonF polygon2 = QPolygonF(points2);

	//Test2: simple test with small polygon, points are given counterclockwise
	EXPECT_TRUE(PolygonUtils::IsSimplePolygon(polygon2)) << "Polygon should be simple";

	//Setup3
	QVector<QPointF> points3(8);
	points3[0] = QPointF(0,0);
	points3[1] = QPointF(0,1);
	points3[2] = QPointF(0,2);
	points3[3] = QPointF(1,2);
	points3[4] = QPointF(2,2);
	points3[5] = QPointF(2,1);
	points3[6] = QPointF(2,0);
	points3[7] = QPointF(1,0);
	QPolygonF polygon3 = QPolygonF(points3);

	//Test3: test with a greater polygon (8 points), points are given clockwise
	EXPECT_TRUE(PolygonUtils::IsSimplePolygon(polygon3)) << "Polygon should be simple";

	//Setup4
	QVector<QPointF> points4(8);
	points4[0] = QPointF(0,0);
	points4[1] = QPointF(1,0);
	points4[2] = QPointF(2,0);
	points4[3] = QPointF(2,1);
	points4[4] = QPointF(2,2);
	points4[5] = QPointF(1,2);
	points4[6] = QPointF(0,2);
	points4[7] = QPointF(0,1);
	QPolygonF polygon4 = QPolygonF(points4);

	//Test4: test with a greater polygon (8 points), points are given counterclockwise
	EXPECT_TRUE(PolygonUtils::IsSimplePolygon(polygon4)) << "Polygon should be simple";

	//Setup5
	QVector<QPointF> points5(8);
	points5[0] = QPointF(1,2);
	points5[1] = QPointF(0,0);
	points5[2] = QPointF(2,2);
	points5[3] = QPointF(2,1);
	points5[4] = QPointF(1,0);
	points5[5] = QPointF(0,2);
	points5[6] = QPointF(2,0);
	points5[7] = QPointF(0,1);
	QPolygonF polygon5 = QPolygonF(points5);

	//Test5: test with a greater polygon (8 points), points are given in a random sequence, and shouldn't give a simple polygon.
	EXPECT_FALSE(PolygonUtils::IsSimplePolygon(polygon5)) << "Polygon should be complex";
}

TEST(UnitPolygonUtils,TestCalculateArea)
{
	//We expect that the points can be given both clockwise and counterclockwise
	//Setup1
	QVector<QPointF> points1(3);
	points1[0] = QPointF(0,0);
	points1[1] = QPointF(1,1);
	points1[2] = QPointF(1,0);
	QPolygonF polygon1 = QPolygonF(points1);

	//Test1: simple test with small polygon, points are given clockwise
	EXPECT_EQ(PolygonUtils::CalculateArea(polygon1),0.5) << "Calculated area isn't correct";

	//Setup2
	QVector<QPointF> points2(3);
	points2[0] = QPointF(0,0);
	points2[1] = QPointF(1,0);
	points2[2] = QPointF(1,1);
	QPolygonF polygon2 = QPolygonF(points2);

	//Test2: simple test with small polygon, points are given counterclockwise
	EXPECT_EQ(PolygonUtils::CalculateArea(polygon2),0.5) << "Calculated area isn't correct";

	//Setup3
	QVector<QPointF> points3(8);
	points3[0] = QPointF(0,0);
	points3[1] = QPointF(0,1);
	points3[2] = QPointF(0,2);
	points3[3] = QPointF(1,2);
	points3[4] = QPointF(2,2);
	points3[5] = QPointF(2,1);
	points3[6] = QPointF(2,0);
	points3[7] = QPointF(1,0);
	QPolygonF polygon3 = QPolygonF(points3);

	//Test3: test with a greater polygon (8 points), points are given clockwise
	EXPECT_EQ(PolygonUtils::CalculateArea(polygon3),4.0) << "Calculated area isn't correct";

	//Setup4
	QVector<QPointF> points4(8);
	points4[0] = QPointF(0,0);
	points4[1] = QPointF(1,0);
	points4[2] = QPointF(2,0);
	points4[3] = QPointF(2,1);
	points4[4] = QPointF(2,2);
	points4[5] = QPointF(1,2);
	points4[6] = QPointF(0,2);
	points4[7] = QPointF(0,1);
	QPolygonF polygon4 = QPolygonF(points4);

	//Test4: test with a greater polygon (8 points), points are given counterclockwise
	EXPECT_EQ(PolygonUtils::CalculateArea(polygon4),4.0) << "Calculated area isn't correct";
}

TEST(UnitPolygonUtils,TestClipPolygon)
{
	//Setup1
	QVector<QPointF> points1(4);
	points1[0] = QPointF(0,0);
	points1[1] = QPointF(1,0);
	points1[2] = QPointF(1,1);
	points1[3] = QPointF(0,1);
	QPolygonF polygon1 = QPolygonF(points1);

	QVector<QPointF> points2(4);
	points2[0] = QPointF(0.5,0.5);
	points2[1] = QPointF(1.5,0.5);
	points2[2] = QPointF(1.5,1.5);
	points2[3] = QPointF(0.5,1.5);
	QPolygonF polygon2 = QPolygonF(points2);

	//Test1: test with two overlapping polygons, with one shared area
	std::list<QPolygonF> polList = PolygonUtils::ClipPolygon(polygon1, polygon2);
	int size = polList.size();
	EXPECT_EQ(size,1) << "Incorrect count of polygons";
	std::vector<QPolygonF> polVec;
	for(std::list<QPolygonF>::iterator it = polList.begin(); it != polList.end(); ++it) {
		polVec.push_back(*it);
	}

	//The one shared area
	EXPECT_TRUE(std::find_if(polVec[0].begin(), polVec[0].end(), [](const QPointF& p)
		{return fabs(0.5 - p.x()) < 10e-12 && fabs(0.5 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[0].begin(), polVec[0].end(), [](const QPointF& p)
		{return fabs(0.5 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[0].begin(), polVec[0].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(0.5 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[0].begin(), polVec[0].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";


	//Setup2
	QVector<QPointF> points3(4);
	points3[0] = QPointF(0,0);
	points3[1] = QPointF(2,0);
	points3[2] = QPointF(2,3);
	points3[3] = QPointF(0,3);
	QPolygonF polygon3 = QPolygonF(points3);

	QVector<QPointF> points4(8);
	points4[0] = QPointF(1,0);
	points4[1] = QPointF(3,0);
	points4[2] = QPointF(3,3);
	points4[3] = QPointF(1,3);
	points4[4] = QPointF(1,2);
	points4[5] = QPointF(2.5,2);
	points4[6] = QPointF(2.5,1);
	points4[7] = QPointF(1,1);
	QPolygonF polygon4 = QPolygonF(points4);

	//Test2: test with two overlapping polygons, with two shared areas
	std::list<QPolygonF> polList2 = PolygonUtils::ClipPolygon(polygon3, polygon4);
	int size2 = polList2.size();
	EXPECT_EQ(size2,2) << "Incorrect count of polygons";
	std::vector<QPolygonF> polVec2;
	for(std::list<QPolygonF>::iterator it = polList2.begin(); it != polList2.end(); ++it) {
		polVec2.push_back(*it);
	}

	//First shared area
	EXPECT_TRUE(std::find_if(polVec2[0].begin(), polVec2[0].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec2[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[0].begin(), polVec2[0].end(), [](const QPointF& p)
		{return fabs(2 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec2[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[0].begin(), polVec2[0].end(), [](const QPointF& p)
		{return fabs(2 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec2[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[0].begin(), polVec2[0].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec2[0].end()) << "Polygon should contain point";

	//Second shared area
	EXPECT_TRUE(std::find_if(polVec2[1].begin(), polVec2[1].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(3 - p.y()) < 10e-12;}) != polVec2[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[1].begin(), polVec2[1].end(), [](const QPointF& p)
		{return fabs(2 - p.x()) < 10e-12 && fabs(2 - p.y()) < 10e-12;}) != polVec2[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[1].begin(), polVec2[1].end(), [](const QPointF& p)
		{return fabs(2 - p.x()) < 10e-12 && fabs(3 - p.y()) < 10e-12;}) != polVec2[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[1].begin(), polVec2[1].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(2 - p.y()) < 10e-12;}) != polVec2[1].end()) << "Polygon should contain point";


	//Setup3
	QVector<QPointF> points5(4);
	points5[0] = QPointF(0,0);
	points5[1] = QPointF(1,0);
	points5[2] = QPointF(1,1);
	points5[3] = QPointF(0,1);
	QPolygonF polygon5 = QPolygonF(points5);

	QVector<QPointF> points6(4);
	points6[0] = QPointF(2,2);
	points6[1] = QPointF(3,2);
	points6[2] = QPointF(3,3);
	points6[3] = QPointF(2,3);
	QPolygonF polygon6 = QPolygonF(points6);

	//Test3: test with two non overlapping polygons, returned list should be empty
	std::list<QPolygonF> polList3 = PolygonUtils::ClipPolygon(polygon5, polygon6);
	int size3 = polList3.size();
	EXPECT_EQ(size3,0) << "Incorrect count of polygons";
}

TEST(UnitPolygonUtils,TestSlicePolygon)
{
	//Setup1
	QVector<QPointF> points1(4);
	points1[0] = QPointF(0,0);
	points1[1] = QPointF(1,0);
	points1[2] = QPointF(1,1);
	points1[3] = QPointF(0,1);
	QPolygonF polygon1 = QPolygonF(points1);

	//Test1: test with a line that cuts through the simple polygon
	std::list<QPolygonF> polList = PolygonUtils::SlicePolygon(polygon1, QLineF(QPointF(0.5,-1),QPointF(0.5,2)));
	int size = polList.size();
	EXPECT_EQ(size,2) << "Incorrect count of polygons";
	std::vector<QPolygonF> polVec;
	for(std::list<QPolygonF>::iterator it = polList.begin(); it != polList.end(); ++it) {
		polVec.push_back(*it);
	}

	//First polygon
	EXPECT_TRUE(std::find_if(polVec[0].begin(), polVec[0].end(), [](const QPointF& p)
		{return fabs(0.5 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[0].begin(), polVec[0].end(), [](const QPointF& p)
		{return fabs(0.5 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[0].begin(), polVec[0].end(), [](const QPointF& p)
		{return fabs(0 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[0].begin(), polVec[0].end(), [](const QPointF& p)
		{return fabs(0 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";

	//Second polygon
	EXPECT_TRUE(std::find_if(polVec[1].begin(), polVec[1].end(), [](const QPointF& p)
		{return fabs(0.5 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[1].begin(), polVec[1].end(), [](const QPointF& p)
		{return fabs(0.5 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[1].begin(), polVec[1].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec[1].begin(), polVec[1].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";

	//Setup2
	QVector<QPointF> points2(4);
	points2.append(QPointF(0,0));
	points2.append(QPointF(1,0));
	points2.append(QPointF(1,1));
	points2.append(QPointF(0,1));
	QPolygonF polygon2 = QPolygonF(points2);

	//Test2: test with a line that doesn't cuts through the simple polygon, should give the original polygon
	std::list<QPolygonF> polList2 = PolygonUtils::SlicePolygon(polygon2, QLineF(QPointF(5,0),QPointF(5,1)));
	int size2 = polList2.size();
	EXPECT_EQ(size2, 1) << "Incorrect count of polygons";
	std::vector<QPolygonF> polVec2;
	for(std::list<QPolygonF>::iterator it = polList2.begin(); it != polList2.end(); ++it) {
		polVec2.push_back(*it);
	}

	//The one returned polygon, should match to the original one
	EXPECT_TRUE(std::find_if(polVec2[0].begin(), polVec2[0].end(), [](const QPointF& p)
		{return fabs(0 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[0].begin(), polVec2[0].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[0].begin(), polVec2[0].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec2[0].begin(), polVec2[0].end(), [](const QPointF& p)
		{return fabs(0 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";

	//Setup3
	QVector<QPointF> points3(4);
	points3.append(QPointF(0,0));
	points3.append(QPointF(1,0));
	points3.append(QPointF(1,1));
	points3.append(QPointF(0,1));
	QPolygonF polygon3 = QPolygonF(points3);

	//Test3: test with a polygon that has on edge on the cut
	std::list<QPolygonF> polList3 = PolygonUtils::SlicePolygon(polygon3, QLineF(QPointF(1,-1),QPointF(1,2)));
	int size3 = polList3.size();
	EXPECT_EQ(size3,1) << "Incorrect count of polygons";
	std::vector<QPolygonF> polVec3;
	for(std::list<QPolygonF>::iterator it = polList3.begin(); it != polList3.end(); ++it) {
		polVec3.push_back(*it);
	}

	//The one returned polygon, should match to the original one
	EXPECT_TRUE(std::find_if(polVec3[0].begin(), polVec3[0].end(), [](const QPointF& p)
		{return fabs(0 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec3[0].begin(), polVec3[0].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(0 - p.y()) < 10e-12;}) != polVec[0].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec3[0].begin(), polVec3[0].end(), [](const QPointF& p)
		{return fabs(1 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";
	EXPECT_TRUE(std::find_if(polVec3[0].begin(), polVec3[0].end(), [](const QPointF& p)
		{return fabs(0 - p.x()) < 10e-12 && fabs(1 - p.y()) < 10e-12;}) != polVec[1].end()) << "Polygon should contain point";
}

} // namespace
} // namespace
