/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests for EditableCellItem.
 */

#include <cpp_sim/util/misc/InstallDirs.h>
#include "bio/Cell.h"
#include "bio/Mesh.h"
#include "bio/Node.h"
#include "bio/PBMBuilder.h"
#include "editor/EditableCellItem.h"
#include "editor/EditableEdgeItem.h"
#include "editor/EditableNodeItem.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <QDir>
#include <gtest/gtest.h>

#include <list>
#include <vector>

namespace SimPT_Editor {
namespace Tests {

using namespace std;
using namespace boost::property_tree;
using namespace boost::property_tree::xml_parser;
using namespace SimPT_Sim::Util;

class UnitEditableCellItem : public ::testing::Test
{
protected:
	UnitEditableCellItem() : ::testing::Test(), m_mesh(0)
	{
	}

	virtual void SetUp()
	{
		//Make intern items
		m_mesh_nodes.push_back(m_mesh.BuildNode(0, {{0, 0, 0}}, false));
		m_mesh_nodes.push_back(m_mesh.BuildNode(1, {{1, 0, 0}}, false));
		m_mesh_nodes.push_back(m_mesh.BuildNode(2, {{1, 1, 0}}, false));
		m_mesh_nodes.push_back(m_mesh.BuildNode(3, {{0, 1, 0}}, false));
		m_mesh_nodes.push_back(m_mesh.BuildNode(4, {{1, -1, 0}}, false));
		m_mesh_nodes.push_back(m_mesh.BuildNode(5, {{0, -1, 0}}, false));
		m_mesh_nodes.push_back(m_mesh.BuildNode(6, {{0.5, 0, 0}}, false));
		m_mesh_cells.push_back(m_mesh.BuildCell(0, {0,1,2,3}));
		m_mesh_cells.push_back(m_mesh.BuildCell(1, {0,1,4,5}));

		//Make editable items
		m_nodes.push_back(new EditableNodeItem(m_mesh_nodes[0], 1));
		m_nodes.push_back(new EditableNodeItem(m_mesh_nodes[1], 1));
		m_nodes.push_back(new EditableNodeItem(m_mesh_nodes[2], 1));
		m_nodes.push_back(new EditableNodeItem(m_mesh_nodes[3], 1));
		m_nodes.push_back(new EditableNodeItem(m_mesh_nodes[4], 1));
		m_nodes.push_back(new EditableNodeItem(m_mesh_nodes[5], 1));
		m_nodes.push_back(new EditableNodeItem(m_mesh_nodes[6], 1));

		m_edges.push_back(new EditableEdgeItem(m_nodes[0],m_nodes[1]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[1],m_nodes[2]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[2],m_nodes[3]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[3],m_nodes[0]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[1],m_nodes[4]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[4],m_nodes[5]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[5],m_nodes[0]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[0],m_nodes[6]));
		m_edges.push_back(new EditableEdgeItem(m_nodes[6],m_nodes[1]));

		m_node_list1.push_back(m_nodes[0]);
		m_node_list1.push_back(m_nodes[1]);
		m_node_list1.push_back(m_nodes[2]);
		m_node_list1.push_back(m_nodes[3]);

		m_node_list2.push_back(m_nodes[0]);
		m_node_list2.push_back(m_nodes[1]);
		m_node_list2.push_back(m_nodes[4]);
		m_node_list2.push_back(m_nodes[5]);

		m_edge_list1.push_back(m_edges[0]);
		m_edge_list1.push_back(m_edges[1]);
		m_edge_list1.push_back(m_edges[2]);
		m_edge_list1.push_back(m_edges[3]);

		m_edge_list2.push_back(m_edges[0]);
		m_edge_list2.push_back(m_edges[4]);
		m_edge_list2.push_back(m_edges[5]);
		m_edge_list2.push_back(m_edges[6]);
		m_cells.push_back(new EditableCellItem(m_node_list1,m_edge_list1,m_mesh_cells[0]));
		m_cells.push_back(new EditableCellItem(m_node_list2,m_edge_list2,m_mesh_cells[1]));
	}

	virtual void TearDown()
	{
		//Clean up cells
		for(vector<EditableCellItem*>::iterator it = m_cells.begin(); it != m_cells.end(); ++it ){
			delete (*it);
		}

		//Clean up edges
		for(vector<EditableEdgeItem*>::iterator it = m_edges.begin(); it != m_edges.end(); ++it ){
			delete (*it);
		}

		//Clean up nodes
		for(vector<EditableNodeItem*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it ){
			delete (*it);
		}
	}

	// Data members of the test fixture
	SimPT_Sim::Mesh                m_mesh;
	list<EditableNodeItem*>        m_node_list1;
	list<EditableNodeItem*>        m_node_list2;
	list<EditableEdgeItem*>        m_edge_list1;
	list<EditableEdgeItem*>        m_edge_list2;
	vector<EditableNodeItem*>      m_nodes;
	vector<EditableEdgeItem*>      m_edges;
	vector<EditableCellItem*>      m_cells;
	vector<SimPT_Sim::Node*>       m_mesh_nodes;
	vector<SimPT_Sim::Cell*>       m_mesh_cells;
};

TEST_F(UnitEditableCellItem, TestIsAtBoundary)
{
	//Setup
	ptree tree;
	read_xml(InstallDirs::GetDataDir() + "/res/UnitEditableCell/TestIsAtBoundary.xml",tree);
	auto mesh = SimPT_Sim::PBMBuilder().Build(tree.get_child("vleaf2.mesh"));
	vector<SimPT_Sim::Cell*> meshCells = mesh->GetCells();
	vector<EditableCellItem*> cells;
	//For this test we wont do anything with nodes and edges so we can leave the lists empty
	list<EditableNodeItem*> nodes;
	list<EditableEdgeItem*> edges;
	for (vector<SimPT_Sim::Cell*>::iterator it = meshCells.begin(); it != meshCells.end() ; ++it) {
		cells.push_back(new EditableCellItem(nodes,edges,*it));
	}

	//Test
	EXPECT_TRUE(cells[0]->IsAtBoundary()) << "Cell should be at boundary";
	EXPECT_TRUE(cells[1]->IsAtBoundary()) << "Cell should be at boundary";
	EXPECT_TRUE(cells[2]->IsAtBoundary()) << "Cell should be at boundary";
	EXPECT_TRUE(cells[3]->IsAtBoundary()) << "Cell should be at boundary";
	EXPECT_TRUE(cells[4]->IsAtBoundary()) << "Cell should be at boundary";
	EXPECT_TRUE(cells[5]->IsAtBoundary()) << "Cell should be at boundary";
	EXPECT_TRUE(cells[6]->IsAtBoundary()) << "Cell should be at boundary";
	EXPECT_TRUE(cells[7]->IsAtBoundary()) << "Cell should be at boundary";
	EXPECT_FALSE(cells[8]->IsAtBoundary()) << "Cell shouldn't be at boundary";
}

TEST_F(UnitEditableCellItem, TestCell)
{
	EXPECT_EQ(m_cells[0]->Cell(), m_mesh_cells[0]) << "Logical cell doesn't match the original one";
	EXPECT_EQ(m_cells[1]->Cell(), m_mesh_cells[1]) << "Logical cell doesn't match the original one";
	EXPECT_NE(m_cells[0]->Cell(), m_mesh_cells[1]) << "Logical cell match the wrong original one";
	EXPECT_NE(m_cells[1]->Cell(), m_mesh_cells[0]) << "Logical cell match the wrong original one";
}

TEST_F(UnitEditableCellItem, TestNodes)
{
	EXPECT_EQ(m_cells[0]->Nodes(), m_node_list1) << "Node list doesn't match";
	EXPECT_EQ(m_cells[1]->Nodes(), m_node_list2) << "Node list doesn't match";
	EXPECT_NE(m_cells[0]->Nodes(), m_node_list2) << "Node list matches";
	EXPECT_NE(m_cells[1]->Nodes(), m_node_list1) << "Node list matches";
}

TEST_F(UnitEditableCellItem, TestEdges)
{
	EXPECT_EQ(m_cells[0]->Edges(), m_edge_list1) << "Edge list doesn't match";
	EXPECT_EQ(m_cells[1]->Edges(), m_edge_list2) << "Edge list doesn't match";
	EXPECT_NE(m_cells[0]->Edges(), m_edge_list2) << "Edge list matches";
	EXPECT_NE(m_cells[1]->Edges(), m_edge_list1) << "Edge list matches";
}

TEST_F(UnitEditableCellItem, TestContainsNode)
{
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[0])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[1])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[2])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[3])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[0]->ContainsNode(m_nodes[4])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[0]->ContainsNode(m_nodes[5])) << "Cell shoudln't contain the given edge";

	EXPECT_TRUE(m_cells[1]->ContainsNode(m_nodes[0])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsNode(m_nodes[1])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsNode(m_nodes[4])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsNode(m_nodes[5])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsNode(m_nodes[2])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsNode(m_nodes[3])) << "Cell shoudln't contain the given edge";
}

TEST_F(UnitEditableCellItem, TestContainsEdge)
{
	EXPECT_TRUE(m_cells[0]->ContainsEdge(m_edges[0])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsEdge(m_edges[1])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsEdge(m_edges[2])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsEdge(m_edges[3])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[0]->ContainsEdge(m_edges[4])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[0]->ContainsEdge(m_edges[5])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[0]->ContainsEdge(m_edges[6])) << "Cell shoudln't contain the given edge";

	EXPECT_TRUE(m_cells[1]->ContainsEdge(m_edges[0])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsEdge(m_edges[4])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsEdge(m_edges[5])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsEdge(m_edges[6])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsEdge(m_edges[1])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsEdge(m_edges[2])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsEdge(m_edges[3])) << "Cell shoudln't contain the given edge";
}

TEST_F(UnitEditableCellItem, TestReplaceEdgeWithTwoEdges)
{
	//Correct one
	m_cells[0]->ReplaceEdgeWithTwoEdges(m_edges[0], m_edges[7], m_edges[8]);
	EXPECT_FALSE(m_cells[0]->ContainsEdge(m_edges[0])) << "Cell shoudln't contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsEdge(m_edges[7])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsEdge(m_edges[8])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[0])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[1])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[6])) << "Cell should contain the given edge";

	//Shouldn't have changed anything to the cell that shared an edge with the cell.
	EXPECT_TRUE(m_cells[1]->ContainsEdge(m_edges[0])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsEdge(m_edges[7])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsEdge(m_edges[8])) << "Cell shoudln't contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsNode(m_nodes[0])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsNode(m_nodes[1])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsNode(m_nodes[6])) << "Cell shoudln't contain the given edge";
}

TEST_F(UnitEditableCellItem, TestReplaceTwoEdgesWithEdge)
{
	//Correct one
	m_cells[0]->ReplaceEdgeWithTwoEdges(m_edges[0], m_edges[7], m_edges[8]);
	m_cells[0]->ReplaceTwoEdgesWithEdge(m_edges[7], m_edges[8], m_edges[0]);
	EXPECT_TRUE(m_cells[0]->ContainsEdge(m_edges[0])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[0]->ContainsEdge(m_edges[7])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[0]->ContainsEdge(m_edges[8])) << "Cell shoudln't contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[0])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[0]->ContainsNode(m_nodes[1])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[0]->ContainsNode(m_nodes[6])) << "Cell shoudln't contain the given edge";

	//Shouldn't have changed anything to the cell that shared an edge with the cell.
	EXPECT_TRUE(m_cells[1]->ContainsEdge(m_edges[0])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsEdge(m_edges[7])) << "Cell shoudln't contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsEdge(m_edges[8])) << "Cell shoudln't contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsNode(m_nodes[0])) << "Cell should contain the given edge";
	EXPECT_TRUE(m_cells[1]->ContainsNode(m_nodes[1])) << "Cell should contain the given edge";
	EXPECT_FALSE(m_cells[1]->ContainsNode(m_nodes[6])) << "Cell shoudln't contain the given edge";
}

} // namespace
} // namespace
