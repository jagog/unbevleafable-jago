#ifndef GTEST_ASSERTION_TEST_H_INLCUDED
#define GTEST_ASSERTION_TEST_H_INLCUDED
/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * ASSERT_ASSERTION_VIOLATED macro
 */

/**
 * The gtest ASSERT_DEATH macro for testing assert statements, disabled in a release-build
 */
#ifdef NDEBUG

#define ASSERT_ASSERTION_VIOLATED(x, y) static_cast<void>(x)
#define EXPECT_ASSERTION_VIOLATED(x, y) static_cast<void>(x)

#else

#define ASSERT_ASSERTION_VIOLATED(x, y) ASSERT_DEATH(x, y)
#define EXPECT_ASSERTION_VIOLATED(x, y) EXPECT_DEATH(x, y)

#endif

#endif // end-of-include-guard
