/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
/**
 * @file
 * Unit tests of aritmetic with std::array.
 */

#include "math/math.h"

#include <gtest/gtest.h>
#include <ostream>

using namespace std;
using namespace SimPT_Sim::Util;

namespace SimPT_Sim {
namespace Util {
namespace Tests {

const double tol = 1.0e-10;

TEST( UnitArray3, Indexes )
{
	array<double, 3> v {{ 5.0, 3.0, 4.0 }};
	EXPECT_NEAR( 5.0, v[0], tol);
	EXPECT_NEAR( 3.0, v[1], tol);
	EXPECT_NEAR( 4.0, v[2], tol);
}

TEST( UnitArray3, BasicArithmetic )
{
	array<double, 3> u {{1.0, 2.0, 0.5}};
	array<double, 3> v = {{5.0, 3.0, 4.0}};
	array<double, 3> x({{6.0, 5.0, 4.5}});
	array<double, 3> y {{2.0, 4.0, 01.0}};
	auto z = v;
	z += u;
	EXPECT_EQ( x , z);
	z = x;
	z -= u;
	EXPECT_EQ( v , z);
	z = u;
	z *= 2.0;
	EXPECT_EQ( y , z);
	z /= 2.0;
	EXPECT_EQ( u , z);
}

TEST( UnitArray3, Norm )
{
	EXPECT_NEAR( std::sqrt(29), Norm(array<double, 3> {{3.0, 2.0, 4.0}}), tol);
	EXPECT_NEAR( 1.0, Norm( Normalize(array<double, 3> {{3.0, 2.0, 4.0}})), tol);
}

TEST( UnitArray3, Orthogonalize )
{
	const array<double, 3> a1 {{9.0, -3.0, 0.0}};
	const auto a2 = Orthogonalize(array<double, 3> {{3.0, 9.0, 5.0}});
	EXPECT_EQ( a1 , a2 );
	const array<double, 3> d1 {{1.0, 1.0, 1.1}};
	const array<double, 3> d2 {{1.1, 0.9, 1.0}};
	const array<double, 3> d3 {{-10.1, 0.9, 1.0}};
	EXPECT_TRUE( IsSameDirection(d1, d2) );
	EXPECT_FALSE( IsSameDirection(d1, d3) );
}

TEST( UnitArray3, Directions )
{
	const array<double, 3> d1 {{1.0, 1.0, 1.1}};
	const array<double, 3> d2 {{1.1, 0.9, 1.0}};
	const array<double, 3> d3 {{-10.1, 0.9, 1.0}};
	EXPECT_TRUE( IsSameDirection(d1, d2) );
	EXPECT_FALSE( IsSameDirection(d1, d3) );
}

TEST( UnitArray3, Angles )
{
	const array<double, 3> a1 {{-1.0, 1.0, -1.0}};
	const array<double, 3> a2 {{1.0, -1.0, 1.0}};
	EXPECT_NEAR( pi(), Angle(a1, a2), tol);
	const array<double, 3> a3 {{1.0, -1.0, 0.0}};
	const array<double, 3> a4 {{1.0, 0.0, 0.0}};
	EXPECT_NEAR( -pi() / 4, SignedAngle(a3, a4), tol);
	const array<double, 3> v1 {{0.5, 2.0, 1.5}};
	const array<double, 3> v2 {{2.0, 3.0, 5.0}};
	EXPECT_NEAR( 14.5, InnerProduct(v1, v2), tol);
}

TEST( UnitArray3, Output )
{
	array<double, 3> v {{ 10.0, 3.14, 8.0 }};
	std::ostringstream ss;
	ss << v;
	EXPECT_EQ( "(10, 3.14, 8)", ss.str());
}

} // namespace
} // namespace
} // namespace
