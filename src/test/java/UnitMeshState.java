/*
 * Copyright 2011-2016 Universiteit Antwerpen
 *
 * Licensed under the EUPL, Version 1.1 or  as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

package be.ac.ua.simPT.test;

import org.junit.*;
import static org.junit.Assert.*;
import be.ac.ua.simPT.*;

public class UnitMeshState
{
	private MeshState mesh;
	private final double tol = 1.0e-10;
	
	@Before
	public void setUp()
    {
		// Adding the libsimPT_sim library is only required on windows
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)
			System.loadLibrary("libsimPTlib_sim");
			
		System.loadLibrary("simPT_sim_jni");
		mesh = new MeshState();
	}
	
	@Test
	public void testNodes()
    {
		VectorInt id = new VectorInt(3);
		id.add(5);
		id.add(3);
		id.add(2);
		
		VectorDouble x = new VectorDouble(3);
		x.add(1.0);
		x.add(2.0);
		x.add(3.0);
		
		VectorDouble y = new VectorDouble(3);
		y.add(9.0);
		y.add(8.0);
		y.add(7.0);
		
		mesh.SetNumNodes(id.size());
		mesh.SetNodesID(id);
		mesh.SetNodesX(x);
		mesh.SetNodesY(y);
		
		assertEquals(id.size(), mesh.GetNumNodes());
		mesh.GetNodesID();
		mesh.GetNodesX();
		mesh.GetNodesY();
		
		for (int i = 0; i < mesh.GetNumNodes(); i++) {
			assertEquals(id.get(i), mesh.GetNodeID(i));
			assertEquals(x.get(i),  mesh.GetNodeX(i), tol);
			assertEquals(y.get(i),  mesh.GetNodeY(i), tol);
		}
	}
	
	@Test
	public void testCells()
    {
		VectorInt nodes = new VectorInt(3);
		nodes.add(1);
		nodes.add(2);
		nodes.add(3);
		
		VectorInt walls = new VectorInt(3);
		walls.add(7);
		walls.add(8);
		walls.add(9);
		
		mesh.SetNumCells(1);
		mesh.SetCellNodes(0, nodes);
		mesh.SetCellWalls(0, walls);
		mesh.SetBoundaryPolygonNodes(nodes);
		mesh.SetBoundaryPolygonWalls(walls);
		
		assertEquals(1, mesh.GetNumCells());
		mesh.GetCellNodes(0);
		mesh.GetCellWalls(0);
		mesh.GetBoundaryPolygonNodes();
		mesh.GetBoundaryPolygonWalls();
	}
	
	@Test
	public void testWalls()
    {
		PairInt nodes = new PairInt(1, 2);
		PairInt cells = new PairInt(8, 9);
		
		mesh.SetNumWalls(1);
		mesh.SetWallNodes(0, nodes);
		mesh.SetWallCells(0, cells);
		
		assertEquals(1, mesh.GetNumWalls());
		mesh.GetWallNodes(0);
		mesh.GetWallCells(0);
	}
	
	@Test
	public void testNodeAttributes()
    {
		mesh.SetNumNodes(1);
		
		mesh.NodeAttributeContainer().AddInt("i");
		mesh.NodeAttributeContainer().AddDouble("d");
		mesh.NodeAttributeContainer().AddString("s");
        
		assertTrue(mesh.NodeAttributeContainer().IsNameInt("i"));
		assertTrue(mesh.NodeAttributeContainer().IsNameDouble("d"));
		assertTrue(mesh.NodeAttributeContainer().IsNameString("s"));
		
		mesh.NodeAttributeContainer().SetInt(0, "i", 5);
		mesh.NodeAttributeContainer().SetDouble(0, "d", 2.0);
		mesh.NodeAttributeContainer().SetString(0, "s", "test");
        
		assertEquals(5,      mesh.NodeAttributeContainer().GetInt(0, "i"));
		assertEquals(2.0,    mesh.NodeAttributeContainer().GetDouble(0, "d"), tol);
		assertEquals("test", mesh.NodeAttributeContainer().GetString(0, "s"));
	}
	
	@Test
	public void testCellAttributes()
    {
		mesh.SetNumCells(1);
		
		mesh.CellAttributeContainer().AddInt("i");
		mesh.CellAttributeContainer().AddDouble("d");
		mesh.CellAttributeContainer().AddString("s");
        
		assertTrue(mesh.CellAttributeContainer().IsNameInt("i"));
		assertTrue(mesh.CellAttributeContainer().IsNameDouble("d"));
		assertTrue(mesh.CellAttributeContainer().IsNameString("s"));
		
		mesh.CellAttributeContainer().SetInt(0, "i", 5);
		mesh.CellAttributeContainer().SetDouble(0, "d", 2.0);
		mesh.CellAttributeContainer().SetString(0, "s", "test");
        
		assertEquals(5,      mesh.CellAttributeContainer().GetInt(0, "i"));
		assertEquals(2.0,    mesh.CellAttributeContainer().GetDouble(0, "d"), tol);
		assertEquals("test", mesh.CellAttributeContainer().GetString(0, "s"));
	}
	
	@Test
	public void testWallAttributes()
    {
		mesh.SetNumWalls(1);
		
		mesh.WallAttributeContainer().AddInt("i");
		mesh.WallAttributeContainer().AddDouble("d");
		mesh.WallAttributeContainer().AddString("s");
        
		assertTrue(mesh.WallAttributeContainer().IsNameInt("i"));
		assertTrue(mesh.WallAttributeContainer().IsNameDouble("d"));
		assertTrue(mesh.WallAttributeContainer().IsNameString("s"));
		
		mesh.WallAttributeContainer().SetInt(0, "i", 5);
		mesh.WallAttributeContainer().SetDouble(0, "d", 2.0);
		mesh.WallAttributeContainer().SetString(0, "s", "test");
        
		assertEquals(5,      mesh.WallAttributeContainer().GetInt(0, "i"));
		assertEquals(2.0,    mesh.WallAttributeContainer().GetDouble(0, "d"), tol);
		assertEquals("test", mesh.WallAttributeContainer().GetString(0, "s"));
	}
}
