#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#
#  This file contains the C++ compile & link configuration.
#  It is meant to be included in the src/CMakeLists.txt and 
#  to provide identical C++ configuration for the main/cpp 
#  and the test/ccp directories and subdirectories.
#
#############################################################################

#----------------------------------------------------------------------------
# Check CMAKE_BUILD_TYPE
#----------------------------------------------------------------------------
if( NOT (CMAKE_BUILD_TYPE MATCHES "Release" OR CMAKE_BUILD_TYPE MATCHES "Debug") )
	message(FATAL_ERROR  "========> CMAKE_BUILD_TYPE HAS TO MATCH EITHER Release OR Debug." )
endif()

#----------------------------------------------------------------------------
# Compile flags
#----------------------------------------------------------------------------
set( CMAKE_CXX_FLAGS         "${CMAKE_CXX_FLAGS} -Wall -Wno-unknown-pragmas -Wno-unused-function -Wno-expansion-to-defined" )
set( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Ofast" )
set( CMAKE_CXX_FLAGS_DEBUG   "${CMAKE_CXX_FLAGS_DEBUG} -O0"   )

#----------------------------------------------------------------------------
# Platform dependent compile flags
#----------------------------------------------------------------------------
if( CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 3.8 )
	set( CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS}  -Wno-inconsistent-missing-override")
endif()
if( CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_HOST_APPLE )
	add_definitions( -D__APPLE__ )
	set( CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libc++")
elseif( CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_HOST_APPLE )
	set( CMAKE_CXX_FLAGS           "${CMAKE_CXX_FLAGS} -fPIC -std=c++11 -stdlib=libc++" )
	add_definitions( -D__extern_always_inline=inline )
elseif( NOT WIN32 )
	set( CMAKE_CXX_FLAGS 	       "${CMAKE_CXX_FLAGS} -fPIC -std=c++11" )
	set( CMAKE_C_FLAGS             "${CMAKE_C_FLAGS} -fPIC" )
elseif( WIN32 )
	set( CMAKE_CXX_FLAGS 	"${CMAKE_CXX_FLAGS} -std=c++0x -U__STRICT_ANSI__ -mthreads" )
endif()
#
include_directories( ${CMAKE_HOME_DIRECTORY}/main                   )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_parex         )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_sim           )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_simptshell    )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_simshell      )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_tissue_edit   )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_util          )
include_directories( ${CMAKE_HOME_DIRECTORY}/main/cpp_logging          )

#----------------------------------------------------------------------------
# OpenSSL configuration
#----------------------------------------------------------------------------
find_package( OpenSSL REQUIRED )
include_directories( ${OPENSSL_INCLUDE_DIR} )

#----------------------------------------------------------------------------
# Qt configuration
#----------------------------------------------------------------------------
set( QT_IS_STATIC       FALSE )
set( QT_USE_QTMAIN      TRUE  )
find_package( Qt5 COMPONENTS Core Gui PrintSupport Network Widgets QUIET )
if (Qt5_FOUND)
	set( SIMPT_QT_VERSION 5 )
	set( QT_INCLUDE_DIR
		${Qt5Core_INCLUDE_DIRS}
		${Qt5Gui_INCLUDE_DIRS}
		${Qt5PrintSupport_INCLUDE_DIRS}
		${Qt5Network_INCLUDE_DIRS}
		${Qt5Widgets_INCLUDE_DIRS} )
	include_directories( ${QT_INCLUDE_DIR} )	
		
	set( QT_LIBRARIES
		${Qt5Core_LIBRARIES}
		${Qt5Gui_LIBRARIES}
		${Qt5PrintSupport_LIBRARIES}
		${Qt5Network_LIBRARIES}
		${Qt5Widgets_LIBRARIES} )
	set(QT_PLUGINS_DIR "${Qt5Core_DIR}/../../../plugins")
else()
	# If no Qt5, require Qt4
	find_package( Qt4 COMPONENTS QtCore QtGui QtNetwork REQUIRED)
	set( SIMPT_QT_VERSION 4 )
	include( ${QT_USE_FILE} )
endif()
set( LIBS   ${LIBS}   ${QT_LIBRARIES} )
#
if( CMAKE_BUILD_TYPE MATCHES "Release" )
	add_definitions( -DQT_NO_DEBUG_OUTPUT -DQT_NO_WARNING_OUTPUT )
endif()
if( CMAKE_BUILD_TYPE MATCHES "Debug" )
	add_definitions( -DQDEBUG_OUTPUT )
endif()

#----------------------------------------------------------------------------
# TRNG Library (Tina's Random Number Generator)
#----------------------------------------------------------------------------
include_directories( SYSTEM ${CMAKE_HOME_DIRECTORY}/main/resources/lib/trng4 )
set( TRNG_LIBRARIES trng4 )
set( LIBS ${LIBS} trng4 )

#----------------------------------------------------------------------------
# TCLAP Library (command line processing)
#----------------------------------------------------------------------------
include_directories( SYSTEM ${CMAKE_HOME_DIRECTORY}/main/resources/lib/tclap/include )

#----------------------------------------------------------------------------
# adapt2rfp Library (raw function pointer from C++ callable entities)
#----------------------------------------------------------------------------
include_directories( ${CMAKE_HOME_DIRECTORY}/main/resources/lib/adapt2rfp )

#----------------------------------------------------------------------------
# libssh Library (ssh and sftp capabilities)
#----------------------------------------------------------------------------
include_directories( ${CMAKE_HOME_DIRECTORY}/main/resources/lib/libssh/include )

#----------------------------------------------------------------------------
# spdlog Library
#----------------------------------------------------------------------------
include_directories( ${CMAKE_HOME_DIRECTORY}/main/resources/lib/spdlog )

#----------------------------------------------------------------------------
# OpenMP
#----------------------------------------------------------------------------
if( NOT SIMPT_FORCE_NO_OPENMP )
	if ( NOT DEFINED HAVE_CHECKED_OpenMP )
	    set( HAVE_CHECKED_OpenMP  TRUE  CACHE  BOOL  "Have checked for OpenMP?" FORCE )
        find_package( OpenMP )
        if( OPENMP_FOUND )
            set( HAVE_FOUND_OpenMP  TRUE  CACHE  BOOL  "Have found OpenMP?" FORCE )
        else()
            set( HAVE_FOUND_OpenMP  FALSE  CACHE  BOOL  "Have found OpenMP?" FORCE )
	    endif()
    endif()
    if ( HAVE_FOUND_OpenMP )
    	set( OPENMP_FOUND TRUE )
        set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}" )
        set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}" )
    else()
    	# This is done to eliminate blank output of undefined CMake variables.
    	set( OPENMP_FOUND FALSE )
    endif()
else()
    # This is done to eliminate blank output of undefined CMake variables.
    set( OPENMP_FOUND FALSE )
endif()
# If not found, use the dummy omp.
if( NOT OPENMP_FOUND )
    include_directories( ${CMAKE_HOME_DIRECTORY}/main/resources/lib/domp/include )
endif()

#----------------------------------------------------------------------------
# Standard math lib
#----------------------------------------------------------------------------
set( LIBS   ${LIBS}   m )

#----------------------------------------------------------------------------
# Boost 
# Boost_FOUND signals headers, for libs see boost_<LIBNAME>_FOUND but
# when REQUIRED is set, missing components trigger an error.
# If Boost iostreams was found check that it has been compiled with zlib 
# capability set (often not on Windows), and define Boost_WITH_GZIP.
# Not to repeat this check --> if(NOT Boost_WITH_GZIP)
#----------------------------------------------------------------------------
find_package( Boost 1.61 COMPONENTS filesystem system REQUIRED )
if( NOT SIMPT_FORCE_NO_BOOST_GZIP )
	find_package( Boost COMPONENTS iostreams )
endif()
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} )
set( LIBS   ${LIBS} ${Boost_LIBRARIES} )

if( Boost_IOSTREAMS_FOUND AND NOT SIMPT_FORCE_NO_BOOST_GZIP )
	if ( NOT DEFINED Boost_WITH_GZIP )
		set( Boost_WITH_GZIP  FALSE  CACHE  BOOL  "Boost has gzip?" )
		file( WRITE ${CMAKE_CURRENT_BINARY_DIR}/boost_with_gzip.cpp
			    "#include <boost/iostreams/filter/gzip.hpp>
			    int main() {auto g = boost::iostreams::gzip_compressor(); return 0;}" )
		try_compile( Boost_WITH_GZIP
			    ${CMAKE_CURRENT_BINARY_DIR} 
			    ${CMAKE_CURRENT_BINARY_DIR}/boost_with_gzip.cpp
			    CMAKE_FLAGS -I${Boost_INCLUDE_DIRS}
			    CMAKE_FLAGS "-DLINK_LIBRARIES:STRING=${Boost_LIBRARIES}" )
	endif()
else()
        # This is done to eliminate blank output of undefined CMake variables.
        set( Boost_IOSTREAMS_FOUND FALSE )  
        set( Boost_WITH_GZIP       FALSE )
endif()

if( Boost_WITH_GZIP )
	add_definitions( -DUSE_BOOST_GZIP )
endif()

#----------------------------------------------------------------------------
# Zlib
# If Boost iostreams has no zlib capability, use system zlib or our own copy 
# of zlib. In the latter case, include dir is binary dir because header files 
# are preprocessed.
#----------------------------------------------------------------------------
if( NOT Boost_WITH_GZIP )
	if( NOT SIMPT_FORCE_NO_ZLIB )
		find_package( ZLIB )
		if ( ZLIB_FOUND )
			include_directories( ${ZLIB_INCLUDE_DIRS} )
			set( LIBS ${LIBS} ${ZLIB_LIBRARIES} )
		else()
			# This is done to eliminate blank output of undefined CMake variables.
			set( ZLIB_FOUND FALSE )
		endif()
	endif()
	if( SIMPT_FORCE_NO_ZLIB OR NOT ZLIB_FOUND )
		include_directories( ${CMAKE_BINARY_DIR}/main/resources/lib/zlib )
		set( LIBS ${LIBS} zlibstatic   )
		set( ZLIB_LIBRARIES zlibstatic )
	endif()
endif()
 
#----------------------------------------------------------------------------
# HDF5 Library
# Try to find the C variant of libhdf5, if found, USE_HDF5 is defined
# and passed to the compilers to allow compilation of selective features
# through preprocessor commands like #ifdef USE_HDF5 and friends.
# Additional defs are required on Ubuntu where lib are installed 
# with hdf5 v1.6 as default behavior.
#----------------------------------------------------------------------------
if( SIMPT_FORCE_NO_HDF5 )
	message( STATUS "---> Skipping HDF5, SIMPT_FORCE_NO_HDF5 set.")
else()
	find_package( HDF5 COMPONENTS C HL )
	if( HDF5_FOUND )
		include_directories(SYSTEM ${HDF5_INCLUDE_DIRS} )
		set( LIBS   ${LIBS}   ${HDF5_LIBRARIES} )
		add_definitions( -DUSE_HDF5 -DH5_NO_DEPRECATED_SYMBOLS )
		add_definitions( -DH5Dcreate_vers=2 -DH5Dopen_vers=2 )
		add_definitions( -DH5Acreate_vers=2 -DH5Gcreate_vers=2 )
		add_definitions( -DH5Gopen_vers=2 )
	else()
		# This is done to eliminate blank output of undefined CMake variables.
		set( HDF5_FOUND FALSE )
	endif()    
endif()

#----------------------------------------------------------------------------
# SWIG for wrappers
#----------------------------------------------------------------------------
if ( SIMPT_MAKE_JAVA_WRAPPER OR SIMPT_MAKE_PYTHON_WRAPPER )
	find_package( SWIG REQUIRED )
endif()
if ( SIMPT_MAKE_PYTHON_WRAPPER )
	find_package( PythonLibs )
endif()

#----------------------------------------------------------------------------
# Linking
# Note: if SIMPT_MAKE_PACKAGE is ON the RPATH 
# will be patched in the packaging process on some platforms, e.g. Mac.
# Note: CMAKE_INSTALL_RPATH_USE_LINK_PATH seems required on OSX El Capitan
# to find the Qt5 dynamic libs. 
#----------------------------------------------------------------------------
set( BUILD_SHARED_LIBS                   ON )
set( CMAKE_INSTALL_RPATH                 ${CMAKE_INSTALL_PREFIX}/bin )
SET( CMAKE_INSTALL_RPATH_USE_LINK_PATH   TRUE )

#----------------------------------------------------------------------------
# Win32/MinGW specific link configuration
#----------------------------------------------------------------------------
if( WIN32 )
	#------------- Setting linker flags
	# DO NOT BREAK THE LINES BELOW: THAT MAKES THE THE LINK STEP BREAK 
	set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -mthreads -Wl,--allow-multiple-definition -Wl,-enable-auto-import")
	set( CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -mthreads -Wl,--allow-multiple-definition -Wl,-enable-auto-import -Wl,-enable-runtime-pseudo-reloc -Wl,-subsystem,windows")		
	#------------- MinGW specific libs
	set( LIBS   ${LIBS}   mingw32 wsock32 )
endif()

#############################################################################
