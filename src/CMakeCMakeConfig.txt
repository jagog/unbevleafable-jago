#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#
#  Configuration for the CMake itself.
#
#############################################################################

#============================================================================
# Basic settings.
#============================================================================
set( CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS  TRUE )
set( CMAKE_COLOR_MAKEFILE               ON   )
set( CMAKE_VERBOSE_MAKEFILE             OFF  )
enable_testing()

#============================================================================
# Additional CMake modules:
#============================================================================
list( APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/main/resources/cmake/" )
list( APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/doc/resources/cmake/" )

#============================================================================
# Macro sets NAME to VALUE iff the NAME has not been defined yet:
#============================================================================
macro(set_if_null NAME VALUE)
        if( NOT DEFINED ${NAME} OR "${NAME}" STREQUAL "" )
                set( ${NAME}    "${VALUE}" )
        endif()
endmacro(set_if_null)

#============================================================================
# Macro removes flag from CMAKE_CXX_FLAGS:
#============================================================================
macro(remove_cxx_flag flag)
	string(REPLACE "${flag}" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
endmacro()

#############################################################################
