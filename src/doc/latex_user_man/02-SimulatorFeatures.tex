%############################################################################
% Copyright 2011-2016 Universiteit Antwerpen
%
% Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
% the European Commission - subsequent versions of the EUPL (the "Licence");
% You may not use this work except in compliance with the Licence.
% You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
%
% Unless required by applicable law or agreed to in writing, software 
% distributed under the Licence is distributed on an "AS IS" basis,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the Licence for the specific language governing
% permissions and limitations under the Licence.
%############################################################################


\chapter{SimPT features}

The design of the Simulator for Plant Tissue intends to make it a rich, flexible and extensible environment for developing simulations of Plat Tissue processes. In this chapter we present a brief overview of of the key feature of SimPT that serve to realize those objectives.

A number of those features relate to the overall design of the interaction between the simulation work shell and have already been discussed in the previous chapter.

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% File formats 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{File formats}
The use of the MVC pattern in the design of the work shell makes it straightforward to extend the work shell with viewers or post-processors for new file formats. At present recognizes a number of file formats for input and output of the simulation data and a number of graphical formats for the post-processing of simulation data. 
 
\subsection{Formats for input-output}
The  simulator supports three file formats for input and output of full simulation data (i.e. a dataset that can be used to restart and extend the simulation): 
\begin{description}
	\item[XML] a well-known human readable or text based markup format (see \url{http://en.wikipedia.org/wiki/XML})
	\item[XML.GZ] compressed file of XML format format (see \url{http://en.wikipedia.org/wiki/Gzip}); provides typically 80-90 percent reduction in file size
	\item[HDF5] a machine readable or binary hierarchical data format often used in data-intensive scientific applications (see \url{www.hdfgroup.org/HDF5/})
\end{description}

HDF5 is a widely used file format in scientific visualisation. It is a 
portable file format that comes with a high-performance software library 
that is available across platforms from laptops to supercomputers, 
with API a.o. for C/C++. It is a free, open source software. 
On \url{www.hdfgroup.org/HDF5/doc/index.html} one finds documentation, 
specifications and examples. HDF5 provides a significant enhancement in 
functionality because it enables the use of Paraview (see \url{www.paraview.org}), 
a state-of-the-art scientific visualisation tool. This requires the use 
of a Paraview plug-in to make the SimPT HDF5 file structure available to Paraview. 
This plug-in has been developed.  The specification of the HDF5 file can be 
found in appendix \ref{chap:SimPT HDF5 file specification}.


\subsection{Formats for post-processing}
SimPT supports the following formats for numeric or graphical (both vector and bitmap) output:
\begin{description}
	\item[BMP] a graphics format that provides a bitmap graphics representation 
	of the mesh (see \url{http://en.wikipedia.org/wiki/BMP_file_format}).
	\item[CSV] a human readable format for storing data in a 
	table (see \url{http://en.wikipedia.org/wiki/Comma-separated_values}); used to provide numeric output .
	\item[JPEG] a graphics format (see \url{http://en.wikipedia.org/wiki/JPEG});  
	used to provide a bitmap graphic representation of the mesh.
	\item[PLY] a human readable format for storing graphical objects that are 
	described as a collection of polygons (see \url{http://paulbourke.net/dataformats/ply/})
	\item[PDF] a graphics format  (see \url{http://en.wikipedia.org/wiki/Portable_Document_Format});  
	used to provide a vector graphic representation of the mesh.
	\item[PNG] a graphics format (see \url{http://en.wikipedia.org/wiki/Portable_Network_Graphics});  
	used to provide a bitmap graphic representation of the mesh.
\end{description}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Customizability
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Customizability}
A number of choices, represented by parameters in the input data file, can be made to customize aspects of the simulation. 
	\begin{itemize}
%
	\item The set of ODE solvers for the transport equations includes fixed step and adaptive step solvers. The full set of solvers provided by the Odeint package of the Boost library is available ( see \url{http://www.boost.org/doc/libs/1_61_0/libs/numeric/odeint/doc/html/index.html}. The choice of ODE solver is specified in the input file in the section \texttt{parameters.ode\_integration} with the parameter \texttt{ode\_solver}. The tolerances and solver time increments are specified in the same section. 
%
		\item Tina's Random Number Generators Library developed by Heiko 
Bauke (see \url{http://numbercrunch.de/trng/} are available. Tina's Random Number 
Generator Library (TRNG) is a state of the art C++ pseudo-random number 
generator library for sequential and parallel Monte Carlo simulations. Its 
design principles are based on a proposal for an extensible random number 
generator facility, that has become part of the C++11 standard. Contary to the standard C++ RNG's, the TRNG implementation allows for use in the context of parallel calculations. The simulator correctly tracks the state of random generators across restarts to have consistent time evolution, irrespective of the number of restarts. The choice of random generator and seed is specified in section \texttt{parameters.random\_engine} in the input file.
%
	\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dynamic parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dynamic parameters}
The use of event mechanisms in the interaction between the graphical user interface and the simulation core make it possible that all parameters in the simulation input file are dynamic. That is: if a used edits parameters via the parameter edit panel in the GUI, the an event is triggered signalling this to the simulation core. The core in turn will take those changed values into account starting at the next time step and in turn trigger an event that causes the viewers to be aware of these changes and store the changed parameters on file (to be available for a simulation restart or post processing analysis). A parameter change can also be a computed change, i.e., effected by the simulator when certain conditions are met such as number of cells exceeds a threshold or simulated time reaches a pre-set value. 

All simulation parameters can be changed dynamically. This includes the selection of the model or of model components and of the time evolution algorithm. 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithmic 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Algorithmic features}
The use algorithmic components creates a lot possibilities and flexibility to customize and extend the simulator. 
	\begin{itemize}
		\item The set of algorithms to generate random displacements of nodes with normal and uniform distributions. These algorithms can be chosen in $``mc\_move\_generator''$ in the model configuration file.
		\item The set of time evolvers for the regulation of the time evolution scheme has been significantly expanded. Now various time evolvers (such as SimPT, Grow, Housekeep, HousekeepGrow, Transport or VLeaf1 are available in $``time\_evolver''$ in the model configuration file.
		\item Metropolis fine tuning: Monte Carlo sweeping was significantly improved. Now Metropolis Monte Carlo sweeping averages energy changes over a number of $``mc\_sliding\_window''$	sweeps to decide to stop or continue the equilibration cycle.
	\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Coupled
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Coupled simulations}
\textbf{!!!!!!!!!!!!!!!!!!!  Discuss coupled simulations briefly (one fat paragraph)..}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interoperability
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Language interoperability}
The core simulator has been isolated into a single package. In addition to 
its internal interface, an adapter has been used to define a clean external 
interface for the simulator with just five methods, see \myListing{SimWrapper.h:FullSource}. 
This has made it possible to build wrappers for the simulator in Java and Python. 
These wrappers, as the name suggests, are native Java or Python classes that can be 
used in regular Java or Python programming. A wrapper object has the same five methods, but now the methods will forward the calls to the similarly named methods of a corresponding C++ object. 
Thus, the SimPT core simulator can be accessed from within Java or Python programs.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Models
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Pre-defined models}
There was only one model in VirtualLeaf for determining the equilibrated 
state of the system by calculation of Hamiltonian. As it was based on some 
geometric constraints, e.g. the cell area constraint, the edge length constraint, 
etc, it is now called ``PlainGC'' (Plain Geometric Constraint) model. 
The VirtualLeaf model repertoire has been significantly extended by adding several new models such as Modified Geometric Constraint model, Elastic Wall model, Maxwell model.

In PlainGC for the constraint expressions the absolute difference of 
parameters (areas, lengths) is used and it causes the roles of larger 
cells to be more dominant than the smaller ones. As a result the smaller 
cells become less and less significant during equilibration and growth cycle. 
In the Modified Geometric Constraint (``ModifiedGC'') model the relative 
difference of cell areas is used in the cell area constraint expression, 
which makes for the equal contribution of both large and small cells.

The ``ElasticWall'' model avoids the edge length constraints in Hamiltonian, 
replacing them by elastic wall term making it additive at wall splitting. 
Additionally, in this model each wall has its individual and variable rest 
length given in the XML file ($``rest\_length''$ in wall attributes), 
instead of the common and constant rest length of 
edges ($``target\_node\_distance''$) in ``PlainGC'' and ``ModifiedGC''.

The ``Maxwell'' model represents the viscoelasticity of the cells 
and cell walls. In this model the turgor pressure term and the elastic 
wall term are used in Hamiltonian instead of cell area and edge length 
constraints. The turgor pressure in each cell is represented by the 
quantity of solute given in XML file ($``solute''$ in cell attributes). 
Contrary to all three previous models, this model is time-dependent: at 
each time step the quantity of solute in each cell and the rest 
length of the wall are updated.

The choice of these models is specified in $``mc\_hamiltonian''$ in 
the model configuration file.

Two new models were added to the wall relaxation/yielding model. 
The first of them is based on the wall length threshold. In this model 
if the wall length (not edge length as in VirtualLeaf 1) exceeds the 
threshold value during the wall extension, its rest length is updated 
by the some rate at each time step until this rest length reaches some value. 
In fact, this model represents the case when an external force is 
applied to the tissue. The second wall yielding model is based on the 
pressure threshold. In this model if the pressure in the cell exceeds 
the threshold, the rest lengths of cell walls are updated by the some 
rate at each time step until these rest lengths reach some values. 
This is a more advanced model for wall yielding in plant 
cells during their growth.

\myCppInput[FullSource]{./}{SimWrapper.h}{``External simulator interface.''}
