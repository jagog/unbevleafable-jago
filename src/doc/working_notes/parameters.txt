#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

BACKGROUND
==========

   When parameters change, the simulation data file is automatically saved, 
   whatever the simulation time (so we have a perfect history of the simulation).

   When saving the simulation data file at time step 100, it has a timestamp
   in its name, "_000100.xml.gz", and it contains:
     - current mesh state after completion of time step 100
     - parameters that will be used for generating the next mesh state i.e. 
       the state after time step 101.
       THESE ARE NOT NECESSARILY THE SAME PARAMETERS THAT WERE USED TO 
       GENERATE MESH UP TO TIMESTEP 100

   This way, a simulation datafile always provides a valid starting point for 
   restarting the simualtion at that time point.

   Right now, when user hits the "Apply" button in the dock window, Qt calls 
   MainGui::ParametersApply() from a new thread.
    * There's a boost::interprocess::interprocess_mutex object in MainBase 
      which is locked while performing a time step (evolving the mesh in time).
    * ParametersApply() waits until this mutex is available and locks it 
      before calling Mesh::SetParameters().
      This way parameters are only set between 2 different timesteps


PROBLEM
=======

   Since viewers are notified of MeshChanged events in MainBase::TimeStep() 
   between lock() and unlock(), they always save the old parameters that were 
   used to generate the mesh during the current timestep, instead of saving the 
   parameters that will be used to generate the next timestep. Parameters are 
   then updated in the mesh and the new parameters are saved one timestep too late.


SOLUTION
========

   ParametersApply() doesn't directly put new parameters in mesh. It stores 
   the new parametes in a temporary field of MainBase. During TimeStep(), MainBase 
   will check if this field is set. If so, the parameters will be copied into 
   mesh (however they won't be used until next timestep) and the new parameters will 
   be saved. There is also a separate mutex (m_viewers_mutex) for reading/writing 
   to this temporary field.
   

  |-------MainBase::TimeStep()--------|-------MainBase::TimeStep()--------|----
  |  timestep ...   |  notify viewers |  timestep ...   |  notify viewers | ...

m_timestep_mutex                      |                                   |
  | lock()                   unlock() | lock()                   unlock() | ...

m_param_mutex                         |                                   |
  |                 | lock() unlock() |                 | lock() unlock() |
  |-----------------+-----------------|-----------------+-----------------|----

      ^ (lock is acquired immediately)
        ParametersApply()
           m_param_mutex.lock();
             m_parameters_state.updated = true;
             m_parameters_state.pt      = pt;
           m_param_mutex.unlock();

                            |---------^ (lock had to wait until "notify viewers" was done)
                                         ParametersApply()
                                            m_param_mutex.lock();
                                              m_parameters_state.updated = true;
                                              m_parameters_state.pt      = pt;
                                            m_param_mutex.unlock();

NEW PROBLEM
===========

    Editing parameters in dock window while simulation is paused at simtime = 100 and 
    hitting "apply" will cause the old parameters to still be used when calculating 
    simtime = 101. The new parameters are then saved in "_000101.xml", and from 
    then on the new parameters are used to calculate mesh with simtime 102, etc.

    Temporary solution would be to directly edit the simulation data file "_000100.xml" 
    and re-opening it after changing the parameters.
