#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################

#============================================================================
# Temporary reports directory:
#============================================================================
file( MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/reports )

#============================================================================
# CppCheck report
#============================================================================
include( ReportCppCheck  )
#
ReportCppCheck( ${CMAKE_SOURCE_DIR}/main/cpp_execs        )
ReportCppCheck( ${CMAKE_SOURCE_DIR}/main/cpp_parex        )
ReportCppCheck( ${CMAKE_SOURCE_DIR}/main/cpp_sim          )
ReportCppCheck( ${CMAKE_SOURCE_DIR}/main/cpp_simptshell   )
ReportCppCheck( ${CMAKE_SOURCE_DIR}/main/cpp_simshell     )
ReportCppCheck( ${CMAKE_SOURCE_DIR}/main/cpp_tissue_edit  )
# a dir with only headers trips up cppcheck
#ReportCppCheck( ${CMAKE_SOURCE_DIR}/main/cpp_util         )

#============================================================================
# Install
#============================================================================
install( DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/reports
	DESTINATION             ${DOC_INSTALL_LOCATION} 
	FILE_PERMISSIONS        OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ 
	DIRECTORY_PERMISSIONS   OWNER_READ OWNER_WRITE OWNER_EXECUTE
				GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
)

#############################################################################
