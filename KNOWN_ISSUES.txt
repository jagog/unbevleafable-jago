#############################################################################
# Copyright 2011-2016 Universiteit Antwerpen
#
# Licensed under the EUPL, Version 1.1 or  as soon they will be approved by 
# the European Commission - subsequent versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl5
#
# Unless required by applicable law or agreed to in writing, software 
# distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
#############################################################################
#
#          Known issues:
#
#############################################################################

#----------------------------------------------------------------------------
#   Mac OS X MAVERICKS
#----------------------------------------------------------------------------
If you have many projects in your workspace, you may have the following error:
> 	QFileSystemWatcher: failed to add paths: ....
> 	QEventDispatcherUNIXPrivate(): Can not continue without a thread pipe
> 	Abort trap: 6

This is due to very strict open file limits on the newer versions of Mac OS X.

The solution is available in the following stackexchange post:
[](http://unix.stackexchange.com/questions/108174/how-to-persist-ulimit-settings-in-osx-mavericks)

> 	maxproc and maxfiles might go unlimited in previous OSX versions, however in Mavericks, they have an artificial maximum value.
> 	
> 	To increase default maxproc and maxfiles in launchctl, append the following lines to /etc/launchd.conf (create if it does not yet exist):
> 	
> 	limit maxfiles 16384 16384
> 	limit maxproc 2048 2048
> 	Be aware: those numbers are about as high as they can go, setting a higher number will not work.
> 	
> 	Now launchctl has the new limits, but your shell may not have new ulimit set yet. To set system-wide default ulimit for any shell, append the following lines to /etc/profile:
> 	
> 	ulimit -n 16384
> 	ulimit -u 2048
> 	Be aware: ulimit settings may not exceed launchctl limit settings.

Please note that you need to restart your system for the new settings to become active.

#----------------------------------------------------------------------------
#   Mac OS X EL CAPITAN - SWIG MACPORTS
#----------------------------------------------------------------------------
If you've installed swig, swig-python and swig-java from macports you'll run
into macports issue 1910. The workaround is available at 
https://trac.macports.org/attachment/ticket/44288/issue10910-workaround.txt

It consists in editing the file (it's a small edit)
/local/Library/Frameworks/Python.framework/Versions/3.4/include/python3.4m/pyport.h.

#############################################################################